=============
Pin Mappings
=============

.. doxygengroup:: group_bsp_pins
   :project: TARGET_CY8CKIT-064B0S2-4343W



API Reference
--------------   

.. toctree::

   group__group__bsp__pins__led.rst
   group__group__bsp__pins__btn.rst
   group__group__bsp__pins__comm.rst
   group__group__bsp__pins__arduino.rst
   group__group__bsp__pins__j2.rst
 
 
