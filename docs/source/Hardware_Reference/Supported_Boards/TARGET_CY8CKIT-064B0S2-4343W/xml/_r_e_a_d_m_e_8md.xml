<doxygen xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" version="1.8.14" xsi:noNamespaceSchemaLocation="compound.xsd">
  <compounddef id="_r_e_a_d_m_e_8md" kind="file" language="Markdown">
    <compoundname>README.md</compoundname>
    <briefdescription>
    </briefdescription>
    <detaileddescription>
    </detaileddescription>
    <programlisting>
<codeline><highlight class="normal">#<sp />CY8CKIT-064B0S2-4343W<sp />BSP</highlight></codeline>
<codeline />
<codeline><highlight class="normal">##<sp />Overview</highlight></codeline>
<codeline />
<codeline><highlight class="normal">The<sp />CY8CKIT-064B0S2-4343W<sp />PSoC<sp />64<sp />Wi-Fi<sp />BT<sp />Pioneer<sp />Kit<sp />is<sp />a<sp />low-cost<sp />hardware<sp />platform<sp />that<sp />enables<sp />design<sp />and<sp />debug<sp />of<sp />PSoC<sp />6<sp />MCUs.<sp />It<sp />comes<sp />with<sp />a<sp />Murata<sp />1LV<sp />Module<sp />(CYW4343W<sp />Wi-Fi<sp />+<sp />Bluetooth<sp />Combo<sp />Chip),<sp />industry-leading<sp />CapSense<sp />for<sp />touch<sp />buttons<sp />and<sp />slider,<sp />on-board<sp />debugger/programmer<sp />with<sp />KitProg3,<sp />microSD<sp />card<sp />interface,<sp />512-Mb<sp />Quad-SPI<sp />NOR<sp />flash,<sp />PDM-PCM<sp />microphone<sp />interface.</highlight></codeline>
<codeline><highlight class="normal">![](board.png)</highlight></codeline>
<codeline />
<codeline><highlight class="normal">To<sp />use<sp />code<sp />from<sp />the<sp />BSP,<sp />simply<sp />include<sp />a<sp />reference<sp />to<sp />`cybsp.h`.</highlight></codeline>
<codeline />
<codeline><highlight class="normal">##<sp />Features</highlight></codeline>
<codeline />
<codeline><highlight class="normal">###<sp />Kit<sp />Features:</highlight></codeline>
<codeline />
<codeline><highlight class="normal">*<sp />Support<sp />of<sp />up<sp />to<sp />2MB<sp />Flash<sp />and<sp />1MB<sp />SRAM</highlight></codeline>
<codeline><highlight class="normal">*<sp />Dedicated<sp />SDHC<sp />to<sp />interface<sp />with<sp />WICED<sp />wireless<sp />devices.</highlight></codeline>
<codeline><highlight class="normal">*<sp />Delivers<sp />dual-cores,<sp />with<sp />a<sp />150-MHz<sp />Arm<sp />Cortex-M4<sp />as<sp />the<sp />primary<sp />application<sp />processor<sp />and<sp />a<sp />100-MHz<sp />Arm<sp />Cortex-M0+<sp />as<sp />the<sp />secondary<sp />processor<sp />for<sp />low-power<sp />operations.</highlight></codeline>
<codeline><highlight class="normal">*<sp />Supports<sp />Full-Speed<sp />USB,<sp />capacitive-sensing<sp />with<sp />CapSense,<sp />a<sp />PDM-PCM<sp />digital<sp />microphone<sp />interface,<sp />a<sp />Quad-SPI<sp />interface,<sp />13<sp />serial<sp />communication<sp />blocks,<sp />7<sp />programmable<sp />analog<sp />blocks,<sp />and<sp />56<sp />programmable<sp />digital<sp />blocks.</highlight></codeline>
<codeline />
<codeline><highlight class="normal">###<sp />Kit<sp />Contents:</highlight></codeline>
<codeline />
<codeline><highlight class="normal">*<sp />PSoC<sp />64<sp />Wi-Fi<sp />BT<sp />Pioneer<sp />Board</highlight></codeline>
<codeline><highlight class="normal">*<sp />USB<sp />Type-A<sp />to<sp />Micro-B<sp />cable</highlight></codeline>
<codeline><highlight class="normal">*<sp />Quick<sp />Start<sp />Guide</highlight></codeline>
<codeline><highlight class="normal">*<sp />Four<sp />jumper<sp />wires<sp />(4<sp />inches<sp />each)</highlight></codeline>
<codeline><highlight class="normal">*<sp />Two<sp />jumper<sp />wires<sp />(5<sp />inches<sp />each)</highlight></codeline>
<codeline />
<codeline><highlight class="normal">##<sp />BSP<sp />Configuration</highlight></codeline>
<codeline />
<codeline><highlight class="normal">The<sp />BSP<sp />has<sp />a<sp />few<sp />hooks<sp />that<sp />allow<sp />its<sp />behavior<sp />to<sp />be<sp />configured.<sp />Some<sp />of<sp />these<sp />items<sp />are<sp />enabled<sp />by<sp />default<sp />while<sp />others<sp />must<sp />be<sp />explicitly<sp />enabled.<sp />Items<sp />enabled<sp />by<sp />default<sp />are<sp />specified<sp />in<sp />the<sp />CY8CKIT-064B0S2-4343W.mk<sp />file.<sp />The<sp />items<sp />that<sp />are<sp />enabled<sp />can<sp />be<sp />changed<sp />by<sp />creating<sp />a<sp />custom<sp />BSP<sp />or<sp />by<sp />editing<sp />the<sp />application<sp />makefile.</highlight></codeline>
<codeline />
<codeline><highlight class="normal">Components:</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp />*<sp />Device<sp />specific<sp />HAL<sp />reference<sp />(e.g.:<sp />PSOC6HAL)<sp />-<sp />This<sp />component,<sp />enabled<sp />by<sp />default,<sp />pulls<sp />in<sp />the<sp />version<sp />of<sp />the<sp />HAL<sp />that<sp />is<sp />applicable<sp />for<sp />this<sp />board.</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp />*<sp />BSP_DESIGN_MODUS<sp />-<sp />This<sp />component,<sp />enabled<sp />by<sp />default,<sp />causes<sp />the<sp />Configurator<sp />generated<sp />code<sp />for<sp />this<sp />specific<sp />BSP<sp />to<sp />be<sp />included.<sp />This<sp />should<sp />not<sp />be<sp />used<sp />at<sp />the<sp />same<sp />time<sp />as<sp />the<sp />CUSTOM_DESIGN_MODUS<sp />component.</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp />*<sp />CUSTOM_DESIGN_MODUS<sp />-<sp />This<sp />component,<sp />disabled<sp />by<sp />default,<sp />causes<sp />the<sp />Configurator<sp />generated<sp />code<sp />from<sp />the<sp />application<sp />to<sp />be<sp />included.<sp />This<sp />assumes<sp />that<sp />the<sp />application<sp />provides<sp />configurator<sp />generated<sp />code.<sp />This<sp />should<sp />not<sp />be<sp />used<sp />at<sp />the<sp />same<sp />time<sp />as<sp />the<sp />BSP_DESIGN_MODUS<sp />component.</highlight></codeline>
<codeline />
<codeline><highlight class="normal">Defines:</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp />*<sp />CYBSP_WIFI_CAPABLE<sp />-<sp />This<sp />define,<sp />disabled<sp />by<sp />default,<sp />causes<sp />the<sp />BSP<sp />to<sp />initialize<sp />the<sp />interface<sp />to<sp />an<sp />onboard<sp />wireless<sp />chip.</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp />*<sp />CY_USING_HAL<sp />-<sp />This<sp />define,<sp />enabled<sp />by<sp />default,<sp />specifies<sp />that<sp />the<sp />HAL<sp />is<sp />intended<sp />to<sp />be<sp />used<sp />by<sp />the<sp />application.<sp />This<sp />will<sp />cause<sp />the<sp />BSP<sp />to<sp />include<sp />the<sp />applicable<sp />header<sp />file<sp />and<sp />to<sp />initialize<sp />the<sp />system<sp />level<sp />drivers.</highlight></codeline>
<codeline />
<codeline><highlight class="normal">###<sp />Clock<sp />Configuration</highlight></codeline>
<codeline />
<codeline><highlight class="normal">|<sp />Clock<sp /><sp /><sp /><sp />|<sp />Source<sp /><sp /><sp /><sp />|<sp />Output<sp />Frequency<sp />|</highlight></codeline>
<codeline><highlight class="normal">|----------|-----------|------------------|</highlight></codeline>
<codeline><highlight class="normal">|<sp />CLK_HF0<sp /><sp />|<sp />CLK_PATH0<sp />|<sp />100<sp />MHz<sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp />|</highlight></codeline>
<codeline><highlight class="normal">|<sp />CLK_HF2<sp /><sp />|<sp />CLK_PATH0<sp />|<sp />50<sp />MHz<sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp />|</highlight></codeline>
<codeline><highlight class="normal">|<sp />CLK_HF3<sp /><sp />|<sp />CLK_PATH0<sp />|<sp />100<sp />MHz<sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp />|</highlight></codeline>
<codeline><highlight class="normal">|<sp />CLK_HF4<sp /><sp />|<sp />CLK_PATH0<sp />|<sp />100<sp />MHz<sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp />|</highlight></codeline>
<codeline />
<codeline><highlight class="normal">###<sp />Power<sp />Configuration</highlight></codeline>
<codeline />
<codeline><highlight class="normal">*<sp />System<sp />Active<sp />Power<sp />Mode:<sp />LP</highlight></codeline>
<codeline><highlight class="normal">*<sp />System<sp />Idle<sp />Power<sp />Mode:<sp />Deep<sp />Sleep</highlight></codeline>
<codeline><highlight class="normal">*<sp />VDDA<sp />Voltage:<sp />3300<sp />mV</highlight></codeline>
<codeline><highlight class="normal">*<sp />VDDD<sp />Voltage:<sp />3300<sp />mV</highlight></codeline>
<codeline />
<codeline><highlight class="normal">##<sp />API<sp />Reference<sp />Manual</highlight></codeline>
<codeline />
<codeline><highlight class="normal">The<sp />CY8CKIT-064B0S2-4343W<sp />Board<sp />Support<sp />Package<sp />provides<sp />a<sp />set<sp />of<sp />APIs<sp />to<sp />configure,<sp />initialize<sp />and<sp />use<sp />the<sp />board<sp />resources.</highlight></codeline>
<codeline />
<codeline><highlight class="normal">See<sp />the<sp />[BSP<sp />API<sp />Reference<sp />Manual][api]<sp />for<sp />the<sp />complete<sp />list<sp />of<sp />the<sp />provided<sp />interfaces.</highlight></codeline>
<codeline />
<codeline><highlight class="normal">##<sp />More<sp />information</highlight></codeline>
<codeline><highlight class="normal">*<sp />[CY8CKIT-064B0S2-4343W<sp />BSP<sp />API<sp />Reference<sp />Manual][api]</highlight></codeline>
<codeline><highlight class="normal">*<sp />[CY8CKIT-064B0S2-4343W<sp />Documentation](http://www.cypress.com/CY8CKIT-064B0S2-4343W)</highlight></codeline>
<codeline><highlight class="normal">*<sp />[Cypress<sp />Semiconductor,<sp />an<sp />Infineon<sp />Technologies<sp />Company](http://www.cypress.com)</highlight></codeline>
<codeline><highlight class="normal">*<sp />[Cypress<sp />Semiconductor<sp />GitHub](https://github.com/cypresssemiconductorco)</highlight></codeline>
<codeline><highlight class="normal">*<sp />[ModusToolbox](https://www.cypress.com/products/modustoolbox-software-environment)</highlight></codeline>
<codeline />
<codeline><highlight class="normal">[api]:<sp />modules.html</highlight></codeline>
<codeline />
<codeline><highlight class="normal">---</highlight></codeline>
<codeline><highlight class="normal">©<sp />Cypress<sp />Semiconductor<sp />Corporation,<sp />2019-2020.</highlight></codeline>
    </programlisting>
    <location file="output/bsp/TARGET_CY8CKIT-064B0S2-4343W/README.md" />
  </compounddef>
</doxygen>