<doxygen xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" version="1.8.14" xsi:noNamespaceSchemaLocation="compound.xsd">
  <compounddef id="_r_e_a_d_m_e_8md" kind="file" language="Markdown">
    <compoundname>README.md</compoundname>
    <briefdescription>
    </briefdescription>
    <detaileddescription>
    </detaileddescription>
    <programlisting>
<codeline><highlight class="normal">#<sp />CY8CPROTO-064B0S1-BLE<sp />BSP</highlight></codeline>
<codeline />
<codeline><highlight class="normal">##<sp />Overview</highlight></codeline>
<codeline />
<codeline><highlight class="normal">The<sp />PSoC<sp />64<sp />Secure<sp />Boot<sp />BLE<sp />Prototyping<sp />Kit<sp />(CY8CPROTO-064B0S1-BLE)<sp />is<sp />a<sp />low-cost<sp />hardware<sp />platform<sp />that<sp />enables<sp />design<sp />and<sp />debug<sp />of<sp />PSoC<sp />64<sp />MCUs.<sp />This<sp />kit<sp />is<sp />designed<sp />with<sp />a<sp />snap-away<sp />form-factor,<sp />allowing<sp />users<sp />to<sp />separate<sp />the<sp />KitProg<sp />(on-board<sp />programmer<sp />and<sp />debugger)<sp />from<sp />the<sp />target<sp />board<sp />and<sp />use<sp />independently.</highlight></codeline>
<codeline><highlight class="normal">![](board.png)</highlight></codeline>
<codeline />
<codeline><highlight class="normal">To<sp />use<sp />code<sp />from<sp />the<sp />BSP,<sp />simply<sp />include<sp />a<sp />reference<sp />to<sp />`cybsp.h`.</highlight></codeline>
<codeline />
<codeline><highlight class="normal">##<sp />Features</highlight></codeline>
<codeline />
<codeline><highlight class="normal">###<sp />Kit<sp />Features:</highlight></codeline>
<codeline />
<codeline><highlight class="normal">*<sp />BLE<sp />5.0<sp />certified<sp />Cypress<sp />CYBLE-4xxxx-xx<sp />EZ-BLE<sp />module<sp />with<sp />onboard<sp />crystal<sp />oscillators,<sp />trace<sp />antenna,<sp />passive<sp />components<sp />and<sp />PSoC<sp />64<sp />MCU</highlight></codeline>
<codeline><highlight class="normal">*<sp />Up<sp />to<sp />36<sp />GPIOs<sp />in<sp />a<sp />14x18.5x2<sp />mm<sp />package</highlight></codeline>
<codeline><highlight class="normal">*<sp />Supports<sp />digital<sp />programmable<sp />logic,<sp />capacitive-sensing<sp />with<sp />CapSense,<sp />a<sp />PDM-PCM<sp />digital<sp />microphone<sp />interface,<sp />a<sp />Quad-SPI<sp />interface,<sp />high-performance<sp />analog-to-digital<sp />converter<sp />(ADC),<sp />low-power<sp />comparators,<sp />and<sp />standard<sp />communication<sp />and<sp />timing<sp />peripherals.</highlight></codeline>
<codeline />
<codeline><highlight class="normal">###<sp />Kit<sp />Contents:</highlight></codeline>
<codeline />
<codeline><highlight class="normal">*<sp />2x<sp />PSoC<sp />64<sp />BLE<sp />Prototyping<sp />Board</highlight></codeline>
<codeline><highlight class="normal">*<sp />USB<sp />Type-A<sp />to<sp />Micro-B<sp />cable</highlight></codeline>
<codeline><highlight class="normal">*<sp />Quick<sp />Start<sp />Guide</highlight></codeline>
<codeline />
<codeline><highlight class="normal">##<sp />BSP<sp />Configuration</highlight></codeline>
<codeline />
<codeline><highlight class="normal">The<sp />BSP<sp />has<sp />a<sp />few<sp />hooks<sp />that<sp />allow<sp />its<sp />behavior<sp />to<sp />be<sp />configured.<sp />Some<sp />of<sp />these<sp />items<sp />are<sp />enabled<sp />by<sp />default<sp />while<sp />others<sp />must<sp />be<sp />explicitly<sp />enabled.<sp />Items<sp />enabled<sp />by<sp />default<sp />are<sp />specified<sp />in<sp />the<sp />CY8CPROTO-064B0S1-BLE.mk<sp />file.<sp />The<sp />items<sp />that<sp />are<sp />enabled<sp />can<sp />be<sp />changed<sp />by<sp />creating<sp />a<sp />custom<sp />BSP<sp />or<sp />by<sp />editing<sp />the<sp />application<sp />makefile.</highlight></codeline>
<codeline />
<codeline><highlight class="normal">Components:</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp />*<sp />Device<sp />specific<sp />HAL<sp />reference<sp />(e.g.:<sp />PSOC6HAL)<sp />-<sp />This<sp />component,<sp />enabled<sp />by<sp />default,<sp />pulls<sp />in<sp />the<sp />version<sp />of<sp />the<sp />HAL<sp />that<sp />is<sp />applicable<sp />for<sp />this<sp />board.</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp />*<sp />BSP_DESIGN_MODUS<sp />-<sp />This<sp />component,<sp />enabled<sp />by<sp />default,<sp />causes<sp />the<sp />Configurator<sp />generated<sp />code<sp />for<sp />this<sp />specific<sp />BSP<sp />to<sp />be<sp />included.<sp />This<sp />should<sp />not<sp />be<sp />used<sp />at<sp />the<sp />same<sp />time<sp />as<sp />the<sp />CUSTOM_DESIGN_MODUS<sp />component.</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp />*<sp />CUSTOM_DESIGN_MODUS<sp />-<sp />This<sp />component,<sp />disabled<sp />by<sp />default,<sp />causes<sp />the<sp />Configurator<sp />generated<sp />code<sp />from<sp />the<sp />application<sp />to<sp />be<sp />included.<sp />This<sp />assumes<sp />that<sp />the<sp />application<sp />provides<sp />configurator<sp />generated<sp />code.<sp />This<sp />should<sp />not<sp />be<sp />used<sp />at<sp />the<sp />same<sp />time<sp />as<sp />the<sp />BSP_DESIGN_MODUS<sp />component.</highlight></codeline>
<codeline />
<codeline><highlight class="normal">Defines:</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp />*<sp />CYBSP_WIFI_CAPABLE<sp />-<sp />This<sp />define,<sp />disabled<sp />by<sp />default,<sp />causes<sp />the<sp />BSP<sp />to<sp />initialize<sp />the<sp />interface<sp />to<sp />an<sp />onboard<sp />wireless<sp />chip.</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp />*<sp />CY_USING_HAL<sp />-<sp />This<sp />define,<sp />enabled<sp />by<sp />default,<sp />specifies<sp />that<sp />the<sp />HAL<sp />is<sp />intended<sp />to<sp />be<sp />used<sp />by<sp />the<sp />application.<sp />This<sp />will<sp />cause<sp />the<sp />BSP<sp />to<sp />include<sp />the<sp />applicable<sp />header<sp />file<sp />and<sp />to<sp />initialize<sp />the<sp />system<sp />level<sp />drivers.</highlight></codeline>
<codeline />
<codeline><highlight class="normal">###<sp />Clock<sp />Configuration</highlight></codeline>
<codeline />
<codeline><highlight class="normal">|<sp />Clock<sp /><sp /><sp /><sp />|<sp />Source<sp /><sp /><sp /><sp />|<sp />Output<sp />Frequency<sp />|</highlight></codeline>
<codeline><highlight class="normal">|----------|-----------|------------------|</highlight></codeline>
<codeline><highlight class="normal">|<sp />CLK_HF0<sp /><sp />|<sp />CLK_PATH0<sp />|<sp />100<sp />MHz<sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp />|</highlight></codeline>
<codeline />
<codeline><highlight class="normal">###<sp />Power<sp />Configuration</highlight></codeline>
<codeline />
<codeline><highlight class="normal">*<sp />System<sp />Active<sp />Power<sp />Mode:<sp />LP</highlight></codeline>
<codeline><highlight class="normal">*<sp />System<sp />Idle<sp />Power<sp />Mode:<sp />Deep<sp />Sleep</highlight></codeline>
<codeline><highlight class="normal">*<sp />VDDA<sp />Voltage:<sp />3300<sp />mV</highlight></codeline>
<codeline><highlight class="normal">*<sp />VDDD<sp />Voltage:<sp />3300<sp />mV</highlight></codeline>
<codeline />
<codeline><highlight class="normal">##<sp />API<sp />Reference<sp />Manual</highlight></codeline>
<codeline />
<codeline><highlight class="normal">The<sp />CY8CPROTO-064B0S1-BLE<sp />Board<sp />Support<sp />Package<sp />provides<sp />a<sp />set<sp />of<sp />APIs<sp />to<sp />configure,<sp />initialize<sp />and<sp />use<sp />the<sp />board<sp />resources.</highlight></codeline>
<codeline />
<codeline><highlight class="normal">See<sp />the<sp />[BSP<sp />API<sp />Reference<sp />Manual][api]<sp />for<sp />the<sp />complete<sp />list<sp />of<sp />the<sp />provided<sp />interfaces.</highlight></codeline>
<codeline />
<codeline><highlight class="normal">##<sp />More<sp />information</highlight></codeline>
<codeline><highlight class="normal">*<sp />[CY8CPROTO-064B0S1-BLE<sp />BSP<sp />API<sp />Reference<sp />Manual][api]</highlight></codeline>
<codeline><highlight class="normal">*<sp />[CY8CPROTO-064B0S1-BLE<sp />Documentation](https://www.cypress.com/documentation/development-kitsboards/psoc-64-ble-prototyping-kit-cy8cproto-064b0s1-ble)</highlight></codeline>
<codeline><highlight class="normal">*<sp />[Cypress<sp />Semiconductor,<sp />an<sp />Infineon<sp />Technologies<sp />Company](http://www.cypress.com)</highlight></codeline>
<codeline><highlight class="normal">*<sp />[Cypress<sp />Semiconductor<sp />GitHub](https://github.com/cypresssemiconductorco)</highlight></codeline>
<codeline><highlight class="normal">*<sp />[ModusToolbox](https://www.cypress.com/products/modustoolbox-software-environment)</highlight></codeline>
<codeline />
<codeline><highlight class="normal">[api]:<sp />modules.html</highlight></codeline>
<codeline />
<codeline><highlight class="normal">---</highlight></codeline>
<codeline><highlight class="normal">©<sp />Cypress<sp />Semiconductor<sp />Corporation,<sp />2019-2020.</highlight></codeline>
    </programlisting>
    <location file="output/bsp/TARGET_CY8CPROTO-064B0S1-BLE/README.md" />
  </compounddef>
</doxygen>