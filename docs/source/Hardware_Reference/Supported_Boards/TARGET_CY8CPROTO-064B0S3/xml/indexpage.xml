<doxygen xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" version="1.8.14" xsi:noNamespaceSchemaLocation="compound.xsd">
  <compounddef id="indexpage" kind="group">
    <compoundname>index</compoundname>
    <title>CY8CPROTO-064B0S3 BSP</title>
    <briefdescription>
    </briefdescription>
    <detaileddescription>
<para><heading level="2">Overview</heading>
</para><para>NOTE: THIS IS A EARLY ACCESS TARGET AND REQUIRES CUSTOM TOOLING FROM CYPRESS TO BUILD. </para><para>CY8CPROTO-064B0S3 PSoC 64 SecureBoot Prototyping Kit is a low-cost Prototyping Kit based on PSoC 64 SecureBoot MCU to enable customers to prototype and design with the PSoC 64 SecureBoot device. <image name="board.png" type="html" />
</para><para>To use code from the BSP, simply include a reference to cybsp.h.</para><para><heading level="2">Features</heading>
</para><para><bold>Kit Features:</bold>
</para><para><itemizedlist>
<listitem><para>PSoC 64 Secure MCU</para></listitem><listitem><para>128-Mb Serial NOR flash</para></listitem><listitem><para>Full-speed USB device interface</para></listitem></itemizedlist>
</para><para><bold>Kit Contents:</bold>
</para><para><itemizedlist>
<listitem><para>PSoC 64 SecureBoot Prototyping Board</para></listitem><listitem><para>USB Type-A to Micro-B cable</para></listitem><listitem><para>Quick start guide</para></listitem></itemizedlist>
</para><para><heading level="2">BSP Configuration</heading>
</para><para>The BSP has a few hooks that allow its behavior to be configured. Some of these items are enabled by default while others must be explicitly enabled. Items enabled by default are specified in the CY8CPROTO-064B0S3.mk file. The items that are enabled can be changed by creating a custom BSP or by editing the application makefile.</para><para>Components:<itemizedlist>
<listitem><para>Device specific HAL reference (e.g.: PSOC6HAL) - This component, enabled by default, pulls in the version of the HAL that is applicable for this board.</para></listitem><listitem><para>BSP_DESIGN_MODUS - This component, enabled by default, causes the Configurator generated code for this specific BSP to be included. This should not be used at the same time as the CUSTOM_DESIGN_MODUS component.</para></listitem><listitem><para>CUSTOM_DESIGN_MODUS - This component, disabled by default, causes the Configurator generated code from the application to be included. This assumes that the application provides configurator generated code. This should not be used at the same time as the BSP_DESIGN_MODUS component.</para></listitem></itemizedlist>
</para><para>Defines:<itemizedlist>
<listitem><para>CYBSP_WIFI_CAPABLE - This define, disabled by default, causes the BSP to initialize the interface to an onboard wireless chip.</para></listitem><listitem><para>CY_USING_HAL - This define, enabled by default, specifies that the HAL is intended to be used by the application. This will cause the BSP to include the applicable header file and to initialize the system level drivers.</para></listitem></itemizedlist>
</para><para><bold>Clock Configuration</bold>
</para><para><verbatim>embed:rst 
.. raw:: html

  &lt;table class="docutils align-default"&gt;&lt;tr&gt;&lt;td class="head"&gt;&lt;p&gt;Clock  &lt;/p&gt;&lt;/td&gt;&lt;td class="head"&gt;&lt;p&gt;Source  &lt;/p&gt;&lt;/td&gt;&lt;td class="head"&gt;&lt;p&gt;Output Frequency   &lt;/p&gt;&lt;/td&gt;&lt;/tr&gt;&lt;tr&gt;&lt;td&gt;&lt;p&gt;CLK_HF0  &lt;/p&gt;&lt;/td&gt;&lt;td&gt;&lt;p&gt;CLK_PATH0  &lt;/p&gt;&lt;/td&gt;&lt;td&gt;&lt;p&gt;100 MHz   &lt;/p&gt;&lt;/td&gt;&lt;/tr&gt;&lt;tr&gt;&lt;td&gt;&lt;p&gt;CLK_HF2  &lt;/p&gt;&lt;/td&gt;&lt;td&gt;&lt;p&gt;CLK_PATH0  &lt;/p&gt;&lt;/td&gt;&lt;td&gt;&lt;p&gt;50 MHz   &lt;/p&gt;&lt;/td&gt;&lt;/tr&gt;&lt;tr&gt;&lt;td&gt;&lt;p&gt;CLK_HF3  &lt;/p&gt;&lt;/td&gt;&lt;td&gt;&lt;p&gt;CLK_PATH1  &lt;/p&gt;&lt;/td&gt;&lt;td&gt;&lt;p&gt;48 MHz   &lt;/p&gt;&lt;/td&gt;&lt;/tr&gt;&lt;/table&gt;</verbatim></para><para><bold>Power Configuration</bold>
</para><para><itemizedlist>
<listitem><para>System Active Power Mode: LP</para></listitem><listitem><para>System Idle Power Mode: Deep Sleep</para></listitem><listitem><para>VDDA Voltage: 3300 mV</para></listitem><listitem><para>VDDD Voltage: 3300 mV</para></listitem></itemizedlist>
</para><para><heading level="2">API Reference Manual</heading>
</para><para>The CY8CPROTO-064B0S3 Board Support Package provides a set of APIs to configure, initialize and use the board resources.</para><para>See the <ulink url="modules.html">BSP API Reference Manual</ulink> for the complete list of the provided interfaces.</para><para><heading level="2">More information</heading>
</para><para><itemizedlist>
<listitem><para><ulink url="modules.html">CY8CPROTO-064B0S3 BSP API Reference Manual</ulink></para></listitem><listitem><para><ulink url="http://www.cypress.com/CY8CPROTO-064B0S3">CY8CPROTO-064B0S3 Documentation</ulink></para></listitem><listitem><para><ulink url="http://www.cypress.com">Cypress Semiconductor, an Infineon Technologies Company</ulink></para></listitem><listitem><para><ulink url="https://github.com/cypresssemiconductorco">Cypress Semiconductor GitHub</ulink></para></listitem><listitem><para><ulink url="https://www.cypress.com/products/modustoolbox-software-environment">ModusToolbox</ulink></para></listitem></itemizedlist>
</para><para><hruler />
 © Cypress Semiconductor Corporation, 2019-2020. </para>    </detaileddescription>
  </compounddef>
</doxygen>