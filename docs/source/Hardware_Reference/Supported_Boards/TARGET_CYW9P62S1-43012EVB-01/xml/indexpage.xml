<doxygen xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" version="1.8.14" xsi:noNamespaceSchemaLocation="compound.xsd">
  <compounddef id="indexpage" kind="group">
    <compoundname>index</compoundname>
    <title>CYW9P62S1-43012EVB-01 BSP</title>
    <briefdescription>
    </briefdescription>
    <detaileddescription>
<para><heading level="2">Overview</heading>
</para><para>The CYW9P62S1-43012EVB-01 Kit is a low-cost hardware platform that enables design and debug of the USI WM-BAC-CYW-50 Module. USI WM-BAC-CYW-50 is a System in Package (SiP) module that contains the PSoC 62 MCU (CY8C6247FDI-D52) and the radio part CYW43012 ( WiFi + Bluetooth Combo Chip). <image name="board.png" type="html" />
</para><para>To use code from the BSP, simply include a reference to cybsp.h.</para><para><heading level="2">Features</heading>
</para><para><bold>Kit Features:</bold>
</para><para><itemizedlist>
<listitem><para>Support of up to 1MB Flash and 288KB SRAM</para></listitem><listitem><para>BLE v5.0</para></listitem><listitem><para>IEEE 802.11n, 802.11ac friendly WLAN</para></listitem><listitem><para>Full-speed USB</para></listitem><listitem><para>Serial memory interface</para></listitem><listitem><para>Industry-leading CapSense</para></listitem></itemizedlist>
</para><para><bold>Kit Contents:</bold>
</para><para><itemizedlist>
<listitem><para>PSoC 6S2 Wi-Fi BT Pioneer Board</para></listitem><listitem><para>USB Type-A to Micro-B cable</para></listitem><listitem><para>Quick Start Guide</para></listitem><listitem><para>Four jumper wires (4 inches each)</para></listitem><listitem><para>Two jumper wires (5 inches each)</para></listitem></itemizedlist>
</para><para><heading level="2">BSP Configuration</heading>
</para><para>The BSP has a few hooks that allow its behavior to be configured. Some of these items are enabled by default while others must be explicitly enabled. Items enabled by default are specified in the CYW9P62S1-43012EVB-01.mk file. The items that are enabled can be changed by creating a custom BSP or by editing the application makefile.</para><para>Components:<itemizedlist>
<listitem><para>Device specific HAL reference (e.g.: PSOC6HAL) - This component, enabled by default, pulls in the version of the HAL that is applicable for this board.</para></listitem><listitem><para>BSP_DESIGN_MODUS - This component, enabled by default, causes the Configurator generated code for this specific BSP to be included. This should not be used at the same time as the CUSTOM_DESIGN_MODUS component.</para></listitem><listitem><para>CUSTOM_DESIGN_MODUS - This component, disabled by default, causes the Configurator generated code from the application to be included. This assumes that the application provides configurator generated code. This should not be used at the same time as the BSP_DESIGN_MODUS component.</para></listitem></itemizedlist>
</para><para>Defines:<itemizedlist>
<listitem><para>CYBSP_WIFI_CAPABLE - This define, disabled by default, causes the BSP to initialize the interface to an onboard wireless chip.</para></listitem><listitem><para>CY_USING_HAL - This define, enabled by default, specifies that the HAL is intended to be used by the application. This will cause the BSP to include the applicable header file and to initialize the system level drivers.</para></listitem></itemizedlist>
</para><para><bold>Clock Configuration</bold>
</para><para><verbatim>embed:rst 
.. raw:: html

  &lt;table class="docutils align-default"&gt;&lt;tr&gt;&lt;td class="head"&gt;&lt;p&gt;Clock  &lt;/p&gt;&lt;/td&gt;&lt;td class="head"&gt;&lt;p&gt;Source  &lt;/p&gt;&lt;/td&gt;&lt;td class="head"&gt;&lt;p&gt;Output Frequency   &lt;/p&gt;&lt;/td&gt;&lt;/tr&gt;&lt;tr&gt;&lt;td&gt;&lt;p&gt;CLK_HF0  &lt;/p&gt;&lt;/td&gt;&lt;td&gt;&lt;p&gt;CLK_PATH0  &lt;/p&gt;&lt;/td&gt;&lt;td&gt;&lt;p&gt;100 MHz   &lt;/p&gt;&lt;/td&gt;&lt;/tr&gt;&lt;tr&gt;&lt;td&gt;&lt;p&gt;CLK_HF1  &lt;/p&gt;&lt;/td&gt;&lt;td&gt;&lt;p&gt;CLK_PATH0  &lt;/p&gt;&lt;/td&gt;&lt;td&gt;&lt;p&gt;100 MHz   &lt;/p&gt;&lt;/td&gt;&lt;/tr&gt;&lt;tr&gt;&lt;td&gt;&lt;p&gt;CLK_HF2  &lt;/p&gt;&lt;/td&gt;&lt;td&gt;&lt;p&gt;CLK_PATH0  &lt;/p&gt;&lt;/td&gt;&lt;td&gt;&lt;p&gt;50 MHz   &lt;/p&gt;&lt;/td&gt;&lt;/tr&gt;&lt;tr&gt;&lt;td&gt;&lt;p&gt;CLK_HF3  &lt;/p&gt;&lt;/td&gt;&lt;td&gt;&lt;p&gt;CLK_PATH1  &lt;/p&gt;&lt;/td&gt;&lt;td&gt;&lt;p&gt;48 MHz   &lt;/p&gt;&lt;/td&gt;&lt;/tr&gt;&lt;/table&gt;</verbatim></para><para><bold>Power Configuration</bold>
</para><para><itemizedlist>
<listitem><para>System Active Power Mode: LP</para></listitem><listitem><para>System Idle Power Mode: Deep Sleep</para></listitem><listitem><para>VDDA Voltage: 1800 mV</para></listitem><listitem><para>VDDD Voltage: 1800 mV</para></listitem></itemizedlist>
</para><para><heading level="2">API Reference Manual</heading>
</para><para>The CYW9P62S1-43012EVB-01 Board Support Package provides a set of APIs to configure, initialize and use the board resources.</para><para>See the <ulink url="modules.html">BSP API Reference Manual</ulink> for the complete list of the provided interfaces.</para><para><heading level="2">More information</heading>
</para><para><itemizedlist>
<listitem><para><ulink url="modules.html">CYW9P62S1-43012EVB-01 BSP API Reference Manual</ulink></para></listitem><listitem><para><ulink url="https://www.cypress.com/CYW9P62S1-43012EVB-01">CYW9P62S1-43012EVB-01 Documentation</ulink></para></listitem><listitem><para><ulink url="http://www.cypress.com">Cypress Semiconductor, an Infineon Technologies Company</ulink></para></listitem><listitem><para><ulink url="https://github.com/cypresssemiconductorco">Cypress Semiconductor GitHub</ulink></para></listitem><listitem><para><ulink url="https://www.cypress.com/products/modustoolbox-software-environment">ModusToolbox</ulink></para></listitem></itemizedlist>
</para><para><hruler />
 © Cypress Semiconductor Corporation, 2019-2020. </para>    </detaileddescription>
  </compounddef>
</doxygen>