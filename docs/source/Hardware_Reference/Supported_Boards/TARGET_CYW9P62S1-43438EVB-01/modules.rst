===================
BSP API Reference
===================

.. toctree::
   :hidden:

   group__group__bsp__settings.rst
   group__group__bsp__pin__state.rst
   group__group__bsp__pins.rst
   group__group__bsp__macros.rst
   group__group__bsp__functions.rst

The following provides a list of BSP API documentation


| `BSP Settings <group__group__bsp__settings.html>`__   Peripheral Default HAL Settings:

  
+---------+---------------+----------------+------------------------+
|Resource |Parameter      |Value           |Remarks                 |
+=========+===============+================+========================+
|         |VREF           |1.2 V           |                        |
|ADC      +---------------+----------------+------------------------+
|         |Measurement \  |Single Ended    |                        |
|         |type           |                |                        |
|         +---------------+----------------+------------------------+
|         |Input \        |0 to 2.4 V \    |                        |
|         |voltage range  |(0 to 2*VREF)   |                        |
|         +---------------+----------------+------------------------+
|         |Output range \ |0x000 to 0x7FF  |                        |
|         |               |                |                        |
+---------+---------------+----------------+------------------------+
|         |Reference \    |                |                        |
|         |source         |VDDA            |                        |
|DAC      +---------------+----------------+------------------------+
|         |Input range    |0x000 to 0xFFF  |                        |
|         |               |                |                        |
|         +---------------+----------------+------------------------+
|         |Output range   |0 to VDDA       |                        |
|         |               |                |                        |
|         +---------------+----------------+------------------------+
|         |Output type    |Unbuffered \    |                        |
|         |               |output          |                        |
+---------+---------------+----------------+------------------------+
|         |Role           |Master          |Configurable to slave m\|
|         |               |                |ode through HAL function|
|         +---------------+----------------+------------------------+
|I2C      |Data rate      |100 kbps        |Configurable through \  |
|         |               |                |HAL function            |
|         +---------------+----------------+------------------------+
|         |Drive mode of \|Open Drain \    |External pull-up resis\ |
|         |SCL & SDA pins |(drives low)    |tors are required       |
+---------+---------------+----------------+------------------------+
|LpTimer  |Uses WCO (32.768 kHz) as clock source & MCWDT as \       |
|         |counter; 1 count = 1/32768 second or 32768 counts = 1 \  |
|         |second                                                   |
+---------+---------------+----------------+------------------------+
|SPI      |Data rate      |100 kbps        |Configurable through \  |
|         |               |                |HAL function            |
|         +---------------+----------------+------------------------+
|         |Slave select p\|Active low      |                        |
|         |olarity        |                |                        |
+---------+---------------+----------------+------------------------+
|         |Flow control   |No flow control |Configurable through \  |
|         |               |                |HAL function            |
|         +---------------+----------------+------------------------+
|UART     |Data format    |8N1             |Configurable through \  |
|         |               |                |HAL function            |
|         +---------------+----------------+------------------------+
|         |Baud rate      |115200          |Configurable through \  |
|         |               |                |HAL function            |
+---------+---------------+----------------+------------------------+


| `Pin States <group__group__bsp__pin__state.html>`_
| `Pin Mappings <group__group__bsp__pins.html>`_
|    `LED Pins <group__group__bsp__pins__led.html>`_
|    `Button Pins <group__group__bsp__pins__btn.html>`_
|    `Communication Pins <group__group__bsp__pins__comm.html>`_
|    `Arduino Header Pins <group__group__bsp__pins__arduino.html>`_
|    `J2 Header Pins <group__group__bsp__pins__j2.html>`__
| `Macros <group__group__bsp__macros.html>`_
| `Functions <group__group__bsp__functions.html>`_
