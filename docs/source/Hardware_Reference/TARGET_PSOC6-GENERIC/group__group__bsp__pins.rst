=============
Pin Mappings
=============

.. doxygengroup:: group_bsp_pins
   :project: TARGET_PSOC6-GENERIC

.. raw:: html

    <h2>API Reference</h2>
	

.. toctree::

   group__group__bsp__pins__led.rst
   group__group__bsp__pins__btn.rst
   group__group__bsp__pins__comm.rst
   
 
