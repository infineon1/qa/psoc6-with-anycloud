================================
ModusToolbox 101 Training Class
================================

This class is a survey of the Cypress IoT development platform ModusToolbox 2.0. The learning objective is to introduce you to all the tools in ModusToolbox and help you develop some familiarity with using them. 

.. raw:: html

   <a href="https://github.com/cypresssemiconductorco/CypressAcademy_MTB101_Files" target="_blank">Click here to be taken to the GitHub repository where all the class materials, including the book, are availible for you.</a><br><br>
