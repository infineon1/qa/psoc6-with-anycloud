Secure Sockets results/error codes
====================================

.. doxygengroup:: group_secure_sockets_results
   :project: secure-sockets	
   :members:
   :protected-members:
   :private-members:
   :undoc-members:
   