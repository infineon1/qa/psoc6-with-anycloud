================
Data Structures
================

Here are the data structures with brief descriptions:

+-----------------------------------+-----------------------------------+
| `whd_ap_info <structwhd__ap__     | Structure for storing AP          |
| info.html>`__                     | information                       |
+-----------------------------------+-----------------------------------+
| `whd_band_list_t <structwhd__     | Structure for storing radio band  |
| band__list__t.html>`__            | list information                  |
+-----------------------------------+-----------------------------------+
| `whd_btc_lescan_params <struc     | Structure for LE Scan parameters  |
| twhd__btc__lescan__params.htm     |                                   |
| l>`__                             |                                   |
+-----------------------------------+-----------------------------------+
| `whd_buffer_funcs <structwhd_     | Allows WHD to perform buffer      |
| _buffer__funcs.html>`__           | related operations like,          |
|                                   | allocating, releasing, retrieving |
|                                   | the current pointer of and size   |
|                                   | of a packet buffer                |
+-----------------------------------+-----------------------------------+
| `whd_coex_config <structwhd__     | Structure for coex config         |
| coex__config.html>`__             | parameters which can be set by    |
|                                   | application                       |
+-----------------------------------+-----------------------------------+
| `whd_event <structwhd__event.ht   | Event structure used by driver    |
| ml>`__                            | msgs                              |
+-----------------------------------+-----------------------------------+
| `whd_event_eth_hdr <structwhd     | Structure to store ethernet       |
| __event__eth__hdr.html>`__        | header fields in event packets    |
+-----------------------------------+-----------------------------------+
| `whd_event_ether_header <stru     | Structure to store ethernet       |
| ctwhd__event__ether__header.htm   | destination, source and ethertype |
| l>`__                             | in event packets                  |
+-----------------------------------+-----------------------------------+
| `whd_event_msg <structwhd__ev     | Structure to store fields after   |
| ent__msg.html>`__                 | ethernet header in event message  |
+-----------------------------------+-----------------------------------+
| `whd_init_config <structwhd__     | Structure for storing WHD init    |
| init__config.html>`__             | configurations                    |
+-----------------------------------+-----------------------------------+
| `whd_listen_interval_t <struc     | Structure for storing 802.11      |
| twhd__listen__interval__t.htm     | powersave listen interval values  |
| l>`__                             | See                               |
|                                   | `whd_wifi_get_listen_interval <gr |
|                                   | oup__wifiutilities.html#ga5a62f08 |
|                                   | 54ad148dde280b4f75596a309>`__     |
|                                   | for more information              |
+-----------------------------------+-----------------------------------+
| `whd_mac_t <structwhd__mac__t.ht  | Structure for storing a MAC       |
| ml>`__                            | address (Wi-Fi Media Access       |
|                                   | Control address)                  |
+-----------------------------------+-----------------------------------+
| `whd_maclist_t <structwhd__ma     | Structure describing a list of    |
| clist__t.html>`__                 | associated softAP clients         |
+-----------------------------------+-----------------------------------+
| `whd_netif_funcs <structwhd__     | Contains functions which allows   |
| netif__funcs.html>`__             | WHD to pass received data to the  |
|                                   | network stack, to send an         |
|                                   | ethernet frame to WHD, etc        |
+-----------------------------------+-----------------------------------+
| `whd_oob_config <structwhd__o     | Structure for Out-of-band         |
| ob__config.html>`__               | interrupt config parameters which |
|                                   | can be set by application during  |
|                                   | whd power up                      |
+-----------------------------------+-----------------------------------+
| `whd_packet_filter_t <structw     | Structure describing a packet     |
| hd__packet__filter__t.html>`__    | filter list item                  |
+-----------------------------------+-----------------------------------+
| `whd_resource_source <structw     | Interface to a data source that   |
| hd__resource__source.html>`__     | provides external resources to    |
|                                   | the radio driver                  |
+-----------------------------------+-----------------------------------+
| `whd_scan_extended_params_t <st   | Structure for storing extended    |
| ructwhd__scan__extended__params   | scan parameters                   |
| __t.html>`__                      |                                   |
+-----------------------------------+-----------------------------------+
| `whd_scan_result <structwhd__     | Structure for storing scan        |
| scan__result.html>`__             | results                           |
+-----------------------------------+-----------------------------------+
| `whd_sdio_config <structwhd__     | Structure for SDIO config         |
| sdio__config.html>`__             | parameters which can be set by    |
|                                   | application during whd power up   |
+-----------------------------------+-----------------------------------+
| `whd_simple_scan_result <stru     | Structure to store scan result    |
| ctwhd__simple__scan__result.htm   | parameters for each AP            |
| l>`__                             |                                   |
+-----------------------------------+-----------------------------------+
| `whd_spi_config <structwhd__s     | Structure for SPI config          |
| pi__config.html>`__               | parameters which can be set by    |
|                                   | application during whd power up   |
+-----------------------------------+-----------------------------------+
| `whd_ssid_t <structwhd__ssid_     | Structure for storing a Service   |
| _t.html>`__                       | Set Identifier (i.e               |
+-----------------------------------+-----------------------------------+
| `whd_wep_key_t <structwhd__we     | Structure for storing a WEP key   |
| p__key__t.html>`__                |                                   |
+-----------------------------------+-----------------------------------+
| `wl_bss_info_struct <structwl     | BSS(Basic Service Set)            |
| __bss__info__struct.html>`__      | information structure             |
+-----------------------------------+-----------------------------------+


.. toctree::
   :hidden:

   structwhd__ap__info.rst
   structwhd__band__list__t.rst
   structwhd__btc__lescan__params.rst
   structwhd__buffer__funcs.rst
   structwhd__coex__config.rst
   structwhd__event.rst
   structwhd__event__eth__hdr.rst
   structwhd__event__ether__header.rst
   structwhd__event__msg.rst
   structwhd__init__config.rst
   structwhd__listen__interval__t.rst
   structwhd__mac__t.rst
   structwhd__maclist__t.rst
   structwhd__netif__funcs.rst
   structwhd__oob__config.rst
   structwhd__packet__filter__t.rst
   structwhd__resource__source.rst
   structwhd__scan__extended__params__t.rst
   structwhd__scan__result.rst
   structwhd__sdio__config.rst
   structwhd__simple__scan__result.rst
   structwhd__spi__config.rst
   structwhd__ssid__t.rst
   structwhd__wep__key__t.rst
   structwl__bss__info__struct.rst







