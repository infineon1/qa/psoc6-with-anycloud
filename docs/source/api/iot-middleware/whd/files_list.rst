===========
Files List
===========

Here is a list of all documented files with brief descriptions:

+-----------------------------------+-----------------------------------+
| `whd.h <whd_8h.html>`__           | Provides abstract pointer type to |
|                                   | act as instance for: driver,      |
|                                   | interface, buffer funcs, network  |
|                                   | funcs, resource funcs and bus     |
|                                   | funcs                             |
+-----------------------------------+-----------------------------------+
| `whd_events.h <whd__events_8h.    | Header for Event detection        |
| html>`__                          |                                   |
+-----------------------------------+-----------------------------------+
| `whd_network_types.h <whd__net    | Prototypes of functions           |
| work__types_8h.html>`__           | corresponding to Buffer and       |
|                                   | Network Interface                 |
+-----------------------------------+-----------------------------------+
| `whd_resource_api.h <whd__reso    | Prototypes of functions for       |
| urce__api_8h.html>`__             | providing external resources to   |
|                                   | the radio driver                  |
+-----------------------------------+-----------------------------------+
| `whd_types.h <whd__types_8h.ht    | Defines common data types used    |
| ml>`__                            | with WHD                          |
+-----------------------------------+-----------------------------------+
| `whd_wifi_api.h <whd__wifi__ap    | Prototypes of functions for       |
| i_8h.html>`__                     | controlling the Wi-Fi system      |
+-----------------------------------+-----------------------------------+

.. toctree::
   :hidden:

   whd_8h.rst
   whd__events_8h.rst
   whd__network__types_8h.rst
   whd__resource__api_8h.rst
   whd__types_8h.rst
   whd__wifi__api_8h.rst




