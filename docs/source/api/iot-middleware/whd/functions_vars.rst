===========
Variables
===========

 
.. rubric:: - a -
   :name: a--

-  addr :
   `whd_event_msg <structwhd__event__msg.html#a7c939001af591772db111e0be822b469>`__
-  assoc :
   `whd_listen_interval_t <structwhd__listen__interval__t.html#af001610277052191979db17c0133c933>`__
-  atim_window :
   `wl_bss_info_struct <structwl__bss__info__struct.html#a1bec52968a72b2101def88458f0371ec>`__
-  auth_type :
   `whd_event_msg <structwhd__event__msg.html#a6d9c484262d9d2e9fceaa54a2e58b3c0>`__

.. rubric:: - b -
   :name: b--

-  band :
   `whd_ap_info <structwhd__ap__info.html#aee4db592d40d1e5a4305eb6ba177c1d7>`__
   ,
   `whd_scan_result <structwhd__scan__result.html#aee4db592d40d1e5a4305eb6ba177c1d7>`__
-  basic_mcs :
   `wl_bss_info_struct <structwl__bss__info__struct.html#a7efd75a130623350ac4a98d3688ee791>`__
-  beacon :
   `whd_listen_interval_t <structwhd__listen__interval__t.html#a59de6cdff8214507260f142834f20cee>`__
-  beacon_period :
   `wl_bss_info_struct <structwl__bss__info__struct.html#ab3cd1ed6ac499c1d63772712337edba8>`__
-  bss_type :
   `whd_ap_info <structwhd__ap__info.html#ad1f59c68ce8ec9dad45866f67ee0ef44>`__
   ,
   `whd_scan_result <structwhd__scan__result.html#ad1f59c68ce8ec9dad45866f67ee0ef44>`__
-  bsscfgidx :
   `whd_event_msg <structwhd__event__msg.html#aa2c53e1708eb89382779539506cb4359>`__
-  BSSID :
   `whd_ap_info <structwhd__ap__info.html#acff14471b28062500a2992114ae54765>`__
   ,
   `whd_scan_result <structwhd__scan__result.html#acff14471b28062500a2992114ae54765>`__
   ,
   `whd_simple_scan_result <structwhd__simple__scan__result.html#acff14471b28062500a2992114ae54765>`__
   ,
   `wl_bss_info_struct <structwl__bss__info__struct.html#acff14471b28062500a2992114ae54765>`__

.. rubric:: - c -
   :name: c--

-  capability :
   `wl_bss_info_struct <structwl__bss__info__struct.html#a4d6a7cc78819fe901fa45a1b51266e18>`__
-  ccode :
   `whd_scan_result <structwhd__scan__result.html#a4f626e97f0f0ea28fea702a7ecaad54f>`__
-  channel :
   `whd_ap_info <structwhd__ap__info.html#a715f5cb061d11eb75981741eda4dafcd>`__
   ,
   `whd_scan_result <structwhd__scan__result.html#a715f5cb061d11eb75981741eda4dafcd>`__
   ,
   `whd_simple_scan_result <structwhd__simple__scan__result.html#a715f5cb061d11eb75981741eda4dafcd>`__
-  chanspec :
   `wl_bss_info_struct <structwl__bss__info__struct.html#aa5d9886a989c0e36349581e3f06cd4c0>`__
-  count :
   `whd_maclist_t <structwhd__maclist__t.html#a86988a65e0d3ece7990c032c159786d6>`__
   ,
   `wl_bss_info_struct <structwl__bss__info__struct.html#a86988a65e0d3ece7990c032c159786d6>`__
-  country :
   `whd_init_config <structwhd__init__config.html#a7c552f1f715c8aa245f40d4be48b0fc9>`__
-  ctl_ch :
   `wl_bss_info_struct <structwl__bss__info__struct.html#aaba7272626229ace409b150bb1981044>`__
-  current_band :
   `whd_band_list_t <structwhd__band__list__t.html#aed8b81544ada7366584d9cdea48639f9>`__

.. rubric:: - d -
   :name: d--

-  data :
   `whd_wep_key_t <structwhd__wep__key__t.html#a6460a21fbea84550b8bf9c7ce257e4ed>`__
-  datalen :
   `whd_event_msg <structwhd__event__msg.html#a20ef2f8b0dc25d28c0c76be72919a60a>`__
-  destination_address :
   `whd_event_ether_header <structwhd__event__ether__header.html#aecece25a401285d039a6f30991dad78e>`__
-  dev_gpio_sel :
   `whd_oob_config <structwhd__oob__config.html#a2d83d43c26f20c82203b65cef7cf9d2f>`__
-  dtim :
   `whd_listen_interval_t <structwhd__listen__interval__t.html#a011e406e7bba2200194b90d308ea2e82>`__
-  dtim_period :
   `wl_bss_info_struct <structwl__bss__info__struct.html#a2817cb129e69f6ce5a24dfb2dd8706cc>`__
-  duty_cycle :
   `whd_btc_lescan_params <structwhd__btc__lescan__params.html#ad0362052c89424bf11eb63b726b05182>`__

.. rubric:: - e -
   :name: e--

-  enabled_status :
   `whd_packet_filter_t <structwhd__packet__filter__t.html#ad9dfd7c243623af351789a69a792beb8>`__
-  eth :
   `whd_event <structwhd__event.html#ae36ac55a3c73f9f7dfa065165130b39e>`__
-  eth_evt_hdr :
   `whd_event <structwhd__event.html#a304eef8ceefeab88f71947f57df3d61b>`__
-  ethertype :
   `whd_event_ether_header <structwhd__event__ether__header.html#aa9296c58dc24c63c4ee927db394a97d7>`__
-  event_type :
   `whd_event_msg <structwhd__event__msg.html#a45def65890de0da17bc0d6e2ca2b3a9f>`__

.. rubric:: - f -
   :name: f--

-  flags :
   `whd_event_msg <structwhd__event__msg.html#a1e87af3c18a2fd36c61faf89949bdc3f>`__
   ,
   `whd_scan_result <structwhd__scan__result.html#aa2585d779da0ab21273a8d92de9a0ebe>`__
   ,
   `wl_bss_info_struct <structwl__bss__info__struct.html#aa2585d779da0ab21273a8d92de9a0ebe>`__

.. rubric:: - h -
   :name: h--

-  high_speed_sdio_clock :
   `whd_sdio_config <structwhd__sdio__config.html#aa23e026ad94f9606f2a0e5fef47df13d>`__
-  host_oob_pin :
   `whd_oob_config <structwhd__oob__config.html#a6c705dc46f1c4249cef91f1fc8a877dc>`__

.. rubric:: - i -
   :name: i--

-  id :
   `whd_packet_filter_t <structwhd__packet__filter__t.html#abaabdc509cdaba7df9f56c6c76f3ae19>`__
-  ie_len :
   `whd_scan_result <structwhd__scan__result.html#a5530db0834a2796234ed42c3fcb5c0f4>`__
-  ie_length :
   `wl_bss_info_struct <structwl__bss__info__struct.html#a5312679515b5ee9c1875196c703a8a52>`__
-  ie_offset :
   `wl_bss_info_struct <structwl__bss__info__struct.html#a67b93773e8da423543b703cad84da5a5>`__
-  ie_ptr :
   `whd_scan_result <structwhd__scan__result.html#a460ec020636ddecd8758543cfa83e101>`__
-  ifidx :
   `whd_event_msg <structwhd__event__msg.html#aee54e454588cb953b7f349e589c2d09f>`__
-  ifname :
   `whd_event_msg <structwhd__event__msg.html#a6416f2a9886b328d85974b42f5a34ce5>`__
-  index :
   `whd_wep_key_t <structwhd__wep__key__t.html#aae5a12e607d0f782506d9e6ec6179c64>`__
-  int_grant :
   `whd_btc_lescan_params <structwhd__btc__lescan__params.html#a8dcea8fcf19e2facaf59891ab47375d9>`__
-  intr_priority :
   `whd_oob_config <structwhd__oob__config.html#a948130b9ee9aaba4463a9a678c830b2f>`__
-  is_falling_edge :
   `whd_oob_config <structwhd__oob__config.html#afac6867bb224b40190d70a7bed57b556>`__
-  is_spi_normal_mode :
   `whd_spi_config <structwhd__spi__config.html#a65649e24a48dbfcf74a6e3b0ea729181>`__

.. rubric:: - l -
   :name: l--

-  le_scan_params :
   `whd_coex_config <structwhd__coex__config.html#afcc1e187cd7f285de8d56162fbbb52bb>`__
-  length :
   `whd_event_eth_hdr <structwhd__event__eth__hdr.html#a1892eba2086d12ac2b09005aeb09ea3b>`__
   ,
   `whd_ssid_t <structwhd__ssid__t.html#ab2b3adeb2a67e656ff030b56727fd0ac>`__
   ,
   `whd_wep_key_t <structwhd__wep__key__t.html#ab2b3adeb2a67e656ff030b56727fd0ac>`__
   ,
   `wl_bss_info_struct <structwl__bss__info__struct.html#aebb70c2aab3407a9f05334c47131a43b>`__

.. rubric:: - m -
   :name: m--

-  mac_list :
   `whd_maclist_t <structwhd__maclist__t.html#a789742dac11adf87a322501f5f81dbb2>`__
-  mask :
   `whd_packet_filter_t <structwhd__packet__filter__t.html#aeef3362381f6b1bbd1ae35e60f5aa077>`__
-  mask_size :
   `whd_packet_filter_t <structwhd__packet__filter__t.html#a4366e37b92d06167be3c708a731a904a>`__
-  max_data_rate :
   `whd_ap_info <structwhd__ap__info.html#a2deb22a1108e6c9371d92d496c07da01>`__
   ,
   `whd_scan_result <structwhd__scan__result.html#a2deb22a1108e6c9371d92d496c07da01>`__
-  max_win :
   `whd_btc_lescan_params <structwhd__btc__lescan__params.html#a531bd65b280391995c4497112ca4e9c3>`__

.. rubric:: - n -
   :name: n--

-  n_cap :
   `wl_bss_info_struct <structwl__bss__info__struct.html#aaa58323f8c11ee56cc8d8c2e66938230>`__
-  nbss_cap :
   `wl_bss_info_struct <structwl__bss__info__struct.html#aafd41688d55159e60c6970dddbfdf4ba>`__
-  next :
   `whd_ap_info <structwhd__ap__info.html#a79b534cf566945b2c8b016126425102b>`__
   ,
   `whd_scan_result <structwhd__scan__result.html#a98448b39c64ea60aa2166916acc60849>`__
-  number_of_bands :
   `whd_band_list_t <structwhd__band__list__t.html#aa96a08185b53b8f402dfbc637c6f162d>`__
-  number_of_probes_per_channel :
   `whd_scan_extended_params_t <structwhd__scan__extended__params__t.html#ae142c15dcc2633a4ab09a282fc942186>`__

.. rubric:: - o -
   :name: o--

-  octet :
   `whd_mac_t <structwhd__mac__t.html#abc3755f1f66dea95fce153ee4f49e907>`__
-  offset :
   `whd_packet_filter_t <structwhd__packet__filter__t.html#ac681806181c80437cfab37335f62ff39>`__
-  oob_config :
   `whd_sdio_config <structwhd__sdio__config.html#a5fa87ee1769d21a2bf8a871ab7f4d1bb>`__
   ,
   `whd_spi_config <structwhd__spi__config.html#a5fa87ee1769d21a2bf8a871ab7f4d1bb>`__
-  other_band :
   `whd_band_list_t <structwhd__band__list__t.html#ad8ab251449a85be9bcc30eb9113862e6>`__
-  oui :
   `whd_event_eth_hdr <structwhd__event__eth__hdr.html#a5d81eb4d4b4a5b8d3a89be061d06b64d>`__

.. rubric:: - p -
   :name: p--

-  pattern :
   `whd_packet_filter_t <structwhd__packet__filter__t.html#af42a55e4e3cdd2ea57e52a29a0d77ce3>`__
-  phy_noise :
   `wl_bss_info_struct <structwl__bss__info__struct.html#a85ba569dec5085a93016434929bbf7d1>`__
-  priority :
   `whd_btc_lescan_params <structwhd__btc__lescan__params.html#a0815784d41b3c13d42ce22367abfba1d>`__

.. rubric:: - r -
   :name: r--

-  rates :
   `wl_bss_info_struct <structwl__bss__info__struct.html#a686551a9eef0a3911a371e315bb24d36>`__
-  rateset :
   `wl_bss_info_struct <structwl__bss__info__struct.html#aee32c70ea369b8be44e5cf2b9a1184a2>`__
-  reason :
   `whd_event_msg <structwhd__event__msg.html#a5ac0b16c31813f87b98e97cc5bacd64a>`__
-  reserved :
   `wl_bss_info_struct <structwl__bss__info__struct.html#a72aca6ea6d8153b28ea8f139b932ec3e>`__
-  reserved32 :
   `wl_bss_info_struct <structwl__bss__info__struct.html#ae3f4b5f7822024fbf0bea3a8845c67c4>`__
-  RSSI :
   `wl_bss_info_struct <structwl__bss__info__struct.html#ae45c71dee229890b4401a4c0105d73cf>`__
-  rule :
   `whd_packet_filter_t <structwhd__packet__filter__t.html#a6b5d1db53f323ed75aaaa1ecaabe4533>`__

.. rubric:: - s -
   :name: s--

-  scan_active_dwell_time_per_channel_ms :
   `whd_scan_extended_params_t <structwhd__scan__extended__params__t.html#addbd186dd08b75e5de298a3d8dbb76a1>`__
-  scan_home_channel_dwell_time_between_channels_ms :
   `whd_scan_extended_params_t <structwhd__scan__extended__params__t.html#a325bae58ff955b428047edab0a2f0799>`__
-  scan_int :
   `whd_btc_lescan_params <structwhd__btc__lescan__params.html#ab9fbbe93d8ab19fa81bebf4ade2deddd>`__
-  scan_passive_dwell_time_per_channel_ms :
   `whd_scan_extended_params_t <structwhd__scan__extended__params__t.html#afe48c256e4f4f14eb202d51dd8348818>`__
-  scan_win :
   `whd_btc_lescan_params <structwhd__btc__lescan__params.html#a02d72a917cfb65e36cd0527ed472bf8f>`__
-  sdio_1bit_mode :
   `whd_sdio_config <structwhd__sdio__config.html#aaf50e6ef7ee9717f104ced9db86efd6a>`__
-  security :
   `whd_ap_info <structwhd__ap__info.html#ae8d5baabacdabe6d3590465572849754>`__
   ,
   `whd_scan_result <structwhd__scan__result.html#ae8d5baabacdabe6d3590465572849754>`__
   ,
   `whd_simple_scan_result <structwhd__simple__scan__result.html#ae8d5baabacdabe6d3590465572849754>`__
-  signal_strength :
   `whd_ap_info <structwhd__ap__info.html#ac303b69da3c469c92299a6ff260e2859>`__
   ,
   `whd_scan_result <structwhd__scan__result.html#ac303b69da3c469c92299a6ff260e2859>`__
   ,
   `whd_simple_scan_result <structwhd__simple__scan__result.html#ac303b69da3c469c92299a6ff260e2859>`__
-  SNR :
   `wl_bss_info_struct <structwl__bss__info__struct.html#abcbf8106d506ba6a33c01411d5ec3e99>`__
-  source_address :
   `whd_event_ether_header <structwhd__event__ether__header.html#aceb7473ccdf9d41c5b6aee7a0264972a>`__
-  SSID :
   `whd_ap_info <structwhd__ap__info.html#ae3940d431c3c3ed25294eb70a02c330b>`__
   ,
   `whd_scan_result <structwhd__scan__result.html#ae3940d431c3c3ed25294eb70a02c330b>`__
   ,
   `whd_simple_scan_result <structwhd__simple__scan__result.html#ae3940d431c3c3ed25294eb70a02c330b>`__
   ,
   `wl_bss_info_struct <structwl__bss__info__struct.html#a337a4a90b9c8cb320d1232cf9f88fa90>`__
-  SSID_len :
   `wl_bss_info_struct <structwl__bss__info__struct.html#a7eafb3bd3419add91d9d532df5dcf63e>`__
-  status :
   `whd_event_msg <structwhd__event__msg.html#ade20423e91627f07e610924cb0081623>`__
-  subtype :
   `whd_event_eth_hdr <structwhd__event__eth__hdr.html#ac5d9ab8403fb9ca24facc32b821dd53b>`__

.. rubric:: - t -
   :name: t--

-  thread_priority :
   `whd_init_config <structwhd__init__config.html#abd05090115efa85f6e47087351cc502a>`__
-  thread_stack_size :
   `whd_init_config <structwhd__init__config.html#a6aa2c0bcb2024f1e4c38f9e8f6769c09>`__
-  thread_stack_start :
   `whd_init_config <structwhd__init__config.html#a52591d0d84b73dea8ca0a25c486f62c7>`__

.. rubric:: - u -
   :name: u--

-  usr_subtype :
   `whd_event_eth_hdr <structwhd__event__eth__hdr.html#ae9ad9911fe2a5dc5789001a2b48d5c9d>`__

.. rubric:: - v -
   :name: v--

-  value :
   `whd_ssid_t <structwhd__ssid__t.html#aa88a4115b417ed84082f85ab347f4b02>`__
-  version :
   `whd_event_eth_hdr <structwhd__event__eth__hdr.html#ab22abc2906422da61885ac6c8e6a1a59>`__
   ,
   `whd_event_msg <structwhd__event__msg.html#ab6d7b6f8c2ceaba7acda80aaf05f4899>`__
   ,
   `wl_bss_info_struct <structwl__bss__info__struct.html#acd99bb05ca015e7d74448acb1deba7ca>`__

.. rubric:: - w -
   :name: w--

-  whd_buffer_add_remove_at_front :
   `whd_buffer_funcs <structwhd__buffer__funcs.html#ab4c0aef4cb8722f8b22209592a84594a>`__
-  whd_buffer_get_current_piece_data_pointer :
   `whd_buffer_funcs <structwhd__buffer__funcs.html#a5c2483392b61c9f4619e5fc83ec48601>`__
-  whd_buffer_get_current_piece_size :
   `whd_buffer_funcs <structwhd__buffer__funcs.html#a720960b687167d7ab440ee432e048f9d>`__
-  whd_buffer_release :
   `whd_buffer_funcs <structwhd__buffer__funcs.html#a25e0a4f8235603094896917b3421baac>`__
-  whd_buffer_set_size :
   `whd_buffer_funcs <structwhd__buffer__funcs.html#a2dc10ed21175617bb9f20f34827d16e7>`__
-  whd_event :
   `whd_event <structwhd__event.html#aaca5889552f0d7a52f681e7b96d53720>`__
-  whd_get_resource_block :
   `whd_resource_source <structwhd__resource__source.html#a621cec3fecb5491d46d3534a59b6862b>`__
-  whd_get_resource_block_size :
   `whd_resource_source <structwhd__resource__source.html#af7ac61e99cc2504e39dfb220a34b8d39>`__
-  whd_get_resource_no_of_blocks :
   `whd_resource_source <structwhd__resource__source.html#a2645315e933c6e65046bec5dad04b8e4>`__
-  whd_host_buffer_get :
   `whd_buffer_funcs <structwhd__buffer__funcs.html#a08be981b01c563df3f34025e463a6a32>`__
-  whd_network_process_ethernet_data :
   `whd_netif_funcs <structwhd__netif__funcs.html#aeb98a45468d1d8f37450617a3388387c>`__
-  whd_resource_size :
   `whd_resource_source <structwhd__resource__source.html#ab2a3bf0a36362b733939361143158e02>`__



