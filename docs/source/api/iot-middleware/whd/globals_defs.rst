========
Macros
========
 
.. rubric:: - a -
   :name: a--

-  AES_ENABLED :
   `whd_types.h <whd__types_8h.html#a577a9dbc615ba145496a41a2035bd615>`__

.. rubric:: - b -
   :name: b--

-  BDC_HEADER_OFFSET_TO_DATA :
   `whd_types.h <whd__types_8h.html#a559655385d80ca97754fafa29ac87717>`__
-  BDC_HEADER_WITH_PAD :
   `whd_types.h <whd__types_8h.html#a50f830e25a5ac6d7d0e3eb8d23e90943>`__
-  BLOCK_SIZE :
   `whd_resource_api.h <whd__resource__api_8h.html#ad51ded0bbd705f02f73fc60c0b721ced>`__
-  BUFFER_OVERHEAD :
   `whd_types.h <whd__types_8h.html#a9b159bb86ca28f2200f30661441f232e>`__

.. rubric:: - e -
   :name: e--

-  ENTERPRISE_ENABLED :
   `whd_types.h <whd__types_8h.html#ada6d6331053c0f88c63d77ba8d2019c8>`__

.. rubric:: - f -
   :name: f--

-  FBT_ENABLED :
   `whd_types.h <whd__types_8h.html#aafdddf85f4d0df08ee3aead815ad7c76>`__

.. rubric:: - i -
   :name: i--

-  IBSS_ENABLED :
   `whd_types.h <whd__types_8h.html#a9640c064932a3a633a3312b737658f83>`__

.. rubric:: - m -
   :name: m--

-  MAX_BUS_HEADER_SIZE :
   `whd_types.h <whd__types_8h.html#aab3fad8b23f2db6d6c985134359bec0d>`__
-  MCSSET_LEN :
   `whd_types.h <whd__types_8h.html#aa44a8901a98f9868efefee50db967736>`__
-  MK_CNTRY :
   `whd_types.h <whd__types_8h.html#a4ab8a2182e68bb029551aa754bff1861>`__

.. rubric:: - n -
   :name: n--

-  NO_POWERSAVE_MODE :
   `whd_types.h <whd__types_8h.html#a287bbd4f521f0d1cf44165bc617cebf5>`__

.. rubric:: - p -
   :name: p--

-  PACKET_FILTER_LIST_BUFFER_MAX_LEN :
   `whd_types.h <whd__types_8h.html#a1905b69d7646a2d2e21da7042bd14374>`__
-  PM1_POWERSAVE_MODE :
   `whd_types.h <whd__types_8h.html#a32f56429462855603066fea3723c5217>`__
-  PM2_POWERSAVE_MODE :
   `whd_types.h <whd__types_8h.html#af29e5543837b68c29417a7d15e3228b7>`__
-  PORT_FILTER_LEN :
   `whd_types.h <whd__types_8h.html#a7d8ed7e88b85772d09799ca87a27370d>`__

.. rubric:: - r -
   :name: r--

-  REFERENCE_DEBUG_ONLY_VARIABLE :
   `whd_types.h <whd__types_8h.html#a1491cb4c4adc44f22a91f18609dfb2f7>`__

.. rubric:: - s -
   :name: s--

-  SDPCM_HEADER :
   `whd_types.h <whd__types_8h.html#aaf73ac170e07969ed067a6cd5088044f>`__
-  SECURITY_MASK :
   `whd_types.h <whd__types_8h.html#a537103c16413b65ffb4d9ee0156248f5>`__
-  SHARED_ENABLED :
   `whd_types.h <whd__types_8h.html#a4d4a4586c264fe8e4acb0bf7169b7b0f>`__
-  SSID_NAME_SIZE :
   `whd_types.h <whd__types_8h.html#a9ee2fe056ad3787e30bb8da7248592c7>`__

.. rubric:: - t -
   :name: t--

-  TKIP_ENABLED :
   `whd_types.h <whd__types_8h.html#a20f0d7118c2d35a688bcdc5b8b0920d9>`__
-  TKO_DATA_OFFSET :
   `whd_types.h <whd__types_8h.html#a3e98ad24780d89cefbec71da9fac2da6>`__

.. rubric:: - u -
   :name: u--

-  UNUSED_PARAMETER :
   `whd_types.h <whd__types_8h.html#a3c95a90e7806e4b0d21edfae15b73465>`__
-  UNUSED_VARIABLE :
   `whd_types.h <whd__types_8h.html#a4048bf3892868ded8a28f8cbdd339c09>`__

.. rubric:: - w -
   :name: w--

-  WEP_ENABLED :
   `whd_types.h <whd__types_8h.html#a4199a1b37dba92f482ff0b9eb406c323>`__
-  WHD_ACCESS_POINT_NOT_FOUND :
   `whd_types.h <whd__types_8h.html#a38cbe87adc541246f2eae5534501231a>`__
-  WHD_ADDRESS_ALREADY_REGISTERED :
   `whd_types.h <whd__types_8h.html#a7aecc5610dbe6e1584b349bfb202a2a7>`__
-  WHD_AP_ALREADY_UP :
   `whd_types.h <whd__types_8h.html#a3ed786d3514cdb56daecc1713feb8199>`__
-  WHD_BADARG :
   `whd_types.h <whd__types_8h.html#a572bf9261bd3f234bd804d55b284f55d>`__
-  WHD_BUFFER_ALLOC_FAIL :
   `whd_types.h <whd__types_8h.html#a18719c46ce4cff61ff3c4d50309261e2>`__
-  WHD_BUFFER_POINTER_MOVE_ERROR :
   `whd_types.h <whd__types_8h.html#a4453a7518a649b88128f21a5af2a8755>`__
-  WHD_BUFFER_SIZE_SET_ERROR :
   `whd_types.h <whd__types_8h.html#af7974469bcf8a4bad24d339d623297e7>`__
-  WHD_BUFFER_UNAVAILABLE_PERMANENT :
   `whd_types.h <whd__types_8h.html#a730333b8281a84dcda0985b757b97823>`__
-  WHD_BUFFER_UNAVAILABLE_TEMPORARY :
   `whd_types.h <whd__types_8h.html#a94f122e793a4a91c8ac044366d1611a9>`__
-  WHD_BUS_READ_REGISTER_ERROR :
   `whd_types.h <whd__types_8h.html#a998914de59ed4ad6f510c063fee877f1>`__
-  WHD_BUS_WRITE_REGISTER_ERROR :
   `whd_types.h <whd__types_8h.html#a659306d5da66cf4bb2d2a5a8c7f530a2>`__
-  WHD_CLM_BLOB_DLOAD_ERROR :
   `whd_types.h <whd__types_8h.html#a6dd36bd14965f3601fee55c6e799dfa1>`__
-  WHD_CONNECTION_LOST :
   `whd_types.h <whd__types_8h.html#a11801e020e110714a7a21527b18bc5c0>`__
-  WHD_CORE_CLOCK_NOT_ENABLED :
   `whd_types.h <whd__types_8h.html#af182b141bdde3c09af9849ca2f6f4598>`__
-  WHD_CORE_IN_RESET :
   `whd_types.h <whd__types_8h.html#adcd663e17b745c4fde47a59bd4ae3886>`__
-  WHD_DELAY_TOO_LONG :
   `whd_types.h <whd__types_8h.html#a7c128bcb61d6db9a8d58a2f5b9ed55a2>`__
-  WHD_DELAY_TOO_SHORT :
   `whd_types.h <whd__types_8h.html#a34cc0bb8645a8046708428c134c76f14>`__
-  WHD_DOES_NOT_EXIST :
   `whd_types.h <whd__types_8h.html#abb82350191c4b2751119d4206044c676>`__
-  WHD_EAPOL_KEY_FAILURE :
   `whd_types.h <whd__types_8h.html#a11f0c32e1137f71695fca3ffcfba3225>`__
-  WHD_EAPOL_KEY_PACKET_G1_TIMEOUT :
   `whd_types.h <whd__types_8h.html#a2b501e596df5290737987b146f4f8ea7>`__
-  WHD_EAPOL_KEY_PACKET_M1_TIMEOUT :
   `whd_types.h <whd__types_8h.html#a433165f9b0097e6c60d21ae88f2f16f6>`__
-  WHD_EAPOL_KEY_PACKET_M3_TIMEOUT :
   `whd_types.h <whd__types_8h.html#a6ebde3f52c86c797f2a6f2e1366febac>`__
-  WHD_ETHERNET_SIZE :
   `whd_types.h <whd__types_8h.html#ac7923e15c2e0a935cb7c8cad13b9dc6e>`__
-  WHD_FILTER_NOT_FOUND :
   `whd_types.h <whd__types_8h.html#a91089285a820e59587c544de682420e3>`__
-  WHD_FLOW_CONTROLLED :
   `whd_types.h <whd__types_8h.html#a23f5c05cf88f6ed945cf15c111c778cb>`__
-  WHD_HAL_ERROR :
   `whd_types.h <whd__types_8h.html#a68e4cdc07dccd14d0cc6c6216ff37fb2>`__
-  WHD_HANDLER_ALREADY_REGISTERED :
   `whd_types.h <whd__types_8h.html#a18206b25da666ffc407fcf2b207a1739>`__
-  WHD_HWTAG_MISMATCH :
   `whd_types.h <whd__types_8h.html#abdd206e05538f81fcb9dca59d4f4404e>`__
-  WHD_INTERFACE_NOT_UP :
   `whd_types.h <whd__types_8h.html#a32f993fc79964aeb95864885ab604a20>`__
-  WHD_INVALID_DUTY_CYCLE :
   `whd_types.h <whd__types_8h.html#a8ef95acfbfd3ccbb9912af09bc4d0784>`__
-  WHD_INVALID_INTERFACE :
   `whd_types.h <whd__types_8h.html#a17a759d5ba4e5b5cfc51a5ea89e16ad0>`__
-  WHD_INVALID_JOIN_STATUS :
   `whd_types.h <whd__types_8h.html#ab0c582f87b55cbcbbfdcdd1551f6be1b>`__
-  WHD_INVALID_KEY :
   `whd_types.h <whd__types_8h.html#a63b3cf80bafef7f1f255c0c826bceb10>`__
-  WHD_IOCTL_FAIL :
   `whd_types.h <whd__types_8h.html#ac9f58fcc05846835a544720f7a80e9f7>`__
-  WHD_JOIN_IN_PROGRESS :
   `whd_types.h <whd__types_8h.html#a39d85692e57da297005929f28430cf8e>`__
-  WHD_LINK_HEADER :
   `whd_types.h <whd__types_8h.html#a19b63b93a33a8d9f330925e1f0e87a5d>`__
-  WHD_LINK_MTU :
   `whd_types.h <whd__types_8h.html#a6c25e15fb4270bbd5e77169d9ce1da59>`__
-  WHD_MALLOC_FAILURE :
   `whd_types.h <whd__types_8h.html#a6450f155199992d564d036f64963cd54>`__
-  WHD_MSG_IFNAME_MAX :
   `whd_events.h <whd__events_8h.html#ac94cc32da29a68641af2b000d883cd6a>`__
-  WHD_NETWORK_NOT_FOUND :
   `whd_types.h <whd__types_8h.html#a27ea061512955c1fa154666b11346771>`__
-  WHD_NO_CREDITS :
   `whd_types.h <whd__types_8h.html#a6270efc8ded415e7d0e1059b3d5eff1f>`__
-  WHD_NO_PACKET_TO_RECEIVE :
   `whd_types.h <whd__types_8h.html#a5a5122cc33024c33eaa9dcca1d6b2917>`__
-  WHD_NO_PACKET_TO_SEND :
   `whd_types.h <whd__types_8h.html#a1e3927ec83cfaac801196210b8a9af45>`__
-  WHD_NOT_AUTHENTICATED :
   `whd_types.h <whd__types_8h.html#ac2e279a3f388f66c076d4dd73310a27b>`__
-  WHD_NOT_KEYED :
   `whd_types.h <whd__types_8h.html#a4377fd0c321dbebed159e8c84858a1e4>`__
-  WHD_NULL_PTR_ARG :
   `whd_types.h <whd__types_8h.html#ab7612a8165b6e28255c4a2ef78627454>`__
-  WHD_OUT_OF_EVENT_HANDLER_SPACE :
   `whd_types.h <whd__types_8h.html#a24f120d05f15b5c5564c8fc7098516f3>`__
-  WHD_PARTIAL_RESULTS :
   `whd_types.h <whd__types_8h.html#a1b8d28e95bef4c55d0cdbc2ffc6c9ef7>`__
-  WHD_PAYLOAD_MTU :
   `whd_types.h <whd__types_8h.html#a90b4f488c8f53596cffcb88fe6111fcc>`__
-  WHD_PENDING :
   `whd_types.h <whd__types_8h.html#a8c29570e0fc44f87bdea3b6a438efe1c>`__
-  WHD_PHYSICAL_HEADER :
   `whd_types.h <whd__types_8h.html#a65859cd60e3f43138360dfa07e2d0b30>`__
-  WHD_PMK_WRONG_LENGTH :
   `whd_types.h <whd__types_8h.html#a9691a3d9b9499bc36d73483bdf824ab7>`__
-  WHD_QUEUE_ERROR :
   `whd_types.h <whd__types_8h.html#a0c06e71abd08027b33cc93a684c04cd5>`__
-  WHD_RESULT_CREATE :
   `whd_types.h <whd__types_8h.html#adf4a5004401ae1772bf5b33a5e19e108>`__
-  WHD_RESULT_TYPE :
   `whd_types.h <whd__types_8h.html#a24950ac00b520d360c9d6092be69ee68>`__
-  WHD_RTOS_ERROR :
   `whd_types.h <whd__types_8h.html#a2ef366a6a9ac07e3087461c1946499dd>`__
-  WHD_RTOS_STATIC_MEM_LIMIT :
   `whd_types.h <whd__types_8h.html#a6499f6328208f184b4f716a910d7cdf6>`__
-  WHD_RX_BUFFER_ALLOC_FAIL :
   `whd_types.h <whd__types_8h.html#a7609ba95d8336c6d5a3a790b0b61b590>`__
-  WHD_SDIO_BUS_UP_FAIL :
   `whd_types.h <whd__types_8h.html#ac73dc3debf5093500f5fcd0b1750e8e7>`__
-  WHD_SDIO_RETRIES_EXCEEDED :
   `whd_types.h <whd__types_8h.html#ac5725175be9d81f698084386eb44688e>`__
-  WHD_SDIO_RX_FAIL :
   `whd_types.h <whd__types_8h.html#a276b9d9307ec24da1cd6ed1da2f0feaf>`__
-  WHD_SEMAPHORE_ERROR :
   `whd_types.h <whd__types_8h.html#a465f9477fbe7df4860470bf262a20e81>`__
-  WHD_SET_BLOCK_ACK_WINDOW_FAIL :
   `whd_types.h <whd__types_8h.html#a8aedd7c7219349b28905c47b547e3419>`__
-  WHD_SLEEP_ERROR :
   `whd_types.h <whd__types_8h.html#ad506512b8a302ce61df402f59bc7504c>`__
-  WHD_SPI_ID_READ_FAIL :
   `whd_types.h <whd__types_8h.html#a0ed6696f986b3d35b4306c06a708db19>`__
-  WHD_SPI_SIZE_MISMATCH :
   `whd_types.h <whd__types_8h.html#a6e7d6fcd9f48e9df5bd8ed66f88d05f3>`__
-  WHD_SUCCESS :
   `whd_types.h <whd__types_8h.html#a8aaeff5d2f1bfaeeb2931c401ac40a3f>`__
-  WHD_THREAD_CREATE_FAILED :
   `whd_types.h <whd__types_8h.html#ab7fb839bb825a42085305032735fcfff>`__
-  WHD_THREAD_DELETE_FAIL :
   `whd_types.h <whd__types_8h.html#a4deb31cfc8310b82d3b6359b526f7e9b>`__
-  WHD_THREAD_FINISH_FAIL :
   `whd_types.h <whd__types_8h.html#a3be54cee48ade84eff99df8a29979958>`__
-  WHD_THREAD_STACK_NULL :
   `whd_types.h <whd__types_8h.html#a9c694e98139f96882d3098802ec9a947>`__
-  WHD_TIMEOUT :
   `whd_types.h <whd__types_8h.html#a7eee7c29f665510d218cea3c620b3150>`__
-  WHD_UNFINISHED :
   `whd_types.h <whd__types_8h.html#a9a3c109ff2d06746031090b108e9540e>`__
-  WHD_UNKNOWN_INTERFACE :
   `whd_types.h <whd__types_8h.html#ad6972e6627af27af729d9441ac57dbac>`__
-  WHD_UNKNOWN_SECURITY_TYPE :
   `whd_types.h <whd__types_8h.html#a7904772fe0ca7e2de9843ee118def04c>`__
-  WHD_UNSUPPORTED :
   `whd_types.h <whd__types_8h.html#ac555243f1fc71fb3a5037d57d5f70768>`__
-  WHD_WAIT_ABORTED :
   `whd_types.h <whd__types_8h.html#aa716e92104149dfba1cd732a92ddd04f>`__
-  WHD_WEP_KEYLEN_BAD :
   `whd_types.h <whd__types_8h.html#ae33bbaaf58019659592cd22800fc20e0>`__
-  WHD_WEP_NOT_ALLOWED :
   `whd_types.h <whd__types_8h.html#af35d1a67b9c58a0dd84bc13a4fa63e67>`__
-  WHD_WLAN_ACM_NOTSUPPORTED :
   `whd_types.h <whd__types_8h.html#a0ed279e8ccaefdcb4347649c08591280>`__
-  WHD_WLAN_ASSOCIATED :
   `whd_types.h <whd__types_8h.html#afa0df2e6c420b3d8bef2baf60d2addea>`__
-  WHD_WLAN_BAD_VERSION :
   `whd_types.h <whd__types_8h.html#ad06624a14de68f3ceb4464bc2c38ab97>`__
-  WHD_WLAN_BADADDR :
   `whd_types.h <whd__types_8h.html#a1412d0482b0178a4c05c76edb14148af>`__
-  WHD_WLAN_BADARG :
   `whd_types.h <whd__types_8h.html#a8014f6266d91d9393e04bbc9a9b9080b>`__
-  WHD_WLAN_BADBAND :
   `whd_types.h <whd__types_8h.html#a3da74a46eebc41e14e7c25815c1381b1>`__
-  WHD_WLAN_BADCHAN :
   `whd_types.h <whd__types_8h.html#a18200ecab39e4cdedafa57f74be50009>`__
-  WHD_WLAN_BADKEYIDX :
   `whd_types.h <whd__types_8h.html#aa0ab7a6987319fd5e0fd5e3cd1176be2>`__
-  WHD_WLAN_BADLEN :
   `whd_types.h <whd__types_8h.html#afd54025f38ebd9a069a768c74da6d80a>`__
-  WHD_WLAN_BADOPTION :
   `whd_types.h <whd__types_8h.html#ae8f8c4c8662920789e013f15ee23f242>`__
-  WHD_WLAN_BADRATESET :
   `whd_types.h <whd__types_8h.html#a904ea4607ee1a485d849ef38118986e0>`__
-  WHD_WLAN_BADSSIDLEN :
   `whd_types.h <whd__types_8h.html#ac3d931142254845104478204c9d1f87f>`__
-  WHD_WLAN_BUFTOOLONG :
   `whd_types.h <whd__types_8h.html#a0be1d9ad73a6e99f93858cc5b62b37cc>`__
-  WHD_WLAN_BUFTOOSHORT :
   `whd_types.h <whd__types_8h.html#a8a8bbb7fc3c39acc127eb738ad8e41e1>`__
-  WHD_WLAN_BUSY :
   `whd_types.h <whd__types_8h.html#ac1e0aee10477b784b711951247bcf799>`__
-  WHD_WLAN_DISABLED :
   `whd_types.h <whd__types_8h.html#ae42dbb304386a4e16435d6fbbf1e5c55>`__
-  WHD_WLAN_EPERM :
   `whd_types.h <whd__types_8h.html#abb313ccaa4a5cb84f6d14affad96a300>`__
-  WHD_WLAN_ERROR :
   `whd_types.h <whd__types_8h.html#ac23d24b8da4c424644151d0893c83a6d>`__
-  WHD_WLAN_INVALID :
   `whd_types.h <whd__types_8h.html#a03251fa1c42118f23bfb5e8377e2ed3d>`__
-  WHD_WLAN_NOBAND :
   `whd_types.h <whd__types_8h.html#ae528496ed94368d53b938f40aa8f089b>`__
-  WHD_WLAN_NOCLK :
   `whd_types.h <whd__types_8h.html#a8a32264cc4534666f4538abf1b3279b3>`__
-  WHD_WLAN_NODEVICE :
   `whd_types.h <whd__types_8h.html#a0d1ab8d1ea6eb43f00a2a7a55a67016f>`__
-  WHD_WLAN_NOFUNCTION :
   `whd_types.h <whd__types_8h.html#a4e2fb5743b973f4d26dfa641ce7a7d16>`__
-  WHD_WLAN_NOMEM :
   `whd_types.h <whd__types_8h.html#a9ed1ef0145feb5c599c0ffe521cf7016>`__
-  WHD_WLAN_NONRESIDENT :
   `whd_types.h <whd__types_8h.html#aecc3e43dc3a59b8d6f452d6a2972c3f8>`__
-  WHD_WLAN_NORESOURCE :
   `whd_types.h <whd__types_8h.html#a7df68783c789fc1ba43a37b4b06e65f5>`__
-  WHD_WLAN_NOT_WME_ASSOCIATION :
   `whd_types.h <whd__types_8h.html#a7a72b67344e174d59d9da91884500c1a>`__
-  WHD_WLAN_NOTAP :
   `whd_types.h <whd__types_8h.html#a5e8feb3e343b95b26599bf953f8511c3>`__
-  WHD_WLAN_NOTASSOCIATED :
   `whd_types.h <whd__types_8h.html#a9b985926cd74ad21525001e31e37dc60>`__
-  WHD_WLAN_NOTBANDLOCKED :
   `whd_types.h <whd__types_8h.html#ada99a8b76a743e8f4d919b111583303d>`__
-  WHD_WLAN_NOTDOWN :
   `whd_types.h <whd__types_8h.html#a73c623ff49884d56e536136962111df4>`__
-  WHD_WLAN_NOTFOUND :
   `whd_types.h <whd__types_8h.html#ae98f0303ab1877e4e4cda0dd803d6383>`__
-  WHD_WLAN_NOTREADY :
   `whd_types.h <whd__types_8h.html#a7811e88b62f2f8b51d6818c05325fda9>`__
-  WHD_WLAN_NOTSTA :
   `whd_types.h <whd__types_8h.html#a13bd8edb5c29960413f87f584a508810>`__
-  WHD_WLAN_NOTUP :
   `whd_types.h <whd__types_8h.html#aca39c37d54a0b1e2fc06ccb347ff9a6d>`__
-  WHD_WLAN_OUTOFRANGECHAN :
   `whd_types.h <whd__types_8h.html#af790269c01083f486ba9e4e6df6bfccb>`__
-  WHD_WLAN_RADIOOFF :
   `whd_types.h <whd__types_8h.html#afb565415b3fc8dc00b2681e202ed16fa>`__
-  WHD_WLAN_RANGE :
   `whd_types.h <whd__types_8h.html#a10597a7175433ccd2c519babf24d50ce>`__
-  WHD_WLAN_RXFAIL :
   `whd_types.h <whd__types_8h.html#aaa963717ba6115b3a909f7162436c919>`__
-  WHD_WLAN_SDIO_ERROR :
   `whd_types.h <whd__types_8h.html#a634bebec2a4dba9371c7c0168cf9c081>`__
-  WHD_WLAN_TSPEC_NOTFOUND :
   `whd_types.h <whd__types_8h.html#a28b3339dce5b5c2044c5875d6902e964>`__
-  WHD_WLAN_TXFAIL :
   `whd_types.h <whd__types_8h.html#afd4b4619382d94a5e5fa9675bd92f802>`__
-  WHD_WLAN_UNFINISHED :
   `whd_types.h <whd__types_8h.html#a4fa6337820d22f0cfa922f27bbf50250>`__
-  WHD_WLAN_UNSUPPORTED :
   `whd_types.h <whd__types_8h.html#ad74bbd2e81cb23c0ecc35fefda6f4478>`__
-  WHD_WLAN_WLAN_DOWN :
   `whd_types.h <whd__types_8h.html#a2ab103bac375d60d8ae2d942b324376e>`__
-  WHD_WLAN_WME_NOT_ENABLED :
   `whd_types.h <whd__types_8h.html#a34acd1ff790f822872ed736945f683ab>`__
-  WHD_WPA_KEYLEN_BAD :
   `whd_types.h <whd__types_8h.html#a379efc2cb11128835c418e615adfcefc>`__
-  WIFI_IE_OUI_LENGTH :
   `whd_types.h <whd__types_8h.html#ae6328bba53e322f1332e7b7b2d7cc128>`__
-  WLAN_ENUM_OFFSET :
   `whd_types.h <whd__types_8h.html#a01539df5d3e26227c354161d9eda58f8>`__
-  WLC_E_ACTION_FRAME :
   `whd_events.h <whd__events_8h.html#a3c6a8365f82bedb7412bebde8311bee4>`__
-  WLC_E_ACTION_FRAME_COMPLETE :
   `whd_events.h <whd__events_8h.html#a5741fe8a095d2c7721fd5126b8acea56>`__
-  WLC_E_ASSOC :
   `whd_events.h <whd__events_8h.html#a8c9fd116781000929a8b09de58f6288e>`__
-  WLC_E_ASSOC_IND :
   `whd_events.h <whd__events_8h.html#adfb692b8b02e3082657cefc92f6f216d>`__
-  WLC_E_AUTH :
   `whd_events.h <whd__events_8h.html#a2e0540e8fe3f44be75575247648e1c7f>`__
-  WLC_E_DEAUTH :
   `whd_events.h <whd__events_8h.html#a04b3c3a873e93afd4a51643707a20120>`__
-  WLC_E_DEAUTH_IND :
   `whd_events.h <whd__events_8h.html#a6fbce5e7ab3f5f4cde0ef8c11ff90e05>`__
-  WLC_E_DISASSOC :
   `whd_events.h <whd__events_8h.html#a9b9413ac981ea6e20b8f0f80523235c0>`__
-  WLC_E_DISASSOC_IND :
   `whd_events.h <whd__events_8h.html#ae9c1ad1200aaa8eec7c96e4fc3438593>`__
-  WLC_E_ESCAN_RESULT :
   `whd_events.h <whd__events_8h.html#a1b04aa17aa56325d7ac03dbf6495d53b>`__
-  WLC_E_LINK :
   `whd_events.h <whd__events_8h.html#adf984d77e19b6e3a023dd27216599456>`__
-  WLC_E_NONE :
   `whd_events.h <whd__events_8h.html#a5608426af7320ce9e54a8a500fc044e9>`__
-  WLC_E_PROBREQ_MSG :
   `whd_events.h <whd__events_8h.html#a26d330cb0796c551edda9c848f1d826c>`__
-  WLC_E_PSK_SUP :
   `whd_events.h <whd__events_8h.html#a73ab299b66cc3915b300b6f9360ae4be>`__
-  WLC_E_REASSOC :
   `whd_events.h <whd__events_8h.html#a87f949345de32ebd84030a1cc9c9159f>`__
-  WLC_E_REASSOC_IND :
   `whd_events.h <whd__events_8h.html#a8ddd3a253d9fd56a5704d9f3b1dd9b8a>`__
-  WLC_E_SET_SSID :
   `whd_events.h <whd__events_8h.html#ae370e84bd9ed3b3cde25919287673298>`__
-  WLC_E_STATUS_11HQUIET :
   `whd_events.h <whd__events_8h.html#a0ff5a599549f180cca6ae50f4c404bb9>`__
-  WLC_E_STATUS_ABORT :
   `whd_events.h <whd__events_8h.html#a7efd6bc08b75e44e74cfad92084020c3>`__
-  WLC_E_STATUS_ATTEMPT :
   `whd_events.h <whd__events_8h.html#ae66d4ccc84bc98c3018b049d865a4e56>`__
-  WLC_E_STATUS_CCXFASTRM :
   `whd_events.h <whd__events_8h.html#ac2937cd5a00b77988fdeb91aa06e0542>`__
-  WLC_E_STATUS_CS_ABORT :
   `whd_events.h <whd__events_8h.html#ad7ce31ac63e0e91bca76c2f0bbc0e92c>`__
-  WLC_E_STATUS_ERROR :
   `whd_events.h <whd__events_8h.html#ab5a63329b542569285c035ec864322b8>`__
-  WLC_E_STATUS_FAIL :
   `whd_events.h <whd__events_8h.html#a7bbe432d919eb9e29cca5bbeff6f2d22>`__
-  WLC_E_STATUS_INVALID :
   `whd_events.h <whd__events_8h.html#ac597fcbff37082363669370113a7628b>`__
-  WLC_E_STATUS_NEWASSOC :
   `whd_events.h <whd__events_8h.html#ac60199a213be6784ece10816f189e6df>`__
-  WLC_E_STATUS_NEWSCAN :
   `whd_events.h <whd__events_8h.html#ace9d1ee71e3e6a1fd0e5c816347cef85>`__
-  WLC_E_STATUS_NO_ACK :
   `whd_events.h <whd__events_8h.html#a88664d38d341c3cc57dfd901f6bb6010>`__
-  WLC_E_STATUS_NO_NETWORKS :
   `whd_events.h <whd__events_8h.html#a3ba3ab746af65eea3d54945c491e75b5>`__
-  WLC_E_STATUS_NOCHANS :
   `whd_events.h <whd__events_8h.html#a8f01683977e56ff6cf81c9dbef1cb1b5>`__
-  WLC_E_STATUS_PARTIAL :
   `whd_events.h <whd__events_8h.html#aec78b245d5a001d341e370f9a246542a>`__
-  WLC_E_STATUS_SUCCESS :
   `whd_events.h <whd__events_8h.html#a36a1db14fab1a7d56c5d9c2a57cf030f>`__
-  WLC_E_STATUS_SUPPRESS :
   `whd_events.h <whd__events_8h.html#a95517d8b374126d53040e871e0430299>`__
-  WLC_E_STATUS_TIMEOUT :
   `whd_events.h <whd__events_8h.html#a59959134916adbb98a9875a8c7462474>`__
-  WLC_E_STATUS_UNSOLICITED :
   `whd_events.h <whd__events_8h.html#a5a7e4c4f047fff0443e0e7037e35babe>`__
-  WLC_SUP_STATUS_OFFSET :
   `whd_events.h <whd__events_8h.html#ac97c29fe35bab5af9cc08a9eb1fedffc>`__
-  WPA2_SECURITY :
   `whd_types.h <whd__types_8h.html#a8875737a0403d2136a69bbc96401cccf>`__
-  WPA3_SECURITY :
   `whd_types.h <whd__types_8h.html#ad175824d1581f69f5a725e4c9171aa79>`__
-  WPA_SECURITY :
   `whd_types.h <whd__types_8h.html#aa7ccd472bacbd8ee01f31f0f0e2ce3dc>`__
-  WPS_ENABLED :
   `whd_types.h <whd__types_8h.html#aaae7e8a0eb357cae8f130f9099d8e7b8>`__

