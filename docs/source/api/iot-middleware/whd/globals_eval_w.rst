===
w
===
 
.. rubric:: - w -
   :name: w--

-  WHD_802_11_BAND_2_4GHZ :
   `whd_types.h <whd__types_8h.html#aa57cbd1826f7022123be8ec0ac352d85ad83834fecf6a59269c7bdbc02bf0ce2f>`__
-  WHD_802_11_BAND_5GHZ :
   `whd_types.h <whd__types_8h.html#aa57cbd1826f7022123be8ec0ac352d85a0af1540533ecaf4c2511d1953c766119>`__
-  WHD_ADD_CUSTOM_IE :
   `whd_types.h <whd__types_8h.html#a8be1026494a86f0ceeebb2dcbf092cbda79a530656dc2116ab992378d57256869>`__
-  WHD_BSS_TYPE_ADHOC :
   `whd_types.h <whd__types_8h.html#a3070614a06a8989b9eb207e9f1286c5fac4c317dde2a56f56571b0258e6038675>`__
-  WHD_BSS_TYPE_ANY :
   `whd_types.h <whd__types_8h.html#a3070614a06a8989b9eb207e9f1286c5faeae98beecab71ed2434077afc97721d5>`__
-  WHD_BSS_TYPE_INFRASTRUCTURE :
   `whd_types.h <whd__types_8h.html#a3070614a06a8989b9eb207e9f1286c5fa7caa0abe03eadaee7aa958827e2d5365>`__
-  WHD_BSS_TYPE_MESH :
   `whd_types.h <whd__types_8h.html#a3070614a06a8989b9eb207e9f1286c5faa32f92242f0ac074d88260eb2fa76a2f>`__
-  WHD_BSS_TYPE_UNKNOWN :
   `whd_types.h <whd__types_8h.html#a3070614a06a8989b9eb207e9f1286c5fa6711a29485a893344ddebf8fa099ec48>`__
-  WHD_COUNTRY_AFGHANISTAN :
   `whd_types.h <whd__types_8h.html#a5adb0953a8527552bc47001673830602ae770f53071110d4c62c45f3185212a2c>`__
-  WHD_COUNTRY_ALBANIA :
   `whd_types.h <whd__types_8h.html#a5adb0953a8527552bc47001673830602ad6f4894e386515835c1754f164e9bcc6>`__
-  WHD_COUNTRY_ALGERIA :
   `whd_types.h <whd__types_8h.html#a5adb0953a8527552bc47001673830602aa62c554f0e94ff63ef545d067a8f6fd4>`__
-  WHD_COUNTRY_AMERICAN_SAMOA :
   `whd_types.h <whd__types_8h.html#a5adb0953a8527552bc47001673830602af16efa365354075d9f24cd783bd3b692>`__
-  WHD_COUNTRY_ANGOLA :
   `whd_types.h <whd__types_8h.html#a5adb0953a8527552bc47001673830602a563ec781c9ac04774bd120cdb03213b3>`__
-  WHD_COUNTRY_ANGUILLA :
   `whd_types.h <whd__types_8h.html#a5adb0953a8527552bc47001673830602aeb5d277f1549df165d7b220151347e93>`__
-  WHD_COUNTRY_ANTIGUA_AND_BARBUDA :
   `whd_types.h <whd__types_8h.html#a5adb0953a8527552bc47001673830602a75088930532c4c414b7ebe2207fd39c1>`__
-  WHD_COUNTRY_ARGENTINA :
   `whd_types.h <whd__types_8h.html#a5adb0953a8527552bc47001673830602aa9086183077304caa5c0997b083aa09f>`__
-  WHD_COUNTRY_ARMENIA :
   `whd_types.h <whd__types_8h.html#a5adb0953a8527552bc47001673830602a1ca3dac8fb93d58c216e94d87562fe0c>`__
-  WHD_COUNTRY_ARUBA :
   `whd_types.h <whd__types_8h.html#a5adb0953a8527552bc47001673830602afa17325e3a4447caf37db22e4d8dbd35>`__
-  WHD_COUNTRY_AUSTRALIA :
   `whd_types.h <whd__types_8h.html#a5adb0953a8527552bc47001673830602aebbf99f30ed79facff3fdee81fccb539>`__
-  WHD_COUNTRY_AUSTRIA :
   `whd_types.h <whd__types_8h.html#a5adb0953a8527552bc47001673830602af82950fa63c8d479999335fe1dd81d20>`__
-  WHD_COUNTRY_AZERBAIJAN :
   `whd_types.h <whd__types_8h.html#a5adb0953a8527552bc47001673830602affa44f6b56c1b7a070185fe01040e758>`__
-  WHD_COUNTRY_BAHAMAS :
   `whd_types.h <whd__types_8h.html#a5adb0953a8527552bc47001673830602ac6076e03149c89cc2bcd76c4e05dbc4d>`__
-  WHD_COUNTRY_BAHRAIN :
   `whd_types.h <whd__types_8h.html#a5adb0953a8527552bc47001673830602ad32bdf453acfa3bef198aedfa190565b>`__
-  WHD_COUNTRY_BAKER_ISLAND :
   `whd_types.h <whd__types_8h.html#a5adb0953a8527552bc47001673830602a6223c070c73257a38b1deec92a246245>`__
-  WHD_COUNTRY_BANGLADESH :
   `whd_types.h <whd__types_8h.html#a5adb0953a8527552bc47001673830602abaab712c7d16e5675e302e702ed9e09a>`__
-  WHD_COUNTRY_BARBADOS :
   `whd_types.h <whd__types_8h.html#a5adb0953a8527552bc47001673830602a2191b0d5b008ee5b736722a57bded49d>`__
-  WHD_COUNTRY_BELARUS :
   `whd_types.h <whd__types_8h.html#a5adb0953a8527552bc47001673830602adbde897af63475f3377fde09ebdf18ae>`__
-  WHD_COUNTRY_BELGIUM :
   `whd_types.h <whd__types_8h.html#a5adb0953a8527552bc47001673830602ae2d2eec5fc2a62bdd7e5423df889d70c>`__
-  WHD_COUNTRY_BELIZE :
   `whd_types.h <whd__types_8h.html#a5adb0953a8527552bc47001673830602ab08aca5d4ac515c418393cf8b105c1fa>`__
-  WHD_COUNTRY_BENIN :
   `whd_types.h <whd__types_8h.html#a5adb0953a8527552bc47001673830602a8b5718faa1aa137578cac467fd146c98>`__
-  WHD_COUNTRY_BERMUDA :
   `whd_types.h <whd__types_8h.html#a5adb0953a8527552bc47001673830602a7c7bda7e8a07ebb79e29856559fccd2e>`__
-  WHD_COUNTRY_BHUTAN :
   `whd_types.h <whd__types_8h.html#a5adb0953a8527552bc47001673830602abc34c698a3a5368366885bcb628afb96>`__
-  WHD_COUNTRY_BOLIVIA :
   `whd_types.h <whd__types_8h.html#a5adb0953a8527552bc47001673830602ae2ed98e109b1f8cd9e09c94d53ec2a87>`__
-  WHD_COUNTRY_BOSNIA_AND_HERZEGOVINA :
   `whd_types.h <whd__types_8h.html#a5adb0953a8527552bc47001673830602a1e8a3ee4ffdf908b2537918b60627e35>`__
-  WHD_COUNTRY_BOTSWANA :
   `whd_types.h <whd__types_8h.html#a5adb0953a8527552bc47001673830602a8f31c5978a56922f8f15273a057ecc7b>`__
-  WHD_COUNTRY_BRAZIL :
   `whd_types.h <whd__types_8h.html#a5adb0953a8527552bc47001673830602ad991ba1abd3a98b073b1ccc426d8bb51>`__
-  WHD_COUNTRY_BRITISH_INDIAN_OCEAN_TERRITORY :
   `whd_types.h <whd__types_8h.html#a5adb0953a8527552bc47001673830602a75e7187a26194e37529b76b424874f5b>`__
-  WHD_COUNTRY_BRUNEI_DARUSSALAM :
   `whd_types.h <whd__types_8h.html#a5adb0953a8527552bc47001673830602ad58d8ffd6db58aa29f6c0fbee553e81b>`__
-  WHD_COUNTRY_BULGARIA :
   `whd_types.h <whd__types_8h.html#a5adb0953a8527552bc47001673830602a801c4ec653ee9bd12fb1becbe2a394c0>`__
-  WHD_COUNTRY_BURKINA_FASO :
   `whd_types.h <whd__types_8h.html#a5adb0953a8527552bc47001673830602af06898be718ec7dca62de26b7900c8fd>`__
-  WHD_COUNTRY_BURUNDI :
   `whd_types.h <whd__types_8h.html#a5adb0953a8527552bc47001673830602a66177cc6dcd277ba8c2c694502180dc7>`__
-  WHD_COUNTRY_CAMBODIA :
   `whd_types.h <whd__types_8h.html#a5adb0953a8527552bc47001673830602a516f67438861b15a55e22d46235e8e69>`__
-  WHD_COUNTRY_CAMEROON :
   `whd_types.h <whd__types_8h.html#a5adb0953a8527552bc47001673830602af61ade757f15f4d2999d45e5ea2a7baa>`__
-  WHD_COUNTRY_CANADA :
   `whd_types.h <whd__types_8h.html#a5adb0953a8527552bc47001673830602ae73dbffc9994566b8811401bb7b50cab>`__
-  WHD_COUNTRY_CANADA_REV950 :
   `whd_types.h <whd__types_8h.html#a5adb0953a8527552bc47001673830602a86b6fed33793c1b9295aeb75cb51431d>`__
-  WHD_COUNTRY_CAPE_VERDE :
   `whd_types.h <whd__types_8h.html#a5adb0953a8527552bc47001673830602a4e952962e2677ca1ec9014f469bc9924>`__
-  WHD_COUNTRY_CAYMAN_ISLANDS :
   `whd_types.h <whd__types_8h.html#a5adb0953a8527552bc47001673830602a6132980ec6e1d1964aa5e9187961acc5>`__
-  WHD_COUNTRY_CENTRAL_AFRICAN_REPUBLIC :
   `whd_types.h <whd__types_8h.html#a5adb0953a8527552bc47001673830602a1b053b731226015cd1f7a435e232abe1>`__
-  WHD_COUNTRY_CHAD :
   `whd_types.h <whd__types_8h.html#a5adb0953a8527552bc47001673830602a015236ede99e9f4b5e1075c9e03e3e0a>`__
-  WHD_COUNTRY_CHILE :
   `whd_types.h <whd__types_8h.html#a5adb0953a8527552bc47001673830602a19b99f0a9db2c89518ddd5635d7fa100>`__
-  WHD_COUNTRY_CHINA :
   `whd_types.h <whd__types_8h.html#a5adb0953a8527552bc47001673830602a5ed5b8e7c2c1f819e3845d35c4e20e84>`__
-  WHD_COUNTRY_CHRISTMAS_ISLAND :
   `whd_types.h <whd__types_8h.html#a5adb0953a8527552bc47001673830602aecfe598cebf2b72d3cfe77fb5b999244>`__
-  WHD_COUNTRY_COLOMBIA :
   `whd_types.h <whd__types_8h.html#a5adb0953a8527552bc47001673830602a34e4e66cb208eaad502aff27339c5e1e>`__
-  WHD_COUNTRY_COMOROS :
   `whd_types.h <whd__types_8h.html#a5adb0953a8527552bc47001673830602ae3bc50d4684d980480c8e77f5cddbf22>`__
-  WHD_COUNTRY_CONGO :
   `whd_types.h <whd__types_8h.html#a5adb0953a8527552bc47001673830602ac003ca4c4b6ff3363414a5af32d3708f>`__
-  WHD_COUNTRY_CONGO_THE_DEMOCRATIC_REPUBLIC_OF_THE :
   `whd_types.h <whd__types_8h.html#a5adb0953a8527552bc47001673830602a85e2ad1d9cb9db255046ec24e173d23e>`__
-  WHD_COUNTRY_COSTA_RICA :
   `whd_types.h <whd__types_8h.html#a5adb0953a8527552bc47001673830602a726a776bbd473697c7d712c77fec4641>`__
-  WHD_COUNTRY_COTE_DIVOIRE :
   `whd_types.h <whd__types_8h.html#a5adb0953a8527552bc47001673830602ad093bb4e8acab3f68409bd3e1880f08e>`__
-  WHD_COUNTRY_CROATIA :
   `whd_types.h <whd__types_8h.html#a5adb0953a8527552bc47001673830602afe6c1511f2547395ce73ed2ee5be0775>`__
-  WHD_COUNTRY_CUBA :
   `whd_types.h <whd__types_8h.html#a5adb0953a8527552bc47001673830602ad2c7ee32538bd6cfa74c95759530ca3c>`__
-  WHD_COUNTRY_CYPRUS :
   `whd_types.h <whd__types_8h.html#a5adb0953a8527552bc47001673830602a1de20aee06c515b04d36a6deccc21fd0>`__
-  WHD_COUNTRY_CZECH_REPUBLIC :
   `whd_types.h <whd__types_8h.html#a5adb0953a8527552bc47001673830602a3625aa03434d5542977a73e76562fbb9>`__
-  WHD_COUNTRY_DENMARK :
   `whd_types.h <whd__types_8h.html#a5adb0953a8527552bc47001673830602a123e23a550e7a2090f6778e209df88b0>`__
-  WHD_COUNTRY_DJIBOUTI :
   `whd_types.h <whd__types_8h.html#a5adb0953a8527552bc47001673830602a6966612a794d39ad699302b447e6d286>`__
-  WHD_COUNTRY_DOMINICA :
   `whd_types.h <whd__types_8h.html#a5adb0953a8527552bc47001673830602ad80f276af148804fe969d9cfc30afca8>`__
-  WHD_COUNTRY_DOMINICAN_REPUBLIC :
   `whd_types.h <whd__types_8h.html#a5adb0953a8527552bc47001673830602a97e88a10f482d24184d47fd1c8505f7c>`__
-  WHD_COUNTRY_DOWN_UNDER :
   `whd_types.h <whd__types_8h.html#a5adb0953a8527552bc47001673830602a84eec6d380810cde17e1ed34878ccf6d>`__
-  WHD_COUNTRY_ECUADOR :
   `whd_types.h <whd__types_8h.html#a5adb0953a8527552bc47001673830602ae3f3dbde00549fab0b967a026488f6c3>`__
-  WHD_COUNTRY_EGYPT :
   `whd_types.h <whd__types_8h.html#a5adb0953a8527552bc47001673830602a3b16a609ff768c913a88797441df6107>`__
-  WHD_COUNTRY_EL_SALVADOR :
   `whd_types.h <whd__types_8h.html#a5adb0953a8527552bc47001673830602aba94b6ef5f7bcbdc0a196e5c3568162f>`__
-  WHD_COUNTRY_EQUATORIAL_GUINEA :
   `whd_types.h <whd__types_8h.html#a5adb0953a8527552bc47001673830602a6dd76befbc9318e3506cc44bf08cb8d1>`__
-  WHD_COUNTRY_ERITREA :
   `whd_types.h <whd__types_8h.html#a5adb0953a8527552bc47001673830602a5ab25febbbe2582c9ad218f869badcc3>`__
-  WHD_COUNTRY_ESTONIA :
   `whd_types.h <whd__types_8h.html#a5adb0953a8527552bc47001673830602a221e5b5eebcc3071540f6e6fdd692f30>`__
-  WHD_COUNTRY_ETHIOPIA :
   `whd_types.h <whd__types_8h.html#a5adb0953a8527552bc47001673830602af716cd38b781b1098ca4c2770d8c69c2>`__
-  WHD_COUNTRY_EUROPEAN_WIDE_REV895 :
   `whd_types.h <whd__types_8h.html#a5adb0953a8527552bc47001673830602a557dc78eb4d648cf1bbdfe588e2f2912>`__
-  WHD_COUNTRY_FALKLAND_ISLANDS_MALVINAS :
   `whd_types.h <whd__types_8h.html#a5adb0953a8527552bc47001673830602a733054fabf650944bd38e92f280a044b>`__
-  WHD_COUNTRY_FAROE_ISLANDS :
   `whd_types.h <whd__types_8h.html#a5adb0953a8527552bc47001673830602aa83846c28f7996a7b182499c7efd271e>`__
-  WHD_COUNTRY_FIJI :
   `whd_types.h <whd__types_8h.html#a5adb0953a8527552bc47001673830602a0fa63a76100a3a47a94577c07c4fc766>`__
-  WHD_COUNTRY_FINLAND :
   `whd_types.h <whd__types_8h.html#a5adb0953a8527552bc47001673830602ae356b11ca5a6bddbc7a0c905803308b1>`__
-  WHD_COUNTRY_FRANCE :
   `whd_types.h <whd__types_8h.html#a5adb0953a8527552bc47001673830602a747f19b0963ae45c5c344b90f0be09ea>`__
-  WHD_COUNTRY_FRENCH_GUINA :
   `whd_types.h <whd__types_8h.html#a5adb0953a8527552bc47001673830602aef39f8df3efe46b64764eefb3267ae36>`__
-  WHD_COUNTRY_FRENCH_POLYNESIA :
   `whd_types.h <whd__types_8h.html#a5adb0953a8527552bc47001673830602a03b5d72ec6b661e620591f22bae08f4a>`__
-  WHD_COUNTRY_FRENCH_SOUTHERN_TERRITORIES :
   `whd_types.h <whd__types_8h.html#a5adb0953a8527552bc47001673830602abf314a956f934d7795fca3c84bcd21e0>`__
-  WHD_COUNTRY_GABON :
   `whd_types.h <whd__types_8h.html#a5adb0953a8527552bc47001673830602acb40ac85e59a92fea17ea304d17efff9>`__
-  WHD_COUNTRY_GAMBIA :
   `whd_types.h <whd__types_8h.html#a5adb0953a8527552bc47001673830602ae6800f8e2ad045af81da3c898f6497ce>`__
-  WHD_COUNTRY_GEORGIA :
   `whd_types.h <whd__types_8h.html#a5adb0953a8527552bc47001673830602a5c2ad817523941a21074a3e377fa480d>`__
-  WHD_COUNTRY_GERMANY :
   `whd_types.h <whd__types_8h.html#a5adb0953a8527552bc47001673830602a7791d7ac26d9d4343c0496c2e159190b>`__
-  WHD_COUNTRY_GHANA :
   `whd_types.h <whd__types_8h.html#a5adb0953a8527552bc47001673830602ac6ea83c4e447e3521ac8d8e5d898c200>`__
-  WHD_COUNTRY_GIBRALTAR :
   `whd_types.h <whd__types_8h.html#a5adb0953a8527552bc47001673830602a557f77ffbd0cf43e84cf85121e990fed>`__
-  WHD_COUNTRY_GREECE :
   `whd_types.h <whd__types_8h.html#a5adb0953a8527552bc47001673830602ad26b379054843ecf7e70127b29a58976>`__
-  WHD_COUNTRY_GRENADA :
   `whd_types.h <whd__types_8h.html#a5adb0953a8527552bc47001673830602a35f98e8e0ca205e9a7060ce475a2aa40>`__
-  WHD_COUNTRY_GUADELOUPE :
   `whd_types.h <whd__types_8h.html#a5adb0953a8527552bc47001673830602a6706d8c6d48a30baf6f96207bfb04efd>`__
-  WHD_COUNTRY_GUAM :
   `whd_types.h <whd__types_8h.html#a5adb0953a8527552bc47001673830602a98ef170fc691d938ad04b7df7e584a01>`__
-  WHD_COUNTRY_GUATEMALA :
   `whd_types.h <whd__types_8h.html#a5adb0953a8527552bc47001673830602a0b9d8febff8bd8b6f4617ccd21a805eb>`__
-  WHD_COUNTRY_GUERNSEY :
   `whd_types.h <whd__types_8h.html#a5adb0953a8527552bc47001673830602a9b7ec6307cd5ef266db43bb71cb7f128>`__
-  WHD_COUNTRY_GUINEA :
   `whd_types.h <whd__types_8h.html#a5adb0953a8527552bc47001673830602aa73ce4ffc17a7591a371155e4d4b6109>`__
-  WHD_COUNTRY_GUINEA_BISSAU :
   `whd_types.h <whd__types_8h.html#a5adb0953a8527552bc47001673830602ac0ec790014f14166e36698205dfba57e>`__
-  WHD_COUNTRY_GUYANA :
   `whd_types.h <whd__types_8h.html#a5adb0953a8527552bc47001673830602ac8afd8e2064f40716297eb88a12edadb>`__
-  WHD_COUNTRY_HAITI :
   `whd_types.h <whd__types_8h.html#a5adb0953a8527552bc47001673830602aaf070b0358c343df358068652afcce68>`__
-  WHD_COUNTRY_HOLY_SEE_VATICAN_CITY_STATE :
   `whd_types.h <whd__types_8h.html#a5adb0953a8527552bc47001673830602aea3bf67e3565999f055885608c7c4daf>`__
-  WHD_COUNTRY_HONDURAS :
   `whd_types.h <whd__types_8h.html#a5adb0953a8527552bc47001673830602a21e299a7a875490886684f9e4dba7576>`__
-  WHD_COUNTRY_HONG_KONG :
   `whd_types.h <whd__types_8h.html#a5adb0953a8527552bc47001673830602a8f05d8daae41dacec62c08cd35d38706>`__
-  WHD_COUNTRY_HUNGARY :
   `whd_types.h <whd__types_8h.html#a5adb0953a8527552bc47001673830602a41dc8df1b22373ccefcfffac5fb922e0>`__
-  WHD_COUNTRY_ICELAND :
   `whd_types.h <whd__types_8h.html#a5adb0953a8527552bc47001673830602aad262a8e2fc5122082aec4edb2fd14d5>`__
-  WHD_COUNTRY_INDIA :
   `whd_types.h <whd__types_8h.html#a5adb0953a8527552bc47001673830602af465e8755d5762ca58ba23e2bf1f533f>`__
-  WHD_COUNTRY_INDONESIA :
   `whd_types.h <whd__types_8h.html#a5adb0953a8527552bc47001673830602abdf6018376c85284c09ffed41e5dd9ca>`__
-  WHD_COUNTRY_IRAN_ISLAMIC_REPUBLIC_OF :
   `whd_types.h <whd__types_8h.html#a5adb0953a8527552bc47001673830602ae4eecfdb70f4da9290fa8f6b0eb834c2>`__
-  WHD_COUNTRY_IRAQ :
   `whd_types.h <whd__types_8h.html#a5adb0953a8527552bc47001673830602a294d8250b0b00730df005a1cc005c7f6>`__
-  WHD_COUNTRY_IRELAND :
   `whd_types.h <whd__types_8h.html#a5adb0953a8527552bc47001673830602a5e5fe6acc6fa80f06e269b673b943e20>`__
-  WHD_COUNTRY_ISRAEL :
   `whd_types.h <whd__types_8h.html#a5adb0953a8527552bc47001673830602a3b2c184733b592037f45faad93efa8cc>`__
-  WHD_COUNTRY_ITALY :
   `whd_types.h <whd__types_8h.html#a5adb0953a8527552bc47001673830602a9e18703f874e4d1536686ae5852114e2>`__
-  WHD_COUNTRY_JAMAICA :
   `whd_types.h <whd__types_8h.html#a5adb0953a8527552bc47001673830602af35909c9e6d81e88969fd94ea556f2ce>`__
-  WHD_COUNTRY_JAPAN :
   `whd_types.h <whd__types_8h.html#a5adb0953a8527552bc47001673830602ae0b960378ac4240b1687552a2a79bea9>`__
-  WHD_COUNTRY_JERSEY :
   `whd_types.h <whd__types_8h.html#a5adb0953a8527552bc47001673830602a72c36ec2c352536afb89aaa3b1406391>`__
-  WHD_COUNTRY_JORDAN :
   `whd_types.h <whd__types_8h.html#a5adb0953a8527552bc47001673830602ac52c278360c46c8fb831604c3333adf6>`__
-  WHD_COUNTRY_KAZAKHSTAN :
   `whd_types.h <whd__types_8h.html#a5adb0953a8527552bc47001673830602af33a3ab91c5fd1285254748d5a809f68>`__
-  WHD_COUNTRY_KENYA :
   `whd_types.h <whd__types_8h.html#a5adb0953a8527552bc47001673830602a208d456b89f5fbc1c2da13b3ada26291>`__
-  WHD_COUNTRY_KIRIBATI :
   `whd_types.h <whd__types_8h.html#a5adb0953a8527552bc47001673830602a36804c3a53fc28ada67114eecf9e11d7>`__
-  WHD_COUNTRY_KOREA_REPUBLIC_OF :
   `whd_types.h <whd__types_8h.html#a5adb0953a8527552bc47001673830602ae545ebb8fee76591be2faada7da6bdac>`__
-  WHD_COUNTRY_KOSOVO :
   `whd_types.h <whd__types_8h.html#a5adb0953a8527552bc47001673830602a228d180b77b08c9bdcc42dff20c70cdf>`__
-  WHD_COUNTRY_KUWAIT :
   `whd_types.h <whd__types_8h.html#a5adb0953a8527552bc47001673830602aee9c8a1ad5b822ff69c51a8566a08c24>`__
-  WHD_COUNTRY_KYRGYZSTAN :
   `whd_types.h <whd__types_8h.html#a5adb0953a8527552bc47001673830602ac3abc7083fa3c72445a9f41944d2864a>`__
-  WHD_COUNTRY_LAO_PEOPLES_DEMOCRATIC_REPUBIC :
   `whd_types.h <whd__types_8h.html#a5adb0953a8527552bc47001673830602a1c3b3f7fd37f28f55ac4b6a447bb7003>`__
-  WHD_COUNTRY_LATVIA :
   `whd_types.h <whd__types_8h.html#a5adb0953a8527552bc47001673830602ab8f7eb7a7adad5c463f41159749350d3>`__
-  WHD_COUNTRY_LEBANON :
   `whd_types.h <whd__types_8h.html#a5adb0953a8527552bc47001673830602a519a0d9341a05a24f93e5ca13b0d3d92>`__
-  WHD_COUNTRY_LESOTHO :
   `whd_types.h <whd__types_8h.html#a5adb0953a8527552bc47001673830602a8d2cc9cb1e8a9d86ca261da4de20d8a2>`__
-  WHD_COUNTRY_LIBERIA :
   `whd_types.h <whd__types_8h.html#a5adb0953a8527552bc47001673830602a148c223dec8f4b7f24c6c84ff679a5e3>`__
-  WHD_COUNTRY_LIBYAN_ARAB_JAMAHIRIYA :
   `whd_types.h <whd__types_8h.html#a5adb0953a8527552bc47001673830602a084316071a4390d666555196182e4cdd>`__
-  WHD_COUNTRY_LIECHTENSTEIN :
   `whd_types.h <whd__types_8h.html#a5adb0953a8527552bc47001673830602a7106096dfeaa18acc3bc56aac6b4db4d>`__
-  WHD_COUNTRY_LITHUANIA :
   `whd_types.h <whd__types_8h.html#a5adb0953a8527552bc47001673830602a9946d1d75b70b438d0de9571303b0a96>`__
-  WHD_COUNTRY_LUXEMBOURG :
   `whd_types.h <whd__types_8h.html#a5adb0953a8527552bc47001673830602a22c88aec1158e2a34abb255170bf1245>`__
-  WHD_COUNTRY_MACAO :
   `whd_types.h <whd__types_8h.html#a5adb0953a8527552bc47001673830602aa6d065b2a35b3b3fe4547b04b97e8f1d>`__
-  WHD_COUNTRY_MACEDONIA_FORMER_YUGOSLAV_REPUBLIC_OF :
   `whd_types.h <whd__types_8h.html#a5adb0953a8527552bc47001673830602acfc2f020b3295cd704cfa2ff2a667eee>`__
-  WHD_COUNTRY_MADAGASCAR :
   `whd_types.h <whd__types_8h.html#a5adb0953a8527552bc47001673830602aa5d0401f6610b50f594272a66ead46e3>`__
-  WHD_COUNTRY_MALAWI :
   `whd_types.h <whd__types_8h.html#a5adb0953a8527552bc47001673830602ace2810c6a0a379bea125a63a0b8fa0af>`__
-  WHD_COUNTRY_MALAYSIA :
   `whd_types.h <whd__types_8h.html#a5adb0953a8527552bc47001673830602a82d471d6333b18fd0d290a8b375033e2>`__
-  WHD_COUNTRY_MALDIVES :
   `whd_types.h <whd__types_8h.html#a5adb0953a8527552bc47001673830602ab965a8aeb2862f58fb27eeaa67f3d04f>`__
-  WHD_COUNTRY_MALI :
   `whd_types.h <whd__types_8h.html#a5adb0953a8527552bc47001673830602abfbb05227a672d8d3fecf071fe427161>`__
-  WHD_COUNTRY_MALTA :
   `whd_types.h <whd__types_8h.html#a5adb0953a8527552bc47001673830602acfb6daa860d8e5cd60801237bf25aedf>`__
-  WHD_COUNTRY_MAN_ISLE_OF :
   `whd_types.h <whd__types_8h.html#a5adb0953a8527552bc47001673830602a869f7c0d7efda7142f47a7e88bd5e344>`__
-  WHD_COUNTRY_MARTINIQUE :
   `whd_types.h <whd__types_8h.html#a5adb0953a8527552bc47001673830602a076de94a02a472d0fd8162f48921c018>`__
-  WHD_COUNTRY_MAURITANIA :
   `whd_types.h <whd__types_8h.html#a5adb0953a8527552bc47001673830602a5d9d1c3cd6cd3aa6206d756a1c915c81>`__
-  WHD_COUNTRY_MAURITIUS :
   `whd_types.h <whd__types_8h.html#a5adb0953a8527552bc47001673830602aa5178f7f44391b25f409a75b37cd0cdc>`__
-  WHD_COUNTRY_MAYOTTE :
   `whd_types.h <whd__types_8h.html#a5adb0953a8527552bc47001673830602a96454a25ed1b69b7f0f0d92601ed9987>`__
-  WHD_COUNTRY_MEXICO :
   `whd_types.h <whd__types_8h.html#a5adb0953a8527552bc47001673830602a95eef4bf4935992c4297f8d5bee6dbae>`__
-  WHD_COUNTRY_MICRONESIA_FEDERATED_STATES_OF :
   `whd_types.h <whd__types_8h.html#a5adb0953a8527552bc47001673830602ad60eb0ac67cf8cc0881c9170f7eb80d8>`__
-  WHD_COUNTRY_MOLDOVA_REPUBLIC_OF :
   `whd_types.h <whd__types_8h.html#a5adb0953a8527552bc47001673830602ad2e6f80c3b8389267daf5573df4779d2>`__
-  WHD_COUNTRY_MONACO :
   `whd_types.h <whd__types_8h.html#a5adb0953a8527552bc47001673830602a18059d3339f927a34115b35520162c26>`__
-  WHD_COUNTRY_MONGOLIA :
   `whd_types.h <whd__types_8h.html#a5adb0953a8527552bc47001673830602a2d81e9ec568f1df7a72d2d23eb23f258>`__
-  WHD_COUNTRY_MONTENEGRO :
   `whd_types.h <whd__types_8h.html#a5adb0953a8527552bc47001673830602a39adf99ed1f2478a0739346f1d222a7c>`__
-  WHD_COUNTRY_MONTSERRAT :
   `whd_types.h <whd__types_8h.html#a5adb0953a8527552bc47001673830602ac637c256031eecbb8a885af1d4a47fb3>`__
-  WHD_COUNTRY_MOROCCO :
   `whd_types.h <whd__types_8h.html#a5adb0953a8527552bc47001673830602a2ed09927575bce96192c493fba569987>`__
-  WHD_COUNTRY_MOZAMBIQUE :
   `whd_types.h <whd__types_8h.html#a5adb0953a8527552bc47001673830602a77be84b525fc558598d9fd5ba5b81706>`__
-  WHD_COUNTRY_MYANMAR :
   `whd_types.h <whd__types_8h.html#a5adb0953a8527552bc47001673830602a194bf205760d740770b47f1e2dbb1782>`__
-  WHD_COUNTRY_NAMIBIA :
   `whd_types.h <whd__types_8h.html#a5adb0953a8527552bc47001673830602a8ea6a47eca07425b9418899dd083801c>`__
-  WHD_COUNTRY_NAURU :
   `whd_types.h <whd__types_8h.html#a5adb0953a8527552bc47001673830602a674644e323d1b20022c50686b21d67f4>`__
-  WHD_COUNTRY_NEPAL :
   `whd_types.h <whd__types_8h.html#a5adb0953a8527552bc47001673830602a87c031d1c96980cdbbc4e1df6af14b45>`__
-  WHD_COUNTRY_NETHERLANDS :
   `whd_types.h <whd__types_8h.html#a5adb0953a8527552bc47001673830602ab88b8fb0927f56275ad1dbc1263bd7f2>`__
-  WHD_COUNTRY_NETHERLANDS_ANTILLES :
   `whd_types.h <whd__types_8h.html#a5adb0953a8527552bc47001673830602acc1a6ae1a250255331d3566a8ec1d67c>`__
-  WHD_COUNTRY_NEW_CALEDONIA :
   `whd_types.h <whd__types_8h.html#a5adb0953a8527552bc47001673830602a84403d618acd04b681e0c749e7ffacb2>`__
-  WHD_COUNTRY_NEW_ZEALAND :
   `whd_types.h <whd__types_8h.html#a5adb0953a8527552bc47001673830602a1c1aee2de9eaffcdfdfbe6c1f78d0637>`__
-  WHD_COUNTRY_NICARAGUA :
   `whd_types.h <whd__types_8h.html#a5adb0953a8527552bc47001673830602a43dfe0f1a2e6638f1275bfd973f7a389>`__
-  WHD_COUNTRY_NIGER :
   `whd_types.h <whd__types_8h.html#a5adb0953a8527552bc47001673830602aabe3b5667f6a0bd781a8ba2d9430ca01>`__
-  WHD_COUNTRY_NIGERIA :
   `whd_types.h <whd__types_8h.html#a5adb0953a8527552bc47001673830602a442808483cd3fdb7c928cb428e92080e>`__
-  WHD_COUNTRY_NORFOLK_ISLAND :
   `whd_types.h <whd__types_8h.html#a5adb0953a8527552bc47001673830602a1623028336473099ee7a101ec8a521b5>`__
-  WHD_COUNTRY_NORTHERN_MARIANA_ISLANDS :
   `whd_types.h <whd__types_8h.html#a5adb0953a8527552bc47001673830602aab8168ab9ea38a7873e32eed16e3e8b3>`__
-  WHD_COUNTRY_NORWAY :
   `whd_types.h <whd__types_8h.html#a5adb0953a8527552bc47001673830602ab3e3b29686764f32fbe45a7246e00953>`__
-  WHD_COUNTRY_OMAN :
   `whd_types.h <whd__types_8h.html#a5adb0953a8527552bc47001673830602a99c85200419992bd4eeb3d244d6ac631>`__
-  WHD_COUNTRY_PAKISTAN :
   `whd_types.h <whd__types_8h.html#a5adb0953a8527552bc47001673830602a4580c23e0c842b0ab42f390f301c8079>`__
-  WHD_COUNTRY_PALAU :
   `whd_types.h <whd__types_8h.html#a5adb0953a8527552bc47001673830602addfc3a89a81a980fb05ac51c57d6df16>`__
-  WHD_COUNTRY_PANAMA :
   `whd_types.h <whd__types_8h.html#a5adb0953a8527552bc47001673830602aba97b764cd570b1592a685270463ab86>`__
-  WHD_COUNTRY_PAPUA_NEW_GUINEA :
   `whd_types.h <whd__types_8h.html#a5adb0953a8527552bc47001673830602a6a9a094f05b9cee9b5033a2d815866c2>`__
-  WHD_COUNTRY_PARAGUAY :
   `whd_types.h <whd__types_8h.html#a5adb0953a8527552bc47001673830602a877c185178c8c8a4a14fffad2ea352b9>`__
-  WHD_COUNTRY_PERU :
   `whd_types.h <whd__types_8h.html#a5adb0953a8527552bc47001673830602a4811b46e2cedb4b3707cbbb7f9d373e7>`__
-  WHD_COUNTRY_PHILIPPINES :
   `whd_types.h <whd__types_8h.html#a5adb0953a8527552bc47001673830602a9ff1e9fdba8d5e35a8e5447cbeb587fd>`__
-  WHD_COUNTRY_POLAND :
   `whd_types.h <whd__types_8h.html#a5adb0953a8527552bc47001673830602ace120d194662e88e238af89d55f28fc3>`__
-  WHD_COUNTRY_PORTUGAL :
   `whd_types.h <whd__types_8h.html#a5adb0953a8527552bc47001673830602ae21bd728e2fa7610b73c3e7a5e8c3020>`__
-  WHD_COUNTRY_PUETO_RICO :
   `whd_types.h <whd__types_8h.html#a5adb0953a8527552bc47001673830602adbcf2230a367ee819d3d1f5fd8229656>`__
-  WHD_COUNTRY_QATAR :
   `whd_types.h <whd__types_8h.html#a5adb0953a8527552bc47001673830602a0878d0014862206af7cac468a3972727>`__
-  WHD_COUNTRY_REUNION :
   `whd_types.h <whd__types_8h.html#a5adb0953a8527552bc47001673830602a14a0ff94a7eee02e70b3de50ab8eb7a3>`__
-  WHD_COUNTRY_ROMANIA :
   `whd_types.h <whd__types_8h.html#a5adb0953a8527552bc47001673830602aa2ab39eef5becb203a96138c3c820c1b>`__
-  WHD_COUNTRY_RUSSIAN_FEDERATION :
   `whd_types.h <whd__types_8h.html#a5adb0953a8527552bc47001673830602a33aa2fbce6047122fd93158b35ea0b05>`__
-  WHD_COUNTRY_RWANDA :
   `whd_types.h <whd__types_8h.html#a5adb0953a8527552bc47001673830602a7d7367cc418bdf56d8501fc947c1ec68>`__
-  WHD_COUNTRY_SAINT_KITTS_AND_NEVIS :
   `whd_types.h <whd__types_8h.html#a5adb0953a8527552bc47001673830602a0daf67a923f71fb27ca2030c912004f7>`__
-  WHD_COUNTRY_SAINT_LUCIA :
   `whd_types.h <whd__types_8h.html#a5adb0953a8527552bc47001673830602a54824ebf705be95da5afceef14ba7f04>`__
-  WHD_COUNTRY_SAINT_PIERRE_AND_MIQUELON :
   `whd_types.h <whd__types_8h.html#a5adb0953a8527552bc47001673830602ac0682dfa09b0d3fd562f273acd0d4bab>`__
-  WHD_COUNTRY_SAINT_VINCENT_AND_THE_GRENADINES :
   `whd_types.h <whd__types_8h.html#a5adb0953a8527552bc47001673830602ac4b57b5f4f2a11929c50ac6294803363>`__
-  WHD_COUNTRY_SAMOA :
   `whd_types.h <whd__types_8h.html#a5adb0953a8527552bc47001673830602aa765025ec6762254e8174229f85edf48>`__
-  WHD_COUNTRY_SANIT_MARTIN_SINT_MARTEEN :
   `whd_types.h <whd__types_8h.html#a5adb0953a8527552bc47001673830602ab22c39087bcf2366631c55d19257aef9>`__
-  WHD_COUNTRY_SAO_TOME_AND_PRINCIPE :
   `whd_types.h <whd__types_8h.html#a5adb0953a8527552bc47001673830602a724068ed705cd4d0786735529e72aa8a>`__
-  WHD_COUNTRY_SAUDI_ARABIA :
   `whd_types.h <whd__types_8h.html#a5adb0953a8527552bc47001673830602a6180fa8d8a4c429b1b0f1164bbb74780>`__
-  WHD_COUNTRY_SENEGAL :
   `whd_types.h <whd__types_8h.html#a5adb0953a8527552bc47001673830602a428c7a86948575df4c6ce6fa18c90da8>`__
-  WHD_COUNTRY_SERBIA :
   `whd_types.h <whd__types_8h.html#a5adb0953a8527552bc47001673830602af81575db32ea976752b28b6a765f8a79>`__
-  WHD_COUNTRY_SEYCHELLES :
   `whd_types.h <whd__types_8h.html#a5adb0953a8527552bc47001673830602a0966efe7527ffd00cb54a61b30880763>`__
-  WHD_COUNTRY_SIERRA_LEONE :
   `whd_types.h <whd__types_8h.html#a5adb0953a8527552bc47001673830602a8b0888491f1d99fa9e0098313ab4a457>`__
-  WHD_COUNTRY_SINGAPORE :
   `whd_types.h <whd__types_8h.html#a5adb0953a8527552bc47001673830602a7ceec39dcab743bda2fd3af599579696>`__
-  WHD_COUNTRY_SLOVAKIA :
   `whd_types.h <whd__types_8h.html#a5adb0953a8527552bc47001673830602a675936d175b7a1d64b9f3bbbd8a46cf1>`__
-  WHD_COUNTRY_SLOVENIA :
   `whd_types.h <whd__types_8h.html#a5adb0953a8527552bc47001673830602a48e7173849521e008b96e6808cac6042>`__
-  WHD_COUNTRY_SOLOMON_ISLANDS :
   `whd_types.h <whd__types_8h.html#a5adb0953a8527552bc47001673830602a74863678fb25be3b767188a861091468>`__
-  WHD_COUNTRY_SOMALIA :
   `whd_types.h <whd__types_8h.html#a5adb0953a8527552bc47001673830602a0b2df2e6288d95b93887f501295240bb>`__
-  WHD_COUNTRY_SOUTH_AFRICA :
   `whd_types.h <whd__types_8h.html#a5adb0953a8527552bc47001673830602a07bee2924ee54d1c74f80190d5e7e7a9>`__
-  WHD_COUNTRY_SPAIN :
   `whd_types.h <whd__types_8h.html#a5adb0953a8527552bc47001673830602a37a91d2fca382aacac6a409768592453>`__
-  WHD_COUNTRY_SRI_LANKA :
   `whd_types.h <whd__types_8h.html#a5adb0953a8527552bc47001673830602af2dfe3f5a822d08a6e15438e71116ca3>`__
-  WHD_COUNTRY_SURINAME :
   `whd_types.h <whd__types_8h.html#a5adb0953a8527552bc47001673830602a9a7652d4a0e94aec5b347f06adde9dd0>`__
-  WHD_COUNTRY_SWAZILAND :
   `whd_types.h <whd__types_8h.html#a5adb0953a8527552bc47001673830602abb595de433f8d521c19b29e7df568f10>`__
-  WHD_COUNTRY_SWEDEN :
   `whd_types.h <whd__types_8h.html#a5adb0953a8527552bc47001673830602a169ff9217bce13d40b61c7981be6500c>`__
-  WHD_COUNTRY_SWITZERLAND :
   `whd_types.h <whd__types_8h.html#a5adb0953a8527552bc47001673830602ab49759646877a2f88dfcf21024882024>`__
-  WHD_COUNTRY_SYRIAN_ARAB_REPUBLIC :
   `whd_types.h <whd__types_8h.html#a5adb0953a8527552bc47001673830602a11fa3c6d43509b7108e52c0591792695>`__
-  WHD_COUNTRY_TAIWAN_PROVINCE_OF_CHINA :
   `whd_types.h <whd__types_8h.html#a5adb0953a8527552bc47001673830602af46c80327d3dd82d77f871ca3b7a8412>`__
-  WHD_COUNTRY_TAJIKISTAN :
   `whd_types.h <whd__types_8h.html#a5adb0953a8527552bc47001673830602a0490ac27378c2aac97428c87e6c22eb3>`__
-  WHD_COUNTRY_TANZANIA_UNITED_REPUBLIC_OF :
   `whd_types.h <whd__types_8h.html#a5adb0953a8527552bc47001673830602a63c8174222e8251ca58b296afe4fb0b2>`__
-  WHD_COUNTRY_THAILAND :
   `whd_types.h <whd__types_8h.html#a5adb0953a8527552bc47001673830602a2b7d2bd6cd4caa9aefa2e830a64dcd7a>`__
-  WHD_COUNTRY_TOGO :
   `whd_types.h <whd__types_8h.html#a5adb0953a8527552bc47001673830602a44bbe0126f99fdcd6a31fa6f8f4ff10c>`__
-  WHD_COUNTRY_TONGA :
   `whd_types.h <whd__types_8h.html#a5adb0953a8527552bc47001673830602abcaab8b85a785f167e4db8f9eca42eeb>`__
-  WHD_COUNTRY_TRINIDAD_AND_TOBAGO :
   `whd_types.h <whd__types_8h.html#a5adb0953a8527552bc47001673830602a5f064f5a7a02bc8c51a8fdd4b6b3f34e>`__
-  WHD_COUNTRY_TUNISIA :
   `whd_types.h <whd__types_8h.html#a5adb0953a8527552bc47001673830602ae593d1db3974edc9f2929bfce1a60f9f>`__
-  WHD_COUNTRY_TURKEY :
   `whd_types.h <whd__types_8h.html#a5adb0953a8527552bc47001673830602afd09a8ca5076e0fc26af0ba15519d12c>`__
-  WHD_COUNTRY_TURKMENISTAN :
   `whd_types.h <whd__types_8h.html#a5adb0953a8527552bc47001673830602ab1db73e8a705bdff1ae4d5f32d23e76c>`__
-  WHD_COUNTRY_TURKS_AND_CAICOS_ISLANDS :
   `whd_types.h <whd__types_8h.html#a5adb0953a8527552bc47001673830602a0226d0b32ce0ae55beb66b6984e183b1>`__
-  WHD_COUNTRY_TUVALU :
   `whd_types.h <whd__types_8h.html#a5adb0953a8527552bc47001673830602ac6a05fb5107eae3d7abc5d44c768c467>`__
-  WHD_COUNTRY_UGANDA :
   `whd_types.h <whd__types_8h.html#a5adb0953a8527552bc47001673830602ac5a999f73d5f6068f3051188dcfdf484>`__
-  WHD_COUNTRY_UKRAINE :
   `whd_types.h <whd__types_8h.html#a5adb0953a8527552bc47001673830602a52653ed0fa500aec109e48062b37628a>`__
-  WHD_COUNTRY_UNITED_ARAB_EMIRATES :
   `whd_types.h <whd__types_8h.html#a5adb0953a8527552bc47001673830602ab483ff06b30aed116e2c304a81a50991>`__
-  WHD_COUNTRY_UNITED_KINGDOM :
   `whd_types.h <whd__types_8h.html#a5adb0953a8527552bc47001673830602a7f380c41dc00c60e605977eecefc12d8>`__
-  WHD_COUNTRY_UNITED_STATES :
   `whd_types.h <whd__types_8h.html#a5adb0953a8527552bc47001673830602a5916c7373b3c5c51a37408b7e6fc9a11>`__
-  WHD_COUNTRY_UNITED_STATES_MINOR_OUTLYING_ISLANDS :
   `whd_types.h <whd__types_8h.html#a5adb0953a8527552bc47001673830602a8c5cc2e06d48a055e87dc50dd476bb8f>`__
-  WHD_COUNTRY_UNITED_STATES_NO_DFS :
   `whd_types.h <whd__types_8h.html#a5adb0953a8527552bc47001673830602a7978297eadb16cdcc60ce94eb2243514>`__
-  WHD_COUNTRY_UNITED_STATES_REV4 :
   `whd_types.h <whd__types_8h.html#a5adb0953a8527552bc47001673830602a2a146a0f3575c98e2de26e96d3cfe411>`__
-  WHD_COUNTRY_UNITED_STATES_REV931 :
   `whd_types.h <whd__types_8h.html#a5adb0953a8527552bc47001673830602a6139ef6c6fc963d6f65606011593abfa>`__
-  WHD_COUNTRY_URUGUAY :
   `whd_types.h <whd__types_8h.html#a5adb0953a8527552bc47001673830602a8a30da815d77006bd79261a1a8f6ba13>`__
-  WHD_COUNTRY_UZBEKISTAN :
   `whd_types.h <whd__types_8h.html#a5adb0953a8527552bc47001673830602a29a6b48684d5f13913e074d5eff373ef>`__
-  WHD_COUNTRY_VANUATU :
   `whd_types.h <whd__types_8h.html#a5adb0953a8527552bc47001673830602a42567f62fb3db9b331d667cadf521c4b>`__
-  WHD_COUNTRY_VENEZUELA :
   `whd_types.h <whd__types_8h.html#a5adb0953a8527552bc47001673830602a32f35514879a6d2388dd287f2b733a95>`__
-  WHD_COUNTRY_VIET_NAM :
   `whd_types.h <whd__types_8h.html#a5adb0953a8527552bc47001673830602ad6a9f5318cc8cbff8f62ebc055ea5bc6>`__
-  WHD_COUNTRY_VIRGIN_ISLANDS_BRITISH :
   `whd_types.h <whd__types_8h.html#a5adb0953a8527552bc47001673830602a51b412f5fbbc776aaba60e6539f09960>`__
-  WHD_COUNTRY_VIRGIN_ISLANDS_US :
   `whd_types.h <whd__types_8h.html#a5adb0953a8527552bc47001673830602ac6c0b157b4dd0c702f8b8efbabe72e3a>`__
-  WHD_COUNTRY_WALLIS_AND_FUTUNA :
   `whd_types.h <whd__types_8h.html#a5adb0953a8527552bc47001673830602a4ead1c340c1b0b803a1e7229ad2d8c6e>`__
-  WHD_COUNTRY_WEST_BANK :
   `whd_types.h <whd__types_8h.html#a5adb0953a8527552bc47001673830602a62b82918fc05f1161b649ba7868b9cb1>`__
-  WHD_COUNTRY_WESTERN_SAHARA :
   `whd_types.h <whd__types_8h.html#a5adb0953a8527552bc47001673830602ab84d497102e7afac1a6d976bc2e1f52f>`__
-  WHD_COUNTRY_WORLD_WIDE_XV_REV983 :
   `whd_types.h <whd__types_8h.html#a5adb0953a8527552bc47001673830602af3850579e2b2432fc0e1747d33bbec02>`__
-  WHD_COUNTRY_WORLD_WIDE_XX :
   `whd_types.h <whd__types_8h.html#a5adb0953a8527552bc47001673830602a16ebc6cfb59f870500fc7339ae75b0c8>`__
-  WHD_COUNTRY_WORLD_WIDE_XX_REV17 :
   `whd_types.h <whd__types_8h.html#a5adb0953a8527552bc47001673830602aa474c04ef58a2de055b26a4894f73dcc>`__
-  WHD_COUNTRY_YEMEN :
   `whd_types.h <whd__types_8h.html#a5adb0953a8527552bc47001673830602ae2d344ec27a73529f793ff47ad765f90>`__
-  WHD_COUNTRY_ZAMBIA :
   `whd_types.h <whd__types_8h.html#a5adb0953a8527552bc47001673830602afe0f095c0ee7807b48694026f7689e0b>`__
-  WHD_COUNTRY_ZIMBABWE :
   `whd_types.h <whd__types_8h.html#a5adb0953a8527552bc47001673830602a4337f5e3720a79a511cae64e63123293>`__
-  WHD_DOT11_RC_RESERVED :
   `whd_types.h <whd__types_8h.html#ac81b31559ee1db82f01e8acfb8eea55daf2b4dc47610f370bd154aa27c2434398>`__
-  WHD_DOT11_RC_UNSPECIFIED :
   `whd_types.h <whd__types_8h.html#ac81b31559ee1db82f01e8acfb8eea55da03fab52dfc684e41485202d5a72588d1>`__
-  WHD_FALSE :
   `whd_types.h <whd__types_8h.html#a7cd94a03f2e7e6aab7217ed559c7a0aca13563ade40eb686d1a1d81372cb31412>`__
-  WHD_IOVAR_GET_LISTEN_INTERVAL :
   `whd_types.h <whd__types_8h.html#adc3a23bad78e0110546c9edc00456dafa335ed12c48af483e1c930e5c7b9f5d57>`__
-  WHD_IOVAR_GET_MAC_ADDRESS :
   `whd_types.h <whd__types_8h.html#adc3a23bad78e0110546c9edc00456dafa5ebc26219966e0db67e2cdbb8f89d986>`__
-  WHD_LISTEN_INTERVAL_TIME_UNIT_BEACON :
   `whd_types.h <whd__types_8h.html#a98ac1a2590358bafac6a2da287034638ab1001ef79a4819c782281e4fb275a53b>`__
-  WHD_LISTEN_INTERVAL_TIME_UNIT_DTIM :
   `whd_types.h <whd__types_8h.html#a98ac1a2590358bafac6a2da287034638ae1f7bbf9c410906413d74238699e277c>`__
-  WHD_NETWORK_RX :
   `whd_network_types.h <group__buffif.html#gga44a64c51498b204ceef5555209e29452a11af875b5bd8b18c4a0c878af601d1ca>`__
-  WHD_NETWORK_TX :
   `whd_network_types.h <group__buffif.html#gga44a64c51498b204ceef5555209e29452a3d24bf5423cc175fe6ee80bd359dc5bd>`__
-  WHD_PACKET_FILTER_RULE_NEGATIVE_MATCHING :
   `whd_types.h <whd__types_8h.html#a36b806b9b0e986bf290ae005d1bae038a1f6065677744b90d146bafbcb3832cac>`__
-  WHD_PACKET_FILTER_RULE_POSITIVE_MATCHING :
   `whd_types.h <whd__types_8h.html#a36b806b9b0e986bf290ae005d1bae038a75352695a5824e5b1759743869bca6d9>`__
-  WHD_REMOVE_CUSTOM_IE :
   `whd_types.h <whd__types_8h.html#a8be1026494a86f0ceeebb2dcbf092cbdadc803bf5bf9286741e59da1b850561ce>`__
-  WHD_RESOURCE_WLAN_CLM :
   `whd_resource_api.h <whd__resource__api_8h.html#a26c678527cf0cb5196be511d9ad62c0cad763d3e34be0df4a36b630bb7c37e51c>`__
-  WHD_RESOURCE_WLAN_FIRMWARE :
   `whd_resource_api.h <whd__resource__api_8h.html#a26c678527cf0cb5196be511d9ad62c0caf1635251dcc37bfa67af783a53f0acf3>`__
-  WHD_RESOURCE_WLAN_NVRAM :
   `whd_resource_api.h <whd__resource__api_8h.html#a26c678527cf0cb5196be511d9ad62c0ca397297fd6225d6eae30e7fa0d40a94f7>`__
-  WHD_SCAN_ABORTED :
   `whd_types.h <whd__types_8h.html#a34d5a5749a0bcc00b7249108a8670adaa83e78acc28b9e74f743db6deb743472b>`__
-  WHD_SCAN_COMPLETED_SUCCESSFULLY :
   `whd_types.h <whd__types_8h.html#a34d5a5749a0bcc00b7249108a8670adaafca7e19867e47de64de826d409bc7c7d>`__
-  WHD_SCAN_INCOMPLETE :
   `whd_types.h <whd__types_8h.html#a34d5a5749a0bcc00b7249108a8670adaa5c525f0cbf37c43871a67556a2581dcd>`__
-  WHD_SCAN_RESULT_FLAG_BEACON :
   `whd_types.h <whd__types_8h.html#a4bb9a3034dfb9a44507ef6201842edb7a7c6e2726f436bdc8b7d45f7cc34c504b>`__
-  WHD_SCAN_RESULT_FLAG_RSSI_OFF_CHANNEL :
   `whd_types.h <whd__types_8h.html#a4bb9a3034dfb9a44507ef6201842edb7a5239fb226233ffc16252907b91fa8be7>`__
-  WHD_SCAN_TYPE_ACTIVE :
   `whd_types.h <whd__types_8h.html#af7ac1b0be4ce67f3e82b876c2f27fd3aaac68ee09cf61767ddf825d3164fc48f6>`__
-  WHD_SCAN_TYPE_NO_BSSID_FILTER :
   `whd_types.h <whd__types_8h.html#af7ac1b0be4ce67f3e82b876c2f27fd3aa3973e30c4f21443ba51cb43a60e80927>`__
-  WHD_SCAN_TYPE_PASSIVE :
   `whd_types.h <whd__types_8h.html#af7ac1b0be4ce67f3e82b876c2f27fd3aa1f46026d7581b09ec416e4e33dbdeb0e>`__
-  WHD_SCAN_TYPE_PNO :
   `whd_types.h <whd__types_8h.html#af7ac1b0be4ce67f3e82b876c2f27fd3aa6906a0f7483557038c547223181666f1>`__
-  WHD_SCAN_TYPE_PROHIBITED_CHANNELS :
   `whd_types.h <whd__types_8h.html#af7ac1b0be4ce67f3e82b876c2f27fd3aae2c9c7e9d61ed8493aef4ae4fef5f742>`__
-  WHD_SECURITY_FORCE_32_BIT :
   `whd_types.h <whd__types_8h.html#aaeeea5666743710aa4f01ff264b27059aa6ab763d4d6b3fcffd944d14b3edd1f4>`__
-  WHD_SECURITY_IBSS_OPEN :
   `whd_types.h <whd__types_8h.html#aaeeea5666743710aa4f01ff264b27059aa9ded800120d37ad54c4467688416abc>`__
-  WHD_SECURITY_OPEN :
   `whd_types.h <whd__types_8h.html#aaeeea5666743710aa4f01ff264b27059a2b5521d2587803363849e1987775e78b>`__
-  WHD_SECURITY_UNKNOWN :
   `whd_types.h <whd__types_8h.html#aaeeea5666743710aa4f01ff264b27059aebaa913f60b77b94277287056775c27d>`__
-  WHD_SECURITY_WEP_PSK :
   `whd_types.h <whd__types_8h.html#aaeeea5666743710aa4f01ff264b27059abe451579838fff23cd0a1fc1d1ff313d>`__
-  WHD_SECURITY_WEP_SHARED :
   `whd_types.h <whd__types_8h.html#aaeeea5666743710aa4f01ff264b27059a25a16cfc764ee18ff73d440b9a307228>`__
-  WHD_SECURITY_WPA2_AES_ENT :
   `whd_types.h <whd__types_8h.html#aaeeea5666743710aa4f01ff264b27059a278600fd506f0522cd923a8ee066c3f4>`__
-  WHD_SECURITY_WPA2_AES_PSK :
   `whd_types.h <whd__types_8h.html#aaeeea5666743710aa4f01ff264b27059a81b7002ff8c0d488af8525ae614b6cf5>`__
-  WHD_SECURITY_WPA2_FBT_ENT :
   `whd_types.h <whd__types_8h.html#aaeeea5666743710aa4f01ff264b27059a843c4f3ffe1f3e006cde49b7bdee5a09>`__
-  WHD_SECURITY_WPA2_FBT_PSK :
   `whd_types.h <whd__types_8h.html#aaeeea5666743710aa4f01ff264b27059ae380b414a2bc8a9343d0a6f4a71c0c0c>`__
-  WHD_SECURITY_WPA2_MIXED_ENT :
   `whd_types.h <whd__types_8h.html#aaeeea5666743710aa4f01ff264b27059af37aae67b60c8d1cfc83be2bbbeeb894>`__
-  WHD_SECURITY_WPA2_MIXED_PSK :
   `whd_types.h <whd__types_8h.html#aaeeea5666743710aa4f01ff264b27059a602d04db8143c505cc674c738bf20150>`__
-  WHD_SECURITY_WPA2_TKIP_ENT :
   `whd_types.h <whd__types_8h.html#aaeeea5666743710aa4f01ff264b27059ad3888b236e75c3d5d5e491d01de5cd7f>`__
-  WHD_SECURITY_WPA2_TKIP_PSK :
   `whd_types.h <whd__types_8h.html#aaeeea5666743710aa4f01ff264b27059a901ffc3e307d8b44bc80150e7cea8fbb>`__
-  WHD_SECURITY_WPA2_WPA_AES_PSK :
   `whd_types.h <whd__types_8h.html#aaeeea5666743710aa4f01ff264b27059a2ecbc670d4cb7b82a2fbe4ee9af3a217>`__
-  WHD_SECURITY_WPA2_WPA_MIXED_PSK :
   `whd_types.h <whd__types_8h.html#aaeeea5666743710aa4f01ff264b27059a769e1d8325cd5eda1363e1d2b9157235>`__
-  WHD_SECURITY_WPA3_SAE :
   `whd_types.h <whd__types_8h.html#aaeeea5666743710aa4f01ff264b27059a9d550fa7cc0ea96ac1e9a4de5223d2b0>`__
-  WHD_SECURITY_WPA3_WPA2_PSK :
   `whd_types.h <whd__types_8h.html#aaeeea5666743710aa4f01ff264b27059af4d58bc2335113f0c70abdbd3019bb61>`__
-  WHD_SECURITY_WPA_AES_ENT :
   `whd_types.h <whd__types_8h.html#aaeeea5666743710aa4f01ff264b27059a45b9dd3606cbe683b3db7fd13b392ec2>`__
-  WHD_SECURITY_WPA_AES_PSK :
   `whd_types.h <whd__types_8h.html#aaeeea5666743710aa4f01ff264b27059a8905c2aea364d45edf8496e5190effd0>`__
-  WHD_SECURITY_WPA_MIXED_ENT :
   `whd_types.h <whd__types_8h.html#aaeeea5666743710aa4f01ff264b27059a67aad7934dfb63684dcf3859f3dd1d88>`__
-  WHD_SECURITY_WPA_MIXED_PSK :
   `whd_types.h <whd__types_8h.html#aaeeea5666743710aa4f01ff264b27059afd3dd8c5b97f85f737177097fc0481b4>`__
-  WHD_SECURITY_WPA_TKIP_ENT :
   `whd_types.h <whd__types_8h.html#aaeeea5666743710aa4f01ff264b27059a8331ab29a560b0936a5f16ed2745b550>`__
-  WHD_SECURITY_WPA_TKIP_PSK :
   `whd_types.h <whd__types_8h.html#aaeeea5666743710aa4f01ff264b27059ad73ab4a21acbd437cc43c3b58ef45771>`__
-  WHD_SECURITY_WPS_SECURE :
   `whd_types.h <whd__types_8h.html#aaeeea5666743710aa4f01ff264b27059a2d39ccad06b7d7225b03186160144195>`__
-  WHD_TRUE :
   `whd_types.h <whd__types_8h.html#a7cd94a03f2e7e6aab7217ed559c7a0aca665c5318c953dbd96c759579729c87b6>`__
-  WL_MFP_CAPABLE :
   `whd_types.h <whd__types_8h.html#a06fc87d81c62e9abb8790b6e5713c55ba51d019c133381c69b48678f6aee69c14>`__
-  WL_MFP_NONE :
   `whd_types.h <whd__types_8h.html#a06fc87d81c62e9abb8790b6e5713c55ba0843901f508ccc7996257015fb07c850>`__
-  WL_MFP_REQUIRED :
   `whd_types.h <whd__types_8h.html#a06fc87d81c62e9abb8790b6e5713c55ba3ed2ab2dcadf0adaa51abb1e5af9a786>`__
-  WLC_SUP_AUTHENTICATED :
   `whd_events.h <whd__events_8h.html#aba9543ff6a1542d81c7adc66bb339c4caa4fb668202b9e57bbfaedf0b20304a6e>`__
-  WLC_SUP_AUTHENTICATING :
   `whd_events.h <whd__events_8h.html#aba9543ff6a1542d81c7adc66bb339c4cab0fea5debce5e8abf993c6a4e6aa2cce>`__
-  WLC_SUP_CONNECTING :
   `whd_events.h <whd__events_8h.html#aba9543ff6a1542d81c7adc66bb339c4ca8ac4d7e7fb55b33bb65316c9ba42f147>`__
-  WLC_SUP_DISCONNECTED :
   `whd_events.h <whd__events_8h.html#aba9543ff6a1542d81c7adc66bb339c4cac9bc0eb3bd21bc5189a4a46793ea7680>`__
-  WLC_SUP_IDREQUIRED :
   `whd_events.h <whd__events_8h.html#aba9543ff6a1542d81c7adc66bb339c4ca6d0a906929483eb4d9633c875cffd6ce>`__
-  WLC_SUP_KEYED :
   `whd_events.h <whd__events_8h.html#aba9543ff6a1542d81c7adc66bb339c4cab04882e234f679807f486fcd34ee9a1a>`__
-  WLC_SUP_KEYXCHANGE :
   `whd_events.h <whd__events_8h.html#aba9543ff6a1542d81c7adc66bb339c4ca47709bf89f9482d964fb8f80110b9938>`__
-  WLC_SUP_KEYXCHANGE_PREP_G2 :
   `whd_events.h <whd__events_8h.html#aba9543ff6a1542d81c7adc66bb339c4caa1a479ce249334165efc2abb7a417303>`__
-  WLC_SUP_KEYXCHANGE_PREP_M2 :
   `whd_events.h <whd__events_8h.html#aba9543ff6a1542d81c7adc66bb339c4ca82ffd4e7e1eda9cde4a2e4f93a625623>`__
-  WLC_SUP_KEYXCHANGE_PREP_M4 :
   `whd_events.h <whd__events_8h.html#aba9543ff6a1542d81c7adc66bb339c4caf6103c24206afe80f6f3918a5f139772>`__
-  WLC_SUP_KEYXCHANGE_WAIT_G1 :
   `whd_events.h <whd__events_8h.html#aba9543ff6a1542d81c7adc66bb339c4ca8f648a422bf489e97c275b0e81b4e69a>`__
-  WLC_SUP_KEYXCHANGE_WAIT_M1 :
   `whd_events.h <whd__events_8h.html#aba9543ff6a1542d81c7adc66bb339c4ca05d3731c95722d8e0499673a12697abe>`__
-  WLC_SUP_KEYXCHANGE_WAIT_M3 :
   `whd_events.h <whd__events_8h.html#aba9543ff6a1542d81c7adc66bb339c4caefe14a3fa955320276622a8c2a3df3f6>`__
-  WLC_SUP_LAST_BASIC_STATE :
   `whd_events.h <whd__events_8h.html#aba9543ff6a1542d81c7adc66bb339c4ca949fb8270a4429fe0bdd53eb654d10df>`__
-  WLC_SUP_TIMEOUT :
   `whd_events.h <whd__events_8h.html#aba9543ff6a1542d81c7adc66bb339c4ca9e0cfe079f25369646bb199aba3f1240>`__

