===============
Modules
===============

Here is a list of all modules:

+-----------------------------------+-----------------------------------+
| `WHD Event handling               | Functions that allow user         |
| API <group__event.html>`__        | applications to receive event     |
|                                   | callbacks and set event handlers  |
+-----------------------------------+-----------------------------------+
| `WHD Buffer Interface             | Allows WHD to perform buffer      |
| API <group__buffif.html>`__       | related operations like,          |
|                                   | allocating, releasing, retrieving |
|                                   | the current pointer of and size   |
|                                   | of a packet buffer                |
+-----------------------------------+-----------------------------------+
| `WHD Network Interface            | Allows WHD to pass received data  |
| API <group__netif.html>`__        | to the network stack, to send an  |
|                                   | ethernet frame to WHD, etc        |
+-----------------------------------+-----------------------------------+
| `WHD Resource                     | Functions that enable WHD to      |
| API <group__res.html>`__          | download WLAN firmware, NVRAM and |
|                                   | CLM BLOB on a particular hardware |
|                                   | platform                          |
+-----------------------------------+-----------------------------------+
| `WHD Wi-Fi                        | APIs for controlling the Wi-Fi    |
| API <group__wifi.html>`__         | system                            |
+-----------------------------------+-----------------------------------+
| `WHD Wi-Fi Management             | Initialisation and other          |
| API <group__wifimanagement.htm    | management functions for WHD      |
| l>`__                             | system                            |
+-----------------------------------+-----------------------------------+
| `WHD Wi-Fi Join, Scan and Halt    | Wi-Fi APIs for join, scan & leave |
| API <group__wifijoin.html>`__     |                                   |
+-----------------------------------+-----------------------------------+
| `WHD Wi-Fi Utility                | Allows WHD to perform utility     |
| API <group__wifiutilities.htm     | operations                        |
| l>`__                             |                                   |
+-----------------------------------+-----------------------------------+
| `WHD Wi-Fi SoftAP                 | Wi-Fi APIs to perform SoftAP      |
| API <group__wifisoftap.html>`__   | related functionalities           |
+-----------------------------------+-----------------------------------+
| `WHD Wi-Fi Power Save             | Wi-Fi functions for WLAN low      |
| API <group__wifipowersave.htm     | power modes                       |
| l>`__                             |                                   |
+-----------------------------------+-----------------------------------+
| `WHD Wi-Fi IOCTL Set/Get          | Set and get IOCTL values          |
| API <group__wifiioctl.html>`__    |                                   |
+-----------------------------------+-----------------------------------+
| `WHD Wi-Fi Debug                  | WHD APIs which allows debugging   |
| API <group__dbg.html>`__          | like, printing whd log            |
|                                   | information, getting whd stats,   |
|                                   | etc                               |
+-----------------------------------+-----------------------------------+
| `WHD Bus                          | Allows WHD to operate with        |
| API <group__busapi.html>`__       | specific SDIO/SPI bus             |
+-----------------------------------+-----------------------------------+

.. toctree::
   :hidden:
   
   group__event.rst
   group__buffif.rst
   group__netif.rst
   group__res.rst
   group__wifi.rst
   group__busapi.rst
   
   

