===============================
CRC (Cyclic Redundancy Check)
===============================

.. doxygengroup:: group_hal_crc
   :project: hal
   :members:
   :protected-members:
   :private-members:
   :undoc-members:


   

.. toctree::
   :hidden:

   roup__group__hal__results__crc.rst
   

