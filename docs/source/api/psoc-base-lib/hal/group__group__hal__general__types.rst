===============
General Types
===============

.. doxygengroup:: group_hal_general_types
   :project: hal
   :members:
   :protected-members:
   :private-members:
   :undoc-members:


   

.. toctree::
   :hidden:

   group__group__hal__results.rst
   group__group__hal__syspm.rst
   group__group__hal__clock.rst

