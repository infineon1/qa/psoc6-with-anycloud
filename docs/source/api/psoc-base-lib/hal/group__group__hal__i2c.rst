==================================
I2C (Inter-Integrated Circuit)
==================================

.. doxygengroup:: group_hal_i2c
   :project: hal
   :members:
   :protected-members:
   :private-members:
   :undoc-members:


   

.. toctree::
   :hidden:

   group__group__hal__results__i2c.rst
   

