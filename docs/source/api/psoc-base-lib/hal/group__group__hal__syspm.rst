========================
System Power Management
========================

.. doxygengroup:: group_hal_syspm
   :project: hal
   :members:
   :protected-members:
   :private-members:
   :undoc-members:


   

.. toctree::
   :hidden:

   group__group__hal__results__syspm.rst
   

