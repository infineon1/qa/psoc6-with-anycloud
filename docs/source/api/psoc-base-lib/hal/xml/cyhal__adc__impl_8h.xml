<doxygen xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" version="1.8.14" xsi:noNamespaceSchemaLocation="compound.xsd">
  <compounddef id="cyhal__adc__impl_8h" kind="file" language="C++">
    <compoundname>cyhal_adc_impl.h</compoundname>
    <includes local="yes" refid="cyhal__hw__resources_8h">cyhal_hw_resources.h</includes>
      <sectiondef kind="user-defined">
      <memberdef id="cyhal__adc__impl_8h_1a722f74fbfc67ef6842b9d3b1a7971c73" kind="define" prot="public" static="no">
        <name>CYHAL_ADC_AVG_MODE_SEQUENTIAL</name>
        <initializer>(1u &lt;&lt; (CYHAL_ADC_AVG_MODE_MAX_SHIFT + 1u))</initializer>
        <briefdescription>
<para>Convert all samples to be averaged back to back, before proceeding to the next channel. </para>        </briefdescription>
        <detaileddescription>
<para>This is the default behavior. </para>        </detaileddescription>
        <inbodydescription>
        </inbodydescription>
        <location bodyend="-1" bodyfile="output/libs/COMPONENT_DEPRECATED/psoc6hal/COMPONENT_PSOC6HAL/include/cyhal_adc_impl.h" bodystart="41" column="9" file="output/libs/COMPONENT_DEPRECATED/psoc6hal/COMPONENT_PSOC6HAL/include/cyhal_adc_impl.h" line="41" />
      </memberdef>
      <memberdef id="cyhal__adc__impl_8h_1ac595adaf5d8965e12450b0bcd68a6c16" kind="define" prot="public" static="no">
        <name>CYHAL_ADC_AVG_MODE_INTERLEAVED</name>
        <initializer>(1u &lt;&lt; (CYHAL_ADC_AVG_MODE_MAX_SHIFT + 2u))</initializer>
        <briefdescription>
<para>Convert one sample to be averaged per scan, interleaved with the rest of the channels. </para>        </briefdescription>
        <detaileddescription>
<para>This maintains a consistent spacing of samples regardless of whether samples are averaged or not. However, it also means that a new value will only be provided once every n scans, where n is the configured averaging count. At the conclusion of any other scan, the read operation will return the same value as the previous scan. The application is responsible for keeping tracks of the scans on which the value will be updated. <verbatim>embed:rst 
.. raw:: html

  &lt;div class="admonition note"&gt;&lt;p class="admonition-title"&gt;note&lt;/p&gt;&lt;p&gt;&lt;p&gt;This mode is incompatible with &lt;a href="group__group__hal__adc.html#group__group__hal__adc_1gaffbef3cc844fa6ff749e8230c6ef6d54"&gt;CYHAL_ADC_AVG_MODE_ACCUMULATE&lt;/a&gt; &lt;/p&gt;&lt;/p&gt;&lt;/div&gt;</verbatim><verbatim>embed:rst 
.. raw:: html

  &lt;div class="admonition note"&gt;&lt;p class="admonition-title"&gt;note&lt;/p&gt;&lt;p&gt;&lt;p&gt;In this mode, the averaging count should be set to less than 16 to avoid overflowing the working register that is used to accumulate intermediate results. &lt;/p&gt;&lt;/p&gt;&lt;/div&gt;</verbatim></para>        </detaileddescription>
        <inbodydescription>
        </inbodydescription>
        <location bodyend="-1" bodyfile="output/libs/COMPONENT_DEPRECATED/psoc6hal/COMPONENT_PSOC6HAL/include/cyhal_adc_impl.h" bodystart="53" column="9" file="output/libs/COMPONENT_DEPRECATED/psoc6hal/COMPONENT_PSOC6HAL/include/cyhal_adc_impl.h" line="53" />
      </memberdef>
      </sectiondef>
    <briefdescription>
    </briefdescription>
    <detaileddescription>
    </detaileddescription>
    <programlisting>
<codeline lineno="1"><highlight class="comment">/*******************************************************************************</highlight></codeline>
<codeline lineno="2"><highlight class="comment">*<sp />File<sp />Name:<sp />cyhal_clock_impl.h</highlight></codeline>
<codeline lineno="3"><highlight class="comment">*</highlight></codeline>
<codeline lineno="4"><highlight class="comment">*<sp />Description:</highlight></codeline>
<codeline lineno="5"><highlight class="comment">*<sp />PSoC<sp />6<sp />specific<sp />implementation<sp />for<sp />clocks<sp />API.</highlight></codeline>
<codeline lineno="6"><highlight class="comment">*</highlight></codeline>
<codeline lineno="7"><highlight class="comment">********************************************************************************</highlight></codeline>
<codeline lineno="8"><highlight class="comment">*<sp />\copyright</highlight></codeline>
<codeline lineno="9"><highlight class="comment">*<sp />Copyright<sp />2018-2019<sp />Cypress<sp />Semiconductor<sp />Corporation</highlight></codeline>
<codeline lineno="10"><highlight class="comment">*<sp />SPDX-License-Identifier:<sp />Apache-2.0</highlight></codeline>
<codeline lineno="11"><highlight class="comment">*</highlight></codeline>
<codeline lineno="12"><highlight class="comment">*<sp />Licensed<sp />under<sp />the<sp />Apache<sp />License,<sp />Version<sp />2.0<sp />(the<sp />"License");</highlight></codeline>
<codeline lineno="13"><highlight class="comment">*<sp />you<sp />may<sp />not<sp />use<sp />this<sp />file<sp />except<sp />in<sp />compliance<sp />with<sp />the<sp />License.</highlight></codeline>
<codeline lineno="14"><highlight class="comment">*<sp />You<sp />may<sp />obtain<sp />a<sp />copy<sp />of<sp />the<sp />License<sp />at</highlight></codeline>
<codeline lineno="15"><highlight class="comment">*</highlight></codeline>
<codeline lineno="16"><highlight class="comment">*<sp /><sp /><sp /><sp /><sp />http://www.apache.org/licenses/LICENSE-2.0</highlight></codeline>
<codeline lineno="17"><highlight class="comment">*</highlight></codeline>
<codeline lineno="18"><highlight class="comment">*<sp />Unless<sp />required<sp />by<sp />applicable<sp />law<sp />or<sp />agreed<sp />to<sp />in<sp />writing,<sp />software</highlight></codeline>
<codeline lineno="19"><highlight class="comment">*<sp />distributed<sp />under<sp />the<sp />License<sp />is<sp />distributed<sp />on<sp />an<sp />"AS<sp />IS"<sp />BASIS,</highlight></codeline>
<codeline lineno="20"><highlight class="comment">*<sp />WITHOUT<sp />WARRANTIES<sp />OR<sp />CONDITIONS<sp />OF<sp />ANY<sp />KIND,<sp />either<sp />express<sp />or<sp />implied.</highlight></codeline>
<codeline lineno="21"><highlight class="comment">*<sp />See<sp />the<sp />License<sp />for<sp />the<sp />specific<sp />language<sp />governing<sp />permissions<sp />and</highlight></codeline>
<codeline lineno="22"><highlight class="comment">*<sp />limitations<sp />under<sp />the<sp />License.</highlight></codeline>
<codeline lineno="23"><highlight class="comment">*******************************************************************************/</highlight><highlight class="normal" /></codeline>
<codeline lineno="24"><highlight class="normal" /></codeline>
<codeline lineno="25"><highlight class="normal" /><highlight class="preprocessor">#pragma<sp />once</highlight><highlight class="normal" /></codeline>
<codeline lineno="26"><highlight class="normal" /></codeline>
<codeline lineno="27"><highlight class="normal" /><highlight class="preprocessor">#include<sp />"cyhal_hw_resources.h"</highlight><highlight class="normal" /></codeline>
<codeline lineno="28"><highlight class="normal" /></codeline>
<codeline lineno="29"><highlight class="normal" /><highlight class="preprocessor">#if<sp />defined(__cplusplus)</highlight><highlight class="normal" /></codeline>
<codeline lineno="30"><highlight class="normal" /><highlight class="keyword">extern</highlight><highlight class="normal"><sp /></highlight><highlight class="stringliteral">"C"</highlight><highlight class="normal" /></codeline>
<codeline lineno="31"><highlight class="normal">{</highlight></codeline>
<codeline lineno="32"><highlight class="normal" /><highlight class="preprocessor">#endif</highlight><highlight class="normal" /></codeline>
<codeline lineno="33"><highlight class="normal" /></codeline>
<codeline lineno="41"><highlight class="preprocessor">#define<sp />CYHAL_ADC_AVG_MODE_SEQUENTIAL<sp />(1u<sp />&lt;&lt;<sp />(CYHAL_ADC_AVG_MODE_MAX_SHIFT<sp />+<sp />1u))</highlight><highlight class="normal" /></codeline>
<codeline lineno="42"><highlight class="normal" /></codeline>
<codeline lineno="53"><highlight class="preprocessor">#define<sp />CYHAL_ADC_AVG_MODE_INTERLEAVED<sp />(1u<sp />&lt;&lt;<sp />(CYHAL_ADC_AVG_MODE_MAX_SHIFT<sp />+<sp />2u))</highlight><highlight class="normal" /></codeline>
<codeline lineno="54"><highlight class="normal" /></codeline>
<codeline lineno="57"><highlight class="preprocessor">#if<sp />defined(__cplusplus)</highlight><highlight class="normal" /></codeline>
<codeline lineno="58"><highlight class="normal">}</highlight></codeline>
<codeline lineno="59"><highlight class="normal" /><highlight class="preprocessor">#endif</highlight></codeline>
    </programlisting>
    <location file="output/libs/COMPONENT_DEPRECATED/psoc6hal/COMPONENT_PSOC6HAL/include/cyhal_adc_impl.h" />
  </compounddef>
</doxygen>