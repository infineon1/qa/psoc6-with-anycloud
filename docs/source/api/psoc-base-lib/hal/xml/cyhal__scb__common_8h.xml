<doxygen xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" version="1.8.14" xsi:noNamespaceSchemaLocation="compound.xsd">
  <compounddef id="cyhal__scb__common_8h" kind="file" language="C++">
    <compoundname>cyhal_scb_common.h</compoundname>
    <includedby local="yes" refid="cyhal__ezi2c_8c">cyhal_ezi2c.c</includedby>
    <includedby local="yes" refid="cyhal__hwmgr_8c">cyhal_hwmgr.c</includedby>
    <includedby local="yes" refid="cyhal__i2c_8c">cyhal_i2c.c</includedby>
    <includedby local="yes" refid="cyhal__scb__common_8c">cyhal_scb_common.c</includedby>
    <includedby local="yes" refid="cyhal__spi_8c">cyhal_spi.c</includedby>
    <includedby local="yes" refid="cyhal__uart_8c">cyhal_uart.c</includedby>
    <briefdescription>
<para>Provides a struct definitions for configuration resources in the SCB (UART, I2C, SPI). </para>    </briefdescription>
    <detaileddescription>
<para><simplesect kind="copyright"><para>Copyright 2018-2020 Cypress Semiconductor Corporation SPDX-License-Identifier: Apache-2.0</para></simplesect>
Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with the License. You may obtain a copy of the License at <verbatim>http://www.apache.org/licenses/LICENSE-2.0
</verbatim></para><para>Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the specific language governing permissions and limitations under the License. </para>    </detaileddescription>
    <programlisting>
<codeline lineno="1"><highlight class="comment">/***************************************************************************/</highlight></codeline>
<codeline lineno="33"><highlight class="preprocessor">#pragma<sp />once</highlight><highlight class="normal" /></codeline>
<codeline lineno="34"><highlight class="normal" /></codeline>
<codeline lineno="35"><highlight class="normal" /><highlight class="preprocessor">#include<sp />"cy_device.h"</highlight><highlight class="normal" /></codeline>
<codeline lineno="36"><highlight class="normal" /><highlight class="preprocessor">#include<sp />"cy_pdl.h"</highlight><highlight class="normal" /></codeline>
<codeline lineno="37"><highlight class="normal" /><highlight class="preprocessor">#include<sp />"cyhal_utils.h"</highlight><highlight class="normal" /></codeline>
<codeline lineno="38"><highlight class="normal" /></codeline>
<codeline lineno="39"><highlight class="normal" /><highlight class="preprocessor">#if<sp />defined(__cplusplus)</highlight><highlight class="normal" /></codeline>
<codeline lineno="40"><highlight class="normal" /><highlight class="keyword">extern</highlight><highlight class="normal"><sp /></highlight><highlight class="stringliteral">"C"</highlight><highlight class="normal"><sp />{</highlight></codeline>
<codeline lineno="41"><highlight class="normal" /><highlight class="preprocessor">#endif</highlight><highlight class="normal" /></codeline>
<codeline lineno="42"><highlight class="normal" /></codeline>
<codeline lineno="43"><highlight class="normal" /><highlight class="preprocessor">#if<sp />defined(CY_IP_MXSCB_INSTANCES)</highlight><highlight class="normal" /></codeline>
<codeline lineno="44"><highlight class="normal" /><highlight class="preprocessor">#if<sp />defined(CY_DEVICE_PSOC6A256K)</highlight><highlight class="normal" /></codeline>
<codeline lineno="45"><highlight class="normal" /><highlight class="comment">//Special<sp />case<sp />for<sp />256k<sp />device<sp />which<sp />has<sp />6<sp />SCBs<sp />numbered<sp />0,<sp />1,<sp />2,<sp />4,<sp />5,<sp />6</highlight><highlight class="normal" /></codeline>
<codeline lineno="46"><highlight class="normal" /><highlight class="preprocessor">#define<sp />_SCB_ARRAY_SIZE<sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp />(CY_IP_MXSCB_INSTANCES<sp />+<sp />1)</highlight><highlight class="normal" /></codeline>
<codeline lineno="47"><highlight class="normal" /><highlight class="preprocessor">#else</highlight><highlight class="normal" /></codeline>
<codeline lineno="48"><highlight class="normal" /><highlight class="preprocessor">#define<sp />_SCB_ARRAY_SIZE<sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp />(CY_IP_MXSCB_INSTANCES)</highlight><highlight class="normal" /></codeline>
<codeline lineno="49"><highlight class="normal" /><highlight class="preprocessor">#endif<sp /></highlight><highlight class="comment">/*<sp />CY_DEVICE_PSOC6A256K<sp />*/</highlight><highlight class="preprocessor" /><highlight class="normal" /></codeline>
<codeline lineno="50"><highlight class="normal" /><highlight class="preprocessor">#elif<sp />defined(CY_IP_M0S8SCB_INSTANCES)</highlight><highlight class="normal" /></codeline>
<codeline lineno="51"><highlight class="normal" /><highlight class="preprocessor">#define<sp />_SCB_ARRAY_SIZE<sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp />(CY_IP_M0S8SCB_INSTANCES)</highlight><highlight class="normal" /></codeline>
<codeline lineno="52"><highlight class="normal" /><highlight class="preprocessor">#endif<sp /></highlight><highlight class="comment">/*<sp />CY_IP_MXSCB_INSTANCES<sp />*/</highlight><highlight class="preprocessor" /><highlight class="normal" /></codeline>
<codeline lineno="53"><highlight class="normal" /></codeline>
<codeline lineno="55"><highlight class="keyword">extern</highlight><highlight class="normal"><sp />CySCB_Type*<sp /></highlight><highlight class="keyword">const</highlight><highlight class="normal"><sp />_CYHAL_SCB_BASE_ADDRESSES[_SCB_ARRAY_SIZE];</highlight></codeline>
<codeline lineno="57"><highlight class="keyword">extern</highlight><highlight class="normal"><sp /></highlight><highlight class="keyword">const</highlight><highlight class="normal"><sp />IRQn_Type<sp />_CYHAL_SCB_IRQ_N[_SCB_ARRAY_SIZE];</highlight></codeline>
<codeline lineno="58"><highlight class="normal" /></codeline>
<codeline lineno="59"><highlight class="normal" /></codeline>
<codeline lineno="65"><highlight class="normal">uint8_t<sp />cyhal_scb_get_block_from_irqn(IRQn_Type<sp />irqn);</highlight></codeline>
<codeline lineno="66"><highlight class="normal" /></codeline>
<codeline lineno="71"><highlight class="keywordtype">void</highlight><highlight class="normal"><sp />*_cyhal_scb_get_irq_obj(</highlight><highlight class="keywordtype">void</highlight><highlight class="normal">);</highlight></codeline>
<codeline lineno="72"><highlight class="normal" /></codeline>
<codeline lineno="81"><highlight class="normal">uint32_t<sp />_cyhal_i2c_set_peri_divider(CySCB_Type<sp />*base,<sp />uint32_t<sp />block_num,<sp /><ref kindref="compound" refid="structcyhal__clock__t">cyhal_clock_t</ref><sp />*clock,<sp />uint32_t<sp />freq,<sp /></highlight><highlight class="keywordtype">bool</highlight><highlight class="normal"><sp />is_slave);</highlight></codeline>
<codeline lineno="82"><highlight class="normal" /></codeline>
<codeline lineno="89"><highlight class="keyword">const</highlight><highlight class="normal"><sp /><ref kindref="compound" refid="structcyhal__resource__pin__mapping__t">cyhal_resource_pin_mapping_t</ref>*<sp />_cyhal_scb_find_map(<ref kindref="member" refid="group__group__hal__impl__pin__package__psoc6__01__104__m__csp__ble_1ga707195ce0627016bf371643bdd9caa51">cyhal_gpio_t</ref><sp />pin,<sp /></highlight><highlight class="keyword">const</highlight><highlight class="normal"><sp /><ref kindref="compound" refid="structcyhal__resource__pin__mapping__t">cyhal_resource_pin_mapping_t</ref><sp />*pin_map,<sp /></highlight></codeline>
<codeline lineno="90"><highlight class="normal"><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /></highlight><highlight class="keywordtype">size_t</highlight><highlight class="normal"><sp />count,<sp /></highlight><highlight class="keyword">const</highlight><highlight class="normal"><sp /><ref kindref="compound" refid="structcyhal__resource__inst__t">cyhal_resource_inst_t</ref><sp />*block_res);</highlight></codeline>
<codeline lineno="91"><highlight class="normal" /></codeline>
<codeline lineno="92"><highlight class="normal" /><highlight class="preprocessor">#define<sp />_CYHAL_SCB_FIND_MAP(pin,<sp />pin_map)<sp />\</highlight></codeline>
<codeline lineno="93"><highlight class="preprocessor"><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp />_cyhal_scb_find_map(pin,<sp />pin_map,<sp />sizeof(pin_map)/sizeof(cyhal_resource_pin_mapping_t),<sp />NULL)</highlight><highlight class="normal" /></codeline>
<codeline lineno="94"><highlight class="normal" /><highlight class="preprocessor">#define<sp />_CYHAL_SCB_FIND_MAP_BLOCK(pin,<sp />pin_map,<sp />block)<sp />\</highlight></codeline>
<codeline lineno="95"><highlight class="preprocessor"><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp />_cyhal_scb_find_map(pin,<sp />pin_map,<sp />sizeof(pin_map)/sizeof(cyhal_resource_pin_mapping_t),<sp />block)</highlight><highlight class="normal" /></codeline>
<codeline lineno="96"><highlight class="normal" /></codeline>
<codeline lineno="100"><highlight class="keyword">typedef</highlight><highlight class="normal"><sp />bool<sp />(*cyhal_scb_instance_pm_callback)(</highlight><highlight class="keywordtype">void</highlight><highlight class="normal"><sp />*obj_ptr,<sp /><ref kindref="member" refid="group__group__hal__syspm_1ga560aa7873d3131c4eb5d1453711305d2">cyhal_syspm_callback_state_t</ref><sp />state,<sp />cy_en_syspm_callback_mode_t<sp />pdl_mode);</highlight></codeline>
<codeline lineno="101"><highlight class="normal" /></codeline>
<codeline lineno="106"><highlight class="keywordtype">void</highlight><highlight class="normal"><sp />_cyhal_scb_update_instance_data(uint8_t<sp />block_num,<sp /></highlight><highlight class="keywordtype">void</highlight><highlight class="normal"><sp />*obj,<sp />cyhal_scb_instance_pm_callback<sp />pm_callback);</highlight></codeline>
<codeline lineno="107"><highlight class="normal" /></codeline>
<codeline lineno="109"><highlight class="keywordtype">bool</highlight><highlight class="normal"><sp />_cyhal_scb_pm_transition_pending(</highlight><highlight class="keywordtype">void</highlight><highlight class="normal">);</highlight></codeline>
<codeline lineno="110"><highlight class="normal" /></codeline>
<codeline lineno="116"><highlight class="keyword">static</highlight><highlight class="normal"><sp /></highlight><highlight class="keyword">inline</highlight><highlight class="normal"><sp />en_clk_dst_t<sp />_cyhal_scb_get_clock_index(uint32_t<sp />block_num)</highlight></codeline>
<codeline lineno="117"><highlight class="normal">{</highlight></codeline>
<codeline lineno="118"><highlight class="normal"><sp /><sp /><sp /><sp />en_clk_dst_t<sp />clk;</highlight></codeline>
<codeline lineno="119"><highlight class="normal"><sp /><sp /><sp /><sp /></highlight><highlight class="comment">//<sp />PSOC6A256K<sp />does<sp />not<sp />have<sp />SCB<sp />3</highlight><highlight class="normal" /></codeline>
<codeline lineno="120"><highlight class="normal" /><highlight class="preprocessor"><sp /><sp /><sp /><sp />#if<sp />defined(CY_DEVICE_PSOC6A256K)</highlight><highlight class="normal" /></codeline>
<codeline lineno="121"><highlight class="normal"><sp /><sp /><sp /><sp /></highlight><highlight class="keywordflow">if</highlight><highlight class="normal"><sp />(block_num<sp />&lt;<sp />3)</highlight></codeline>
<codeline lineno="122"><highlight class="normal"><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp />clk<sp />=<sp />(en_clk_dst_t)((uint32_t)PCLK_SCB0_CLOCK<sp />+<sp />block_num);</highlight></codeline>
<codeline lineno="123"><highlight class="normal"><sp /><sp /><sp /><sp /></highlight><highlight class="keywordflow">else</highlight><highlight class="normal" /></codeline>
<codeline lineno="124"><highlight class="normal"><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp />clk<sp />=<sp />(en_clk_dst_t)((uint32_t)PCLK_SCB0_CLOCK<sp />+<sp />block_num<sp />-1);</highlight></codeline>
<codeline lineno="125"><highlight class="normal" /><highlight class="preprocessor"><sp /><sp /><sp /><sp />#else</highlight><highlight class="normal" /></codeline>
<codeline lineno="126"><highlight class="normal"><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp />clk<sp />=<sp />(en_clk_dst_t)((uint32_t)PCLK_SCB0_CLOCK<sp />+<sp />block_num);</highlight></codeline>
<codeline lineno="127"><highlight class="normal" /><highlight class="preprocessor"><sp /><sp /><sp /><sp />#endif</highlight><highlight class="normal" /></codeline>
<codeline lineno="128"><highlight class="normal"><sp /><sp /><sp /><sp /></highlight><highlight class="keywordflow">return</highlight><highlight class="normal"><sp />clk;</highlight></codeline>
<codeline lineno="129"><highlight class="normal">}</highlight></codeline>
<codeline lineno="130"><highlight class="normal" /></codeline>
<codeline lineno="131"><highlight class="normal" /><highlight class="preprocessor">#if<sp />defined(__cplusplus)</highlight><highlight class="normal" /></codeline>
<codeline lineno="132"><highlight class="normal">}</highlight></codeline>
<codeline lineno="133"><highlight class="normal" /><highlight class="preprocessor">#endif</highlight><highlight class="normal" /></codeline>
<codeline lineno="134"><highlight class="normal" /></codeline>
    </programlisting>
    <location file="output/libs/COMPONENT_DEPRECATED/psoc6hal/COMPONENT_PSOC6HAL/include/cyhal_scb_common.h" />
  </compounddef>
</doxygen>