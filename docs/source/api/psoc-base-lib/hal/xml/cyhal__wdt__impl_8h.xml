<doxygen xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" version="1.8.14" xsi:noNamespaceSchemaLocation="compound.xsd">
  <compounddef id="cyhal__wdt__impl_8h" kind="file" language="C++">
    <compoundname>cyhal_wdt_impl.h</compoundname>
    <includes local="yes" refid="cyhal__wdt__impl__common_8h">cyhal_wdt_impl_common.h</includes>
    <includedby local="yes" refid="cyhal__wdt_8c">cyhal_wdt.c</includedby>
    <briefdescription>
<para>PSoC 6 specific implementation for WDT API. </para>    </briefdescription>
    <detaileddescription>
<para><simplesect kind="copyright"><para>Copyright 2019-2020 Cypress Semiconductor Corporation SPDX-License-Identifier: Apache-2.0</para></simplesect>
Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with the License. You may obtain a copy of the License at <verbatim>http://www.apache.org/licenses/LICENSE-2.0
</verbatim></para><para>Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the specific language governing permissions and limitations under the License. </para>    </detaileddescription>
    <programlisting>
<codeline lineno="1"><highlight class="comment">/***************************************************************************/</highlight></codeline>
<codeline lineno="25"><highlight class="preprocessor">#pragma<sp />once</highlight><highlight class="normal" /></codeline>
<codeline lineno="26"><highlight class="normal" /></codeline>
<codeline lineno="85"><highlight class="preprocessor">#include<sp />"cyhal_wdt_impl_common.h"</highlight><highlight class="normal" /></codeline>
<codeline lineno="86"><highlight class="normal" /></codeline>
<codeline lineno="87"><highlight class="normal" /><highlight class="preprocessor">#if<sp />defined(__cplusplus)</highlight><highlight class="normal" /></codeline>
<codeline lineno="88"><highlight class="normal" /><highlight class="keyword">extern</highlight><highlight class="normal"><sp /></highlight><highlight class="stringliteral">"C"</highlight><highlight class="normal"><sp />{</highlight></codeline>
<codeline lineno="89"><highlight class="normal" /><highlight class="preprocessor">#endif<sp /></highlight><highlight class="comment">/*<sp />__cplusplus<sp />*/</highlight><highlight class="preprocessor" /><highlight class="normal" /></codeline>
<codeline lineno="90"><highlight class="normal" /></codeline>
<codeline lineno="95"><highlight class="comment">//<sp />ILO<sp />Frequency<sp />=<sp />32768<sp />Hz</highlight><highlight class="normal" /></codeline>
<codeline lineno="96"><highlight class="normal" /><highlight class="comment">//<sp />ILO<sp />Period<sp />=<sp />1<sp />/<sp />32768<sp />Hz<sp />=<sp />.030518<sp />ms</highlight><highlight class="normal" /></codeline>
<codeline lineno="97"><highlight class="normal" /><highlight class="comment">//<sp />WDT<sp />Reset<sp />Period<sp />(timeout_ms)<sp />=<sp />.030518<sp />ms<sp />*<sp />(2<sp />*<sp />2^(16<sp />-<sp />ignore_bits)<sp />+<sp />match)</highlight><highlight class="normal" /></codeline>
<codeline lineno="98"><highlight class="normal" /><highlight class="comment">//<sp />ignore_bits<sp />range:<sp />0<sp />-<sp />12</highlight><highlight class="normal" /></codeline>
<codeline lineno="99"><highlight class="normal" /><highlight class="comment">//<sp />match<sp />range:<sp />0<sp />-<sp />2^(16<sp />-<sp />ignore_bits)</highlight><highlight class="normal" /></codeline>
<codeline lineno="100"><highlight class="normal" /><highlight class="keyword">static</highlight><highlight class="normal"><sp /></highlight><highlight class="keyword">const</highlight><highlight class="normal"><sp />_cyhal_wdt_ignore_bits_data_t<sp />_cyhal_wdt_ignore_data[]<sp />=<sp />{</highlight></codeline>
<codeline lineno="101"><highlight class="normal"><sp /><sp /><sp /><sp />{4000,<sp />3001},<sp /></highlight><highlight class="comment">//<sp /><sp />0<sp />bit(s):<sp />min<sp />period:<sp />4000ms,<sp />max<sp />period:<sp />6000ms,<sp />round<sp />up<sp />from<sp />3001+ms</highlight><highlight class="normal" /></codeline>
<codeline lineno="102"><highlight class="normal"><sp /><sp /><sp /><sp />{2000,<sp />1501},<sp /></highlight><highlight class="comment">//<sp /><sp />1<sp />bit(s):<sp />min<sp />period:<sp />2000ms,<sp />max<sp />period:<sp />3000ms,<sp />round<sp />up<sp />from<sp />1501+ms</highlight><highlight class="normal" /></codeline>
<codeline lineno="103"><highlight class="normal"><sp /><sp /><sp /><sp />{1000,<sp /><sp />751},<sp /></highlight><highlight class="comment">//<sp /><sp />2<sp />bit(s):<sp />min<sp />period:<sp />1000ms,<sp />max<sp />period:<sp />1500ms,<sp />round<sp />up<sp />from<sp />751+ms</highlight><highlight class="normal" /></codeline>
<codeline lineno="104"><highlight class="normal"><sp /><sp /><sp /><sp />{<sp />500,<sp /><sp />376},<sp /></highlight><highlight class="comment">//<sp /><sp />3<sp />bit(s):<sp />min<sp />period:<sp /><sp />500ms,<sp />max<sp />period:<sp /><sp />750ms,<sp />round<sp />up<sp />from<sp />376+ms</highlight><highlight class="normal" /></codeline>
<codeline lineno="105"><highlight class="normal"><sp /><sp /><sp /><sp />{<sp />250,<sp /><sp />188},<sp /></highlight><highlight class="comment">//<sp /><sp />4<sp />bit(s):<sp />min<sp />period:<sp /><sp />250ms,<sp />max<sp />period:<sp /><sp />375ms,<sp />round<sp />up<sp />from<sp />188+ms</highlight><highlight class="normal" /></codeline>
<codeline lineno="106"><highlight class="normal"><sp /><sp /><sp /><sp />{<sp />125,<sp /><sp /><sp />94},<sp /></highlight><highlight class="comment">//<sp /><sp />5<sp />bit(s):<sp />min<sp />period:<sp /><sp />125ms,<sp />max<sp />period:<sp /><sp />187ms,<sp />round<sp />up<sp />from<sp />94+ms</highlight><highlight class="normal" /></codeline>
<codeline lineno="107"><highlight class="normal"><sp /><sp /><sp /><sp />{<sp /><sp />63,<sp /><sp /><sp />47},<sp /></highlight><highlight class="comment">//<sp /><sp />6<sp />bit(s):<sp />min<sp />period:<sp /><sp /><sp />63ms,<sp />max<sp />period:<sp /><sp /><sp />93ms,<sp />round<sp />up<sp />from<sp />47+ms</highlight><highlight class="normal" /></codeline>
<codeline lineno="108"><highlight class="normal"><sp /><sp /><sp /><sp />{<sp /><sp />32,<sp /><sp /><sp />24},<sp /></highlight><highlight class="comment">//<sp /><sp />7<sp />bit(s):<sp />min<sp />period:<sp /><sp /><sp />32ms,<sp />max<sp />period:<sp /><sp /><sp />46ms,<sp />round<sp />up<sp />from<sp />24+ms</highlight><highlight class="normal" /></codeline>
<codeline lineno="109"><highlight class="normal"><sp /><sp /><sp /><sp />{<sp /><sp />16,<sp /><sp /><sp />12},<sp /></highlight><highlight class="comment">//<sp /><sp />8<sp />bit(s):<sp />min<sp />period:<sp /><sp /><sp />16ms,<sp />max<sp />period:<sp /><sp /><sp />23ms,<sp />round<sp />up<sp />from<sp />12+ms</highlight><highlight class="normal" /></codeline>
<codeline lineno="110"><highlight class="normal"><sp /><sp /><sp /><sp />{<sp /><sp /><sp />8,<sp /><sp /><sp /><sp />6},<sp /></highlight><highlight class="comment">//<sp /><sp />9<sp />bit(s):<sp />min<sp />period:<sp /><sp /><sp /><sp />8ms,<sp />max<sp />period:<sp /><sp /><sp />11ms,<sp />round<sp />up<sp />from<sp />6+ms</highlight><highlight class="normal" /></codeline>
<codeline lineno="111"><highlight class="normal"><sp /><sp /><sp /><sp />{<sp /><sp /><sp />4,<sp /><sp /><sp /><sp />3},<sp /></highlight><highlight class="comment">//<sp />10<sp />bit(s):<sp />min<sp />period:<sp /><sp /><sp /><sp />4ms,<sp />max<sp />period:<sp /><sp /><sp /><sp />5ms,<sp />round<sp />up<sp />from<sp />3+ms</highlight><highlight class="normal" /></codeline>
<codeline lineno="112"><highlight class="normal"><sp /><sp /><sp /><sp />{<sp /><sp /><sp />2,<sp /><sp /><sp /><sp />2},<sp /></highlight><highlight class="comment">//<sp />11<sp />bit(s):<sp />min<sp />period:<sp /><sp /><sp /><sp />2ms,<sp />max<sp />period:<sp /><sp /><sp /><sp />2ms,<sp />round<sp />up<sp />from<sp />2+ms</highlight><highlight class="normal" /></codeline>
<codeline lineno="113"><highlight class="normal"><sp /><sp /><sp /><sp />{<sp /><sp /><sp />1,<sp /><sp /><sp /><sp />1},<sp /></highlight><highlight class="comment">//<sp />12<sp />bit(s):<sp />min<sp />period:<sp /><sp /><sp /><sp />1ms,<sp />max<sp />period:<sp /><sp /><sp /><sp />1ms,<sp />round<sp />up<sp />from<sp />1+ms</highlight><highlight class="normal" /></codeline>
<codeline lineno="114"><highlight class="normal">};</highlight></codeline>
<codeline lineno="115"><highlight class="normal" /></codeline>
<codeline lineno="116"><highlight class="normal" /><highlight class="comment">//<sp />(2^16<sp />*<sp />3)<sp />*<sp />.030518<sp />ms</highlight></codeline>
<codeline lineno="118"><highlight class="comment" /><highlight class="preprocessor">#define<sp />_CYHAL_WDT_MAX_TIMEOUT_MS<sp />6000</highlight><highlight class="normal" /></codeline>
<codeline lineno="119"><highlight class="normal" /></codeline>
<codeline lineno="121"><highlight class="preprocessor">#define<sp />_CYHAL_WDT_MAX_IGNORE_BITS<sp />12</highlight><highlight class="normal" /></codeline>
<codeline lineno="122"><highlight class="normal" /></codeline>
<codeline lineno="125"><highlight class="preprocessor">#if<sp />defined(__cplusplus)</highlight><highlight class="normal" /></codeline>
<codeline lineno="126"><highlight class="normal">}</highlight></codeline>
<codeline lineno="127"><highlight class="normal" /><highlight class="preprocessor">#endif<sp /></highlight><highlight class="comment">/*<sp />__cplusplus<sp />*/</highlight><highlight class="preprocessor" /></codeline>
    </programlisting>
    <location file="output/libs/COMPONENT_DEPRECATED/psoc6hal/COMPONENT_PSOC6HAL/include/cyhal_wdt_impl.h" />
  </compounddef>
</doxygen>