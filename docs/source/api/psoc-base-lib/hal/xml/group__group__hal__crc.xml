<doxygen xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" version="1.8.14" xsi:noNamespaceSchemaLocation="compound.xsd">
  <compounddef id="group__group__hal__crc" kind="group">
    <compoundname>group_hal_crc</compoundname>
    <title>CRC (Cyclic Redundancy Check)</title>
    <innerclass prot="public" refid="structcrc__algorithm__t">crc_algorithm_t</innerclass>
    <innergroup refid="group__group__hal__results__crc">CRC HAL Results</innergroup>
      <sectiondef kind="func">
      <memberdef const="no" explicit="no" id="group__group__hal__crc_1gaf7d80ad97b75d1284db96b94274a837a" inline="no" kind="function" prot="public" static="no" virt="non-virtual">
        <type><ref kindref="member" refid="group__group__result_1gaca79700fcc701534ce61778a9bcf57d1">cy_rslt_t</ref></type>
        <definition>cy_rslt_t cyhal_crc_init</definition>
        <argsstring>(cyhal_crc_t *obj)</argsstring>
        <name>cyhal_crc_init</name>
        <param>
          <type><ref kindref="compound" refid="structcyhal__crc__t">cyhal_crc_t</ref> *</type>
          <declname>obj</declname>
        </param>
        <briefdescription>
<para>Initialize the CRC generator. </para>        </briefdescription>
        <detaileddescription>
<para>This function reserves the CRYPTO block for CRC calculations.</para><para><parameterlist kind="param"><parameteritem>
<parameternamelist>
<parametername direction="out">obj</parametername>
</parameternamelist>
<parameterdescription>
<para>Pointer to a CRC generator object. The caller must allocate the memory for this object but the init function will initialize its contents. </para></parameterdescription>
</parameteritem>
</parameterlist>
<simplesect kind="return"><para>The status of the init request.</para></simplesect>
Returns <ref kindref="member" refid="group__group__result_1gaf58fac450d9fff4472f03ad68f6e546e">CY_RSLT_SUCCESS</ref> if the operation was successful. Refer <ref kindref="member" refid="group__group__hal__crc_1subsection_crc_snippet_1">Snippet1: CRC Generation</ref> for more information. </para>        </detaileddescription>
        <inbodydescription>
        </inbodydescription>
        <location column="1" file="output/libs/COMPONENT_DEPRECATED/psoc6hal/include/cyhal_crc.h" line="104" />
      </memberdef>
      <memberdef const="no" explicit="no" id="group__group__hal__crc_1ga13e98c8757355f979fbe955d94763336" inline="no" kind="function" prot="public" static="no" virt="non-virtual">
        <type>void</type>
        <definition>void cyhal_crc_free</definition>
        <argsstring>(cyhal_crc_t *obj)</argsstring>
        <name>cyhal_crc_free</name>
        <param>
          <type><ref kindref="compound" refid="structcyhal__crc__t">cyhal_crc_t</ref> *</type>
          <declname>obj</declname>
        </param>
        <briefdescription>
<para>Release the CRC generator. </para>        </briefdescription>
        <detaileddescription>
<para><parameterlist kind="param"><parameteritem>
<parameternamelist>
<parametername direction="inout">obj</parametername>
</parameternamelist>
<parameterdescription>
<para>The CRC generator object </para></parameterdescription>
</parameteritem>
</parameterlist>
</para>        </detaileddescription>
        <inbodydescription>
        </inbodydescription>
        <location column="1" file="output/libs/COMPONENT_DEPRECATED/psoc6hal/include/cyhal_crc.h" line="110" />
      </memberdef>
      <memberdef const="no" explicit="no" id="group__group__hal__crc_1gac4d388f57a325090b05d718d7f20e333" inline="no" kind="function" prot="public" static="no" virt="non-virtual">
        <type><ref kindref="member" refid="group__group__result_1gaca79700fcc701534ce61778a9bcf57d1">cy_rslt_t</ref></type>
        <definition>cy_rslt_t cyhal_crc_start</definition>
        <argsstring>(cyhal_crc_t *obj, const crc_algorithm_t *algorithm)</argsstring>
        <name>cyhal_crc_start</name>
        <param>
          <type><ref kindref="compound" refid="structcyhal__crc__t">cyhal_crc_t</ref> *</type>
          <declname>obj</declname>
        </param>
        <param>
          <type>const <ref kindref="compound" refid="structcrc__algorithm__t">crc_algorithm_t</ref> *</type>
          <declname>algorithm</declname>
        </param>
        <briefdescription>
<para>The CRC block is setup to perform CRC computation. </para>        </briefdescription>
        <detaileddescription>
<para><parameterlist kind="param"><parameteritem>
<parameternamelist>
<parametername direction="inout">obj</parametername>
</parameternamelist>
<parameterdescription>
<para>The CRC generator object </para></parameterdescription>
</parameteritem>
<parameteritem>
<parameternamelist>
<parametername direction="in">algorithm</parametername>
</parameternamelist>
<parameterdescription>
<para>The CRC algorithm to use for computations Refer <ref kindref="compound" refid="structcrc__algorithm__t">crc_algorithm_t</ref>. </para></parameterdescription>
</parameteritem>
</parameterlist>
<simplesect kind="return"><para>The status of the compute request</para></simplesect>
Returns <ref kindref="member" refid="group__group__result_1gaf58fac450d9fff4472f03ad68f6e546e">CY_RSLT_SUCCESS</ref> if the operation was successful. </para>        </detaileddescription>
        <inbodydescription>
        </inbodydescription>
        <location column="1" file="output/libs/COMPONENT_DEPRECATED/psoc6hal/include/cyhal_crc.h" line="120" />
      </memberdef>
      <memberdef const="no" explicit="no" id="group__group__hal__crc_1ga17e1a6544fecaa60880111d9ad2428f2" inline="no" kind="function" prot="public" static="no" virt="non-virtual">
        <type><ref kindref="member" refid="group__group__result_1gaca79700fcc701534ce61778a9bcf57d1">cy_rslt_t</ref></type>
        <definition>cy_rslt_t cyhal_crc_compute</definition>
        <argsstring>(const cyhal_crc_t *obj, const uint8_t *data, size_t length)</argsstring>
        <name>cyhal_crc_compute</name>
        <param>
          <type>const <ref kindref="compound" refid="structcyhal__crc__t">cyhal_crc_t</ref> *</type>
          <declname>obj</declname>
        </param>
        <param>
          <type>const uint8_t *</type>
          <declname>data</declname>
        </param>
        <param>
          <type>size_t</type>
          <declname>length</declname>
        </param>
        <briefdescription>
<para>Computes the CRC for the given data and accumulates the CRC with the CRC generated from previous calls. </para>        </briefdescription>
        <detaileddescription>
<para>This function can be called multiple times to provide additional data. <verbatim>embed:rst 
.. raw:: html

  &lt;div class="admonition note"&gt;&lt;p class="admonition-title"&gt;note&lt;/p&gt;&lt;p&gt;&lt;p&gt;Input data must be 4-byte aligned. Refer &lt;a href="#group__group__hal__crc_1subsection_crc_snippet_1"&gt;Snippet1: CRC Generation&lt;/a&gt; for more details. &lt;/p&gt;&lt;/p&gt;&lt;/div&gt;</verbatim><parameterlist kind="param"><parameteritem>
<parameternamelist>
<parametername direction="in">obj</parametername>
</parameternamelist>
<parameterdescription>
<para>The CRC generator object </para></parameterdescription>
</parameteritem>
<parameteritem>
<parameternamelist>
<parametername direction="in">data</parametername>
</parameternamelist>
<parameterdescription>
<para>The input data </para></parameterdescription>
</parameteritem>
<parameteritem>
<parameternamelist>
<parametername direction="in">length</parametername>
</parameternamelist>
<parameterdescription>
<para>The number of bytes in the data </para></parameterdescription>
</parameteritem>
</parameterlist>
<simplesect kind="return"><para>The status of the compute request</para></simplesect>
Returns <ref kindref="member" refid="group__group__result_1gaf58fac450d9fff4472f03ad68f6e546e">CY_RSLT_SUCCESS</ref> if the operation was successful. </para>        </detaileddescription>
        <inbodydescription>
        </inbodydescription>
        <location column="1" file="output/libs/COMPONENT_DEPRECATED/psoc6hal/include/cyhal_crc.h" line="132" />
      </memberdef>
      <memberdef const="no" explicit="no" id="group__group__hal__crc_1ga2457c4fa6536551e89c21f9bd476c601" inline="no" kind="function" prot="public" static="no" virt="non-virtual">
        <type><ref kindref="member" refid="group__group__result_1gaca79700fcc701534ce61778a9bcf57d1">cy_rslt_t</ref></type>
        <definition>cy_rslt_t cyhal_crc_finish</definition>
        <argsstring>(const cyhal_crc_t *obj, uint32_t *crc)</argsstring>
        <name>cyhal_crc_finish</name>
        <param>
          <type>const <ref kindref="compound" refid="structcyhal__crc__t">cyhal_crc_t</ref> *</type>
          <declname>obj</declname>
        </param>
        <param>
          <type>uint32_t *</type>
          <declname>crc</declname>
        </param>
        <briefdescription>
<para>Finalizes the CRC computation and returns the CRC for the complete set of data passed through a single call or multiple calls to <ref kindref="member" refid="group__group__hal__crc_1ga17e1a6544fecaa60880111d9ad2428f2">cyhal_crc_compute</ref>. </para>        </briefdescription>
        <detaileddescription>
<para><verbatim>embed:rst 
.. raw:: html

  &lt;div class="admonition note"&gt;&lt;p class="admonition-title"&gt;note&lt;/p&gt;&lt;p&gt;&lt;p&gt;The computed CRC pointer provided must be 4 byte aligned. Refer &lt;a href="#group__group__hal__crc_1subsection_crc_snippet_1"&gt;Snippet1: CRC Generation&lt;/a&gt; for more details.&lt;/p&gt;&lt;/p&gt;&lt;/div&gt;</verbatim><parameterlist kind="param"><parameteritem>
<parameternamelist>
<parametername direction="in">obj</parametername>
</parameternamelist>
<parameterdescription>
<para>The CRC generator object </para></parameterdescription>
</parameteritem>
<parameteritem>
<parameternamelist>
<parametername direction="out">crc</parametername>
</parameternamelist>
<parameterdescription>
<para>The computed CRC </para></parameterdescription>
</parameteritem>
</parameterlist>
<simplesect kind="return"><para>The status of the compute request</para></simplesect>
Returns <ref kindref="member" refid="group__group__result_1gaf58fac450d9fff4472f03ad68f6e546e">CY_RSLT_SUCCESS</ref> if the operation was successful. </para>        </detaileddescription>
        <inbodydescription>
        </inbodydescription>
        <location column="1" file="output/libs/COMPONENT_DEPRECATED/psoc6hal/include/cyhal_crc.h" line="143" />
      </memberdef>
      </sectiondef>
    <briefdescription>
<para>High level interface for interacting with the CRC, which provides hardware accelerated CRC computations. </para>    </briefdescription>
    <detaileddescription>
<para>The CRC APIs are structured to enable usage in situations where the entire input data set is not available in memory at the same time. Therefore, each conversion consists of three steps:<itemizedlist>
<listitem><para>A single call to <ref kindref="member" refid="group__group__hal__crc_1gac4d388f57a325090b05d718d7f20e333">cyhal_crc_start</ref>, to initialize data structures used to compute CRC</para></listitem><listitem><para>One or more calls to <ref kindref="member" refid="group__group__hal__crc_1ga17e1a6544fecaa60880111d9ad2428f2">cyhal_crc_compute</ref>, to provide chunks of data</para></listitem><listitem><para>A single call to <ref kindref="member" refid="group__group__hal__crc_1ga2457c4fa6536551e89c21f9bd476c601">cyhal_crc_finish</ref>, to finalize the computation and retrieve the result</para></listitem></itemizedlist>
</para><para>Many of the algorithm parameters can be customized.</para><para>See <ref kindref="compound" refid="structcrc__algorithm__t">crc_algorithm_t</ref> and <ref kindref="member" refid="group__group__hal__crc_1subsection_crc_snippet_1">Snippet1: CRC Generation</ref> for more details.</para>
<para><heading level="1">Quick Start</heading></para>
<para><ref kindref="member" refid="group__group__hal__crc_1gaf7d80ad97b75d1284db96b94274a837a">cyhal_crc_init</ref> initializes the CRC generator and passes the pointer to the CRC block through the <bold>obj</bold> object of type <ref kindref="compound" refid="structcyhal__crc__t">cyhal_crc_t</ref>.</para>
<para><bold>Snippet1: CRC Generation</bold></para>
<para>The following snippet initializes a CRC generator and computes the CRC for a sample message.</para><para><programlisting filename="crc.c"><codeline><highlight class="normal" /></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /></highlight><highlight class="comment">/*<sp />CRC<sp />parameters<sp />for<sp />some<sp />CRC<sp />algorithms:</highlight></codeline>
<codeline><highlight class="comment"><sp /><sp /><sp /><sp />+---------------------+-----+------------+------------+------+------+-----+------------+------------+</highlight></codeline>
<codeline><highlight class="comment"><sp /><sp /><sp /><sp />|<sp />CRC<sp />algorithm<sp />Name<sp /><sp />|<sp />CRC<sp />|<sp /><sp />Polynom<sp /><sp /><sp />|<sp /><sp /><sp />Initial<sp /><sp />|<sp />Data<sp />|<sp />Data<sp />|<sp />Rem<sp />|<sp /><sp />Remainder<sp />|<sp /><sp />Expected<sp /><sp />|</highlight></codeline>
<codeline><highlight class="comment"><sp /><sp /><sp /><sp />|<sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp />|<sp />len<sp />|<sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp />|<sp /><sp /><sp /><sp />seed<sp /><sp /><sp /><sp />|<sp />REV<sp /><sp />|<sp />XOR<sp /><sp />|<sp />REV<sp />|<sp /><sp /><sp /><sp />XOR<sp /><sp /><sp /><sp /><sp />|<sp /><sp /><sp /><sp />CRC<sp /><sp /><sp /><sp /><sp />|</highlight></codeline>
<codeline><highlight class="comment"><sp /><sp /><sp /><sp />|<sp />-------------------<sp />|<sp />---<sp />|<sp />----------<sp />|-----------<sp />|<sp />----<sp />|<sp />----<sp />|<sp />---<sp />|<sp />----------<sp />|<sp />----------<sp />|</highlight></codeline>
<codeline><highlight class="comment"><sp /><sp /><sp /><sp />|<sp />CRC-6<sp /><sp />/<sp />CDMA2000-A<sp />|<sp /><sp />6<sp /><sp />|<sp /><sp /><sp /><sp />0x27<sp /><sp /><sp /><sp />|<sp /><sp /><sp /><sp />0x3F<sp /><sp /><sp /><sp />|<sp /><sp /><sp />0<sp /><sp />|<sp /><sp /><sp />0<sp /><sp />|<sp /><sp />0<sp /><sp />|<sp /><sp /><sp /><sp />0x00<sp /><sp /><sp /><sp />|<sp /><sp /><sp /><sp />0x0D<sp /><sp /><sp /><sp />|</highlight></codeline>
<codeline><highlight class="comment"><sp /><sp /><sp /><sp />|<sp />CRC-6<sp /><sp />/<sp />CDMA2000-B<sp />|<sp /><sp />6<sp /><sp />|<sp /><sp /><sp /><sp />0x07<sp /><sp /><sp /><sp />|<sp /><sp /><sp /><sp />0x3F<sp /><sp /><sp /><sp />|<sp /><sp /><sp />0<sp /><sp />|<sp /><sp /><sp />0<sp /><sp />|<sp /><sp />0<sp /><sp />|<sp /><sp /><sp /><sp />0x00<sp /><sp /><sp /><sp />|<sp /><sp /><sp /><sp />0x3B<sp /><sp /><sp /><sp />|</highlight></codeline>
<codeline><highlight class="comment"><sp /><sp /><sp /><sp />|<sp />CRC-6<sp /><sp />/<sp />DARC<sp /><sp /><sp /><sp /><sp /><sp /><sp />|<sp /><sp />6<sp /><sp />|<sp /><sp /><sp /><sp />0x19<sp /><sp /><sp /><sp />|<sp /><sp /><sp /><sp />0x00<sp /><sp /><sp /><sp />|<sp /><sp /><sp />1<sp /><sp />|<sp /><sp /><sp />0<sp /><sp />|<sp /><sp />1<sp /><sp />|<sp /><sp /><sp /><sp />0x00<sp /><sp /><sp /><sp />|<sp /><sp /><sp /><sp />0x26<sp /><sp /><sp /><sp />|</highlight></codeline>
<codeline><highlight class="comment"><sp /><sp /><sp /><sp />|<sp />CRC-6<sp /><sp />/<sp />ITU<sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp />|<sp /><sp />6<sp /><sp />|<sp /><sp /><sp /><sp />0x03<sp /><sp /><sp /><sp />|<sp /><sp /><sp /><sp />0x00<sp /><sp /><sp /><sp />|<sp /><sp /><sp />1<sp /><sp />|<sp /><sp /><sp />0<sp /><sp />|<sp /><sp />1<sp /><sp />|<sp /><sp /><sp /><sp />0x00<sp /><sp /><sp /><sp />|<sp /><sp /><sp /><sp />0x06<sp /><sp /><sp /><sp />|</highlight></codeline>
<codeline><highlight class="comment"><sp /><sp /><sp /><sp />|<sp />CRC-8<sp /><sp />/<sp />ITU<sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp />|<sp /><sp />8<sp /><sp />|<sp /><sp /><sp /><sp />0x07<sp /><sp /><sp /><sp />|<sp /><sp /><sp /><sp />0x00<sp /><sp /><sp /><sp />|<sp /><sp /><sp />0<sp /><sp />|<sp /><sp /><sp />0<sp /><sp />|<sp /><sp />0<sp /><sp />|<sp /><sp /><sp /><sp />0x55<sp /><sp /><sp /><sp />|<sp /><sp /><sp /><sp />0xA1<sp /><sp /><sp /><sp />|</highlight></codeline>
<codeline><highlight class="comment"><sp /><sp /><sp /><sp />|<sp />CRC-8<sp /><sp />/<sp />MAXIM<sp /><sp /><sp /><sp /><sp /><sp />|<sp /><sp />8<sp /><sp />|<sp /><sp /><sp /><sp />0x31<sp /><sp /><sp /><sp />|<sp /><sp /><sp /><sp />0x00<sp /><sp /><sp /><sp />|<sp /><sp /><sp />1<sp /><sp />|<sp /><sp /><sp />0<sp /><sp />|<sp /><sp />1<sp /><sp />|<sp /><sp /><sp /><sp />0x00<sp /><sp /><sp /><sp />|<sp /><sp /><sp /><sp />0xA1<sp /><sp /><sp /><sp />|</highlight></codeline>
<codeline><highlight class="comment"><sp /><sp /><sp /><sp />|<sp />CRC-8<sp /><sp />/<sp />ROHC<sp /><sp /><sp /><sp /><sp /><sp /><sp />|<sp /><sp />8<sp /><sp />|<sp /><sp /><sp /><sp />0x07<sp /><sp /><sp /><sp />|<sp /><sp /><sp /><sp />0xFF<sp /><sp /><sp /><sp />|<sp /><sp /><sp />1<sp /><sp />|<sp /><sp /><sp />0<sp /><sp />|<sp /><sp />1<sp /><sp />|<sp /><sp /><sp /><sp />0x00<sp /><sp /><sp /><sp />|<sp /><sp /><sp /><sp />0xD0<sp /><sp /><sp /><sp />|</highlight></codeline>
<codeline><highlight class="comment"><sp /><sp /><sp /><sp />|<sp />CRC-8<sp /><sp />/<sp />WCDMA<sp /><sp /><sp /><sp /><sp /><sp />|<sp /><sp />8<sp /><sp />|<sp /><sp /><sp /><sp />0x9B<sp /><sp /><sp /><sp />|<sp /><sp /><sp /><sp />0x00<sp /><sp /><sp /><sp />|<sp /><sp /><sp />1<sp /><sp />|<sp /><sp /><sp />0<sp /><sp />|<sp /><sp />1<sp /><sp />|<sp /><sp /><sp /><sp />0x00<sp /><sp /><sp /><sp />|<sp /><sp /><sp /><sp />0x25<sp /><sp /><sp /><sp />|</highlight></codeline>
<codeline><highlight class="comment"><sp /><sp /><sp /><sp />|<sp />CRC-16<sp />/<sp />CCITT-0<sp /><sp /><sp /><sp />|<sp />16<sp /><sp />|<sp /><sp /><sp />0x1021<sp /><sp /><sp />|<sp /><sp /><sp />0xFFFF<sp /><sp /><sp />|<sp /><sp /><sp />0<sp /><sp />|<sp /><sp /><sp />0<sp /><sp />|<sp /><sp />0<sp /><sp />|<sp /><sp /><sp />0x0000<sp /><sp /><sp />|<sp /><sp /><sp />0x29B1<sp /><sp /><sp />|</highlight></codeline>
<codeline><highlight class="comment"><sp /><sp /><sp /><sp />|<sp />CRC-16<sp />/<sp />CDMA2000<sp /><sp /><sp />|<sp />16<sp /><sp />|<sp /><sp /><sp />0xC867<sp /><sp /><sp />|<sp /><sp /><sp />0xFFFF<sp /><sp /><sp />|<sp /><sp /><sp />0<sp /><sp />|<sp /><sp /><sp />0<sp /><sp />|<sp /><sp />0<sp /><sp />|<sp /><sp /><sp />0x0000<sp /><sp /><sp />|<sp /><sp /><sp />0x4C06<sp /><sp /><sp />|</highlight></codeline>
<codeline><highlight class="comment"><sp /><sp /><sp /><sp />|<sp />CRC-32<sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp />|<sp />32<sp /><sp />|<sp />0x04C11DB7<sp />|<sp />0xFFFFFFFF<sp />|<sp /><sp /><sp />1<sp /><sp />|<sp /><sp /><sp />0<sp /><sp />|<sp /><sp />1<sp /><sp />|<sp />0xFFFFFFFF<sp />|<sp />0xCBF43926<sp />|</highlight></codeline>
<codeline><highlight class="comment"><sp /><sp /><sp /><sp />|<sp />CRC-32<sp />/<sp />BZIP2<sp /><sp /><sp /><sp /><sp /><sp />|<sp />32<sp /><sp />|<sp />0x04C11DB7<sp />|<sp />0xFFFFFFFF<sp />|<sp /><sp /><sp />0<sp /><sp />|<sp /><sp /><sp />0<sp /><sp />|<sp /><sp />0<sp /><sp />|<sp />0xFFFFFFFF<sp />|<sp />0xFC891918<sp />|</highlight></codeline>
<codeline><highlight class="comment"><sp /><sp /><sp /><sp />+---------------------+-----+------------+------------+------+------+-----+------------+------------+</highlight></codeline>
<codeline><highlight class="comment"><sp /><sp /><sp /><sp />*/</highlight><highlight class="normal" /></codeline>
<codeline><highlight class="normal" /></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /></highlight><highlight class="comment">/*<sp />Using<sp />CRC-16/CCITT-0<sp />Algorithm<sp />*/</highlight><highlight class="normal" /></codeline>
<codeline><highlight class="normal" /><highlight class="preprocessor"><sp /><sp /><sp /><sp />#define<sp />CRC_BITLEN<sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp />(16u)</highlight><highlight class="normal" /></codeline>
<codeline><highlight class="normal" /><highlight class="preprocessor"><sp /><sp /><sp /><sp />#define<sp />CRC_POLYNOMIAL<sp /><sp /><sp /><sp /><sp /><sp />(0x1021u)</highlight><highlight class="normal" /></codeline>
<codeline><highlight class="normal" /><highlight class="preprocessor"><sp /><sp /><sp /><sp />#define<sp />CRC_LFSR_SEED<sp /><sp /><sp /><sp /><sp /><sp /><sp />(0xffffu)</highlight><highlight class="normal" /></codeline>
<codeline><highlight class="normal" /><highlight class="preprocessor"><sp /><sp /><sp /><sp />#define<sp />CRC_DATA_REVERSE<sp /><sp /><sp /><sp />(0u)</highlight><highlight class="normal" /></codeline>
<codeline><highlight class="normal" /><highlight class="preprocessor"><sp /><sp /><sp /><sp />#define<sp />CRC_DATA_XOR<sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp />(0u)</highlight><highlight class="normal" /></codeline>
<codeline><highlight class="normal" /><highlight class="preprocessor"><sp /><sp /><sp /><sp />#define<sp />CRC_REM_REVERSE<sp /><sp /><sp /><sp /><sp />(0u)</highlight><highlight class="normal" /></codeline>
<codeline><highlight class="normal" /><highlight class="preprocessor"><sp /><sp /><sp /><sp />#define<sp />CRC_REM_XOR<sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp />(0x0000u)</highlight><highlight class="normal" /></codeline>
<codeline><highlight class="normal" /></codeline>
<codeline><highlight class="normal" /><highlight class="preprocessor"><sp /><sp /><sp /><sp />#define<sp />MSG_LENGTH<sp />9</highlight><highlight class="normal" /></codeline>
<codeline><highlight class="normal" /></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp />CY_ALIGN(4)<sp />uint8_t<sp />message[MSG_LENGTH]<sp />=<sp />"123456789";</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp />CY_ALIGN(4)<sp />uint32_t<sp />calc_crc<sp />=<sp />0;</highlight></codeline>
<codeline><highlight class="normal" /></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /></highlight><highlight class="comment">/*<sp />Set<sp />CRC<sp />algorithm<sp />parameters<sp />*/</highlight><highlight class="normal" /></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /><ref kindref="compound" refid="structcrc__algorithm__t">crc_algorithm_t</ref><sp />my_algorithm<sp />=<sp />{</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp />.<ref kindref="member" refid="structcrc__algorithm__t_1a43f6798e59b2c14c20cf662a9c8952e6">width</ref><sp />=<sp />CRC_BITLEN,</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp />.polynomial<sp />=<sp />CRC_POLYNOMIAL,</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp />.lfsrInitState<sp />=<sp />CRC_LFSR_SEED,</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp />.dataReverse<sp />=<sp />CRC_DATA_REVERSE,</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp />.dataXor<sp />=<sp />CRC_DATA_XOR,</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp />.remReverse<sp />=<sp />CRC_REM_REVERSE,</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp />.remXor<sp />=<sp />CRC_REM_XOR</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp />};</highlight></codeline>
<codeline><highlight class="normal" /></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /><ref kindref="member" refid="group__group__result_1gaca79700fcc701534ce61778a9bcf57d1">cy_rslt_t</ref><sp />rslt;</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /><ref kindref="compound" refid="structcyhal__crc__t">cyhal_crc_t</ref><sp />crc_obj;</highlight></codeline>
<codeline><highlight class="normal" /></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /></highlight><highlight class="comment">/*<sp />Initialize<sp />the<sp />CRC<sp />Generator<sp />*/</highlight><highlight class="normal" /></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp />rslt<sp />=<sp /><ref kindref="member" refid="group__group__hal__crc_1gaf7d80ad97b75d1284db96b94274a837a">cyhal_crc_init</ref>(&amp;crc_obj);</highlight></codeline>
<codeline><highlight class="normal" /></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /></highlight><highlight class="comment">/*<sp />Initialize<sp />the<sp />CRC<sp />algorithm<sp />parameters<sp />*/</highlight><highlight class="normal" /></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp />rslt<sp />=<sp /><ref kindref="member" refid="group__group__hal__crc_1gac4d388f57a325090b05d718d7f20e333">cyhal_crc_start</ref>(&amp;crc_obj,<sp />&amp;my_algorithm);</highlight></codeline>
<codeline><highlight class="normal" /></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /></highlight><highlight class="comment">/*<sp />Compute<sp />CRC<sp />*/</highlight><highlight class="normal" /></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp />rslt<sp />=<sp /><ref kindref="member" refid="group__group__hal__crc_1ga17e1a6544fecaa60880111d9ad2428f2">cyhal_crc_compute</ref>(&amp;crc_obj,<sp />message,<sp />MSG_LENGTH);</highlight></codeline>
<codeline><highlight class="normal" /></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /></highlight><highlight class="comment">/*<sp />Finish<sp />computation<sp />*/</highlight><highlight class="normal" /></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp />rslt<sp />=<sp /><ref kindref="member" refid="group__group__hal__crc_1ga2457c4fa6536551e89c21f9bd476c601">cyhal_crc_finish</ref>(&amp;crc_obj,<sp />&amp;calc_crc);</highlight></codeline>
<codeline><highlight class="normal" /></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /></highlight><highlight class="comment">/*<sp />Release<sp />the<sp />CRC<sp />generator</highlight></codeline>
<codeline><highlight class="comment"><sp /><sp /><sp /><sp /><sp />*<sp />Note:<sp />Free<sp />only<sp />if<sp />not<sp />required<sp />anymore</highlight></codeline>
<codeline><highlight class="comment"><sp /><sp /><sp /><sp /><sp />*/</highlight><highlight class="normal" /></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /><ref kindref="member" refid="group__group__hal__crc_1ga13e98c8757355f979fbe955d94763336">cyhal_crc_free</ref>(&amp;crc_obj);</highlight></codeline>
<codeline><highlight class="normal" /></codeline>
</programlisting></para>

    </detaileddescription>
  </compounddef>
</doxygen>