<doxygen xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" version="1.8.14" xsi:noNamespaceSchemaLocation="compound.xsd">
  <compounddef id="group__group__hal__hwmgr" kind="group">
    <compoundname>group_hal_hwmgr</compoundname>
    <title>HWMGR (Hardware Manager)</title>
    <innergroup refid="group__group__hal__results__hwmgr">HWMGR HAL Results</innergroup>
      <sectiondef kind="func">
      <memberdef const="no" explicit="no" id="group__group__hal__hwmgr_1ga903814cf5fa0cbc8c55c703a3275628c" inline="no" kind="function" prot="public" static="no" virt="non-virtual">
        <type><ref kindref="member" refid="group__group__result_1gaca79700fcc701534ce61778a9bcf57d1">cy_rslt_t</ref></type>
        <definition>cy_rslt_t cyhal_hwmgr_init</definition>
        <argsstring>(void)</argsstring>
        <name>cyhal_hwmgr_init</name>
        <param>
          <type>void</type>
        </param>
        <briefdescription>
<para>Initializes the hardware manager to keep track of what resources are being used. </para>        </briefdescription>
        <detaileddescription>
<para><simplesect kind="return"><para>The status of the init request </para></simplesect>
</para>        </detaileddescription>
        <inbodydescription>
        </inbodydescription>
        <location bodyend="780" bodyfile="output/libs/COMPONENT_DEPRECATED/psoc6hal/COMPONENT_PSOC6HAL/source/cyhal_hwmgr.c" bodystart="777" column="1" file="output/libs/COMPONENT_DEPRECATED/psoc6hal/include/cyhal_hwmgr.h" line="98" />
      </memberdef>
      <memberdef const="no" explicit="no" id="group__group__hal__hwmgr_1ga33e7c9bb9beaedeb87825dadf604a0b9" inline="no" kind="function" prot="public" static="no" virt="non-virtual">
        <type><ref kindref="member" refid="group__group__result_1gaca79700fcc701534ce61778a9bcf57d1">cy_rslt_t</ref></type>
        <definition>cy_rslt_t cyhal_hwmgr_reserve</definition>
        <argsstring>(const cyhal_resource_inst_t *obj)</argsstring>
        <name>cyhal_hwmgr_reserve</name>
        <param>
          <type>const <ref kindref="compound" refid="structcyhal__resource__inst__t">cyhal_resource_inst_t</ref> *</type>
          <declname>obj</declname>
        </param>
        <briefdescription>
<para>Reserve the specified resource. </para>        </briefdescription>
        <detaileddescription>
<para>The exact block number and channel number must be specified. If this is not know, use <ref kindref="member" refid="group__group__hal__hwmgr_1ga16c02006114bccbf64acbdfd5f2066c2">cyhal_hwmgr_allocate</ref>.</para><para><verbatim>embed:rst 
.. raw:: html

  &lt;div class="admonition note"&gt;&lt;p class="admonition-title"&gt;note&lt;/p&gt;&lt;p&gt;&lt;p&gt;This function is called by the init function of hardware blocks. Calling this again for the same block will result in error since the hardware block is already marked as consumed.&lt;/p&gt;&lt;/p&gt;&lt;/div&gt;</verbatim><parameterlist kind="param"><parameteritem>
<parameternamelist>
<parametername direction="in">obj</parametername>
</parameternamelist>
<parameterdescription>
<para>The resource object that should be reserved </para></parameterdescription>
</parameteritem>
</parameterlist>
<simplesect kind="return"><para>The status of the reserve request</para></simplesect>
See <ref kindref="member" refid="group__group__hal__hwmgr_1subsection_hwmgr_snippet_1">Snippet 1: Freeing a block used by PDL or configurators</ref> </para>        </detaileddescription>
        <inbodydescription>
        </inbodydescription>
        <location bodyend="799" bodyfile="output/libs/COMPONENT_DEPRECATED/psoc6hal/COMPONENT_PSOC6HAL/source/cyhal_hwmgr.c" bodystart="782" column="1" file="output/libs/COMPONENT_DEPRECATED/psoc6hal/include/cyhal_hwmgr.h" line="113" />
      </memberdef>
      <memberdef const="no" explicit="no" id="group__group__hal__hwmgr_1gaa1979bd596d90251c7b861a7e94b884f" inline="no" kind="function" prot="public" static="no" virt="non-virtual">
        <type>void</type>
        <definition>void cyhal_hwmgr_free</definition>
        <argsstring>(const cyhal_resource_inst_t *obj)</argsstring>
        <name>cyhal_hwmgr_free</name>
        <param>
          <type>const <ref kindref="compound" refid="structcyhal__resource__inst__t">cyhal_resource_inst_t</ref> *</type>
          <declname>obj</declname>
        </param>
        <briefdescription>
<para>Free the specified resource to allow it to be reused. </para>        </briefdescription>
        <detaileddescription>
<para><parameterlist kind="param"><parameteritem>
<parameternamelist>
<parametername direction="inout">obj</parametername>
</parameternamelist>
<parameterdescription>
<para>The resource object to free</para></parameterdescription>
</parameteritem>
</parameterlist>
See <ref kindref="member" refid="group__group__hal__hwmgr_1subsection_hwmgr_snippet_1">Snippet 1: Freeing a block used by PDL or configurators</ref> </para>        </detaileddescription>
        <inbodydescription>
        </inbodydescription>
        <location bodyend="808" bodyfile="output/libs/COMPONENT_DEPRECATED/psoc6hal/COMPONENT_PSOC6HAL/source/cyhal_hwmgr.c" bodystart="801" column="1" file="output/libs/COMPONENT_DEPRECATED/psoc6hal/include/cyhal_hwmgr.h" line="121" />
      </memberdef>
      <memberdef const="no" explicit="no" id="group__group__hal__hwmgr_1ga16c02006114bccbf64acbdfd5f2066c2" inline="no" kind="function" prot="public" static="no" virt="non-virtual">
        <type><ref kindref="member" refid="group__group__result_1gaca79700fcc701534ce61778a9bcf57d1">cy_rslt_t</ref></type>
        <definition>cy_rslt_t cyhal_hwmgr_allocate</definition>
        <argsstring>(cyhal_resource_t type, cyhal_resource_inst_t *obj)</argsstring>
        <name>cyhal_hwmgr_allocate</name>
        <param>
          <type><ref kindref="member" refid="group__group__hal__impl__hw__types_1ga63b023ea7b5e73db59ddc0423c77975b">cyhal_resource_t</ref></type>
          <declname>type</declname>
        </param>
        <param>
          <type><ref kindref="compound" refid="structcyhal__resource__inst__t">cyhal_resource_inst_t</ref> *</type>
          <declname>obj</declname>
        </param>
        <briefdescription>
<para>Allocates a free block of the specified type if available This function is used when the exact block number and channel number of the resource is not known. </para>        </briefdescription>
        <detaileddescription>
<para>This function loops through all available resource of the specified type and assigns the resource if available. This function internally calls <ref kindref="member" refid="group__group__hal__hwmgr_1ga33e7c9bb9beaedeb87825dadf604a0b9">cyhal_hwmgr_reserve</ref> function and hence, it should not be called again.</para><para><parameterlist kind="param"><parameteritem>
<parameternamelist>
<parametername direction="in">type</parametername>
</parameternamelist>
<parameterdescription>
<para>The type of resource to allocate. </para></parameterdescription>
</parameteritem>
<parameteritem>
<parameternamelist>
<parametername direction="out">obj</parametername>
</parameternamelist>
<parameterdescription>
<para>The resource object. </para></parameterdescription>
</parameteritem>
</parameterlist>
<simplesect kind="return"><para>The status of the allocate request. </para></simplesect>
</para>        </detaileddescription>
        <inbodydescription>
        </inbodydescription>
        <location bodyend="855" bodyfile="output/libs/COMPONENT_DEPRECATED/psoc6hal/COMPONENT_PSOC6HAL/source/cyhal_hwmgr.c" bodystart="810" column="1" file="output/libs/COMPONENT_DEPRECATED/psoc6hal/include/cyhal_hwmgr.h" line="135" />
      </memberdef>
      </sectiondef>
    <briefdescription>
<para>High level interface to the Hardware Manager. </para>    </briefdescription>
    <detaileddescription>

<para><heading level="1">Features</heading></para>
<para><itemizedlist>
<listitem><para>Allows HAL drivers or application firmware to mark specific hardware resources as reserved. When this is done, other reservation requests for the same resource will be denied.</para></listitem><listitem><para>For resources which are interchangeable such as clock dividers, provides allocation and reservation of an available instance.</para></listitem></itemizedlist>
</para>

<para><heading level="1">Quick Start</heading></para>
<para><itemizedlist>
<listitem><para><ref kindref="member" refid="group__group__hal__hwmgr_1ga903814cf5fa0cbc8c55c703a3275628c">cyhal_hwmgr_init</ref> is used to initialize the hardware manager to keep track of resources being used.</para></listitem><listitem><para><ref kindref="member" refid="group__group__hal__hwmgr_1ga33e7c9bb9beaedeb87825dadf604a0b9">cyhal_hwmgr_reserve</ref> reserves a specified resource if available.</para></listitem><listitem><para><ref kindref="member" refid="group__group__hal__hwmgr_1gaa1979bd596d90251c7b861a7e94b884f">cyhal_hwmgr_free</ref> frees the specified resource.</para></listitem><listitem><para><ref kindref="member" refid="group__group__hal__hwmgr_1ga16c02006114bccbf64acbdfd5f2066c2">cyhal_hwmgr_allocate</ref> can be used to allocate a free block of specified resource, if available.</para></listitem></itemizedlist>
</para>

<para><heading level="1">Code snippets</heading></para>

<para><bold>Snippet 1: Freeing a block used by PDL or configurators</bold></para>
<para>The following snippet shows how a specific resource used directly in PDL or the configurators can be freed so that it can be used by HAL.<linebreak />
</para><para><programlisting filename="hw_mgr.c"><codeline><highlight class="normal"><sp /><sp /><sp /><sp /></highlight><highlight class="comment">/*<sp />The<sp />resource<sp />that<sp />is<sp />being<sp />used<sp />*/</highlight><highlight class="normal" /></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /><ref kindref="compound" refid="structcyhal__resource__inst__t">cyhal_resource_inst_t</ref><sp />resource<sp />=</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp />{</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp />.<ref kindref="member" refid="structcyhal__resource__inst__t_1ad111187a65d182386ea96858369b83d4">type</ref><sp />=<sp /><ref kindref="member" refid="group__group__hal__impl__hw__types_1gga63b023ea7b5e73db59ddc0423c77975baa445456a2983f23d4be55613f2d09eb6">CYHAL_RSC_TCPWM</ref>,</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp />.block_num<sp />=<sp />0,</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp />.channel_num<sp />=<sp />3</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp />};</highlight></codeline>
<codeline><highlight class="normal" /></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /></highlight><highlight class="comment">/*<sp />Use<sp />resource<sp />directly<sp />in<sp />PDL<sp />or<sp />configurators<sp />*/</highlight><highlight class="normal" /></codeline>
<codeline><highlight class="normal" /></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /></highlight><highlight class="comment">/*<sp />If<sp />the<sp />resource<sp />is<sp />no<sp />longer<sp />needed<sp />and<sp />can<sp />be<sp />used<sp />by<sp />the<sp />HAL,<sp />free<sp />the<sp />resource<sp />in<sp />Hardware<sp />Manager<sp />*/</highlight><highlight class="normal" /></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /><ref kindref="member" refid="group__group__hal__hwmgr_1gaa1979bd596d90251c7b861a7e94b884f">cyhal_hwmgr_free</ref>(&amp;resource);</highlight></codeline>
<codeline><highlight class="normal" /></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /></highlight><highlight class="comment">/*<sp />Disconnect<sp />any<sp />interconnects<sp />used<sp />between<sp />resources<sp />and<sp />pin<sp />*/</highlight><highlight class="normal" /></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp />_cyhal_utils_disconnect_and_free(<ref kindref="member" refid="group__group__hal__impl__pin__package__psoc6__01__104__m__csp__ble_1gga707195ce0627016bf371643bdd9caa51a23d505c049c81443b12e53d5b00b1be9">P0_0</ref>);</highlight></codeline>
<codeline><highlight class="normal" /></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /></highlight><highlight class="comment">/*<sp />resource<sp />can<sp />now<sp />be<sp />used<sp />by<sp />HAL<sp />*/</highlight><highlight class="normal" /></codeline>
<codeline><highlight class="normal" /></codeline>
</programlisting></para>

    </detaileddescription>
  </compounddef>
</doxygen>