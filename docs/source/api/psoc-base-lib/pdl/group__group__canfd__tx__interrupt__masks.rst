===================
Tx Interrupt masks
===================

.. doxygengroup:: group_canfd_tx_interrupt_masks
   :project: pdl
   :members:
   :protected-members:
   :private-members:
   :undoc-members: