========================
Common Data Structures
========================


.. doxygengroup:: group_crypto_data_structures
   :project: pdl
   :members:
   :protected-members:
   :private-members:
   :undoc-members: