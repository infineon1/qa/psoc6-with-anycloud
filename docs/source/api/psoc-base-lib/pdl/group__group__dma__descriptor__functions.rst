=====================
Descriptor Functions
=====================


.. doxygengroup:: group_dma_descriptor_functions
   :project: pdl
   :members:
   :protected-members:
   :private-members:
   :undoc-members: