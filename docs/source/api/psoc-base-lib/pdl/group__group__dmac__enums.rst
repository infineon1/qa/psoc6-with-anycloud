=================
Enumerated Types
=================



.. doxygengroup:: group_dmac_enums
   :project: pdl
   :members:
   :protected-members:
   :private-members:
   :undoc-members: