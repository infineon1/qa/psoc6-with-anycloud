========
Macros
========



.. toctree::
   
   group__group__dmac__macros__interrupt__masks.rst 
   
   

.. doxygengroup:: group_dmac_macros
   :project: pdl
   :members:
   :protected-members:
   :private-members:
   :undoc-members: