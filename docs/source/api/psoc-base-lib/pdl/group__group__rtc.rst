======================
RTC (Real-Time Clock)
======================

.. doxygengroup:: group_rtc
   :project: pdl

.. toctree::
   
   group__group__rtc__macros.rst
   group__group__rtc__functions.rst
   group__group__rtc__data__structures.rst
   group__group__rtc__enums.rst
   
   
   
