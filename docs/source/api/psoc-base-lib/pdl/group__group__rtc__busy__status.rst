=======================
RTC Status definitions
=======================



.. doxygengroup:: group_rtc_busy_status
   :project: pdl
   :members:
   :protected-members:
   :private-members:
   :undoc-members: