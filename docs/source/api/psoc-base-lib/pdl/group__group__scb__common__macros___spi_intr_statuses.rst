=======================
SPI Interrupt Statuses
=======================




.. doxygengroup:: group_scb_common_macros_SpiIntrStatuses
   :project: pdl
   :members:
   :protected-members:
   :private-members:
   :undoc-members: