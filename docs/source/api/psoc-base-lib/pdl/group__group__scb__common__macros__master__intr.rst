==========================
Master Interrupt Statuses
==========================




.. doxygengroup:: group_scb_common_macros_master_intr
   :project: pdl
   :members:
   :protected-members:
   :private-members:
   :undoc-members: