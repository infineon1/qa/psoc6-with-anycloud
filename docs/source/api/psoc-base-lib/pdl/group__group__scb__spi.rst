==========
SPI (SCB)
==========

.. doxygengroup:: group_scb_spi 
   :project: pdl 
   :members:


.. toctree::

   group__group__scb__spi__macros.rst
   group__group__scb__spi__functions.rst
   group__group__scb__spi__data__structures.rst
   group__group__scb__spi__enums.rst

