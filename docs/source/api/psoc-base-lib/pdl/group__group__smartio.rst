===================
SmartIO (Smart I/O)
===================

.. doxygengroup:: group_smartio
   :project: pdl
   

.. toctree::

   group__group__smartio__macros.rst
   group__group__smartio__functions.rst
   group__group__smartio__data__structures.rst
   group__group__smartio__enums.rst
   


