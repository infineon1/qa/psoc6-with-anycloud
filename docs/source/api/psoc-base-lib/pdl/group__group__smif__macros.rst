=======
Macros
=======

.. doxygengroup:: group_smif_macros
   :project: pdl
   :members:
   :protected-members:
   :private-members:
   :undoc-members:
   

API Reference
^^^^^^^^^^^^^^

.. toctree::

   group__group__smif__macros__status.rst
   group__group__smif__macros__cmd.rst
   group__group__smif__macros__flags.rst
   group__group__smif__macros__sfdp.rst
   group__group__smif__macros__isr.rst