=================
Enumerated Types
=================

.. doxygengroup:: group_sysclk_calclk_enums
   :project: pdl
   :members:
   :protected-members:
   :private-members:
   :undoc-members: