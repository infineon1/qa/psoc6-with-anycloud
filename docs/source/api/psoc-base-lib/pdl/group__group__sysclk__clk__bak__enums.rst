================
Enumerated Types
================

.. doxygengroup:: group_sysclk_clk_bak_enums
   :project: pdl
   :members:
   :protected-members:
   :private-members:
   :undoc-members: