============
Timer Clock
============

.. doxygengroup:: group_sysclk_clk_timer
   :project: pdl
   :members:
   :protected-members:
   :private-members:
   :undoc-members:
   

API Reference
==============

.. toctree::

   group__group__sysclk__clk__timer__funcs.rst
   group__group__sysclk__clk__timer__enums.rst