=======
Macros
=======

.. doxygengroup:: group_syslib_macros
   :project: pdl
   :members:
   :protected-members:
   :private-members:
   :undoc-members:
   

API Referencence
^^^^^^^^^^^^^^
.. toctree::

   group__group__syslib__macros__status__codes.rst
   group__group__syslib__macros__assert.rst
   group__group__syslib__macros__reset__cause.rst
   group__group__syslib__macros__unique__id.rst