==========
Functions
==========

.. doxygengroup:: group_syspm_functions
   :project: pdl
   :members:
   :protected-members:
   :private-members:
   :undoc-members:
   
   
API Reference
^^^^^^^^^^^^^^
.. toctree::

   group__group__syspm__functions__general.rst
   group__group__syspm__functions__power.rst
   group__group__syspm__functions__power__status.rst
   group__group__syspm__functions__iofreeze.rst
   group__group__syspm__functions__core__regulators.rst
   group__group__syspm__functions__pmic.rst
   group__group__syspm__functions__backup.rst
   group__group__syspm__functions__callback.rst