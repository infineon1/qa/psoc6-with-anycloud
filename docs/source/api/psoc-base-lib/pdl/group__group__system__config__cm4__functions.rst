==================
Cortex-M4 Control
==================

.. doxygengroup:: group_system_config_cm4_functions
   :project: pdl
   :members:
   :protected-members:
   :private-members:
   :undoc-members: