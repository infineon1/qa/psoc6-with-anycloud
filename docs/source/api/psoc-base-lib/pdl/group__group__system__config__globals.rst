=================
Global Variables
=================

.. doxygengroup:: group_system_config_globals
   :project: pdl
   :members:
   :protected-members:
   :private-members:
   :undoc-members: