=====================
Timer/Counter (TCPWM)
=====================

.. doxygengroup:: group_tcpwm_counter
   :project: pdl
   :members:
   :protected-members:
   :private-members:
   :undoc-members:
   

API Reference
^^^^^^^^^^^^^^
.. toctree::

   group__group__tcpwm__macros__counter.rst
   group__group__tcpwm__functions__counter.rst
   group__group__tcpwm__data__structures__counter.rst