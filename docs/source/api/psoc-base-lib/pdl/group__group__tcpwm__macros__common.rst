=======
Macros
=======
API Reference
^^^^^^^^^^^^^^
.. toctree::

   group__group__tcpwm__input__selection.rst
   group__group__tcpwm__input__modes.rst
   group__group__tcpwm__output__trigger__modes.rst
   group__group__tcpwm__interrupt__sources.rst   
   group__group__tcpwm__reg__const.rst 

.. doxygengroup:: group_tcpwm_macros_common
   :project: pdl
   :members:
   :protected-members:
   :private-members:
   :undoc-members:


  