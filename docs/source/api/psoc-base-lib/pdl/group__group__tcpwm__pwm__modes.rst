==========
PWM modes
==========

.. doxygengroup:: group_tcpwm_pwm_modes
   :project: pdl
   :members:
   :protected-members:
   :private-members:
   :undoc-members: