==============
PWM run modes
==============

.. doxygengroup:: group_tcpwm_pwm_run_modes
   :project: pdl
   :members:
   :protected-members:
   :private-members:
   :undoc-members:
