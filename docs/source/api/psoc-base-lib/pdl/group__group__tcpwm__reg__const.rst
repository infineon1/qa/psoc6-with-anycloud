============================
Default registers constants
============================

.. doxygengroup:: group_tcpwm_reg_const
   :project: pdl
   :members:
   :protected-members:
   :private-members:
   :undoc-members: