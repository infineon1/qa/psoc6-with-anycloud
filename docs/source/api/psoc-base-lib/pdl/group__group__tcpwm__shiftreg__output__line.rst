============================
Shift Register Output Lines
============================

.. doxygengroup:: group_tcpwm_shiftreg_output_line
   :project: pdl
   :members:
   :protected-members:
   :private-members:
   :undoc-members:
   