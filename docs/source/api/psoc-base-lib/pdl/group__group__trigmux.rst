=============================
TrigMux (Trigger Multiplexer)
=============================

.. doxygengroup:: group_trigmux
   :project: pdl
   

.. toctree::

   group__group__trigmux__macros.rst
   group__group__trigmux__functions.rst
   group__group__trigmux__enums.rst
   


