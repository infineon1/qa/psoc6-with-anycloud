=======================================
Distribution Trigger Mutiplexer Inputs
=======================================

.. doxygengroup:: group_trigmux_dst_in_enums
   :project: pdl
   :members:
   :protected-members:
   :private-members:
   :undoc-members: