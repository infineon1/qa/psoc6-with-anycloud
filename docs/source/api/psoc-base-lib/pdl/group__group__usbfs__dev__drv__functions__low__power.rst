====================
Low Power Functions
====================

.. doxygengroup:: group_usbfs_dev_drv_functions_low_power
   :project: pdl
   :members:
   :protected-members:
   :private-members:
   :undoc-members: