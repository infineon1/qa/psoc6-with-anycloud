=========================
Initialization Functions
=========================

.. doxygengroup:: group_usbfs_dev_hal_functions_common
   :project: pdl
   :members:
   :protected-members:
   :private-members:
   :undoc-members: