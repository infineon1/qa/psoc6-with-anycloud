<doxygen xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" version="1.8.13" xsi:noNamespaceSchemaLocation="compound.xsd">
  <compounddef id="cy__crypto__core__trng__v1_8h" kind="file" language="C++">
    <compoundname>cy_crypto_core_trng_v1.h</compoundname>
    <includes local="yes" refid="cy__crypto__common_8h">cy_crypto_common.h</includes>
    <includedby local="yes" refid="cy__crypto__core__trng_8h">cy_crypto_core_trng.h</includedby>
    <includedby local="yes" refid="cy__crypto__core__trng__v1_8c">cy_crypto_core_trng_v1.c</includedby>
      <sectiondef kind="func">
      <memberdef const="no" explicit="no" id="cy__crypto__core__trng__v1_8h_1adcd2d85977501b67c1d4e82b67bb036f" inline="no" kind="function" prot="public" static="no" virt="non-virtual">
        <type><ref kindref="member" refid="group__group__crypto__enums_1ga3a65158da3dc5cb6aa016ae599542aad">cy_en_crypto_status_t</ref></type>
        <definition>cy_en_crypto_status_t Cy_Crypto_Core_V1_Trng</definition>
        <argsstring>(CRYPTO_Type *base, uint32_t GAROPol, uint32_t FIROPol, uint32_t max, uint32_t *randomNum)</argsstring>
        <name>Cy_Crypto_Core_V1_Trng</name>
        <param>
          <type>CRYPTO_Type *</type>
          <declname>base</declname>
        </param>
        <param>
          <type>uint32_t</type>
          <declname>GAROPol</declname>
        </param>
        <param>
          <type>uint32_t</type>
          <declname>FIROPol</declname>
        </param>
        <param>
          <type>uint32_t</type>
          <declname>max</declname>
        </param>
        <param>
          <type>uint32_t *</type>
          <declname>randomNum</declname>
        </param>
        <briefdescription>
<para>Generates a True Random Number. </para>        </briefdescription>
        <detaileddescription>
<para><parameterlist kind="param"><parameteritem>
<parameternamelist>
<parametername>base</parametername>
</parameternamelist>
<parameterdescription>
<para>The pointer to the CRYPTO instance.</para></parameterdescription>
</parameteritem>
<parameteritem>
<parameternamelist>
<parametername>GAROPol</parametername>
</parameternamelist>
<parameterdescription>
<para>The polynomial for the programmable Galois ring oscillator.</para></parameterdescription>
</parameteritem>
<parameteritem>
<parameternamelist>
<parametername>FIROPol</parametername>
</parameternamelist>
<parameterdescription>
<para>The polynomial for the programmable Fibonacci ring oscillator.</para></parameterdescription>
</parameteritem>
<parameteritem>
<parameternamelist>
<parametername>max</parametername>
</parameternamelist>
<parameterdescription>
<para>The maximum length of a random number, in the range [0, 32] bits.</para></parameterdescription>
</parameteritem>
<parameteritem>
<parameternamelist>
<parametername>randomNum</parametername>
</parameternamelist>
<parameterdescription>
<para>The pointer to a generated true random number. Must be 4-byte aligned.</para></parameterdescription>
</parameteritem>
</parameterlist>
<simplesect kind="return"><para><ref kindref="member" refid="group__group__crypto__enums_1ga3a65158da3dc5cb6aa016ae599542aad">cy_en_crypto_status_t</ref> </para></simplesect>
</para>        </detaileddescription>
        <inbodydescription>
        </inbodydescription>
        <location bodyend="132" bodyfile="/var/tmp/gitlab-runner1/builds/7e5c9300/0/repo/psoc6pdl/drivers/source/cy_crypto_core_trng_v1.c" bodystart="104" column="1" file="/var/tmp/gitlab-runner1/builds/7e5c9300/0/repo/psoc6pdl/drivers/include/cy_crypto_core_trng_v1.h" line="40" />
      </memberdef>
      </sectiondef>
    <briefdescription>
<para>This file provides provides constant and parameters for the API of the TRNG in the Crypto block driver. </para>    </briefdescription>
    <detaileddescription>
<para><simplesect kind="version"><para>2.30.3</para></simplesect>
Copyright 2016-2020 Cypress Semiconductor Corporation SPDX-License-Identifier: Apache-2.0</para><para>Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with the License. You may obtain a copy of the License at</para><para><ulink url="http://www.apache.org/licenses/LICENSE-2.0">http://www.apache.org/licenses/LICENSE-2.0</ulink></para><para>Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the specific language governing permissions and limitations under the License. </para>    </detaileddescription>
    <programlisting>
<codeline lineno="1"><highlight class="comment">/***************************************************************************/</highlight></codeline>
<codeline lineno="27"><highlight class="preprocessor">#if<sp />!defined(CY_CRYPTO_CORE_TRNG_V1_H)</highlight><highlight class="normal" /></codeline>
<codeline lineno="28"><highlight class="normal" /><highlight class="preprocessor">#define<sp />CY_CRYPTO_CORE_TRNG_V1_H</highlight><highlight class="normal" /></codeline>
<codeline lineno="29"><highlight class="normal" /></codeline>
<codeline lineno="30"><highlight class="normal" /><highlight class="preprocessor">#include<sp />"cy_crypto_common.h"</highlight><highlight class="normal" /></codeline>
<codeline lineno="31"><highlight class="normal" /></codeline>
<codeline lineno="32"><highlight class="normal" /><highlight class="preprocessor">#if<sp />defined(CY_IP_MXCRYPTO)</highlight><highlight class="normal" /></codeline>
<codeline lineno="33"><highlight class="normal" /></codeline>
<codeline lineno="34"><highlight class="normal" /><highlight class="preprocessor">#if<sp />defined(__cplusplus)</highlight><highlight class="normal" /></codeline>
<codeline lineno="35"><highlight class="normal" /><highlight class="keyword">extern</highlight><highlight class="normal"><sp /></highlight><highlight class="stringliteral">"C"</highlight><highlight class="normal"><sp />{</highlight></codeline>
<codeline lineno="36"><highlight class="normal" /><highlight class="preprocessor">#endif</highlight><highlight class="normal" /></codeline>
<codeline lineno="37"><highlight class="normal" /></codeline>
<codeline lineno="38"><highlight class="normal" /><highlight class="preprocessor">#if<sp />(CPUSS_CRYPTO_TR<sp />==<sp />1)</highlight><highlight class="normal" /></codeline>
<codeline lineno="39"><highlight class="normal" /></codeline>
<codeline lineno="40"><highlight class="normal"><ref kindref="member" refid="group__group__crypto__enums_1ga3a65158da3dc5cb6aa016ae599542aad">cy_en_crypto_status_t</ref><sp />Cy_Crypto_Core_V1_Trng(CRYPTO_Type<sp />*base,</highlight></codeline>
<codeline lineno="41"><highlight class="normal"><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp />uint32_t<sp /><sp />GAROPol,</highlight></codeline>
<codeline lineno="42"><highlight class="normal"><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp />uint32_t<sp /><sp />FIROPol,</highlight></codeline>
<codeline lineno="43"><highlight class="normal"><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp />uint32_t<sp /><sp />max,</highlight></codeline>
<codeline lineno="44"><highlight class="normal"><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp />uint32_t<sp />*randomNum);</highlight></codeline>
<codeline lineno="45"><highlight class="normal" /></codeline>
<codeline lineno="46"><highlight class="normal" /></codeline>
<codeline lineno="47"><highlight class="normal" /><highlight class="preprocessor">#endif<sp /></highlight><highlight class="comment">/*<sp />#if<sp />(CPUSS_CRYPTO_TR<sp />==<sp />1)<sp />*/</highlight><highlight class="preprocessor" /><highlight class="normal" /></codeline>
<codeline lineno="48"><highlight class="normal" /></codeline>
<codeline lineno="49"><highlight class="normal" /><highlight class="preprocessor">#if<sp />defined(__cplusplus)</highlight><highlight class="normal" /></codeline>
<codeline lineno="50"><highlight class="normal">}</highlight></codeline>
<codeline lineno="51"><highlight class="normal" /><highlight class="preprocessor">#endif</highlight><highlight class="normal" /></codeline>
<codeline lineno="52"><highlight class="normal" /></codeline>
<codeline lineno="53"><highlight class="normal" /><highlight class="preprocessor">#endif<sp /></highlight><highlight class="comment">/*<sp />CY_IP_MXCRYPTO<sp />*/</highlight><highlight class="preprocessor" /><highlight class="normal" /></codeline>
<codeline lineno="54"><highlight class="normal" /></codeline>
<codeline lineno="55"><highlight class="normal" /><highlight class="preprocessor">#endif<sp /></highlight><highlight class="comment">/*<sp />#if<sp />!defined(CY_CRYPTO_CORE_TRNG_V1_H)<sp />*/</highlight><highlight class="preprocessor" /><highlight class="normal" /></codeline>
<codeline lineno="56"><highlight class="normal" /></codeline>
<codeline lineno="57"><highlight class="normal" /></codeline>
<codeline lineno="58"><highlight class="normal" /><highlight class="comment">/*<sp />[]<sp />END<sp />OF<sp />FILE<sp />*/</highlight><highlight class="normal" /></codeline>
    </programlisting>
    <location file="/var/tmp/gitlab-runner1/builds/7e5c9300/0/repo/psoc6pdl/drivers/include/cy_crypto_core_trng_v1.h" />
  </compounddef>
</doxygen>