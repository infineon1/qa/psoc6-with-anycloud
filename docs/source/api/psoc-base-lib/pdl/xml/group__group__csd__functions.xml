<doxygen xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" version="1.8.13" xsi:noNamespaceSchemaLocation="compound.xsd">
  <compounddef id="group__group__csd__functions" kind="group">
    <compoundname>group_csd_functions</compoundname>
    <title>Functions</title>
      <sectiondef kind="func">
      <memberdef const="no" explicit="no" id="group__group__csd__functions_1ga22ab53f2e99efe2990b5c99c06dc0be7" inline="no" kind="function" prot="public" static="no" virt="non-virtual">
        <type><ref kindref="member" refid="group__group__csd__enums_1ga6cde526987c66fff895f48902589f979">cy_en_csd_status_t</ref></type>
        <definition>cy_en_csd_status_t Cy_CSD_Init</definition>
        <argsstring>(CSD_Type *base, cy_stc_csd_config_t const *config, cy_en_csd_key_t key, cy_stc_csd_context_t *context)</argsstring>
        <name>Cy_CSD_Init</name>
        <param>
          <type>CSD_Type *</type>
          <declname>base</declname>
        </param>
        <param>
          <type><ref kindref="compound" refid="structcy__stc__csd__config__t">cy_stc_csd_config_t</ref> const *</type>
          <declname>config</declname>
        </param>
        <param>
          <type><ref kindref="member" refid="group__group__csd__enums_1ga281a1b4a5cddb0bee365a7835985ccba">cy_en_csd_key_t</ref></type>
          <declname>key</declname>
        </param>
        <param>
          <type><ref kindref="compound" refid="structcy__stc__csd__context__t">cy_stc_csd_context_t</ref> *</type>
          <declname>context</declname>
        </param>
        <briefdescription>
<para>Acquires and locks the CSD HW block and configures it. </para>        </briefdescription>
        <detaileddescription>
<para>If the CSD HW block is already in use by other middleware or by the application program, then the function returns the CY_CSD_LOCKED status and does not configure the CSD HW block.</para><para>In case of successful acquisition, this function writes configuration data into all CSD HW block registers (except read-only registers and SEQ_START register) at once. Because the SEQ_START register is excluded from write, use the <ref kindref="member" refid="group__group__csd__functions_1ga3689a475c33996ff4cb5e8c24d1194c1">Cy_CSD_WriteReg()</ref> function to trigger the state machine for scan or conversion.</para><para><parameterlist kind="param"><parameteritem>
<parameternamelist>
<parametername>base</parametername>
</parameternamelist>
<parameterdescription>
<para>Pointer to a CSD HW block base address.</para></parameterdescription>
</parameteritem>
<parameteritem>
<parameternamelist>
<parametername>config</parametername>
</parameternamelist>
<parameterdescription>
<para>The pointer to a configuration structure that contains the initial configuration.</para></parameterdescription>
</parameteritem>
<parameteritem>
<parameternamelist>
<parametername>key</parametername>
</parameternamelist>
<parameterdescription>
<para>An ID of middleware or user-level function that is going to work with the specified CSD HW block.</para></parameterdescription>
</parameteritem>
<parameteritem>
<parameternamelist>
<parametername>context</parametername>
</parameternamelist>
<parameterdescription>
<para>The pointer to the context structure allocated by a user or middleware.</para></parameterdescription>
</parameteritem>
</parameterlist>
<simplesect kind="return"><para>Returns an operation result status (CSD status code). See <ref kindref="member" refid="group__group__csd__enums_1ga6cde526987c66fff895f48902589f979">cy_en_csd_status_t</ref>. </para></simplesect>
</para>        </detaileddescription>
        <inbodydescription>
        </inbodydescription>
        <location bodyend="92" bodyfile="/var/tmp/gitlab-runner1/builds/7e5c9300/0/repo/psoc6pdl/drivers/source/cy_csd.c" bodystart="74" column="1" file="/var/tmp/gitlab-runner1/builds/7e5c9300/0/repo/psoc6pdl/drivers/include/cy_csd.h" line="653" />
      </memberdef>
      <memberdef const="no" explicit="no" id="group__group__csd__functions_1ga72f4009cdd9b57d9edc9f9789d896b7c" inline="no" kind="function" prot="public" static="no" virt="non-virtual">
        <type><ref kindref="member" refid="group__group__csd__enums_1ga6cde526987c66fff895f48902589f979">cy_en_csd_status_t</ref></type>
        <definition>cy_en_csd_status_t Cy_CSD_DeInit</definition>
        <argsstring>(const CSD_Type *base, cy_en_csd_key_t key, cy_stc_csd_context_t *context)</argsstring>
        <name>Cy_CSD_DeInit</name>
        <param>
          <type>const CSD_Type *</type>
          <declname>base</declname>
        </param>
        <param>
          <type><ref kindref="member" refid="group__group__csd__enums_1ga281a1b4a5cddb0bee365a7835985ccba">cy_en_csd_key_t</ref></type>
          <declname>key</declname>
        </param>
        <param>
          <type><ref kindref="compound" refid="structcy__stc__csd__context__t">cy_stc_csd_context_t</ref> *</type>
          <declname>context</declname>
        </param>
        <briefdescription>
<para>Releases the CSD HW block previously captured and locked by the caller. </para>        </briefdescription>
        <detaileddescription>
<para>If the CSD HW block is acquired by another caller or the block is in the busy state (performing scan or conversion), the de-initialization request is ignored and the corresponding status is returned.</para><para><parameterlist kind="param"><parameteritem>
<parameternamelist>
<parametername>base</parametername>
</parameternamelist>
<parameterdescription>
<para>Pointer to a CSD HW block base address.</para></parameterdescription>
</parameteritem>
<parameteritem>
<parameternamelist>
<parametername>key</parametername>
</parameternamelist>
<parameterdescription>
<para>An ID of middleware or user-level function that is going to work with a specified CSD HW block.</para></parameterdescription>
</parameteritem>
<parameteritem>
<parameternamelist>
<parametername>context</parametername>
</parameternamelist>
<parameterdescription>
<para>The pointer to the context structure allocated by a user or middleware.</para></parameterdescription>
</parameteritem>
</parameterlist>
<simplesect kind="return"><para>Returns an operation result status (CSD status code). See <ref kindref="member" refid="group__group__csd__enums_1ga6cde526987c66fff895f48902589f979">cy_en_csd_status_t</ref>. </para></simplesect>
</para>        </detaileddescription>
        <inbodydescription>
        </inbodydescription>
        <location bodyend="138" bodyfile="/var/tmp/gitlab-runner1/builds/7e5c9300/0/repo/psoc6pdl/drivers/source/cy_csd.c" bodystart="120" column="1" file="/var/tmp/gitlab-runner1/builds/7e5c9300/0/repo/psoc6pdl/drivers/include/cy_csd.h" line="654" />
      </memberdef>
      <memberdef const="no" explicit="no" id="group__group__csd__functions_1gaed0da3eeb7d2217581b1946ce102f0ae" inline="no" kind="function" prot="public" static="no" virt="non-virtual">
        <type><ref kindref="member" refid="group__group__csd__enums_1ga6cde526987c66fff895f48902589f979">cy_en_csd_status_t</ref></type>
        <definition>cy_en_csd_status_t Cy_CSD_Configure</definition>
        <argsstring>(CSD_Type *base, const cy_stc_csd_config_t *config, cy_en_csd_key_t key, const cy_stc_csd_context_t *context)</argsstring>
        <name>Cy_CSD_Configure</name>
        <param>
          <type>CSD_Type *</type>
          <declname>base</declname>
        </param>
        <param>
          <type>const <ref kindref="compound" refid="structcy__stc__csd__config__t">cy_stc_csd_config_t</ref> *</type>
          <declname>config</declname>
        </param>
        <param>
          <type><ref kindref="member" refid="group__group__csd__enums_1ga281a1b4a5cddb0bee365a7835985ccba">cy_en_csd_key_t</ref></type>
          <declname>key</declname>
        </param>
        <param>
          <type>const <ref kindref="compound" refid="structcy__stc__csd__context__t">cy_stc_csd_context_t</ref> *</type>
          <declname>context</declname>
        </param>
        <briefdescription>
<para>Sets configuration of all CSD HW block registers at once. </para>        </briefdescription>
        <detaileddescription>
<para>This function writes configuration data into all CSD block registers (except read-only registers and the SEQ_START register) at once. Because the SEQ_START register is excluded from write, use the <ref kindref="member" refid="group__group__csd__functions_1ga3689a475c33996ff4cb5e8c24d1194c1">Cy_CSD_WriteReg()</ref> function to perform triggering state machine for scan or conversion.</para><para><parameterlist kind="param"><parameteritem>
<parameternamelist>
<parametername>base</parametername>
</parameternamelist>
<parameterdescription>
<para>Pointer to a CSD HW block base address.</para></parameterdescription>
</parameteritem>
<parameteritem>
<parameternamelist>
<parametername>config</parametername>
</parameternamelist>
<parameterdescription>
<para>The pointer to a configuration structure that contains initial configuration.</para></parameterdescription>
</parameteritem>
<parameteritem>
<parameternamelist>
<parametername>key</parametername>
</parameternamelist>
<parameterdescription>
<para>An ID of middleware or user-level function that is going to work with the specified CSD HW block.</para></parameterdescription>
</parameteritem>
<parameteritem>
<parameternamelist>
<parametername>context</parametername>
</parameternamelist>
<parameterdescription>
<para>The pointer to the context structure allocated by a user or middleware.</para></parameterdescription>
</parameteritem>
</parameterlist>
<simplesect kind="return"><para>Returns an operation result status (CSD status code). See <ref kindref="member" refid="group__group__csd__enums_1ga6cde526987c66fff895f48902589f979">cy_en_csd_status_t</ref>. </para></simplesect>
</para>        </detaileddescription>
        <inbodydescription>
        </inbodydescription>
        <location bodyend="220" bodyfile="/var/tmp/gitlab-runner1/builds/7e5c9300/0/repo/psoc6pdl/drivers/source/cy_csd.c" bodystart="170" column="1" file="/var/tmp/gitlab-runner1/builds/7e5c9300/0/repo/psoc6pdl/drivers/include/cy_csd.h" line="655" />
      </memberdef>
      <memberdef const="no" explicit="no" id="group__group__csd__functions_1gafc4c25c2faefdfb3f3fdcf070b645c13" inline="no" kind="function" prot="public" static="no" virt="non-virtual">
        <type>__STATIC_INLINE <ref kindref="member" refid="group__group__csd__enums_1ga281a1b4a5cddb0bee365a7835985ccba">cy_en_csd_key_t</ref></type>
        <definition>__STATIC_INLINE cy_en_csd_key_t Cy_CSD_GetLockStatus</definition>
        <argsstring>(const CSD_Type *base, const cy_stc_csd_context_t *context)</argsstring>
        <name>Cy_CSD_GetLockStatus</name>
        <param>
          <type>const CSD_Type *</type>
          <declname>base</declname>
        </param>
        <param>
          <type>const <ref kindref="compound" refid="structcy__stc__csd__context__t">cy_stc_csd_context_t</ref> *</type>
          <declname>context</declname>
        </param>
        <briefdescription>
<para>Verifies whether the specified CSD HW block is acquired and locked by a higher-level firmware. </para>        </briefdescription>
        <detaileddescription>
<para><parameterlist kind="param"><parameteritem>
<parameternamelist>
<parametername>base</parametername>
</parameternamelist>
<parameterdescription>
<para>Pointer to a CSD HW block base address.</para></parameterdescription>
</parameteritem>
<parameteritem>
<parameternamelist>
<parametername>context</parametername>
</parameternamelist>
<parameterdescription>
<para>The pointer to the context structure allocated by a user or middleware.</para></parameterdescription>
</parameteritem>
</parameterlist>
<simplesect kind="return"><para>Returns a key code. See <ref kindref="member" refid="group__group__csd__enums_1ga281a1b4a5cddb0bee365a7835985ccba">cy_en_csd_key_t</ref>.</para></simplesect>
<simplesect kind="par"><title>Function Usage</title><para /></simplesect>
<programlisting><codeline><highlight class="normal" /></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /><ref kindref="member" refid="group__group__csd__enums_1ga281a1b4a5cddb0bee365a7835985ccba">cy_en_csd_key_t</ref><sp />key;</highlight></codeline>
<codeline><highlight class="normal" /></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp />key<sp />=<sp /><ref kindref="member" refid="group__group__csd__functions_1gafc4c25c2faefdfb3f3fdcf070b645c13">Cy_CSD_GetLockStatus</ref>(CSD0,<sp />&amp;csdContext);</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /></highlight><highlight class="keywordflow">if</highlight><highlight class="normal"><sp />(<ref kindref="member" refid="group__group__csd__enums_1gga281a1b4a5cddb0bee365a7835985ccbaa6ecb94eb2fc43363fdeb79fa9dd965b2">CY_CSD_NONE_KEY</ref><sp />==<sp />key)</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp />{</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /></highlight><highlight class="comment">/*<sp />The<sp />CSD<sp />block<sp />is<sp />not<sp />captured<sp />*/</highlight><highlight class="normal" /></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp />}</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /></highlight></codeline>
</programlisting></para>        </detaileddescription>
        <inbodydescription>
        </inbodydescription>
        <location bodyend="813" bodyfile="/var/tmp/gitlab-runner1/builds/7e5c9300/0/repo/psoc6pdl/drivers/include/cy_csd.h" bodystart="809" column="1" file="/var/tmp/gitlab-runner1/builds/7e5c9300/0/repo/psoc6pdl/drivers/include/cy_csd.h" line="657" />
      </memberdef>
      <memberdef const="no" explicit="no" id="group__group__csd__functions_1gab3e991dec9a467c332da3f904bdf5acc" inline="no" kind="function" prot="public" static="no" virt="non-virtual">
        <type>__STATIC_INLINE <ref kindref="member" refid="group__group__csd__enums_1ga6cde526987c66fff895f48902589f979">cy_en_csd_status_t</ref></type>
        <definition>__STATIC_INLINE cy_en_csd_status_t Cy_CSD_GetConversionStatus</definition>
        <argsstring>(const CSD_Type *base, const cy_stc_csd_context_t *context)</argsstring>
        <name>Cy_CSD_GetConversionStatus</name>
        <param>
          <type>const CSD_Type *</type>
          <declname>base</declname>
        </param>
        <param>
          <type>const <ref kindref="compound" refid="structcy__stc__csd__context__t">cy_stc_csd_context_t</ref> *</type>
          <declname>context</declname>
        </param>
        <briefdescription>
<para>Verifies whether the specified CSD HW block is busy (performing scan or conversion). </para>        </briefdescription>
        <detaileddescription>
<para><parameterlist kind="param"><parameteritem>
<parameternamelist>
<parametername>base</parametername>
</parameternamelist>
<parameterdescription>
<para>Pointer to a CSD HW block base address.</para></parameterdescription>
</parameteritem>
<parameteritem>
<parameternamelist>
<parametername>context</parametername>
</parameternamelist>
<parameterdescription>
<para>The pointer to the context structure allocated by a user or middleware.</para></parameterdescription>
</parameteritem>
</parameterlist>
<simplesect kind="return"><para>Returns status code. See <ref kindref="member" refid="group__group__csd__enums_1ga6cde526987c66fff895f48902589f979">cy_en_csd_status_t</ref>.</para></simplesect>
<simplesect kind="par"><title>Function Usage</title><para /></simplesect>
<programlisting><codeline><highlight class="normal"><sp /><sp /><sp /><sp /></highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /><ref kindref="member" refid="group__group__csd__enums_1ga6cde526987c66fff895f48902589f979">cy_en_csd_status_t</ref><sp />status;</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /></highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp />status<sp />=<sp /><ref kindref="member" refid="group__group__csd__functions_1gab3e991dec9a467c332da3f904bdf5acc">Cy_CSD_GetConversionStatus</ref>(CSD0,<sp />&amp;csdContext);</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /></highlight><highlight class="keywordflow">if</highlight><highlight class="normal"><sp />(<ref kindref="member" refid="group__group__csd__enums_1gga6cde526987c66fff895f48902589f979a0899108469f2574d39e14fa3262aec11">CY_CSD_BUSY</ref><sp />==<sp />status)</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp />{</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /></highlight><highlight class="comment">/*<sp />The<sp />CSD<sp />block<sp />is<sp />performing<sp />a<sp />conversion<sp />*/</highlight><highlight class="normal" /></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp />}</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /></highlight><highlight class="keywordflow">else</highlight><highlight class="normal" /></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp />{</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /></highlight><highlight class="comment">/*<sp />The<sp />CSD<sp />block<sp />is<sp />not<sp />busy<sp />and<sp />a<sp />new<sp />conversion<sp />could<sp />be<sp />started<sp />*/</highlight><highlight class="normal" /></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp />}</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /></highlight></codeline>
</programlisting></para>        </detaileddescription>
        <inbodydescription>
        </inbodydescription>
        <location bodyend="849" bodyfile="/var/tmp/gitlab-runner1/builds/7e5c9300/0/repo/psoc6pdl/drivers/include/cy_csd.h" bodystart="837" column="1" file="/var/tmp/gitlab-runner1/builds/7e5c9300/0/repo/psoc6pdl/drivers/include/cy_csd.h" line="658" />
      </memberdef>
      <memberdef const="no" explicit="no" id="group__group__csd__functions_1ga7685ad3c3b00b1c77bf67b03f944752f" inline="no" kind="function" prot="public" static="no" virt="non-virtual">
        <type>uint32_t</type>
        <definition>uint32_t Cy_CSD_GetVrefTrim</definition>
        <argsstring>(uint32_t referenceVoltage)</argsstring>
        <name>Cy_CSD_GetVrefTrim</name>
        <param>
          <type>uint32_t</type>
          <declname>referenceVoltage</declname>
        </param>
        <briefdescription>
<para>Adjusts the provided reference voltage based on factory trimmed SFALSH Vref trim registers. </para>        </briefdescription>
        <detaileddescription>
<para>This function is mainly used by CSDADC middleware only to get the most accurate reference voltage possible.</para><para><parameterlist kind="param"><parameteritem>
<parameternamelist>
<parametername>referenceVoltage</parametername>
</parameternamelist>
<parameterdescription>
<para>The reference voltage to trim.</para></parameterdescription>
</parameteritem>
</parameterlist>
<simplesect kind="return"><para>Returns a trimmed reference voltage. </para></simplesect>
</para>        </detaileddescription>
        <inbodydescription>
        </inbodydescription>
        <location bodyend="299" bodyfile="/var/tmp/gitlab-runner1/builds/7e5c9300/0/repo/psoc6pdl/drivers/source/cy_csd.c" bodystart="240" column="1" file="/var/tmp/gitlab-runner1/builds/7e5c9300/0/repo/psoc6pdl/drivers/include/cy_csd.h" line="660" />
      </memberdef>
      <memberdef const="no" explicit="no" id="group__group__csd__functions_1ga34f48f5162adea39ed451877893a1552" inline="no" kind="function" prot="public" static="no" virt="non-virtual">
        <type>__STATIC_INLINE uint32_t</type>
        <definition>__STATIC_INLINE uint32_t Cy_CSD_ReadReg</definition>
        <argsstring>(const CSD_Type *base, uint32_t offset)</argsstring>
        <name>Cy_CSD_ReadReg</name>
        <param>
          <type>const CSD_Type *</type>
          <declname>base</declname>
        </param>
        <param>
          <type>uint32_t</type>
          <declname>offset</declname>
        </param>
        <briefdescription>
<para>Reads value from the specified the CSD HW block register. </para>        </briefdescription>
        <detaileddescription>
<para><parameterlist kind="param"><parameteritem>
<parameternamelist>
<parametername>base</parametername>
</parameternamelist>
<parameterdescription>
<para>Pointer to a CSD HW block base address.</para></parameterdescription>
</parameteritem>
<parameteritem>
<parameternamelist>
<parametername>offset</parametername>
</parameternamelist>
<parameterdescription>
<para>Register offset relative to base address.</para></parameterdescription>
</parameteritem>
</parameterlist>
<simplesect kind="return"><para>Returns a value of the CSD HW block register, specified by the offset parameter. </para></simplesect>
</para>        </detaileddescription>
        <inbodydescription>
        </inbodydescription>
        <location bodyend="688" bodyfile="/var/tmp/gitlab-runner1/builds/7e5c9300/0/repo/psoc6pdl/drivers/include/cy_csd.h" bodystart="685" column="1" file="/var/tmp/gitlab-runner1/builds/7e5c9300/0/repo/psoc6pdl/drivers/include/cy_csd.h" line="662" />
      </memberdef>
      <memberdef const="no" explicit="no" id="group__group__csd__functions_1ga3689a475c33996ff4cb5e8c24d1194c1" inline="no" kind="function" prot="public" static="no" virt="non-virtual">
        <type>__STATIC_INLINE void</type>
        <definition>__STATIC_INLINE void Cy_CSD_WriteReg</definition>
        <argsstring>(CSD_Type *base, uint32_t offset, uint32_t value)</argsstring>
        <name>Cy_CSD_WriteReg</name>
        <param>
          <type>CSD_Type *</type>
          <declname>base</declname>
        </param>
        <param>
          <type>uint32_t</type>
          <declname>offset</declname>
        </param>
        <param>
          <type>uint32_t</type>
          <declname>value</declname>
        </param>
        <briefdescription>
<para>Writes a value to the specified CSD HW block register. </para>        </briefdescription>
        <detaileddescription>
<para><parameterlist kind="param"><parameteritem>
<parameternamelist>
<parametername>base</parametername>
</parameternamelist>
<parameterdescription>
<para>Pointer to a CSD HW block base address.</para></parameterdescription>
</parameteritem>
<parameteritem>
<parameternamelist>
<parametername>offset</parametername>
</parameternamelist>
<parameterdescription>
<para>Register offset relative to base address.</para></parameterdescription>
</parameteritem>
<parameteritem>
<parameternamelist>
<parametername>value</parametername>
</parameternamelist>
<parameterdescription>
<para>Value to be written to the register. </para></parameterdescription>
</parameteritem>
</parameterlist>
</para>        </detaileddescription>
        <inbodydescription>
        </inbodydescription>
        <location bodyend="710" bodyfile="/var/tmp/gitlab-runner1/builds/7e5c9300/0/repo/psoc6pdl/drivers/include/cy_csd.h" bodystart="707" column="1" file="/var/tmp/gitlab-runner1/builds/7e5c9300/0/repo/psoc6pdl/drivers/include/cy_csd.h" line="663" />
      </memberdef>
      <memberdef const="no" explicit="no" id="group__group__csd__functions_1ga1cd332894daaff0869d0d3eaec40a811" inline="no" kind="function" prot="public" static="no" virt="non-virtual">
        <type>__STATIC_INLINE void</type>
        <definition>__STATIC_INLINE void Cy_CSD_SetBits</definition>
        <argsstring>(CSD_Type *base, uint32_t offset, uint32_t mask)</argsstring>
        <name>Cy_CSD_SetBits</name>
        <param>
          <type>CSD_Type *</type>
          <declname>base</declname>
        </param>
        <param>
          <type>uint32_t</type>
          <declname>offset</declname>
        </param>
        <param>
          <type>uint32_t</type>
          <declname>mask</declname>
        </param>
        <briefdescription>
<para>Sets bits, specified by the Mask parameter in the CSD HW block register, specified by the Offset parameter. </para>        </briefdescription>
        <detaileddescription>
<para><parameterlist kind="param"><parameteritem>
<parameternamelist>
<parametername>base</parametername>
</parameternamelist>
<parameterdescription>
<para>Pointer to a CSD HW block base address.</para></parameterdescription>
</parameteritem>
<parameteritem>
<parameternamelist>
<parametername>offset</parametername>
</parameternamelist>
<parameterdescription>
<para>Register offset relative to base address.</para></parameterdescription>
</parameteritem>
<parameteritem>
<parameternamelist>
<parametername>mask</parametername>
</parameternamelist>
<parameterdescription>
<para>Mask value for register bits to be set. </para></parameterdescription>
</parameteritem>
</parameterlist>
</para>        </detaileddescription>
        <inbodydescription>
        </inbodydescription>
        <location bodyend="734" bodyfile="/var/tmp/gitlab-runner1/builds/7e5c9300/0/repo/psoc6pdl/drivers/include/cy_csd.h" bodystart="730" column="1" file="/var/tmp/gitlab-runner1/builds/7e5c9300/0/repo/psoc6pdl/drivers/include/cy_csd.h" line="664" />
      </memberdef>
      <memberdef const="no" explicit="no" id="group__group__csd__functions_1gae0dba82a3024adfe048684f19979cf31" inline="no" kind="function" prot="public" static="no" virt="non-virtual">
        <type>__STATIC_INLINE void</type>
        <definition>__STATIC_INLINE void Cy_CSD_ClrBits</definition>
        <argsstring>(CSD_Type *base, uint32_t offset, uint32_t mask)</argsstring>
        <name>Cy_CSD_ClrBits</name>
        <param>
          <type>CSD_Type *</type>
          <declname>base</declname>
        </param>
        <param>
          <type>uint32_t</type>
          <declname>offset</declname>
        </param>
        <param>
          <type>uint32_t</type>
          <declname>mask</declname>
        </param>
        <briefdescription>
<para>Clears bits, specified by the Mask parameter in the CSD HW block register, specified by the Offset parameter. </para>        </briefdescription>
        <detaileddescription>
<para><parameterlist kind="param"><parameteritem>
<parameternamelist>
<parametername>base</parametername>
</parameternamelist>
<parameterdescription>
<para>Pointer to a CSD HW block base address.</para></parameterdescription>
</parameteritem>
<parameteritem>
<parameternamelist>
<parametername>offset</parametername>
</parameternamelist>
<parameterdescription>
<para>Register offset relative to base address.</para></parameterdescription>
</parameteritem>
<parameteritem>
<parameternamelist>
<parametername>mask</parametername>
</parameternamelist>
<parameterdescription>
<para>Mask value for register bits to be cleared. </para></parameterdescription>
</parameteritem>
</parameterlist>
</para>        </detaileddescription>
        <inbodydescription>
        </inbodydescription>
        <location bodyend="758" bodyfile="/var/tmp/gitlab-runner1/builds/7e5c9300/0/repo/psoc6pdl/drivers/include/cy_csd.h" bodystart="754" column="1" file="/var/tmp/gitlab-runner1/builds/7e5c9300/0/repo/psoc6pdl/drivers/include/cy_csd.h" line="665" />
      </memberdef>
      <memberdef const="no" explicit="no" id="group__group__csd__functions_1ga86d12b64c2b1db82c94df03532d57ca6" inline="no" kind="function" prot="public" static="no" virt="non-virtual">
        <type>__STATIC_INLINE void</type>
        <definition>__STATIC_INLINE void Cy_CSD_WriteBits</definition>
        <argsstring>(CSD_Type *base, uint32_t offset, uint32_t mask, uint32_t value)</argsstring>
        <name>Cy_CSD_WriteBits</name>
        <param>
          <type>CSD_Type *</type>
          <declname>base</declname>
        </param>
        <param>
          <type>uint32_t</type>
          <declname>offset</declname>
        </param>
        <param>
          <type>uint32_t</type>
          <declname>mask</declname>
        </param>
        <param>
          <type>uint32_t</type>
          <declname>value</declname>
        </param>
        <briefdescription>
<para>Writes field, specified by the Mask parameter with the value, specified by the Value parameter. </para>        </briefdescription>
        <detaileddescription>
<para><parameterlist kind="param"><parameteritem>
<parameternamelist>
<parametername>base</parametername>
</parameternamelist>
<parameterdescription>
<para>Pointer to a CSD HW block base address.</para></parameterdescription>
</parameteritem>
<parameteritem>
<parameternamelist>
<parametername>offset</parametername>
</parameternamelist>
<parameterdescription>
<para>Register offset relative to base address.</para></parameterdescription>
</parameteritem>
<parameteritem>
<parameternamelist>
<parametername>mask</parametername>
</parameternamelist>
<parameterdescription>
<para>Specifies bits to be modified.</para></parameterdescription>
</parameteritem>
<parameteritem>
<parameternamelist>
<parametername>value</parametername>
</parameternamelist>
<parameterdescription>
<para>Specifies a value to be written to the register. </para></parameterdescription>
</parameteritem>
</parameterlist>
</para>        </detaileddescription>
        <inbodydescription>
        </inbodydescription>
        <location bodyend="785" bodyfile="/var/tmp/gitlab-runner1/builds/7e5c9300/0/repo/psoc6pdl/drivers/include/cy_csd.h" bodystart="781" column="1" file="/var/tmp/gitlab-runner1/builds/7e5c9300/0/repo/psoc6pdl/drivers/include/cy_csd.h" line="666" />
      </memberdef>
      </sectiondef>
    <briefdescription>
    </briefdescription>
    <detaileddescription>
    </detaileddescription>
  </compounddef>
</doxygen>