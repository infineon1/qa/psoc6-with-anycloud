<doxygen xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" version="1.8.13" xsi:noNamespaceSchemaLocation="compound.xsd">
  <compounddef id="group__group__prot__functions__busmaster" kind="group">
    <compoundname>group_prot_functions_busmaster</compoundname>
    <title>Bus Master and PC Functions</title>
      <sectiondef kind="func">
      <memberdef const="no" explicit="no" id="group__group__prot__functions__busmaster_1ga4b69f79e24b30f22a706e973b05dd079" inline="no" kind="function" prot="public" static="no" virt="non-virtual">
        <type><ref kindref="member" refid="group__group__prot__enums_1ga13ede85383a4afaf5ed3a14b470e38bd">cy_en_prot_status_t</ref></type>
        <definition>cy_en_prot_status_t Cy_Prot_ConfigBusMaster</definition>
        <argsstring>(en_prot_master_t busMaster, bool privileged, bool secure, uint32_t pcMask)</argsstring>
        <name>Cy_Prot_ConfigBusMaster</name>
        <param>
          <type>en_prot_master_t</type>
          <declname>busMaster</declname>
        </param>
        <param>
          <type>bool</type>
          <declname>privileged</declname>
        </param>
        <param>
          <type>bool</type>
          <declname>secure</declname>
        </param>
        <param>
          <type>uint32_t</type>
          <declname>pcMask</declname>
        </param>
        <briefdescription>
<para>Configures the allowed protection contexts, security (secure/non-secure) and privilege level of the bus transaction created by the specified master. </para>        </briefdescription>
        <detaileddescription>
<para><verbatim>embed:rst 
.. raw:: html

  &lt;div class="admonition note"&gt;&lt;p class="admonition-title"&gt;note&lt;/p&gt;&lt;p&gt;&lt;p&gt;This function is applicable for both CPUSS ver_1 and ver_2.&lt;/p&gt;&lt;/p&gt;&lt;/div&gt;</verbatim><parameterlist kind="param"><parameteritem>
<parameternamelist>
<parametername>busMaster</parametername>
</parameternamelist>
<parameterdescription>
<para>Indicates which master needs to be configured. Refer to the CPUSS_MS_ID_X defines in the device config header file.</para></parameterdescription>
</parameteritem>
<parameteritem>
<parameternamelist>
<parametername>privileged</parametername>
</parameternamelist>
<parameterdescription>
<para>Boolean to define the privilege level of all subsequent bus transfers. True - privileged, False - not privileged. Note that this is an inherited value. If not inherited, then this bit will be used.</para></parameterdescription>
</parameteritem>
<parameteritem>
<parameternamelist>
<parametername>secure</parametername>
</parameternamelist>
<parameterdescription>
<para>Security setting for the master. True - Secure, False - Not secure.</para></parameterdescription>
</parameteritem>
<parameteritem>
<parameternamelist>
<parametername>pcMask</parametername>
</parameternamelist>
<parameterdescription>
<para>This is a 16 bit value of the allowed contexts, it is an OR'ed (|) field of the provided defines in cy_prot.h. For example: (CY_PROT_PCMASK1 | CY_PROT_PCMASK3 | CY_PROT_PCMASK4) </para></parameterdescription>
</parameteritem>
</parameterlist>
<verbatim>embed:rst 
.. raw:: html

  &lt;div class="admonition note"&gt;&lt;p class="admonition-title"&gt;note&lt;/p&gt;&lt;p&gt;&lt;p&gt;The function accepts pcMask values from CY_PROT_PCMASK1 to CY_PROT_PCMASK15. But each device has its own number of available protection contexts. That number is defined by PERI_PC_NR in the config file.&lt;/p&gt;&lt;/p&gt;&lt;/div&gt;</verbatim><simplesect kind="return"><para>Status of the function call.</para></simplesect>
<verbatim>embed:rst 
.. raw:: html

  &lt;table class="docutils align-default"&gt;&lt;tr&gt;&lt;td class="head"&gt;&lt;p&gt;Status &lt;/p&gt;&lt;/td&gt;&lt;td class="head"&gt;&lt;p&gt;Description  &lt;/p&gt;&lt;/td&gt;&lt;/tr&gt;&lt;tr&gt;&lt;td&gt;&lt;p&gt;CY_PROT_SUCCESS &lt;/p&gt;&lt;/td&gt;&lt;td&gt;&lt;p&gt;The function completed successfully. &lt;/p&gt;&lt;/td&gt;&lt;/tr&gt;&lt;tr&gt;&lt;td&gt;&lt;p&gt;CY_PROT_FAILURE &lt;/p&gt;&lt;/td&gt;&lt;td&gt;&lt;p&gt;The resource is locked. &lt;/p&gt;&lt;/td&gt;&lt;/tr&gt;&lt;/table&gt;</verbatim><simplesect kind="par"><title>Function Usage</title><para><programlisting><codeline><highlight class="normal" /></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /></highlight><highlight class="comment">/*<sp />Scenario:<sp />CM0+<sp />needs<sp />to<sp />be<sp />able<sp />to<sp />access<sp />resources<sp />protected<sp />with<sp />privileged,<sp /></highlight></codeline>
<codeline><highlight class="comment"><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp />secure<sp />and<sp />set<sp />PC<sp />value<sp />(PC=0,<sp />PC=1<sp />or<sp />PC=2)<sp />access<sp />settings<sp />*/</highlight><highlight class="normal" /></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /></highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /></highlight><highlight class="comment">/*<sp />Configure<sp />the<sp />CM0+<sp />bus<sp />master<sp />-<sp />privileged,<sp />secure,<sp />allow<sp />setting<sp />PC<sp />to<sp />PC=1<sp />and<sp />PC=2<sp />*/</highlight><highlight class="normal" /></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /></highlight><highlight class="keywordflow">if</highlight><highlight class="normal">(<ref kindref="member" refid="group__group__prot__enums_1gga13ede85383a4afaf5ed3a14b470e38bda462f07fcb31ed8a07dd273af4290691d">CY_PROT_SUCCESS</ref><sp />!=<sp /><ref kindref="member" refid="group__group__prot__functions__busmaster_1ga4b69f79e24b30f22a706e973b05dd079">Cy_Prot_ConfigBusMaster</ref>(CPUSS_MS_ID_CM0,<sp /></highlight><highlight class="keyword">true</highlight><highlight class="normal">,<sp /></highlight><highlight class="keyword">true</highlight><highlight class="normal">,<sp /><ref kindref="member" refid="group__group__prot__enums_1gga4486bc74d6e98ad99c1ce4d532e48d0fabf25e25bbce833636da932ce78e453e8">CY_PROT_PCMASK1</ref><sp />|<sp /><ref kindref="member" refid="group__group__prot__enums_1gga4486bc74d6e98ad99c1ce4d532e48d0fa0b17d740a164ae90f06a4ab2f17d79d0">CY_PROT_PCMASK2</ref>))</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp />{</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /></highlight><highlight class="comment">/*<sp />Insert<sp />error<sp />handling<sp />*/</highlight><highlight class="normal" /></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp />}</highlight></codeline>
<codeline><highlight class="normal" /></codeline>
</programlisting></para></simplesect>
</para>        </detaileddescription>
        <inbodydescription>
        </inbodydescription>
        <location bodyend="126" bodyfile="/var/tmp/gitlab-runner1/builds/7e5c9300/0/repo/psoc6pdl/drivers/source/cy_prot.c" bodystart="98" column="1" file="/var/tmp/gitlab-runner1/builds/7e5c9300/0/repo/psoc6pdl/drivers/include/cy_prot.h" line="969" />
      </memberdef>
      <memberdef const="no" explicit="no" id="group__group__prot__functions__busmaster_1ga2d3a54039578a9fae98f6c7b4c4cff41" inline="no" kind="function" prot="public" static="no" virt="non-virtual">
        <type><ref kindref="member" refid="group__group__prot__enums_1ga13ede85383a4afaf5ed3a14b470e38bd">cy_en_prot_status_t</ref></type>
        <definition>cy_en_prot_status_t Cy_Prot_SetActivePC</definition>
        <argsstring>(en_prot_master_t busMaster, uint32_t pc)</argsstring>
        <name>Cy_Prot_SetActivePC</name>
        <param>
          <type>en_prot_master_t</type>
          <declname>busMaster</declname>
        </param>
        <param>
          <type>uint32_t</type>
          <declname>pc</declname>
        </param>
        <briefdescription>
<para>Sets the current/active protection context of the specified bus master. </para>        </briefdescription>
        <detaileddescription>
<para>Allowed PC values are 1-15. If this value is not inherited from another bus master, the value set through this function is used.</para><para><verbatim>embed:rst 
.. raw:: html

  &lt;div class="admonition note"&gt;&lt;p class="admonition-title"&gt;note&lt;/p&gt;&lt;p&gt;&lt;p&gt;This function is applicable for both CPUSS ver_1 and ver_2.&lt;/p&gt;&lt;/p&gt;&lt;/div&gt;</verbatim><parameterlist kind="param"><parameteritem>
<parameternamelist>
<parametername>busMaster</parametername>
</parameternamelist>
<parameterdescription>
<para>The bus master to configure. Refer to the CPUSS_MS_ID_X defines in the device config header file.</para></parameterdescription>
</parameteritem>
<parameteritem>
<parameternamelist>
<parametername>pc</parametername>
</parameternamelist>
<parameterdescription>
<para>Active protection context of the specified master <ref kindref="member" refid="group__group__prot__enums_1ga4467b93f3da4304e584726b975d3faba">cy_en_prot_pc_t</ref>. </para></parameterdescription>
</parameteritem>
</parameterlist>
<verbatim>embed:rst 
.. raw:: html

  &lt;div class="admonition note"&gt;&lt;p class="admonition-title"&gt;note&lt;/p&gt;&lt;p&gt;&lt;p&gt;that only those protection contexts allowed by the pcMask (which was configured in &lt;a href="group__group__prot__functions__busmaster.html#group__group__prot__functions__busmaster_1ga4b69f79e24b30f22a706e973b05dd079"&gt;Cy_Prot_ConfigBusMaster&lt;/a&gt;) will take effect. &lt;/p&gt;&lt;/p&gt;&lt;/div&gt;</verbatim><verbatim>embed:rst 
.. raw:: html

  &lt;div class="admonition note"&gt;&lt;p class="admonition-title"&gt;note&lt;/p&gt;&lt;p&gt;&lt;p&gt;The function accepts pcMask values from CY_PROT_PC1 to CY_PROT_PC15. But each device has its own number of available protection contexts. That number is defined by PERI_PC_NR in the config file.&lt;/p&gt;&lt;/p&gt;&lt;/div&gt;</verbatim><simplesect kind="return"><para>Status of the function call.</para></simplesect>
<verbatim>embed:rst 
.. raw:: html

  &lt;table class="docutils align-default"&gt;&lt;tr&gt;&lt;td class="head"&gt;&lt;p&gt;Status &lt;/p&gt;&lt;/td&gt;&lt;td class="head"&gt;&lt;p&gt;Description  &lt;/p&gt;&lt;/td&gt;&lt;/tr&gt;&lt;tr&gt;&lt;td&gt;&lt;p&gt;CY_PROT_SUCCESS &lt;/p&gt;&lt;/td&gt;&lt;td&gt;&lt;p&gt;The function completed successfully. &lt;/p&gt;&lt;/td&gt;&lt;/tr&gt;&lt;tr&gt;&lt;td&gt;&lt;p&gt;CY_PROT_FAILURE &lt;/p&gt;&lt;/td&gt;&lt;td&gt;&lt;p&gt;The resource is locked. &lt;/p&gt;&lt;/td&gt;&lt;/tr&gt;&lt;/table&gt;</verbatim><simplesect kind="par"><title>Function Usage</title><para><programlisting><codeline><highlight class="normal" /></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /></highlight><highlight class="comment">/*<sp />Scenario:<sp />CM0+<sp />needs<sp />to<sp />access<sp />a<sp />protected<sp />resources<sp />that<sp />requires<sp />PC=1<sp />attribute<sp />*/</highlight><highlight class="normal" /></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /></highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /></highlight><highlight class="comment">/*<sp />Configure<sp />the<sp />bus<sp />master<sp />to<sp />allow<sp />setting<sp />PC<sp />to<sp />PC=1<sp />and<sp />PC=2<sp />*/</highlight><highlight class="normal" /></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /><sp /><ref kindref="member" refid="group__group__prot__functions__busmaster_1ga4b69f79e24b30f22a706e973b05dd079">Cy_Prot_ConfigBusMaster</ref>(CPUSS_MS_ID_CM0,<sp /></highlight><highlight class="keyword">true</highlight><highlight class="normal">,<sp /></highlight><highlight class="keyword">true</highlight><highlight class="normal">,<sp /><ref kindref="member" refid="group__group__prot__enums_1gga4486bc74d6e98ad99c1ce4d532e48d0fabf25e25bbce833636da932ce78e453e8">CY_PROT_PCMASK1</ref><sp />|<sp /><ref kindref="member" refid="group__group__prot__enums_1gga4486bc74d6e98ad99c1ce4d532e48d0fa0b17d740a164ae90f06a4ab2f17d79d0">CY_PROT_PCMASK2</ref>);</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /></highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /></highlight><highlight class="comment">/*<sp />Get<sp />the<sp />active<sp />PC<sp />value<sp />of<sp />CM0+<sp />*/</highlight><highlight class="normal" /></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /></highlight><highlight class="keywordflow">if</highlight><highlight class="normal">(<ref kindref="member" refid="group__group__prot__enums_1gga4467b93f3da4304e584726b975d3fabaa10dc15f2738ba076ded84cb7b9eac7fa">CY_PROT_PC1</ref><sp />!=<sp /><ref kindref="member" refid="group__group__prot__functions__busmaster_1gac6af4217bc8ac5517edb39ebe11e91ec">Cy_Prot_GetActivePC</ref>(CPUSS_MS_ID_CM0))</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp />{</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /></highlight><highlight class="comment">/*<sp />Set<sp />the<sp />CM0+<sp />PC<sp />value<sp />to<sp />1<sp />*/</highlight><highlight class="normal" /></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /></highlight><highlight class="keywordflow">if</highlight><highlight class="normal">(<ref kindref="member" refid="group__group__prot__enums_1gga13ede85383a4afaf5ed3a14b470e38bda462f07fcb31ed8a07dd273af4290691d">CY_PROT_SUCCESS</ref><sp />!=<sp /><ref kindref="member" refid="group__group__prot__functions__busmaster_1ga2d3a54039578a9fae98f6c7b4c4cff41">Cy_Prot_SetActivePC</ref>(CPUSS_MS_ID_CM0,<sp /><ref kindref="member" refid="group__group__prot__enums_1gga4467b93f3da4304e584726b975d3fabaa10dc15f2738ba076ded84cb7b9eac7fa">CY_PROT_PC1</ref>))</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp />{</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /></highlight><highlight class="comment">/*<sp />Insert<sp />error<sp />handling<sp />*/</highlight><highlight class="normal" /></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp />}</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp />}</highlight></codeline>
<codeline><highlight class="normal" /></codeline>
</programlisting></para></simplesect>
</para>        </detaileddescription>
        <inbodydescription>
        </inbodydescription>
        <location bodyend="175" bodyfile="/var/tmp/gitlab-runner1/builds/7e5c9300/0/repo/psoc6pdl/drivers/source/cy_prot.c" bodystart="164" column="1" file="/var/tmp/gitlab-runner1/builds/7e5c9300/0/repo/psoc6pdl/drivers/include/cy_prot.h" line="970" />
      </memberdef>
      <memberdef const="no" explicit="no" id="group__group__prot__functions__busmaster_1gac6af4217bc8ac5517edb39ebe11e91ec" inline="no" kind="function" prot="public" static="no" virt="non-virtual">
        <type>uint32_t</type>
        <definition>uint32_t Cy_Prot_GetActivePC</definition>
        <argsstring>(en_prot_master_t busMaster)</argsstring>
        <name>Cy_Prot_GetActivePC</name>
        <param>
          <type>en_prot_master_t</type>
          <declname>busMaster</declname>
        </param>
        <briefdescription>
        </briefdescription>
        <detaileddescription>
<para><verbatim>embed:rst 
.. raw:: html

  &lt;div class="admonition note"&gt;&lt;p class="admonition-title"&gt;note&lt;/p&gt;&lt;p&gt;&lt;p&gt;This function is applicable for both CPUSS ver_1 and ver_2.&lt;/p&gt;&lt;/p&gt;&lt;/div&gt;Returns the active protection context of a master.</verbatim></para><para><parameterlist kind="param"><parameteritem>
<parameternamelist>
<parametername>busMaster</parametername>
</parameternamelist>
<parameterdescription>
<para>The bus master, whose protection context is being read. Refer to the CPUSS_MS_ID_X defines in the device config header file.</para></parameterdescription>
</parameteritem>
</parameterlist>
<simplesect kind="return"><para>Active protection context of the master <ref kindref="member" refid="group__group__prot__enums_1ga4467b93f3da4304e584726b975d3faba">cy_en_prot_pc_t</ref>.</para></simplesect>
<simplesect kind="par"><title>Function Usage</title><para><programlisting><codeline><highlight class="normal" /></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /></highlight><highlight class="comment">/*<sp />Scenario:<sp />CM0+<sp />needs<sp />to<sp />access<sp />a<sp />protected<sp />resources<sp />that<sp />requires<sp />PC=1<sp />attribute<sp />*/</highlight><highlight class="normal" /></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /></highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /></highlight><highlight class="comment">/*<sp />Configure<sp />the<sp />bus<sp />master<sp />to<sp />allow<sp />setting<sp />PC<sp />to<sp />PC=1<sp />and<sp />PC=2<sp />*/</highlight><highlight class="normal" /></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /><sp /><ref kindref="member" refid="group__group__prot__functions__busmaster_1ga4b69f79e24b30f22a706e973b05dd079">Cy_Prot_ConfigBusMaster</ref>(CPUSS_MS_ID_CM0,<sp /></highlight><highlight class="keyword">true</highlight><highlight class="normal">,<sp /></highlight><highlight class="keyword">true</highlight><highlight class="normal">,<sp /><ref kindref="member" refid="group__group__prot__enums_1gga4486bc74d6e98ad99c1ce4d532e48d0fabf25e25bbce833636da932ce78e453e8">CY_PROT_PCMASK1</ref><sp />|<sp /><ref kindref="member" refid="group__group__prot__enums_1gga4486bc74d6e98ad99c1ce4d532e48d0fa0b17d740a164ae90f06a4ab2f17d79d0">CY_PROT_PCMASK2</ref>);</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /></highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /></highlight><highlight class="comment">/*<sp />Get<sp />the<sp />active<sp />PC<sp />value<sp />of<sp />CM0+<sp />*/</highlight><highlight class="normal" /></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /></highlight><highlight class="keywordflow">if</highlight><highlight class="normal">(<ref kindref="member" refid="group__group__prot__enums_1gga4467b93f3da4304e584726b975d3fabaa10dc15f2738ba076ded84cb7b9eac7fa">CY_PROT_PC1</ref><sp />!=<sp /><ref kindref="member" refid="group__group__prot__functions__busmaster_1gac6af4217bc8ac5517edb39ebe11e91ec">Cy_Prot_GetActivePC</ref>(CPUSS_MS_ID_CM0))</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp />{</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /></highlight><highlight class="comment">/*<sp />Set<sp />the<sp />CM0+<sp />PC<sp />value<sp />to<sp />1<sp />*/</highlight><highlight class="normal" /></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /></highlight><highlight class="keywordflow">if</highlight><highlight class="normal">(<ref kindref="member" refid="group__group__prot__enums_1gga13ede85383a4afaf5ed3a14b470e38bda462f07fcb31ed8a07dd273af4290691d">CY_PROT_SUCCESS</ref><sp />!=<sp /><ref kindref="member" refid="group__group__prot__functions__busmaster_1ga2d3a54039578a9fae98f6c7b4c4cff41">Cy_Prot_SetActivePC</ref>(CPUSS_MS_ID_CM0,<sp /><ref kindref="member" refid="group__group__prot__enums_1gga4467b93f3da4304e584726b975d3fabaa10dc15f2738ba076ded84cb7b9eac7fa">CY_PROT_PC1</ref>))</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp />{</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /></highlight><highlight class="comment">/*<sp />Insert<sp />error<sp />handling<sp />*/</highlight><highlight class="normal" /></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp />}</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp />}</highlight></codeline>
<codeline><highlight class="normal" /></codeline>
</programlisting></para></simplesect>
</para>        </detaileddescription>
        <inbodydescription>
        </inbodydescription>
        <location bodyend="203" bodyfile="/var/tmp/gitlab-runner1/builds/7e5c9300/0/repo/psoc6pdl/drivers/source/cy_prot.c" bodystart="197" column="1" file="/var/tmp/gitlab-runner1/builds/7e5c9300/0/repo/psoc6pdl/drivers/include/cy_prot.h" line="971" />
      </memberdef>
      </sectiondef>
    <briefdescription>
    </briefdescription>
    <detaileddescription>
    </detaileddescription>
  </compounddef>
</doxygen>