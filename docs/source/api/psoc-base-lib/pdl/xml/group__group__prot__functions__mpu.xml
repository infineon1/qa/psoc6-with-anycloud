<doxygen xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" version="1.8.13" xsi:noNamespaceSchemaLocation="compound.xsd">
  <compounddef id="group__group__prot__functions__mpu" kind="group">
    <compoundname>group_prot_functions_mpu</compoundname>
    <title>MPU Functions</title>
      <sectiondef kind="func">
      <memberdef const="no" explicit="no" id="group__group__prot__functions__mpu_1ga8b368e9846d5a3b8cd284131562376b1" inline="no" kind="function" prot="public" static="no" virt="non-virtual">
        <type><ref kindref="member" refid="group__group__prot__enums_1ga13ede85383a4afaf5ed3a14b470e38bd">cy_en_prot_status_t</ref></type>
        <definition>cy_en_prot_status_t Cy_Prot_ConfigMpuStruct</definition>
        <argsstring>(PROT_MPU_MPU_STRUCT_Type *base, const cy_stc_mpu_cfg_t *config)</argsstring>
        <name>Cy_Prot_ConfigMpuStruct</name>
        <param>
          <type><ref kindref="compound" refid="struct_p_r_o_t___m_p_u___m_p_u___s_t_r_u_c_t___type">PROT_MPU_MPU_STRUCT_Type</ref> *</type>
          <declname>base</declname>
        </param>
        <param>
          <type>const <ref kindref="compound" refid="structcy__stc__mpu__cfg__t">cy_stc_mpu_cfg_t</ref> *</type>
          <declname>config</declname>
        </param>
        <briefdescription>
<para>This function configures a memory protection unit (MPU) struct with its protection attributes. </para>        </briefdescription>
        <detaileddescription>
<para>The protection structs act like the gatekeepers for a master's accesses to memory, allowing only the permitted transactions to go through.</para><para><verbatim>embed:rst 
.. raw:: html

  &lt;div class="admonition note"&gt;&lt;p class="admonition-title"&gt;note&lt;/p&gt;&lt;p&gt;&lt;p&gt;This function is applicable for both CPUSS ver_1 and ver_2.&lt;/p&gt;&lt;/p&gt;&lt;/div&gt;</verbatim><parameterlist kind="param"><parameteritem>
<parameternamelist>
<parametername>base</parametername>
</parameternamelist>
<parameterdescription>
<para>The base address for the MPU struct being configured.</para></parameterdescription>
</parameteritem>
<parameteritem>
<parameternamelist>
<parametername>config</parametername>
</parameternamelist>
<parameterdescription>
<para>Initialization structure containing all the protection attributes.</para></parameterdescription>
</parameteritem>
</parameterlist>
<simplesect kind="return"><para>Status of the function call.</para></simplesect>
<verbatim>embed:rst 
.. raw:: html

  &lt;table class="docutils align-default"&gt;&lt;tr&gt;&lt;td class="head"&gt;&lt;p&gt;Status &lt;/p&gt;&lt;/td&gt;&lt;td class="head"&gt;&lt;p&gt;Description  &lt;/p&gt;&lt;/td&gt;&lt;/tr&gt;&lt;tr&gt;&lt;td&gt;&lt;p&gt;CY_PROT_SUCCESS &lt;/p&gt;&lt;/td&gt;&lt;td&gt;&lt;p&gt;The MPU struct was configured. &lt;/p&gt;&lt;/td&gt;&lt;/tr&gt;&lt;tr&gt;&lt;td&gt;&lt;p&gt;CY_PROT_FAILURE &lt;/p&gt;&lt;/td&gt;&lt;td&gt;&lt;p&gt;Configuration failed due to a protection violation. &lt;/p&gt;&lt;/td&gt;&lt;/tr&gt;&lt;/table&gt;</verbatim><simplesect kind="par"><title>Function Usage</title><para><programlisting><codeline><highlight class="normal"><sp /><sp /><sp /><sp />{</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /></highlight><highlight class="comment">/*<sp />Scenario:<sp />Crypto<sp />bus<sp />master<sp />must<sp />not<sp />be<sp />able<sp />access<sp />the<sp />memory<sp />region<sp /></highlight></codeline>
<codeline><highlight class="comment"><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp />0x08000020<sp />~<sp />0x080000FF<sp />unless<sp />it<sp />is<sp />operating<sp />in<sp />privileged</highlight></codeline>
<codeline><highlight class="comment"><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp />and<sp />secure<sp />mode<sp />*/</highlight><highlight class="normal" /></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /></highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><ref kindref="compound" refid="structcy__stc__mpu__cfg__t">cy_stc_mpu_cfg_t</ref><sp />mpuCfg<sp />=<sp />{</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /></highlight><highlight class="comment">/*.address<sp />=*/</highlight><highlight class="normal"><sp />(uint32_t<sp />*)0x08000000UL,<sp /><sp /><sp /><sp /></highlight><highlight class="comment">/*<sp />Starting<sp />address<sp />at<sp />start<sp />of<sp />SRAM<sp />*/</highlight><highlight class="normal" /></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /></highlight><highlight class="comment">/*.regionSize<sp />=*/</highlight><highlight class="normal"><sp /><ref kindref="member" refid="group__group__prot__enums_1ggaad39ed0d259460f717edfe9d08fc005fac95822db72eb8b6b16cd97f84c0d0b12">CY_PROT_SIZE_256B</ref>,<sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /></highlight><highlight class="comment">/*<sp />Protect<sp />block<sp />of<sp />256<sp />Bytes<sp />*/</highlight><highlight class="normal" /></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /></highlight><highlight class="comment">/*.subregions<sp />=*/</highlight><highlight class="normal"><sp /><ref kindref="member" refid="group__group__prot__enums_1gga618195213f866568fb71ed14af59cf6ca9b41ba059d2baba289e8776d46cea7ef">CY_PROT_SUBREGION_DIS0</ref>,<sp /><sp /><sp /></highlight><highlight class="comment">/*<sp />Disable<sp />protection<sp />for<sp />first<sp />32<sp />(256/8)<sp />Bytes<sp />*/</highlight><highlight class="normal" /></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /></highlight><highlight class="comment">/*.userPermission<sp />=*/</highlight><highlight class="normal"><sp /><ref kindref="member" refid="group__group__prot__enums_1gga330789bc5242c26b8a32df63fa8720e5a71e68b733a505b1241d49c8f62cbfb61">CY_PROT_PERM_DISABLED</ref>,</highlight><highlight class="comment">/*<sp />Disallow<sp />all<sp />user<sp />mode<sp />accesses<sp />*/</highlight><highlight class="normal" /></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /></highlight><highlight class="comment">/*.privPermission<sp />=*/</highlight><highlight class="normal"><sp /><ref kindref="member" refid="group__group__prot__enums_1gga330789bc5242c26b8a32df63fa8720e5aa60056a96ee4d8d2133b69e3714a318b">CY_PROT_PERM_RWX</ref>,<sp /><sp /><sp /><sp /><sp /></highlight><highlight class="comment">/*<sp />Allow<sp />all<sp />privileged<sp />mode<sp />accesses<sp />*/</highlight><highlight class="normal" /></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /></highlight><highlight class="comment">/*.secure<sp />=*/</highlight><highlight class="normal"><sp /></highlight><highlight class="keyword">true</highlight><highlight class="normal"><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /></highlight><highlight class="comment">/*<sp />Require<sp />the<sp />bus<sp />master<sp />to<sp />have<sp />"secure"<sp />attribute<sp />*/</highlight><highlight class="normal" /></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp />};</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /></highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /></highlight><highlight class="comment">/*<sp />Configure<sp />the<sp />Crypto<sp />(CPUSS_MS_ID_CRYPTO<sp />=<sp />1)<sp />MPU<sp />struct<sp />#0<sp />*/</highlight><highlight class="normal" /></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /></highlight><highlight class="keywordflow">if</highlight><highlight class="normal">(<ref kindref="member" refid="group__group__prot__enums_1gga13ede85383a4afaf5ed3a14b470e38bda462f07fcb31ed8a07dd273af4290691d">CY_PROT_SUCCESS</ref><sp />!=<sp /><ref kindref="member" refid="group__group__prot__functions__mpu_1ga8b368e9846d5a3b8cd284131562376b1">Cy_Prot_ConfigMpuStruct</ref>(PROT_MPU1_MPU_STRUCT0,<sp />&amp;mpuCfg))</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp />{</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /></highlight><highlight class="comment">/*<sp />Insert<sp />error<sp />handling<sp />*/</highlight><highlight class="normal" /></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp />}</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /></highlight><highlight class="keywordflow">else</highlight><highlight class="normal" /></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp />{</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /></highlight><highlight class="comment">/*<sp />Enable<sp />the<sp />Crypto<sp />MPU<sp />struct<sp />#0<sp />*/</highlight><highlight class="normal" /></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /></highlight><highlight class="keywordflow">if</highlight><highlight class="normal">(<ref kindref="member" refid="group__group__prot__enums_1gga13ede85383a4afaf5ed3a14b470e38bda462f07fcb31ed8a07dd273af4290691d">CY_PROT_SUCCESS</ref><sp />!=<sp /><ref kindref="member" refid="group__group__prot__functions__mpu_1gaf86849b6ef266090238dc4f5d59161f2">Cy_Prot_EnableMpuStruct</ref>(PROT_MPU1_MPU_STRUCT0))</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp />{</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /></highlight><highlight class="comment">/*<sp />Insert<sp />error<sp />handling<sp />*/</highlight><highlight class="normal" /></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp />}</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp />}</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp />}</highlight></codeline>
</programlisting></para></simplesect>
</para>        </detaileddescription>
        <inbodydescription>
        </inbodydescription>
        <location bodyend="258" bodyfile="/var/tmp/gitlab-runner1/builds/7e5c9300/0/repo/psoc6pdl/drivers/source/cy_prot.c" bodystart="236" column="1" file="/var/tmp/gitlab-runner1/builds/7e5c9300/0/repo/psoc6pdl/drivers/include/cy_prot.h" line="978" />
      </memberdef>
      <memberdef const="no" explicit="no" id="group__group__prot__functions__mpu_1gaf86849b6ef266090238dc4f5d59161f2" inline="no" kind="function" prot="public" static="no" virt="non-virtual">
        <type><ref kindref="member" refid="group__group__prot__enums_1ga13ede85383a4afaf5ed3a14b470e38bd">cy_en_prot_status_t</ref></type>
        <definition>cy_en_prot_status_t Cy_Prot_EnableMpuStruct</definition>
        <argsstring>(PROT_MPU_MPU_STRUCT_Type *base)</argsstring>
        <name>Cy_Prot_EnableMpuStruct</name>
        <param>
          <type><ref kindref="compound" refid="struct_p_r_o_t___m_p_u___m_p_u___s_t_r_u_c_t___type">PROT_MPU_MPU_STRUCT_Type</ref> *</type>
          <declname>base</declname>
        </param>
        <briefdescription>
<para>Enables the MPU struct, which allows the MPU protection attributes to take effect. </para>        </briefdescription>
        <detaileddescription>
<para><verbatim>embed:rst 
.. raw:: html

  &lt;div class="admonition note"&gt;&lt;p class="admonition-title"&gt;note&lt;/p&gt;&lt;p&gt;&lt;p&gt;This function is applicable for both CPUSS ver_1 and ver_2.&lt;/p&gt;&lt;/p&gt;&lt;/div&gt;</verbatim><parameterlist kind="param"><parameteritem>
<parameternamelist>
<parametername>base</parametername>
</parameternamelist>
<parameterdescription>
<para>The base address of the MPU struct being configured.</para></parameterdescription>
</parameteritem>
</parameterlist>
<simplesect kind="return"><para>Status of the function call.</para></simplesect>
<verbatim>embed:rst 
.. raw:: html

  &lt;table class="docutils align-default"&gt;&lt;tr&gt;&lt;td class="head"&gt;&lt;p&gt;Status &lt;/p&gt;&lt;/td&gt;&lt;td class="head"&gt;&lt;p&gt;Description  &lt;/p&gt;&lt;/td&gt;&lt;/tr&gt;&lt;tr&gt;&lt;td&gt;&lt;p&gt;CY_PROT_SUCCESS &lt;/p&gt;&lt;/td&gt;&lt;td&gt;&lt;p&gt;The MPU struct was enabled. &lt;/p&gt;&lt;/td&gt;&lt;/tr&gt;&lt;tr&gt;&lt;td&gt;&lt;p&gt;CY_PROT_FAILURE &lt;/p&gt;&lt;/td&gt;&lt;td&gt;&lt;p&gt;The MPU struct is disabled and possibly locked. &lt;/p&gt;&lt;/td&gt;&lt;/tr&gt;&lt;/table&gt;</verbatim><simplesect kind="par"><title>Function Usage</title><para><programlisting><codeline><highlight class="normal" /></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /></highlight><highlight class="comment">/*<sp />Scenario:<sp />PROT_MPU1_MPU_STRUCT0<sp />is<sp />configured<sp />to<sp />protect<sp />a<sp />resource<sp />and</highlight></codeline>
<codeline><highlight class="comment"><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp />needs<sp />to<sp />be<sp />enabled.<sp />*/</highlight><highlight class="normal" /></codeline>
<codeline><highlight class="normal" /></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /></highlight><highlight class="keywordflow">if</highlight><highlight class="normal">(<ref kindref="member" refid="group__group__prot__enums_1gga13ede85383a4afaf5ed3a14b470e38bda462f07fcb31ed8a07dd273af4290691d">CY_PROT_SUCCESS</ref><sp />!=<sp /><ref kindref="member" refid="group__group__prot__functions__mpu_1gaf86849b6ef266090238dc4f5d59161f2">Cy_Prot_EnableMpuStruct</ref>(PROT_MPU1_MPU_STRUCT0))</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp />{</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /></highlight><highlight class="comment">/*<sp />Insert<sp />error<sp />handling<sp />*/</highlight><highlight class="normal" /></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp />}</highlight></codeline>
<codeline><highlight class="normal" /></codeline>
</programlisting></para></simplesect>
</para>        </detaileddescription>
        <inbodydescription>
        </inbodydescription>
        <location bodyend="296" bodyfile="/var/tmp/gitlab-runner1/builds/7e5c9300/0/repo/psoc6pdl/drivers/source/cy_prot.c" bodystart="285" column="1" file="/var/tmp/gitlab-runner1/builds/7e5c9300/0/repo/psoc6pdl/drivers/include/cy_prot.h" line="979" />
      </memberdef>
      <memberdef const="no" explicit="no" id="group__group__prot__functions__mpu_1ga515c855a5703ee9dd3dd905aa50367a1" inline="no" kind="function" prot="public" static="no" virt="non-virtual">
        <type><ref kindref="member" refid="group__group__prot__enums_1ga13ede85383a4afaf5ed3a14b470e38bd">cy_en_prot_status_t</ref></type>
        <definition>cy_en_prot_status_t Cy_Prot_DisableMpuStruct</definition>
        <argsstring>(PROT_MPU_MPU_STRUCT_Type *base)</argsstring>
        <name>Cy_Prot_DisableMpuStruct</name>
        <param>
          <type><ref kindref="compound" refid="struct_p_r_o_t___m_p_u___m_p_u___s_t_r_u_c_t___type">PROT_MPU_MPU_STRUCT_Type</ref> *</type>
          <declname>base</declname>
        </param>
        <briefdescription>
<para>Disables the MPU struct, which prevents the MPU protection attributes from taking effect. </para>        </briefdescription>
        <detaileddescription>
<para><verbatim>embed:rst 
.. raw:: html

  &lt;div class="admonition note"&gt;&lt;p class="admonition-title"&gt;note&lt;/p&gt;&lt;p&gt;&lt;p&gt;This function is applicable for both CPUSS ver_1 and ver_2.&lt;/p&gt;&lt;/p&gt;&lt;/div&gt;</verbatim><parameterlist kind="param"><parameteritem>
<parameternamelist>
<parametername>base</parametername>
</parameternamelist>
<parameterdescription>
<para>The base address of the MPU struct being configured.</para></parameterdescription>
</parameteritem>
</parameterlist>
<simplesect kind="return"><para>Status of the function call.</para></simplesect>
<verbatim>embed:rst 
.. raw:: html

  &lt;table class="docutils align-default"&gt;&lt;tr&gt;&lt;td class="head"&gt;&lt;p&gt;Status &lt;/p&gt;&lt;/td&gt;&lt;td class="head"&gt;&lt;p&gt;Description  &lt;/p&gt;&lt;/td&gt;&lt;/tr&gt;&lt;tr&gt;&lt;td&gt;&lt;p&gt;CY_PROT_SUCCESS &lt;/p&gt;&lt;/td&gt;&lt;td&gt;&lt;p&gt;The MPU struct was disabled. &lt;/p&gt;&lt;/td&gt;&lt;/tr&gt;&lt;tr&gt;&lt;td&gt;&lt;p&gt;CY_PROT_FAILURE &lt;/p&gt;&lt;/td&gt;&lt;td&gt;&lt;p&gt;The MPU struct is enabled and possibly locked. &lt;/p&gt;&lt;/td&gt;&lt;/tr&gt;&lt;/table&gt;</verbatim><simplesect kind="par"><title>Function Usage</title><para><programlisting><codeline><highlight class="normal"><sp /><sp /><sp /><sp /></highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /></highlight><highlight class="comment">/*<sp />Scenario:<sp />PROT_MPU1_MPU_STRUCT0<sp />needs<sp />to<sp />be<sp />disabled.<sp />Or<sp />it<sp />needs<sp />to<sp />be</highlight></codeline>
<codeline><highlight class="comment"><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp />reconfigured<sp />and<sp />hence<sp />must<sp />first<sp />be<sp />disabled.<sp />*/</highlight><highlight class="normal" /></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /></highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /></highlight><highlight class="keywordflow">if</highlight><highlight class="normal">(<ref kindref="member" refid="group__group__prot__enums_1gga13ede85383a4afaf5ed3a14b470e38bda462f07fcb31ed8a07dd273af4290691d">CY_PROT_SUCCESS</ref><sp />!=<sp /><ref kindref="member" refid="group__group__prot__functions__mpu_1ga515c855a5703ee9dd3dd905aa50367a1">Cy_Prot_DisableMpuStruct</ref>(PROT_MPU1_MPU_STRUCT0))</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp />{</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /></highlight><highlight class="comment">/*<sp />Insert<sp />error<sp />handling<sp />*/</highlight><highlight class="normal" /></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp />}</highlight></codeline>
<codeline><highlight class="normal" /></codeline>
</programlisting></para></simplesect>
</para>        </detaileddescription>
        <inbodydescription>
        </inbodydescription>
        <location bodyend="334" bodyfile="/var/tmp/gitlab-runner1/builds/7e5c9300/0/repo/psoc6pdl/drivers/source/cy_prot.c" bodystart="323" column="1" file="/var/tmp/gitlab-runner1/builds/7e5c9300/0/repo/psoc6pdl/drivers/include/cy_prot.h" line="980" />
      </memberdef>
      </sectiondef>
    <briefdescription>
    </briefdescription>
    <detaileddescription>
    </detaileddescription>
  </compounddef>
</doxygen>