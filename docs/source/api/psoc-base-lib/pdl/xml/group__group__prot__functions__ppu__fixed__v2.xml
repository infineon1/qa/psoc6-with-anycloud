<doxygen xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" version="1.8.13" xsi:noNamespaceSchemaLocation="compound.xsd">
  <compounddef id="group__group__prot__functions__ppu__fixed__v2" kind="group">
    <compoundname>group_prot_functions_ppu_fixed_v2</compoundname>
    <title>PPU Fixed (FIXED) v2 Functions</title>
      <sectiondef kind="func">
      <memberdef const="no" explicit="no" id="group__group__prot__functions__ppu__fixed__v2_1gacc7dadd726bd53886aedfbfa1014ae9e" inline="no" kind="function" prot="public" static="no" virt="non-virtual">
        <type><ref kindref="member" refid="group__group__prot__enums_1ga13ede85383a4afaf5ed3a14b470e38bd">cy_en_prot_status_t</ref></type>
        <definition>cy_en_prot_status_t Cy_Prot_ConfigPpuFixedMasterAtt</definition>
        <argsstring>(PERI_MS_PPU_FX_Type *base, uint16_t pcMask, cy_en_prot_perm_t userPermission, cy_en_prot_perm_t privPermission, bool secure)</argsstring>
        <name>Cy_Prot_ConfigPpuFixedMasterAtt</name>
        <param>
          <type>PERI_MS_PPU_FX_Type *</type>
          <declname>base</declname>
        </param>
        <param>
          <type>uint16_t</type>
          <declname>pcMask</declname>
        </param>
        <param>
          <type><ref kindref="member" refid="group__group__prot__enums_1ga330789bc5242c26b8a32df63fa8720e5">cy_en_prot_perm_t</ref></type>
          <declname>userPermission</declname>
        </param>
        <param>
          <type><ref kindref="member" refid="group__group__prot__enums_1ga330789bc5242c26b8a32df63fa8720e5">cy_en_prot_perm_t</ref></type>
          <declname>privPermission</declname>
        </param>
        <param>
          <type>bool</type>
          <declname>secure</declname>
        </param>
        <briefdescription>
<para>Configures the protection structure with its protection attributes of the Fixed Peripheral Protection Unit (PPU FIXED) master. </para>        </briefdescription>
        <detaileddescription>
<para>This function configures the master structure governing the corresponding slave structure pair. It is a mechanism to protect the slave PPU FIXED structure. The memory location of the slave structure is known, so the address, region size and sub-regions of the configuration structure are not applicable.</para><para><verbatim>embed:rst 
.. raw:: html

  &lt;div class="admonition note"&gt;&lt;p class="admonition-title"&gt;note&lt;/p&gt;&lt;p&gt;&lt;p&gt;This function is applicable for CPUSS ver_2 only.&lt;/p&gt;&lt;/p&gt;&lt;/div&gt;</verbatim><parameterlist kind="param"><parameteritem>
<parameternamelist>
<parametername>base</parametername>
</parameternamelist>
<parameterdescription>
<para>The register base address of the protection structure is being configured.</para></parameterdescription>
</parameteritem>
<parameteritem>
<parameternamelist>
<parametername>pcMask</parametername>
</parameternamelist>
<parameterdescription>
<para>The protection context mask. It specifies the protection context or a set of multiple protection contexts to be configured. It is a value of OR'd (|) items of <ref kindref="member" refid="group__group__prot__enums_1ga4486bc74d6e98ad99c1ce4d532e48d0f">cy_en_prot_pcmask_t</ref>. For example: (<ref kindref="member" refid="group__group__prot__enums_1gga4486bc74d6e98ad99c1ce4d532e48d0fabf25e25bbce833636da932ce78e453e8">CY_PROT_PCMASK1</ref> | <ref kindref="member" refid="group__group__prot__enums_1gga4486bc74d6e98ad99c1ce4d532e48d0fa33b7e98df492418f5f7fa8882c27e2bd">CY_PROT_PCMASK3</ref> | <ref kindref="member" refid="group__group__prot__enums_1gga4486bc74d6e98ad99c1ce4d532e48d0fa2e5214be494696cd798202bc14b14029">CY_PROT_PCMASK4</ref>). </para></parameterdescription>
</parameteritem>
</parameterlist>
<verbatim>embed:rst 
.. raw:: html

  &lt;div class="admonition note"&gt;&lt;p class="admonition-title"&gt;note&lt;/p&gt;&lt;p&gt;&lt;p&gt;The function accepts pcMask values from &lt;a href="group__group__prot__enums.html#group__group__prot__enums_1gga4486bc74d6e98ad99c1ce4d532e48d0fabf25e25bbce833636da932ce78e453e8"&gt;CY_PROT_PCMASK1&lt;/a&gt; to &lt;a href="group__group__prot__enums.html#group__group__prot__enums_1gga4486bc74d6e98ad99c1ce4d532e48d0fa839e450c014d2151df162ed6f5dc6aa0"&gt;CY_PROT_PCMASK15&lt;/a&gt;. But each device has its own number of available protection contexts. That number is defined by PERI_PC_NR in the config file.&lt;/p&gt;&lt;/p&gt;&lt;/div&gt;</verbatim><parameterlist kind="param"><parameteritem>
<parameternamelist>
<parametername>userPermission</parametername>
</parameternamelist>
<parameterdescription>
<para>The user permission setting. The CY_PROT_PERM_R or CY_PROT_PERM_RW values are valid for the master.</para></parameterdescription>
</parameteritem>
<parameteritem>
<parameternamelist>
<parametername>privPermission</parametername>
</parameternamelist>
<parameterdescription>
<para>The privileged permission setting. The CY_PROT_PERM_R or CY_PROT_PERM_RW values are valid for the master.</para></parameterdescription>
</parameteritem>
<parameteritem>
<parameternamelist>
<parametername>secure</parametername>
</parameternamelist>
<parameterdescription>
<para>The secure flag.</para></parameterdescription>
</parameteritem>
</parameterlist>
<simplesect kind="return"><para>The status of the function call.</para></simplesect>
<verbatim>embed:rst 
.. raw:: html

  &lt;table class="docutils align-default"&gt;&lt;tr&gt;&lt;td class="head"&gt;&lt;p&gt;Status &lt;/p&gt;&lt;/td&gt;&lt;td class="head"&gt;&lt;p&gt;Description  &lt;/p&gt;&lt;/td&gt;&lt;/tr&gt;&lt;tr&gt;&lt;td&gt;&lt;p&gt;CY_PROT_SUCCESS &lt;/p&gt;&lt;/td&gt;&lt;td&gt;&lt;p&gt;The attributes were set up. &lt;/p&gt;&lt;/td&gt;&lt;/tr&gt;&lt;tr&gt;&lt;td&gt;&lt;p&gt;CY_PROT_FAILURE &lt;/p&gt;&lt;/td&gt;&lt;td&gt;&lt;p&gt;The attributes were not setup and the structure is possibly locked. &lt;/p&gt;&lt;/td&gt;&lt;/tr&gt;&lt;tr&gt;&lt;td&gt;&lt;p&gt;CY_PROT_INVALID_STATE &lt;/p&gt;&lt;/td&gt;&lt;td&gt;&lt;p&gt;The function was called on the device with an unsupported PERI HW version. &lt;/p&gt;&lt;/td&gt;&lt;/tr&gt;&lt;/table&gt;</verbatim><verbatim>embed:rst 
.. raw:: html

  &lt;div class="admonition note"&gt;&lt;p class="admonition-title"&gt;note&lt;/p&gt;&lt;p&gt;&lt;p&gt;Only the user/privileged write permissions are configurable. The read permissions are read-only and cannot be configured.&lt;/p&gt;&lt;/p&gt;&lt;/div&gt;</verbatim><verbatim>embed:rst 
.. raw:: html

  &lt;div class="admonition note"&gt;&lt;p class="admonition-title"&gt;note&lt;/p&gt;&lt;p&gt;&lt;p&gt;PC0 accesses are read-only and are always enabled.&lt;/p&gt;&lt;/p&gt;&lt;/div&gt;</verbatim><simplesect kind="par"><title>Function Usage</title><para><programlisting><codeline><highlight class="normal"><sp /><sp /><sp /><sp /></highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /></highlight><highlight class="comment">/*<sp />Scenario:<sp />A<sp />bus<sp />master<sp />must<sp />not<sp />be<sp />able<sp />to<sp />access<sp />the<sp />PERI_MS_PPU_FX_HSIOM_PRT1_PRT</highlight></codeline>
<codeline><highlight class="comment"><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp />slave<sp />struct<sp />unless<sp />operating<sp />with<sp />PC=0,<sp />"secure"<sp />and</highlight></codeline>
<codeline><highlight class="comment"><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp />"privileged"<sp />access<sp />settings.<sp />The<sp />PERI_MS_PPU_FX_HSIOM_PRT1_PRT<sp />slave<sp />attributes</highlight></codeline>
<codeline><highlight class="comment"><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp />are<sp />already<sp />configured.<sp />*/</highlight><highlight class="normal" /></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /></highlight><highlight class="keywordflow">if</highlight><highlight class="normal">(<ref kindref="member" refid="group__group__prot__enums_1gga13ede85383a4afaf5ed3a14b470e38bda462f07fcb31ed8a07dd273af4290691d">CY_PROT_SUCCESS</ref><sp />!=<sp /><sp /><ref kindref="member" refid="group__group__prot__functions__ppu__fixed__v2_1gacc7dadd726bd53886aedfbfa1014ae9e">Cy_Prot_ConfigPpuFixedMasterAtt</ref>(PERI_MS_PPU_FX_HSIOM_PRT1_PRT,</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp />(uint16_t)0U,<sp /></highlight><highlight class="comment">/*<sp />Only<sp />allow<sp />PC=0<sp />bus<sp />masters<sp />*/</highlight><highlight class="normal" /></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><ref kindref="member" refid="group__group__prot__enums_1gga330789bc5242c26b8a32df63fa8720e5aad6e3850fb76be4bd78240a4fc284985">CY_PROT_PERM_R</ref>,<sp /><sp /></highlight><highlight class="comment">/*<sp />Read<sp />access<sp />always<sp />available<sp />for<sp />Master<sp />*/</highlight><highlight class="normal" /></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><ref kindref="member" refid="group__group__prot__enums_1gga330789bc5242c26b8a32df63fa8720e5aa7d757bf3cedd4f675b65d3ed642d315">CY_PROT_PERM_RW</ref>,<sp /><sp /></highlight><highlight class="comment">/*<sp />Allow<sp />read<sp />and<sp />write<sp />privileged<sp />mode<sp />accesses<sp />*/</highlight><highlight class="normal" /></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /></highlight><highlight class="keyword">true</highlight><highlight class="normal">))<sp /></highlight><highlight class="comment">/*<sp />Only<sp />allow<sp />"secure"<sp />bus<sp />masters<sp />*/</highlight><highlight class="normal" /></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp />{</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /></highlight><highlight class="comment">/*<sp />Insert<sp />error<sp />handling<sp />*/</highlight><highlight class="normal" /></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp />}</highlight></codeline>
<codeline><highlight class="normal" /></codeline>
</programlisting></para></simplesect>
</para>        </detaileddescription>
        <inbodydescription>
        </inbodydescription>
        <location bodyend="1219" bodyfile="/var/tmp/gitlab-runner1/builds/7e5c9300/0/repo/psoc6pdl/drivers/source/cy_prot.c" bodystart="1210" column="1" file="/var/tmp/gitlab-runner1/builds/7e5c9300/0/repo/psoc6pdl/drivers/include/cy_prot.h" line="1017" />
      </memberdef>
      <memberdef const="no" explicit="no" id="group__group__prot__functions__ppu__fixed__v2_1ga6112f6eba93e5db763e8dc571a91740a" inline="no" kind="function" prot="public" static="no" virt="non-virtual">
        <type><ref kindref="member" refid="group__group__prot__enums_1ga13ede85383a4afaf5ed3a14b470e38bd">cy_en_prot_status_t</ref></type>
        <definition>cy_en_prot_status_t Cy_Prot_ConfigPpuFixedSlaveAtt</definition>
        <argsstring>(PERI_MS_PPU_FX_Type *base, uint16_t pcMask, cy_en_prot_perm_t userPermission, cy_en_prot_perm_t privPermission, bool secure)</argsstring>
        <name>Cy_Prot_ConfigPpuFixedSlaveAtt</name>
        <param>
          <type>PERI_MS_PPU_FX_Type *</type>
          <declname>base</declname>
        </param>
        <param>
          <type>uint16_t</type>
          <declname>pcMask</declname>
        </param>
        <param>
          <type><ref kindref="member" refid="group__group__prot__enums_1ga330789bc5242c26b8a32df63fa8720e5">cy_en_prot_perm_t</ref></type>
          <declname>userPermission</declname>
        </param>
        <param>
          <type><ref kindref="member" refid="group__group__prot__enums_1ga330789bc5242c26b8a32df63fa8720e5">cy_en_prot_perm_t</ref></type>
          <declname>privPermission</declname>
        </param>
        <param>
          <type>bool</type>
          <declname>secure</declname>
        </param>
        <briefdescription>
<para>Configures the protection structure with its protection attributes of the Fixed Peripheral Protection Unit (PPU FIXED) slave. </para>        </briefdescription>
        <detaileddescription>
<para>This function configures the slave structure of the PPU FIXED pair, which can protect any peripheral memory region in a device from invalid bus-master access.</para><para><verbatim>embed:rst 
.. raw:: html

  &lt;div class="admonition note"&gt;&lt;p class="admonition-title"&gt;note&lt;/p&gt;&lt;p&gt;&lt;p&gt;This function is applicable for CPUSS ver_2 only.&lt;/p&gt;&lt;/p&gt;&lt;/div&gt;</verbatim><parameterlist kind="param"><parameteritem>
<parameternamelist>
<parametername>base</parametername>
</parameternamelist>
<parameterdescription>
<para>The register base address of the protection structure is being configured.</para></parameterdescription>
</parameteritem>
<parameteritem>
<parameternamelist>
<parametername>pcMask</parametername>
</parameternamelist>
<parameterdescription>
<para>The protection context mask. It specifies the protection context or a set of multiple protection contexts to be configured. It is a value of OR'd (|) items of <ref kindref="member" refid="group__group__prot__enums_1ga4486bc74d6e98ad99c1ce4d532e48d0f">cy_en_prot_pcmask_t</ref>. For example: (<ref kindref="member" refid="group__group__prot__enums_1gga4486bc74d6e98ad99c1ce4d532e48d0fabf25e25bbce833636da932ce78e453e8">CY_PROT_PCMASK1</ref> | <ref kindref="member" refid="group__group__prot__enums_1gga4486bc74d6e98ad99c1ce4d532e48d0fa33b7e98df492418f5f7fa8882c27e2bd">CY_PROT_PCMASK3</ref> | <ref kindref="member" refid="group__group__prot__enums_1gga4486bc74d6e98ad99c1ce4d532e48d0fa2e5214be494696cd798202bc14b14029">CY_PROT_PCMASK4</ref>). </para></parameterdescription>
</parameteritem>
</parameterlist>
<verbatim>embed:rst 
.. raw:: html

  &lt;div class="admonition note"&gt;&lt;p class="admonition-title"&gt;note&lt;/p&gt;&lt;p&gt;&lt;p&gt;The function accepts pcMask values from &lt;a href="group__group__prot__enums.html#group__group__prot__enums_1gga4486bc74d6e98ad99c1ce4d532e48d0fabf25e25bbce833636da932ce78e453e8"&gt;CY_PROT_PCMASK1&lt;/a&gt; to &lt;a href="group__group__prot__enums.html#group__group__prot__enums_1gga4486bc74d6e98ad99c1ce4d532e48d0fa839e450c014d2151df162ed6f5dc6aa0"&gt;CY_PROT_PCMASK15&lt;/a&gt;. But each device has its own number of available protection contexts. That number is defined by PERI_PC_NR in the config file.&lt;/p&gt;&lt;/p&gt;&lt;/div&gt;</verbatim><parameterlist kind="param"><parameteritem>
<parameternamelist>
<parametername>userPermission</parametername>
</parameternamelist>
<parameterdescription>
<para>The user permission setting.</para></parameterdescription>
</parameteritem>
<parameteritem>
<parameternamelist>
<parametername>privPermission</parametername>
</parameternamelist>
<parameterdescription>
<para>The privileged permission setting.</para></parameterdescription>
</parameteritem>
<parameteritem>
<parameternamelist>
<parametername>secure</parametername>
</parameternamelist>
<parameterdescription>
<para>The secure flag.</para></parameterdescription>
</parameteritem>
</parameterlist>
<simplesect kind="return"><para>The status of the function call.</para></simplesect>
<verbatim>embed:rst 
.. raw:: html

  &lt;table class="docutils align-default"&gt;&lt;tr&gt;&lt;td class="head"&gt;&lt;p&gt;Status &lt;/p&gt;&lt;/td&gt;&lt;td class="head"&gt;&lt;p&gt;Description  &lt;/p&gt;&lt;/td&gt;&lt;/tr&gt;&lt;tr&gt;&lt;td&gt;&lt;p&gt;CY_PROT_SUCCESS &lt;/p&gt;&lt;/td&gt;&lt;td&gt;&lt;p&gt;The attributes were set up. &lt;/p&gt;&lt;/td&gt;&lt;/tr&gt;&lt;tr&gt;&lt;td&gt;&lt;p&gt;CY_PROT_FAILURE &lt;/p&gt;&lt;/td&gt;&lt;td&gt;&lt;p&gt;The attributes were not setup and the structure is possibly locked. &lt;/p&gt;&lt;/td&gt;&lt;/tr&gt;&lt;tr&gt;&lt;td&gt;&lt;p&gt;CY_PROT_INVALID_STATE &lt;/p&gt;&lt;/td&gt;&lt;td&gt;&lt;p&gt;The function was called on the device with an unsupported PERI HW version. &lt;/p&gt;&lt;/td&gt;&lt;/tr&gt;&lt;/table&gt;</verbatim><verbatim>embed:rst 
.. raw:: html

  &lt;div class="admonition note"&gt;&lt;p class="admonition-title"&gt;note&lt;/p&gt;&lt;p&gt;&lt;p&gt;PC0 accesses are read-only and are always enabled.&lt;/p&gt;&lt;/p&gt;&lt;/div&gt;</verbatim><simplesect kind="par"><title>Function Usage</title><para><programlisting><codeline><highlight class="normal"><sp /><sp /><sp /><sp /></highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /></highlight><highlight class="comment">/*<sp />Scenario:<sp />Disallow<sp />a<sp />bus<sp />master<sp />on<sp />the<sp />CM4<sp />core<sp />to<sp />change<sp />the<sp />HSIOM<sp />settings<sp /></highlight></codeline>
<codeline><highlight class="comment"><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp />of<sp />port<sp />1<sp />but<sp />allow<sp />reading<sp />the<sp />HSIOM<sp />settings<sp />*/</highlight><highlight class="normal" /></codeline>
<codeline><highlight class="normal" /></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /></highlight><highlight class="comment">/*<sp />Configure<sp />the<sp />CM4<sp />bus<sp />master<sp />-<sp />privileged,<sp />secure,<sp />allow<sp />setting<sp />PC<sp />to<sp />PC=1<sp />*/</highlight><highlight class="normal" /></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /><ref kindref="member" refid="group__group__prot__functions__busmaster_1ga4b69f79e24b30f22a706e973b05dd079">Cy_Prot_ConfigBusMaster</ref>(CPUSS_MS_ID_CM4,<sp /></highlight><highlight class="comment">/*<sp />Bus<sp />Master<sp />is<sp />the<sp />CM4<sp />core<sp />*/</highlight><highlight class="normal" /></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /></highlight><highlight class="keyword">true</highlight><highlight class="normal">,<sp /><sp /></highlight><highlight class="comment">/*<sp />Set<sp />the<sp />privilege<sp />level<sp />to<sp />Bus<sp />Master<sp />*/</highlight><highlight class="normal" /></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /></highlight><highlight class="keyword">true</highlight><highlight class="normal">,<sp /><sp /></highlight><highlight class="comment">/*<sp />Set<sp />secure<sp />mode<sp />to<sp />Bus<sp />Master<sp />*/</highlight><highlight class="normal" /></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><ref kindref="member" refid="group__group__prot__enums_1gga4486bc74d6e98ad99c1ce4d532e48d0fabf25e25bbce833636da932ce78e453e8">CY_PROT_PCMASK1</ref>);<sp /></highlight><highlight class="comment">/*<sp />Allow<sp />setting<sp />PC<sp />to<sp />PC=1<sp />only<sp />*/</highlight><highlight class="normal" /></codeline>
<codeline><highlight class="normal" /></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /></highlight><highlight class="comment">/*<sp />Configure<sp />slave<sp />attributes<sp />for<sp />PC=1<sp />which<sp />allow<sp />reading<sp />the<sp />HSIOM<sp />settings<sp /></highlight></codeline>
<codeline><highlight class="comment"><sp /><sp /><sp /><sp /><sp /><sp /><sp />of<sp />port<sp />1<sp />in<sp />privileged<sp />mode<sp />accesses<sp />for<sp />secure<sp />bus<sp />masters<sp />*/</highlight><highlight class="normal" /></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /></highlight><highlight class="keywordflow">if</highlight><highlight class="normal">(<ref kindref="member" refid="group__group__prot__enums_1gga13ede85383a4afaf5ed3a14b470e38bda462f07fcb31ed8a07dd273af4290691d">CY_PROT_SUCCESS</ref><sp />!=<sp /><ref kindref="member" refid="group__group__prot__functions__ppu__fixed__v2_1ga6112f6eba93e5db763e8dc571a91740a">Cy_Prot_ConfigPpuFixedSlaveAtt</ref>(PERI_MS_PPU_FX_HSIOM_PRT1_PRT,</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp />(uint16_t)<ref kindref="member" refid="group__group__prot__enums_1gga4486bc74d6e98ad99c1ce4d532e48d0fabf25e25bbce833636da932ce78e453e8">CY_PROT_PCMASK1</ref>,<sp /></highlight><highlight class="comment">/*<sp />Allow<sp />PC=0<sp />and<sp />PC=1<sp />accesses<sp />*/</highlight><highlight class="normal" /></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><ref kindref="member" refid="group__group__prot__enums_1gga330789bc5242c26b8a32df63fa8720e5a71e68b733a505b1241d49c8f62cbfb61">CY_PROT_PERM_DISABLED</ref>,<sp /><sp /></highlight><highlight class="comment">/*<sp />Disallow<sp />all<sp />user<sp />mode<sp />accesses<sp />*/</highlight><highlight class="normal" /></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><ref kindref="member" refid="group__group__prot__enums_1gga330789bc5242c26b8a32df63fa8720e5aad6e3850fb76be4bd78240a4fc284985">CY_PROT_PERM_R</ref>,<sp /><sp /></highlight><highlight class="comment">/*<sp />Allow<sp />Read<sp />privileged<sp />mode<sp />accesses<sp />*/</highlight><highlight class="normal" /></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /></highlight><highlight class="keyword">true</highlight><highlight class="normal">))<sp /></highlight><highlight class="comment">/*<sp />Only<sp />allow<sp />"secure"<sp />bus<sp />masters<sp />*/</highlight><highlight class="normal" /></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp />{</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /></highlight><highlight class="comment">/*<sp />Insert<sp />error<sp />handling<sp />*/</highlight><highlight class="normal" /></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp />}</highlight></codeline>
<codeline><highlight class="normal" /></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /></highlight><highlight class="comment">/*<sp />CM4<sp />needs<sp />to<sp />access<sp />a<sp />protected<sp />resources<sp />that<sp />require<sp />PC=1<sp />attribute<sp />*/</highlight><highlight class="normal" /></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /><ref kindref="member" refid="group__group__prot__functions__busmaster_1ga2d3a54039578a9fae98f6c7b4c4cff41">Cy_Prot_SetActivePC</ref>(CPUSS_MS_ID_CM4,<sp /><ref kindref="member" refid="group__group__prot__enums_1gga4467b93f3da4304e584726b975d3fabaa10dc15f2738ba076ded84cb7b9eac7fa">CY_PROT_PC1</ref>);</highlight></codeline>
<codeline><highlight class="normal" /></codeline>
</programlisting></para></simplesect>
</para>        </detaileddescription>
        <inbodydescription>
        </inbodydescription>
        <location bodyend="1280" bodyfile="/var/tmp/gitlab-runner1/builds/7e5c9300/0/repo/psoc6pdl/drivers/source/cy_prot.c" bodystart="1271" column="1" file="/var/tmp/gitlab-runner1/builds/7e5c9300/0/repo/psoc6pdl/drivers/include/cy_prot.h" line="1018" />
      </memberdef>
      </sectiondef>
    <briefdescription>
    </briefdescription>
    <detaileddescription>
    </detaileddescription>
  </compounddef>
</doxygen>