<doxygen xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" version="1.8.13" xsi:noNamespaceSchemaLocation="compound.xsd">
  <compounddef id="group__group__sar__functions__switches" kind="group">
    <compoundname>group_sar_functions_switches</compoundname>
    <title>SARMUX Switch Control Functions</title>
      <sectiondef kind="func">
      <memberdef const="no" explicit="no" id="group__group__sar__functions__switches_1gabc0b47c3fd4c0a21adb41140bb380592" inline="no" kind="function" prot="public" static="no" virt="non-virtual">
        <type>void</type>
        <definition>void Cy_SAR_SetAnalogSwitch</definition>
        <argsstring>(SAR_Type *base, cy_en_sar_switch_register_sel_t switchSelect, uint32_t switchMask, cy_en_sar_switch_state_t state)</argsstring>
        <name>Cy_SAR_SetAnalogSwitch</name>
        <param>
          <type><ref kindref="compound" refid="struct_s_a_r___type">SAR_Type</ref> *</type>
          <declname>base</declname>
        </param>
        <param>
          <type><ref kindref="member" refid="group__group__sar__enums_1gaa45a7677b7c1051696ea01c15439bc11">cy_en_sar_switch_register_sel_t</ref></type>
          <declname>switchSelect</declname>
        </param>
        <param>
          <type>uint32_t</type>
          <declname>switchMask</declname>
        </param>
        <param>
          <type><ref kindref="member" refid="group__group__sar__enums_1gaaf741c5e93385a873ab81b6fccd856ad">cy_en_sar_switch_state_t</ref></type>
          <declname>state</declname>
        </param>
        <briefdescription>
<para>Provide firmware control of the SARMUX switches for firmware sequencing. </para>        </briefdescription>
        <detaileddescription>
<para>Each call to this function can open or close a set of switches. Previously configured switches are untouched.</para><para>If the SARSEQ is enabled, there is no need to use this function.</para><para><parameterlist kind="param"><parameteritem>
<parameternamelist>
<parametername>base</parametername>
</parameternamelist>
<parameterdescription>
<para>Pointer to structure describing registers</para></parameterdescription>
</parameteritem>
<parameteritem>
<parameternamelist>
<parametername>switchSelect</parametername>
</parameternamelist>
<parameterdescription>
<para>The switch register that contains the desired switches. Select a value from <ref kindref="member" refid="group__group__sar__enums_1gaa45a7677b7c1051696ea01c15439bc11">cy_en_sar_switch_register_sel_t</ref>.</para></parameterdescription>
</parameteritem>
<parameteritem>
<parameternamelist>
<parametername>switchMask</parametername>
</parameternamelist>
<parameterdescription>
<para>The mask of the switches to either open or close. Select one or more values from the <ref kindref="member" refid="group__group__sar__mux__switch__register__enums_1ga54fb9cfe9a89cf962de036422f9d9782">cy_en_sar_mux_switch_fw_ctrl_t</ref> enum and "OR" them together.</para></parameterdescription>
</parameteritem>
<parameteritem>
<parameternamelist>
<parametername>state</parametername>
</parameternamelist>
<parameterdescription>
<para>Open or close the desired swithces. Select a value from <ref kindref="member" refid="group__group__sar__enums_1gaaf741c5e93385a873ab81b6fccd856ad">cy_en_sar_switch_state_t</ref>.</para></parameterdescription>
</parameteritem>
</parameterlist>
<simplesect kind="return"><para>None</para></simplesect>
<simplesect kind="par"><title>Function Usage</title><para /></simplesect>
<programlisting><codeline><highlight class="normal"><sp /><sp /><sp /><sp /></highlight><highlight class="comment">/*<sp />Scenario:<sp /></highlight></codeline>
<codeline><highlight class="comment"><sp /><sp /><sp /><sp /><sp />*<sp />-<sp />Channel<sp />0<sp />is<sp />configured<sp />as<sp />a<sp />differential<sp />pair<sp />between<sp />P10.0<sp />and<sp />P10.1.</highlight></codeline>
<codeline><highlight class="comment"><sp /><sp /><sp /><sp /><sp />*<sp />-<sp />Channel<sp />1<sp />is<sp />configured<sp />to<sp />sample<sp />the<sp />internal<sp />DieTemp<sp />sensor.</highlight></codeline>
<codeline><highlight class="comment"><sp /><sp /><sp /><sp /><sp />*<sp />Close<sp />required<sp />switches<sp />to<sp />make<sp />these<sp />connections.</highlight></codeline>
<codeline><highlight class="comment"><sp /><sp /><sp /><sp /><sp />*<sp />Make<sp />sure<sp />to<sp />also<sp />enable<sp />the<sp />SAR<sp />sequencer<sp />control<sp />of<sp />these<sp />same<sp />switches.<sp />*/</highlight><highlight class="normal" /></codeline>
<codeline><highlight class="normal" /></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp />uint32_t<sp />chan0SwitchMask<sp />=<sp /><ref kindref="member" refid="group__group__sar__mux__switch__register__enums_1gga54fb9cfe9a89cf962de036422f9d9782a8c541100815e95e985bcdd51a9c64146">CY_SAR_MUX_FW_P0_VPLUS</ref><sp />|<sp /><ref kindref="member" refid="group__group__sar__mux__switch__register__enums_1gga54fb9cfe9a89cf962de036422f9d9782ac89d1179be76fc8f2e0fcd88459e3f79">CY_SAR_MUX_FW_P1_VMINUS</ref>;</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp />uint32_t<sp />chan1SwitchMask<sp />=<sp /><ref kindref="member" refid="group__group__sar__mux__switch__register__enums_1gga54fb9cfe9a89cf962de036422f9d9782a4d91423741115c1f65c36b51fc2dd667">CY_SAR_MUX_FW_TEMP_VPLUS</ref><sp />|<sp /><ref kindref="member" refid="group__group__sar__mux__switch__register__enums_1gga54fb9cfe9a89cf962de036422f9d9782a01b9e241cc0e9cb3f7480e4ca817948d">CY_SAR_MUX_FW_VSSA_VMINUS</ref>;</highlight></codeline>
<codeline><highlight class="normal" /></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /></highlight><highlight class="comment">/*<sp />Close<sp />the<sp />switches<sp />for<sp />channel<sp />0.<sp />*/</highlight><highlight class="normal" /></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /><ref kindref="member" refid="group__group__sar__functions__switches_1gabc0b47c3fd4c0a21adb41140bb380592">Cy_SAR_SetAnalogSwitch</ref>(SAR,<sp /><ref kindref="member" refid="group__group__sar__enums_1ggaa45a7677b7c1051696ea01c15439bc11a78079f5b79489ddd8d21a0b4ec8be486">CY_SAR_MUX_SWITCH0</ref>,<sp />chan0SwitchMask,<sp /><ref kindref="member" refid="group__group__sar__enums_1ggaaf741c5e93385a873ab81b6fccd856ada209d2342e14329cfee7d0401ea896e21">CY_SAR_SWITCH_CLOSE</ref>);</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /></highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /></highlight><highlight class="comment">/*<sp />Close<sp />the<sp />switches<sp />for<sp />channel<sp />1.<sp />*/</highlight><highlight class="normal" /></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /><ref kindref="member" refid="group__group__sar__functions__switches_1gabc0b47c3fd4c0a21adb41140bb380592">Cy_SAR_SetAnalogSwitch</ref>(SAR,<sp /><ref kindref="member" refid="group__group__sar__enums_1ggaa45a7677b7c1051696ea01c15439bc11a78079f5b79489ddd8d21a0b4ec8be486">CY_SAR_MUX_SWITCH0</ref>,<sp />chan1SwitchMask,<sp /><ref kindref="member" refid="group__group__sar__enums_1ggaaf741c5e93385a873ab81b6fccd856ada209d2342e14329cfee7d0401ea896e21">CY_SAR_SWITCH_CLOSE</ref>);</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /></highlight></codeline>
</programlisting></para>        </detaileddescription>
        <inbodydescription>
        </inbodydescription>
        <location bodyend="1240" bodyfile="/var/tmp/gitlab-runner1/builds/7e5c9300/0/repo/psoc6pdl/drivers/source/cy_sar.c" bodystart="1215" column="1" file="/var/tmp/gitlab-runner1/builds/7e5c9300/0/repo/psoc6pdl/drivers/include/cy_sar.h" line="1459" />
      </memberdef>
      <memberdef const="no" explicit="no" id="group__group__sar__functions__switches_1ga32b1e620f85def61a81a0f66f4349498" inline="no" kind="function" prot="public" static="no" virt="non-virtual">
        <type>uint32_t</type>
        <definition>uint32_t Cy_SAR_GetAnalogSwitch</definition>
        <argsstring>(const SAR_Type *base, cy_en_sar_switch_register_sel_t switchSelect)</argsstring>
        <name>Cy_SAR_GetAnalogSwitch</name>
        <param>
          <type>const <ref kindref="compound" refid="struct_s_a_r___type">SAR_Type</ref> *</type>
          <declname>base</declname>
        </param>
        <param>
          <type><ref kindref="member" refid="group__group__sar__enums_1gaa45a7677b7c1051696ea01c15439bc11">cy_en_sar_switch_register_sel_t</ref></type>
          <declname>switchSelect</declname>
        </param>
        <briefdescription>
<para>Return the state (open or close) of SARMUX switches. </para>        </briefdescription>
        <detaileddescription>
<para><parameterlist kind="param"><parameteritem>
<parameternamelist>
<parametername>base</parametername>
</parameternamelist>
<parameterdescription>
<para>Pointer to structure describing registers</para></parameterdescription>
</parameteritem>
<parameteritem>
<parameternamelist>
<parametername>switchSelect</parametername>
</parameternamelist>
<parameterdescription>
<para>The switch register that contains the desired switches. Select a value from <ref kindref="member" refid="group__group__sar__enums_1gaa45a7677b7c1051696ea01c15439bc11">cy_en_sar_switch_register_sel_t</ref>.</para></parameterdescription>
</parameteritem>
</parameterlist>
<simplesect kind="return"><para>Each bit corresponds to a single switch, where a bit value of 0 is open and 1 is closed. Compare this value to the switch masks in <ref kindref="member" refid="group__group__sar__mux__switch__register__enums_1ga54fb9cfe9a89cf962de036422f9d9782">cy_en_sar_mux_switch_fw_ctrl_t</ref>. </para></simplesect>
</para>        </detaileddescription>
        <inbodydescription>
        </inbodydescription>
        <location bodyend="1267" bodyfile="/var/tmp/gitlab-runner1/builds/7e5c9300/0/repo/psoc6pdl/drivers/source/cy_sar.c" bodystart="1262" column="1" file="/var/tmp/gitlab-runner1/builds/7e5c9300/0/repo/psoc6pdl/drivers/include/cy_sar.h" line="1460" />
      </memberdef>
      <memberdef const="no" explicit="no" id="group__group__sar__functions__switches_1ga7e94452c5ec875c0d3d09d19c8c79021" inline="no" kind="function" prot="public" static="no" virt="non-virtual">
        <type>__STATIC_INLINE void</type>
        <definition>__STATIC_INLINE void Cy_SAR_SetVssaVminusSwitch</definition>
        <argsstring>(SAR_Type *base, cy_en_sar_switch_state_t state)</argsstring>
        <name>Cy_SAR_SetVssaVminusSwitch</name>
        <param>
          <type><ref kindref="compound" refid="struct_s_a_r___type">SAR_Type</ref> *</type>
          <declname>base</declname>
        </param>
        <param>
          <type><ref kindref="member" refid="group__group__sar__enums_1gaaf741c5e93385a873ab81b6fccd856ad">cy_en_sar_switch_state_t</ref></type>
          <declname>state</declname>
        </param>
        <briefdescription>
<para>Open or close the switch between VSSA and Vminus of the SARADC through firmware. </para>        </briefdescription>
        <detaileddescription>
<para>This function calls <ref kindref="member" refid="group__group__sar__functions__switches_1gabc0b47c3fd4c0a21adb41140bb380592">Cy_SAR_SetAnalogSwitch</ref> with switchSelect set to <ref kindref="member" refid="group__group__sar__enums_1ggaa45a7677b7c1051696ea01c15439bc11a78079f5b79489ddd8d21a0b4ec8be486">CY_SAR_MUX_SWITCH0</ref> and switchMask set to SAR_MUX_SWITCH0_MUX_FW_VSSA_VMINUS_Msk.</para><para><parameterlist kind="param"><parameteritem>
<parameternamelist>
<parametername>base</parametername>
</parameternamelist>
<parameterdescription>
<para>Pointer to structure describing registers</para></parameterdescription>
</parameteritem>
<parameteritem>
<parameternamelist>
<parametername>state</parametername>
</parameternamelist>
<parameterdescription>
<para>Open or close the switch. Select a value from <ref kindref="member" refid="group__group__sar__enums_1gaaf741c5e93385a873ab81b6fccd856ad">cy_en_sar_switch_state_t</ref>.</para></parameterdescription>
</parameteritem>
</parameterlist>
<simplesect kind="return"><para>None</para></simplesect>
<simplesect kind="par"><title>Function Usage</title><para /></simplesect>
<programlisting><codeline><highlight class="normal"><sp /><sp /><sp /><sp /></highlight><highlight class="comment">/*<sp />Scenario:<sp />Easily<sp />close<sp />the<sp />switch<sp />between<sp />Vminus<sp />of<sp />the<sp />SARADC<sp />and<sp />VSSA.<sp />*/</highlight><highlight class="normal" /></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /><ref kindref="member" refid="group__group__sar__functions__switches_1ga7e94452c5ec875c0d3d09d19c8c79021">Cy_SAR_SetVssaVminusSwitch</ref>(SAR,<sp /><ref kindref="member" refid="group__group__sar__enums_1ggaaf741c5e93385a873ab81b6fccd856ada209d2342e14329cfee7d0401ea896e21">CY_SAR_SWITCH_CLOSE</ref>);</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /></highlight></codeline>
</programlisting></para>        </detaileddescription>
        <inbodydescription>
        </inbodydescription>
        <location bodyend="2187" bodyfile="/var/tmp/gitlab-runner1/builds/7e5c9300/0/repo/psoc6pdl/drivers/include/cy_sar.h" bodystart="2184" column="1" file="/var/tmp/gitlab-runner1/builds/7e5c9300/0/repo/psoc6pdl/drivers/include/cy_sar.h" line="1461" />
      </memberdef>
      <memberdef const="no" explicit="no" id="group__group__sar__functions__switches_1ga0f2411703292dae743387769f41b14d4" inline="no" kind="function" prot="public" static="no" virt="non-virtual">
        <type>void</type>
        <definition>void Cy_SAR_SetSwitchSarSeqCtrl</definition>
        <argsstring>(SAR_Type *base, uint32_t switchMask, cy_en_sar_switch_sar_seq_ctrl_t ctrl)</argsstring>
        <name>Cy_SAR_SetSwitchSarSeqCtrl</name>
        <param>
          <type><ref kindref="compound" refid="struct_s_a_r___type">SAR_Type</ref> *</type>
          <declname>base</declname>
        </param>
        <param>
          <type>uint32_t</type>
          <declname>switchMask</declname>
        </param>
        <param>
          <type><ref kindref="member" refid="group__group__sar__enums_1gad920c2a523792124fdb8a50052247705">cy_en_sar_switch_sar_seq_ctrl_t</ref></type>
          <declname>ctrl</declname>
        </param>
        <briefdescription>
<para>Enable or disable SARSEQ control of one or more switches. </para>        </briefdescription>
        <detaileddescription>
<para>Previously configured switches are untouched.</para><para><parameterlist kind="param"><parameteritem>
<parameternamelist>
<parametername>base</parametername>
</parameternamelist>
<parameterdescription>
<para>Pointer to structure describing registers</para></parameterdescription>
</parameteritem>
<parameteritem>
<parameternamelist>
<parametername>switchMask</parametername>
</parameternamelist>
<parameterdescription>
<para>The mask of the switches. Select one or more values from the <ref kindref="member" refid="group__group__sar__mux__switch__register__enums_1ga5af95eff1664f03132e1e02dd865f8a4">cy_en_sar_mux_switch_sq_ctrl_t</ref> enum and "OR" them together.</para></parameterdescription>
</parameteritem>
<parameteritem>
<parameternamelist>
<parametername>ctrl</parametername>
</parameternamelist>
<parameterdescription>
<para>Enable or disable SARSEQ control. Select a value from <ref kindref="member" refid="group__group__sar__enums_1gad920c2a523792124fdb8a50052247705">cy_en_sar_switch_sar_seq_ctrl_t</ref>.</para></parameterdescription>
</parameteritem>
</parameterlist>
<simplesect kind="return"><para>None</para></simplesect>
<simplesect kind="par"><title>Function Usage</title><para /></simplesect>
<programlisting><codeline><highlight class="normal"><sp /><sp /><sp /><sp /></highlight><highlight class="comment">/*<sp />Scenario:<sp />The<sp />SAR<sp />sequencer<sp />needs<sp />control<sp />of<sp />all<sp />switches<sp />on<sp />the<sp />dedicated<sp />SARMUX<sp />port</highlight></codeline>
<codeline><highlight class="comment"><sp /><sp /><sp /><sp /><sp />*<sp />so<sp />that<sp />the<sp />channels<sp />can<sp />by<sp />sampled<sp />in<sp />a<sp />round<sp />robin<sp />fashion<sp />without<sp />CPU<sp />intervention.<sp />*/</highlight><highlight class="normal" /></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /></highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp />uint32_t<sp />switchMask<sp /><sp /><sp /><sp /><sp />=<sp /><ref kindref="member" refid="group__group__sar__mux__switch__register__enums_1gga5af95eff1664f03132e1e02dd865f8a4ae2ca2428426932a81a095844b36314b6">CY_SAR_MUX_SQ_CTRL_P0</ref><sp />\</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp />|<sp /><ref kindref="member" refid="group__group__sar__mux__switch__register__enums_1gga5af95eff1664f03132e1e02dd865f8a4a00768f0a6918bae046018ded9e2628ca">CY_SAR_MUX_SQ_CTRL_P1</ref><sp />\</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp />|<sp /><ref kindref="member" refid="group__group__sar__mux__switch__register__enums_1gga5af95eff1664f03132e1e02dd865f8a4ac9f59d4e204a19f5be4b61173d46f221">CY_SAR_MUX_SQ_CTRL_P2</ref><sp />\</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp />|<sp /><ref kindref="member" refid="group__group__sar__mux__switch__register__enums_1gga5af95eff1664f03132e1e02dd865f8a4a9eedfe9852a11bdb757dbe888b9822dc">CY_SAR_MUX_SQ_CTRL_P3</ref><sp />\</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp />|<sp /><ref kindref="member" refid="group__group__sar__mux__switch__register__enums_1gga5af95eff1664f03132e1e02dd865f8a4aaa1c4bc98c48eae355d7c8c06170f660">CY_SAR_MUX_SQ_CTRL_P4</ref><sp />\</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp />|<sp /><ref kindref="member" refid="group__group__sar__mux__switch__register__enums_1gga5af95eff1664f03132e1e02dd865f8a4a2187419a0095068a453be7ac03561b89">CY_SAR_MUX_SQ_CTRL_P5</ref><sp />\</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp />|<sp /><ref kindref="member" refid="group__group__sar__mux__switch__register__enums_1gga5af95eff1664f03132e1e02dd865f8a4a63f28fb1e90c6605f370b7fdfb5c290d">CY_SAR_MUX_SQ_CTRL_P6</ref><sp />\</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp />|<sp /><ref kindref="member" refid="group__group__sar__mux__switch__register__enums_1gga5af95eff1664f03132e1e02dd865f8a4ae3bcf75db0a900ada482b507468874db">CY_SAR_MUX_SQ_CTRL_P7</ref>;</highlight></codeline>
<codeline><highlight class="normal" /></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /><ref kindref="member" refid="group__group__sar__functions__switches_1ga0f2411703292dae743387769f41b14d4">Cy_SAR_SetSwitchSarSeqCtrl</ref>(SAR,<sp />switchMask,<sp /><ref kindref="member" refid="group__group__sar__enums_1ggad920c2a523792124fdb8a50052247705a8c8f5667c8b22cf76bc5dc6a0b250441">CY_SAR_SWITCH_SEQ_CTRL_ENABLE</ref>);</highlight></codeline>
</programlisting></para>        </detaileddescription>
        <inbodydescription>
        </inbodydescription>
        <location bodyend="1310" bodyfile="/var/tmp/gitlab-runner1/builds/7e5c9300/0/repo/psoc6pdl/drivers/source/cy_sar.c" bodystart="1295" column="1" file="/var/tmp/gitlab-runner1/builds/7e5c9300/0/repo/psoc6pdl/drivers/include/cy_sar.h" line="1462" />
      </memberdef>
      <memberdef const="no" explicit="no" id="group__group__sar__functions__switches_1ga07398d4aeee19d82d96d2db02c141a82" inline="no" kind="function" prot="public" static="no" virt="non-virtual">
        <type>__STATIC_INLINE void</type>
        <definition>__STATIC_INLINE void Cy_SAR_SetVssaSarSeqCtrl</definition>
        <argsstring>(SAR_Type *base, cy_en_sar_switch_sar_seq_ctrl_t ctrl)</argsstring>
        <name>Cy_SAR_SetVssaSarSeqCtrl</name>
        <param>
          <type><ref kindref="compound" refid="struct_s_a_r___type">SAR_Type</ref> *</type>
          <declname>base</declname>
        </param>
        <param>
          <type><ref kindref="member" refid="group__group__sar__enums_1gad920c2a523792124fdb8a50052247705">cy_en_sar_switch_sar_seq_ctrl_t</ref></type>
          <declname>ctrl</declname>
        </param>
        <briefdescription>
<para>Enable or disable SARSEQ control of the switch between VSSA and Vminus of the SARADC. </para>        </briefdescription>
        <detaileddescription>
<para>This function calls <ref kindref="member" refid="group__group__sar__functions__switches_1ga0f2411703292dae743387769f41b14d4">Cy_SAR_SetSwitchSarSeqCtrl</ref> with switchMask set to SAR_MUX_SWITCH_SQ_CTRL_MUX_SQ_CTRL_VSSA_Msk.</para><para><parameterlist kind="param"><parameteritem>
<parameternamelist>
<parametername>base</parametername>
</parameternamelist>
<parameterdescription>
<para>Pointer to structure describing registers</para></parameterdescription>
</parameteritem>
<parameteritem>
<parameternamelist>
<parametername>ctrl</parametername>
</parameternamelist>
<parameterdescription>
<para>Enable or disable control. Select a value from <ref kindref="member" refid="group__group__sar__enums_1gad920c2a523792124fdb8a50052247705">cy_en_sar_switch_sar_seq_ctrl_t</ref>.</para></parameterdescription>
</parameteritem>
</parameterlist>
<simplesect kind="return"><para>None</para></simplesect>
<simplesect kind="par"><title>Function Usage</title><para /></simplesect>
<programlisting><codeline><highlight class="normal"><sp /><sp /><sp /><sp /></highlight><highlight class="comment">/*<sp />Scenario:<sp />Easily<sp />enable<sp />SARSEQ<sp />control<sp />of<sp />the<sp />switch<sp />between<sp />Vminus<sp />of<sp />the<sp />SARADC<sp />and<sp />VSSA.<sp />*/</highlight><highlight class="normal" /></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /><ref kindref="member" refid="group__group__sar__functions__switches_1ga07398d4aeee19d82d96d2db02c141a82">Cy_SAR_SetVssaSarSeqCtrl</ref>(SAR,<sp /><ref kindref="member" refid="group__group__sar__enums_1ggad920c2a523792124fdb8a50052247705a8c8f5667c8b22cf76bc5dc6a0b250441">CY_SAR_SWITCH_SEQ_CTRL_ENABLE</ref>);</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /></highlight></codeline>
</programlisting></para>        </detaileddescription>
        <inbodydescription>
        </inbodydescription>
        <location bodyend="2214" bodyfile="/var/tmp/gitlab-runner1/builds/7e5c9300/0/repo/psoc6pdl/drivers/include/cy_sar.h" bodystart="2211" column="1" file="/var/tmp/gitlab-runner1/builds/7e5c9300/0/repo/psoc6pdl/drivers/include/cy_sar.h" line="1463" />
      </memberdef>
      </sectiondef>
    <briefdescription>
<para>This set of functions is for controlling/querying the SARMUX switches. </para>    </briefdescription>
    <detaileddescription>
    </detaileddescription>
  </compounddef>
</doxygen>