<doxygen xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" version="1.8.13" xsi:noNamespaceSchemaLocation="compound.xsd">
  <compounddef id="group__group__scb__spi__low__level__functions" kind="group">
    <compoundname>group_scb_spi_low_level_functions</compoundname>
    <title>Low-Level</title>
      <sectiondef kind="func">
      <memberdef const="no" explicit="no" id="group__group__scb__spi__low__level__functions_1ga169c313a3508261fa76e6b6600cce726" inline="no" kind="function" prot="public" static="no" virt="non-virtual">
        <type>__STATIC_INLINE uint32_t</type>
        <definition>__STATIC_INLINE uint32_t Cy_SCB_SPI_Read</definition>
        <argsstring>(CySCB_Type const *base)</argsstring>
        <name>Cy_SCB_SPI_Read</name>
        <param>
          <type><ref kindref="compound" refid="struct_cy_s_c_b___type">CySCB_Type</ref> const *</type>
          <declname>base</declname>
        </param>
        <briefdescription>
<para>Reads a single data element from the SPI RX FIFO. </para>        </briefdescription>
        <detaileddescription>
<para>This function does not check whether the RX FIFO has data before reading it. If the RX FIFO is empty, the function returns <ref kindref="member" refid="group__group__scb__spi__macros_1ga065d56e0799fbb18c301ba22aaccd13c">CY_SCB_SPI_RX_NO_DATA</ref>.</para><para><parameterlist kind="param"><parameteritem>
<parameternamelist>
<parametername>base</parametername>
</parameternamelist>
<parameterdescription>
<para>The pointer to the SPI SCB instance.</para></parameterdescription>
</parameteritem>
</parameterlist>
<simplesect kind="return"><para>Data from the RX FIFO. The data element size is defined by the configured RX data width.</para></simplesect>
<verbatim>embed:rst 
.. raw:: html

  &lt;div class="admonition note"&gt;&lt;p class="admonition-title"&gt;note&lt;/p&gt;&lt;p&gt;&lt;p&gt;&lt;ul&gt;&lt;li&gt;&lt;p&gt;This function only reads data available in the RX FIFO. It does not initiate an SPI transfer.&lt;/p&gt;&lt;/li&gt;&lt;li&gt;&lt;p&gt;When in the master mode, this function writes data into the TX FIFO and waits until the transfer is completed before reading data from the RX FIFO. &lt;/p&gt;&lt;/li&gt;&lt;/ul&gt;&lt;/p&gt;&lt;/p&gt;&lt;/div&gt;</verbatim></para>        </detaileddescription>
        <inbodydescription>
        </inbodydescription>
        <location bodyend="1262" bodyfile="/var/tmp/gitlab-runner1/builds/7e5c9300/0/repo/psoc6pdl/drivers/include/cy_scb_spi.h" bodystart="1259" column="1" file="/var/tmp/gitlab-runner1/builds/7e5c9300/0/repo/psoc6pdl/drivers/include/cy_scb_spi.h" line="594" />
      </memberdef>
      <memberdef const="no" explicit="no" id="group__group__scb__spi__low__level__functions_1gaecc7c7a8d0ba840fa5d65e5076cb9d84" inline="no" kind="function" prot="public" static="no" virt="non-virtual">
        <type>__STATIC_INLINE uint32_t</type>
        <definition>__STATIC_INLINE uint32_t Cy_SCB_SPI_ReadArray</definition>
        <argsstring>(CySCB_Type const *base, void *buffer, uint32_t size)</argsstring>
        <name>Cy_SCB_SPI_ReadArray</name>
        <param>
          <type><ref kindref="compound" refid="struct_cy_s_c_b___type">CySCB_Type</ref> const *</type>
          <declname>base</declname>
        </param>
        <param>
          <type>void *</type>
          <declname>buffer</declname>
        </param>
        <param>
          <type>uint32_t</type>
          <declname>size</declname>
        </param>
        <briefdescription>
<para>Reads an array of data out of the SPI RX FIFO. </para>        </briefdescription>
        <detaileddescription>
<para>This function does not block. It returns how many data elements were read from the RX FIFO.</para><para><parameterlist kind="param"><parameteritem>
<parameternamelist>
<parametername>base</parametername>
</parameternamelist>
<parameterdescription>
<para>The pointer to the SPI SCB instance.</para></parameterdescription>
</parameteritem>
<parameteritem>
<parameternamelist>
<parametername>buffer</parametername>
</parameternamelist>
<parameterdescription>
<para>The pointer to the location to place data read from the RX FIFO. The element size is defined by the data type, which depends on the configured RX data width.</para></parameterdescription>
</parameteritem>
<parameteritem>
<parameternamelist>
<parametername>size</parametername>
</parameternamelist>
<parameterdescription>
<para>The number of data elements to read from the RX FIFO.</para></parameterdescription>
</parameteritem>
</parameterlist>
<simplesect kind="return"><para>The number of data elements read from the RX FIFO.</para></simplesect>
<verbatim>embed:rst 
.. raw:: html

  &lt;div class="admonition note"&gt;&lt;p class="admonition-title"&gt;note&lt;/p&gt;&lt;p&gt;&lt;p&gt;&lt;ul&gt;&lt;li&gt;&lt;p&gt;This function only reads data available in the RX FIFO. It does not initiate an SPI transfer.&lt;/p&gt;&lt;/li&gt;&lt;li&gt;&lt;p&gt;When in the master mode, this function writes data into the TX FIFO and waits until the transfer is completed before reading data from the RX FIFO. &lt;/p&gt;&lt;/li&gt;&lt;/ul&gt;&lt;/p&gt;&lt;/p&gt;&lt;/div&gt;</verbatim></para>        </detaileddescription>
        <inbodydescription>
        </inbodydescription>
        <location bodyend="1299" bodyfile="/var/tmp/gitlab-runner1/builds/7e5c9300/0/repo/psoc6pdl/drivers/include/cy_scb_spi.h" bodystart="1294" column="1" file="/var/tmp/gitlab-runner1/builds/7e5c9300/0/repo/psoc6pdl/drivers/include/cy_scb_spi.h" line="595" />
      </memberdef>
      <memberdef const="no" explicit="no" id="group__group__scb__spi__low__level__functions_1ga6b613c5544c0595763e30d83da125a4d" inline="no" kind="function" prot="public" static="no" virt="non-virtual">
        <type>__STATIC_INLINE uint32_t</type>
        <definition>__STATIC_INLINE uint32_t Cy_SCB_SPI_Write</definition>
        <argsstring>(CySCB_Type *base, uint32_t data)</argsstring>
        <name>Cy_SCB_SPI_Write</name>
        <param>
          <type><ref kindref="compound" refid="struct_cy_s_c_b___type">CySCB_Type</ref> *</type>
          <declname>base</declname>
        </param>
        <param>
          <type>uint32_t</type>
          <declname>data</declname>
        </param>
        <briefdescription>
<para>Places a single data element in the SPI TX FIFO. </para>        </briefdescription>
        <detaileddescription>
<para>This function does not block. It returns how many data elements were placed in the TX FIFO.</para><para><parameterlist kind="param"><parameteritem>
<parameternamelist>
<parametername>base</parametername>
</parameternamelist>
<parameterdescription>
<para>The pointer to the SPI SCB instance.</para></parameterdescription>
</parameteritem>
<parameteritem>
<parameternamelist>
<parametername>data</parametername>
</parameternamelist>
<parameterdescription>
<para>Data to put in the TX FIFO. The element size is defined by the data type, which depends on the configured TX data width.</para></parameterdescription>
</parameteritem>
</parameterlist>
<simplesect kind="return"><para>The number of data elements placed in the TX FIFO: 0 or 1.</para></simplesect>
<verbatim>embed:rst 
.. raw:: html

  &lt;div class="admonition note"&gt;&lt;p class="admonition-title"&gt;note&lt;/p&gt;&lt;p&gt;&lt;p&gt;&lt;ul&gt;&lt;li&gt;&lt;p&gt;When in the master mode, writing data into the TX FIFO starts an SPI transfer.&lt;/p&gt;&lt;/li&gt;&lt;li&gt;&lt;p&gt;When in the slave mode, writing data into the TX FIFO does not start an SPI transfer. The data is loaded in the TX FIFO and will be sent to the master on its request.&lt;/p&gt;&lt;/li&gt;&lt;li&gt;&lt;p&gt;The SPI interface is full-duplex, therefore reads and writes occur at the same time. Thus, for every data element transferred out of the TX FIFO, one is transferred into the RX FIFO. &lt;/p&gt;&lt;/li&gt;&lt;/ul&gt;&lt;/p&gt;&lt;/p&gt;&lt;/div&gt;</verbatim></para>        </detaileddescription>
        <inbodydescription>
        </inbodydescription>
        <location bodyend="1335" bodyfile="/var/tmp/gitlab-runner1/builds/7e5c9300/0/repo/psoc6pdl/drivers/include/cy_scb_spi.h" bodystart="1332" column="1" file="/var/tmp/gitlab-runner1/builds/7e5c9300/0/repo/psoc6pdl/drivers/include/cy_scb_spi.h" line="597" />
      </memberdef>
      <memberdef const="no" explicit="no" id="group__group__scb__spi__low__level__functions_1gad5415bd46d159b57f08fa0eb375a133e" inline="no" kind="function" prot="public" static="no" virt="non-virtual">
        <type>__STATIC_INLINE uint32_t</type>
        <definition>__STATIC_INLINE uint32_t Cy_SCB_SPI_WriteArray</definition>
        <argsstring>(CySCB_Type *base, void *buffer, uint32_t size)</argsstring>
        <name>Cy_SCB_SPI_WriteArray</name>
        <param>
          <type><ref kindref="compound" refid="struct_cy_s_c_b___type">CySCB_Type</ref> *</type>
          <declname>base</declname>
        </param>
        <param>
          <type>void *</type>
          <declname>buffer</declname>
        </param>
        <param>
          <type>uint32_t</type>
          <declname>size</declname>
        </param>
        <briefdescription>
<para>Places an array of data in the SPI TX FIFO. </para>        </briefdescription>
        <detaileddescription>
<para>This function does not block. It returns how many data elements were placed in the TX FIFO.</para><para><parameterlist kind="param"><parameteritem>
<parameternamelist>
<parametername>base</parametername>
</parameternamelist>
<parameterdescription>
<para>The pointer to the SPI SCB instance.</para></parameterdescription>
</parameteritem>
<parameteritem>
<parameternamelist>
<parametername>buffer</parametername>
</parameternamelist>
<parameterdescription>
<para>The pointer to the data to place in the TX FIFO. The element size is defined by the data type, which depends on the configured TX data width.</para></parameterdescription>
</parameteritem>
<parameteritem>
<parameternamelist>
<parametername>size</parametername>
</parameternamelist>
<parameterdescription>
<para>The number of data elements to transmit.</para></parameterdescription>
</parameteritem>
</parameterlist>
<simplesect kind="return"><para>The number of data elements placed in the TX FIFO.</para></simplesect>
<verbatim>embed:rst 
.. raw:: html

  &lt;div class="admonition note"&gt;&lt;p class="admonition-title"&gt;note&lt;/p&gt;&lt;p&gt;&lt;p&gt;&lt;ul&gt;&lt;li&gt;&lt;p&gt;When in the master mode, writing data into the TX FIFO starts an SPI transfer.&lt;/p&gt;&lt;/li&gt;&lt;li&gt;&lt;p&gt;When in the slave mode, writing data into the TX FIFO does not start an SPI transfer. The data is loaded in the TX FIFO and will be sent to the master on its request.&lt;/p&gt;&lt;/li&gt;&lt;li&gt;&lt;p&gt;The SPI interface is full-duplex, therefore reads and writes occur at the same time. Thus, for every data element transferred out of the TX FIFO, one is transferred into the RX FIFO. &lt;/p&gt;&lt;/li&gt;&lt;/ul&gt;&lt;/p&gt;&lt;/p&gt;&lt;/div&gt;</verbatim></para>        </detaileddescription>
        <inbodydescription>
        </inbodydescription>
        <location bodyend="1375" bodyfile="/var/tmp/gitlab-runner1/builds/7e5c9300/0/repo/psoc6pdl/drivers/include/cy_scb_spi.h" bodystart="1370" column="1" file="/var/tmp/gitlab-runner1/builds/7e5c9300/0/repo/psoc6pdl/drivers/include/cy_scb_spi.h" line="598" />
      </memberdef>
      <memberdef const="no" explicit="no" id="group__group__scb__spi__low__level__functions_1ga6899c2b1dd9f868a7fb625b03a7ee594" inline="no" kind="function" prot="public" static="no" virt="non-virtual">
        <type>__STATIC_INLINE void</type>
        <definition>__STATIC_INLINE void Cy_SCB_SPI_WriteArrayBlocking</definition>
        <argsstring>(CySCB_Type *base, void *buffer, uint32_t size)</argsstring>
        <name>Cy_SCB_SPI_WriteArrayBlocking</name>
        <param>
          <type><ref kindref="compound" refid="struct_cy_s_c_b___type">CySCB_Type</ref> *</type>
          <declname>base</declname>
        </param>
        <param>
          <type>void *</type>
          <declname>buffer</declname>
        </param>
        <param>
          <type>uint32_t</type>
          <declname>size</declname>
        </param>
        <briefdescription>
<para>Places an array of data in the SPI TX FIFO. </para>        </briefdescription>
        <detaileddescription>
<para>This function blocks until the number of data elements specified by size is placed in the SPI TX FIFO.</para><para><parameterlist kind="param"><parameteritem>
<parameternamelist>
<parametername>base</parametername>
</parameternamelist>
<parameterdescription>
<para>The pointer to the SPI SCB instance.</para></parameterdescription>
</parameteritem>
<parameteritem>
<parameternamelist>
<parametername>buffer</parametername>
</parameternamelist>
<parameterdescription>
<para>The pointer to data to place in the TX FIFO. The element size is defined by the data type, which depends on the configured TX data width.</para></parameterdescription>
</parameteritem>
<parameteritem>
<parameternamelist>
<parametername>size</parametername>
</parameternamelist>
<parameterdescription>
<para>The number of data elements to write into the TX FIFO.</para></parameterdescription>
</parameteritem>
</parameterlist>
<verbatim>embed:rst 
.. raw:: html

  &lt;div class="admonition note"&gt;&lt;p class="admonition-title"&gt;note&lt;/p&gt;&lt;p&gt;&lt;p&gt;&lt;ul&gt;&lt;li&gt;&lt;p&gt;When in the master mode, writing data into the TX FIFO starts an SPI transfer.&lt;/p&gt;&lt;/li&gt;&lt;li&gt;&lt;p&gt;When in the slave mode, writing data into the TX FIFO does not start an SPI transfer. The data is loaded in the TX FIFO and will be sent to the master on its request.&lt;/p&gt;&lt;/li&gt;&lt;li&gt;&lt;p&gt;The SPI interface is full-duplex, therefore reads and writes occur at the same time. Thus, for every data element transferred out of the TX FIFO, one is transferred into the RX FIFO. &lt;/p&gt;&lt;/li&gt;&lt;/ul&gt;&lt;/p&gt;&lt;/p&gt;&lt;/div&gt;</verbatim></para>        </detaileddescription>
        <inbodydescription>
        </inbodydescription>
        <location bodyend="1413" bodyfile="/var/tmp/gitlab-runner1/builds/7e5c9300/0/repo/psoc6pdl/drivers/include/cy_scb_spi.h" bodystart="1408" column="1" file="/var/tmp/gitlab-runner1/builds/7e5c9300/0/repo/psoc6pdl/drivers/include/cy_scb_spi.h" line="599" />
      </memberdef>
      <memberdef const="no" explicit="no" id="group__group__scb__spi__low__level__functions_1ga987fdbe86c1fbfbadfa51cdf82f84632" inline="no" kind="function" prot="public" static="no" virt="non-virtual">
        <type>__STATIC_INLINE uint32_t</type>
        <definition>__STATIC_INLINE uint32_t Cy_SCB_SPI_GetTxFifoStatus</definition>
        <argsstring>(CySCB_Type const *base)</argsstring>
        <name>Cy_SCB_SPI_GetTxFifoStatus</name>
        <param>
          <type><ref kindref="compound" refid="struct_cy_s_c_b___type">CySCB_Type</ref> const *</type>
          <declname>base</declname>
        </param>
        <briefdescription>
<para>Returns the current status of the TX FIFO. </para>        </briefdescription>
        <detaileddescription>
<para><parameterlist kind="param"><parameteritem>
<parameternamelist>
<parametername>base</parametername>
</parameternamelist>
<parameterdescription>
<para>The pointer to the SPI SCB instance.</para></parameterdescription>
</parameteritem>
</parameterlist>
<simplesect kind="return"><para><ref kindref="compound" refid="group__group__scb__spi__macros__tx__fifo__status">SPI TX FIFO Statuses</ref> </para></simplesect>
</para>        </detaileddescription>
        <inbodydescription>
        </inbodydescription>
        <location bodyend="1079" bodyfile="/var/tmp/gitlab-runner1/builds/7e5c9300/0/repo/psoc6pdl/drivers/include/cy_scb_spi.h" bodystart="1076" column="1" file="/var/tmp/gitlab-runner1/builds/7e5c9300/0/repo/psoc6pdl/drivers/include/cy_scb_spi.h" line="601" />
      </memberdef>
      <memberdef const="no" explicit="no" id="group__group__scb__spi__low__level__functions_1gadf6044cb7d9567450502fec59537dad4" inline="no" kind="function" prot="public" static="no" virt="non-virtual">
        <type>__STATIC_INLINE void</type>
        <definition>__STATIC_INLINE void Cy_SCB_SPI_ClearTxFifoStatus</definition>
        <argsstring>(CySCB_Type *base, uint32_t clearMask)</argsstring>
        <name>Cy_SCB_SPI_ClearTxFifoStatus</name>
        <param>
          <type><ref kindref="compound" refid="struct_cy_s_c_b___type">CySCB_Type</ref> *</type>
          <declname>base</declname>
        </param>
        <param>
          <type>uint32_t</type>
          <declname>clearMask</declname>
        </param>
        <briefdescription>
<para>Clears the selected statuses of the TX FIFO. </para>        </briefdescription>
        <detaileddescription>
<para><parameterlist kind="param"><parameteritem>
<parameternamelist>
<parametername>base</parametername>
</parameternamelist>
<parameterdescription>
<para>The pointer to the SPI SCB instance.</para></parameterdescription>
</parameteritem>
<parameteritem>
<parameternamelist>
<parametername>clearMask</parametername>
</parameternamelist>
<parameterdescription>
<para>The mask of which statuses to clear. See <ref kindref="compound" refid="group__group__scb__spi__macros__tx__fifo__status">SPI TX FIFO Statuses</ref> for the set of constants.</para></parameterdescription>
</parameteritem>
</parameterlist>
<verbatim>embed:rst 
.. raw:: html

  &lt;div class="admonition note"&gt;&lt;p class="admonition-title"&gt;note&lt;/p&gt;&lt;p&gt;&lt;p&gt;&lt;ul&gt;&lt;li&gt;&lt;p&gt;The status is also used for interrupt generation, so clearing it also clears the interrupt sources.&lt;/p&gt;&lt;/li&gt;&lt;li&gt;&lt;p&gt;Level sensitive statuses such as &lt;a href="group__group__scb__spi__macros__tx__fifo__status.html#group__group__scb__spi__macros__tx__fifo__status_1ga09966d868419acdcc1b39a20e168437a"&gt;CY_SCB_SPI_TX_TRIGGER&lt;/a&gt;, &lt;a href="group__group__scb__spi__macros__tx__fifo__status.html#group__group__scb__spi__macros__tx__fifo__status_1gac78e12f183e9dd8363763baf5c4694ab"&gt;CY_SCB_SPI_TX_EMPTY&lt;/a&gt; and &lt;a href="group__group__scb__spi__macros__tx__fifo__status.html#group__group__scb__spi__macros__tx__fifo__status_1ga02c11eee0328c4d9907fae3ce30f050c"&gt;CY_SCB_SPI_TX_NOT_FULL&lt;/a&gt; set high again after being cleared if the condition remains true. &lt;/p&gt;&lt;/li&gt;&lt;/ul&gt;&lt;/p&gt;&lt;/p&gt;&lt;/div&gt;</verbatim></para>        </detaileddescription>
        <inbodydescription>
        </inbodydescription>
        <location bodyend="1108" bodyfile="/var/tmp/gitlab-runner1/builds/7e5c9300/0/repo/psoc6pdl/drivers/include/cy_scb_spi.h" bodystart="1103" column="1" file="/var/tmp/gitlab-runner1/builds/7e5c9300/0/repo/psoc6pdl/drivers/include/cy_scb_spi.h" line="602" />
      </memberdef>
      <memberdef const="no" explicit="no" id="group__group__scb__spi__low__level__functions_1ga50acb3879906a729e5821fcc87d4f017" inline="no" kind="function" prot="public" static="no" virt="non-virtual">
        <type>__STATIC_INLINE uint32_t</type>
        <definition>__STATIC_INLINE uint32_t Cy_SCB_SPI_GetRxFifoStatus</definition>
        <argsstring>(CySCB_Type const *base)</argsstring>
        <name>Cy_SCB_SPI_GetRxFifoStatus</name>
        <param>
          <type><ref kindref="compound" refid="struct_cy_s_c_b___type">CySCB_Type</ref> const *</type>
          <declname>base</declname>
        </param>
        <briefdescription>
<para>Returns the current status of the RX FIFO. </para>        </briefdescription>
        <detaileddescription>
<para><parameterlist kind="param"><parameteritem>
<parameternamelist>
<parametername>base</parametername>
</parameternamelist>
<parameterdescription>
<para>The pointer to the SPI SCB instance.</para></parameterdescription>
</parameteritem>
</parameterlist>
<simplesect kind="return"><para><ref kindref="compound" refid="group__group__scb__spi__macros__rx__fifo__status">SPI RX FIFO Statuses</ref> </para></simplesect>
</para>        </detaileddescription>
        <inbodydescription>
        </inbodydescription>
        <location bodyend="989" bodyfile="/var/tmp/gitlab-runner1/builds/7e5c9300/0/repo/psoc6pdl/drivers/include/cy_scb_spi.h" bodystart="986" column="1" file="/var/tmp/gitlab-runner1/builds/7e5c9300/0/repo/psoc6pdl/drivers/include/cy_scb_spi.h" line="604" />
      </memberdef>
      <memberdef const="no" explicit="no" id="group__group__scb__spi__low__level__functions_1gae08b31f2964a1c0895531d05487ad383" inline="no" kind="function" prot="public" static="no" virt="non-virtual">
        <type>__STATIC_INLINE void</type>
        <definition>__STATIC_INLINE void Cy_SCB_SPI_ClearRxFifoStatus</definition>
        <argsstring>(CySCB_Type *base, uint32_t clearMask)</argsstring>
        <name>Cy_SCB_SPI_ClearRxFifoStatus</name>
        <param>
          <type><ref kindref="compound" refid="struct_cy_s_c_b___type">CySCB_Type</ref> *</type>
          <declname>base</declname>
        </param>
        <param>
          <type>uint32_t</type>
          <declname>clearMask</declname>
        </param>
        <briefdescription>
<para>Clears the selected statuses of the RX FIFO. </para>        </briefdescription>
        <detaileddescription>
<para><parameterlist kind="param"><parameteritem>
<parameternamelist>
<parametername>base</parametername>
</parameternamelist>
<parameterdescription>
<para>The pointer to the SPI SCB instance.</para></parameterdescription>
</parameteritem>
<parameteritem>
<parameternamelist>
<parametername>clearMask</parametername>
</parameternamelist>
<parameterdescription>
<para>The mask of which statuses to clear. See <ref kindref="compound" refid="group__group__scb__spi__macros__rx__fifo__status">SPI RX FIFO Statuses</ref> for the set of constants.</para></parameterdescription>
</parameteritem>
</parameterlist>
<verbatim>embed:rst 
.. raw:: html

  &lt;div class="admonition note"&gt;&lt;p class="admonition-title"&gt;note&lt;/p&gt;&lt;p&gt;&lt;p&gt;&lt;ul&gt;&lt;li&gt;&lt;p&gt;This status is also used for interrupt generation, so clearing it also clears the interrupt sources.&lt;/p&gt;&lt;/li&gt;&lt;li&gt;&lt;p&gt;Level sensitive statuses such as &lt;a href="group__group__scb__spi__macros__rx__fifo__status.html#group__group__scb__spi__macros__rx__fifo__status_1gaaa87359ee2a7bd8e448ba5417558e8bf"&gt;CY_SCB_SPI_RX_TRIGGER&lt;/a&gt;, &lt;a href="group__group__scb__spi__macros__rx__fifo__status.html#group__group__scb__spi__macros__rx__fifo__status_1ga635fed2d6204ec644ad4ce4c8122350c"&gt;CY_SCB_SPI_RX_NOT_EMPTY&lt;/a&gt; and &lt;a href="group__group__scb__spi__macros__rx__fifo__status.html#group__group__scb__spi__macros__rx__fifo__status_1ga2b1848eb7a3dfe6c4005bb6e66b820cc"&gt;CY_SCB_SPI_RX_FULL&lt;/a&gt; set high again after being cleared if the condition remains true. &lt;/p&gt;&lt;/li&gt;&lt;/ul&gt;&lt;/p&gt;&lt;/p&gt;&lt;/div&gt;</verbatim></para>        </detaileddescription>
        <inbodydescription>
        </inbodydescription>
        <location bodyend="1018" bodyfile="/var/tmp/gitlab-runner1/builds/7e5c9300/0/repo/psoc6pdl/drivers/include/cy_scb_spi.h" bodystart="1013" column="1" file="/var/tmp/gitlab-runner1/builds/7e5c9300/0/repo/psoc6pdl/drivers/include/cy_scb_spi.h" line="605" />
      </memberdef>
      <memberdef const="no" explicit="no" id="group__group__scb__spi__low__level__functions_1gac9e9a4b77a8db35ccbda974e7f9d9d23" inline="no" kind="function" prot="public" static="no" virt="non-virtual">
        <type>__STATIC_INLINE uint32_t</type>
        <definition>__STATIC_INLINE uint32_t Cy_SCB_SPI_GetSlaveMasterStatus</definition>
        <argsstring>(CySCB_Type const *base)</argsstring>
        <name>Cy_SCB_SPI_GetSlaveMasterStatus</name>
        <param>
          <type><ref kindref="compound" refid="struct_cy_s_c_b___type">CySCB_Type</ref> const *</type>
          <declname>base</declname>
        </param>
        <briefdescription>
<para>Returns the current status of either the slave or the master, depending on the configured SPI mode. </para>        </briefdescription>
        <detaileddescription>
<para><parameterlist kind="param"><parameteritem>
<parameternamelist>
<parametername>base</parametername>
</parameternamelist>
<parameterdescription>
<para>The pointer to the SPI SCB instance.</para></parameterdescription>
</parameteritem>
</parameterlist>
<simplesect kind="return"><para><ref kindref="compound" refid="group__group__scb__spi__macros__master__slave__status">SPI Master and Slave Statuses</ref> </para></simplesect>
</para>        </detaileddescription>
        <inbodydescription>
        </inbodydescription>
        <location bodyend="1203" bodyfile="/var/tmp/gitlab-runner1/builds/7e5c9300/0/repo/psoc6pdl/drivers/include/cy_scb_spi.h" bodystart="1189" column="1" file="/var/tmp/gitlab-runner1/builds/7e5c9300/0/repo/psoc6pdl/drivers/include/cy_scb_spi.h" line="607" />
      </memberdef>
      <memberdef const="no" explicit="no" id="group__group__scb__spi__low__level__functions_1ga085a0e7fb73b9feb3b66ca5017449e6b" inline="no" kind="function" prot="public" static="no" virt="non-virtual">
        <type>__STATIC_INLINE void</type>
        <definition>__STATIC_INLINE void Cy_SCB_SPI_ClearSlaveMasterStatus</definition>
        <argsstring>(CySCB_Type *base, uint32_t clearMask)</argsstring>
        <name>Cy_SCB_SPI_ClearSlaveMasterStatus</name>
        <param>
          <type><ref kindref="compound" refid="struct_cy_s_c_b___type">CySCB_Type</ref> *</type>
          <declname>base</declname>
        </param>
        <param>
          <type>uint32_t</type>
          <declname>clearMask</declname>
        </param>
        <briefdescription>
<para>Clears the selected statuses of either the slave or the master. </para>        </briefdescription>
        <detaileddescription>
<para><parameterlist kind="param"><parameteritem>
<parameternamelist>
<parametername>base</parametername>
</parameternamelist>
<parameterdescription>
<para>The pointer to the SPI SCB instance.</para></parameterdescription>
</parameteritem>
<parameteritem>
<parameternamelist>
<parametername>clearMask</parametername>
</parameternamelist>
<parameterdescription>
<para>The mask of which statuses to clear. See <ref kindref="compound" refid="group__group__scb__spi__macros__master__slave__status">SPI Master and Slave Statuses</ref> for the set of constants. </para></parameterdescription>
</parameteritem>
</parameterlist>
</para>        </detaileddescription>
        <inbodydescription>
        </inbodydescription>
        <location bodyend="1234" bodyfile="/var/tmp/gitlab-runner1/builds/7e5c9300/0/repo/psoc6pdl/drivers/include/cy_scb_spi.h" bodystart="1220" column="1" file="/var/tmp/gitlab-runner1/builds/7e5c9300/0/repo/psoc6pdl/drivers/include/cy_scb_spi.h" line="608" />
      </memberdef>
      <memberdef const="no" explicit="no" id="group__group__scb__spi__low__level__functions_1gad9be16f0ae646a85e9fffe9731cb4896" inline="no" kind="function" prot="public" static="no" virt="non-virtual">
        <type>__STATIC_INLINE uint32_t</type>
        <definition>__STATIC_INLINE uint32_t Cy_SCB_SPI_GetNumInTxFifo</definition>
        <argsstring>(CySCB_Type const *base)</argsstring>
        <name>Cy_SCB_SPI_GetNumInTxFifo</name>
        <param>
          <type><ref kindref="compound" refid="struct_cy_s_c_b___type">CySCB_Type</ref> const *</type>
          <declname>base</declname>
        </param>
        <briefdescription>
<para>Returns the number of data elements in the SPI TX FIFO. </para>        </briefdescription>
        <detaileddescription>
<para><parameterlist kind="param"><parameteritem>
<parameternamelist>
<parametername>base</parametername>
</parameternamelist>
<parameterdescription>
<para>The pointer to the SPI SCB instance.</para></parameterdescription>
</parameteritem>
</parameterlist>
<simplesect kind="return"><para>The number of data elements in the TX FIFO. The size of a data element defined by the configured TX data width.</para></simplesect>
<verbatim>embed:rst 
.. raw:: html

  &lt;div class="admonition note"&gt;&lt;p class="admonition-title"&gt;note&lt;/p&gt;&lt;p&gt;&lt;p&gt;This number does not include any data currently in the TX shifter. &lt;/p&gt;&lt;/p&gt;&lt;/div&gt;</verbatim></para>        </detaileddescription>
        <inbodydescription>
        </inbodydescription>
        <location bodyend="1131" bodyfile="/var/tmp/gitlab-runner1/builds/7e5c9300/0/repo/psoc6pdl/drivers/include/cy_scb_spi.h" bodystart="1128" column="1" file="/var/tmp/gitlab-runner1/builds/7e5c9300/0/repo/psoc6pdl/drivers/include/cy_scb_spi.h" line="610" />
      </memberdef>
      <memberdef const="no" explicit="no" id="group__group__scb__spi__low__level__functions_1ga399842a2931b7c6cd85446a72cc2f81b" inline="no" kind="function" prot="public" static="no" virt="non-virtual">
        <type>__STATIC_INLINE bool</type>
        <definition>__STATIC_INLINE bool Cy_SCB_SPI_IsTxComplete</definition>
        <argsstring>(CySCB_Type const *base)</argsstring>
        <name>Cy_SCB_SPI_IsTxComplete</name>
        <param>
          <type><ref kindref="compound" refid="struct_cy_s_c_b___type">CySCB_Type</ref> const *</type>
          <declname>base</declname>
        </param>
        <briefdescription>
<para>Checks whether the TX FIFO and Shifter are empty and there is no more data to send. </para>        </briefdescription>
        <detaileddescription>
<para><parameterlist kind="param"><parameteritem>
<parameternamelist>
<parametername>base</parametername>
</parameternamelist>
<parameterdescription>
<para>Pointer to the SPI SCB instance.</para></parameterdescription>
</parameteritem>
</parameterlist>
<simplesect kind="return"><para>If true, transmission complete. If false, transmission is not complete. </para></simplesect>
</para>        </detaileddescription>
        <inbodydescription>
        </inbodydescription>
        <location bodyend="1150" bodyfile="/var/tmp/gitlab-runner1/builds/7e5c9300/0/repo/psoc6pdl/drivers/include/cy_scb_spi.h" bodystart="1147" column="1" file="/var/tmp/gitlab-runner1/builds/7e5c9300/0/repo/psoc6pdl/drivers/include/cy_scb_spi.h" line="611" />
      </memberdef>
      <memberdef const="no" explicit="no" id="group__group__scb__spi__low__level__functions_1gaea6c37058e015b59c0e307d0d0e77ad0" inline="no" kind="function" prot="public" static="no" virt="non-virtual">
        <type>__STATIC_INLINE uint32_t</type>
        <definition>__STATIC_INLINE uint32_t Cy_SCB_SPI_GetNumInRxFifo</definition>
        <argsstring>(CySCB_Type const *base)</argsstring>
        <name>Cy_SCB_SPI_GetNumInRxFifo</name>
        <param>
          <type><ref kindref="compound" refid="struct_cy_s_c_b___type">CySCB_Type</ref> const *</type>
          <declname>base</declname>
        </param>
        <briefdescription>
<para>Returns the number of data elements in the SPI RX FIFO. </para>        </briefdescription>
        <detaileddescription>
<para><parameterlist kind="param"><parameteritem>
<parameternamelist>
<parametername>base</parametername>
</parameternamelist>
<parameterdescription>
<para>The pointer to the SPI SCB instance.</para></parameterdescription>
</parameteritem>
</parameterlist>
<simplesect kind="return"><para>The number of data elements in the RX FIFO. The size of a data element defined by the configured RX data width.</para></simplesect>
<verbatim>embed:rst 
.. raw:: html

  &lt;div class="admonition note"&gt;&lt;p class="admonition-title"&gt;note&lt;/p&gt;&lt;p&gt;&lt;p&gt;This number does not include any data currently in the RX shifter. &lt;/p&gt;&lt;/p&gt;&lt;/div&gt;</verbatim></para>        </detaileddescription>
        <inbodydescription>
        </inbodydescription>
        <location bodyend="1041" bodyfile="/var/tmp/gitlab-runner1/builds/7e5c9300/0/repo/psoc6pdl/drivers/include/cy_scb_spi.h" bodystart="1038" column="1" file="/var/tmp/gitlab-runner1/builds/7e5c9300/0/repo/psoc6pdl/drivers/include/cy_scb_spi.h" line="613" />
      </memberdef>
      <memberdef const="no" explicit="no" id="group__group__scb__spi__low__level__functions_1gaf8a6bc98792bb02d878acfae0ff07ff0" inline="no" kind="function" prot="public" static="no" virt="non-virtual">
        <type>__STATIC_INLINE void</type>
        <definition>__STATIC_INLINE void Cy_SCB_SPI_ClearRxFifo</definition>
        <argsstring>(CySCB_Type *base)</argsstring>
        <name>Cy_SCB_SPI_ClearRxFifo</name>
        <param>
          <type><ref kindref="compound" refid="struct_cy_s_c_b___type">CySCB_Type</ref> *</type>
          <declname>base</declname>
        </param>
        <briefdescription>
<para>Clears all data out of the SPI RX FIFO. </para>        </briefdescription>
        <detaileddescription>
<para><parameterlist kind="param"><parameteritem>
<parameternamelist>
<parametername>base</parametername>
</parameternamelist>
<parameterdescription>
<para>The pointer to the SPI SCB instance.</para></parameterdescription>
</parameteritem>
</parameterlist>
<simplesect kind="par"><title>Side Effects</title><para>Any data currently in the shifter is cleared and lost. </para></simplesect>
</para>        </detaileddescription>
        <inbodydescription>
        </inbodydescription>
        <location bodyend="1060" bodyfile="/var/tmp/gitlab-runner1/builds/7e5c9300/0/repo/psoc6pdl/drivers/include/cy_scb_spi.h" bodystart="1057" column="1" file="/var/tmp/gitlab-runner1/builds/7e5c9300/0/repo/psoc6pdl/drivers/include/cy_scb_spi.h" line="615" />
      </memberdef>
      <memberdef const="no" explicit="no" id="group__group__scb__spi__low__level__functions_1ga6eea4d58a1455170f4ec41dc6333b0a7" inline="no" kind="function" prot="public" static="no" virt="non-virtual">
        <type>__STATIC_INLINE void</type>
        <definition>__STATIC_INLINE void Cy_SCB_SPI_ClearTxFifo</definition>
        <argsstring>(CySCB_Type *base)</argsstring>
        <name>Cy_SCB_SPI_ClearTxFifo</name>
        <param>
          <type><ref kindref="compound" refid="struct_cy_s_c_b___type">CySCB_Type</ref> *</type>
          <declname>base</declname>
        </param>
        <briefdescription>
<para>Clears all data out of the SPI TX FIFO. </para>        </briefdescription>
        <detaileddescription>
<para><parameterlist kind="param"><parameteritem>
<parameternamelist>
<parametername>base</parametername>
</parameternamelist>
<parameterdescription>
<para>The pointer to the SPI SCB instance.</para></parameterdescription>
</parameteritem>
</parameterlist>
<simplesect kind="par"><title>Side Effects</title><para>The TX FIFO clear operation also clears the shift register; the shifter can be cleared in the middle of a data element transfer, corrupting it. The data element corruption means that all bits that have not been transmitted are transmitted as 1s on the bus. </para></simplesect>
</para>        </detaileddescription>
        <inbodydescription>
        </inbodydescription>
        <location bodyend="1172" bodyfile="/var/tmp/gitlab-runner1/builds/7e5c9300/0/repo/psoc6pdl/drivers/include/cy_scb_spi.h" bodystart="1169" column="1" file="/var/tmp/gitlab-runner1/builds/7e5c9300/0/repo/psoc6pdl/drivers/include/cy_scb_spi.h" line="616" />
      </memberdef>
      </sectiondef>
    <briefdescription>
    </briefdescription>
    <detaileddescription>
    </detaileddescription>
  </compounddef>
</doxygen>