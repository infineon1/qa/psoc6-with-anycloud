<doxygen xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" version="1.8.13" xsi:noNamespaceSchemaLocation="compound.xsd">
  <compounddef id="group__group__syspm__functions__iofreeze" kind="group">
    <compoundname>group_syspm_functions_iofreeze</compoundname>
    <title>I/Os Freeze</title>
      <sectiondef kind="func">
      <memberdef const="no" explicit="no" id="group__group__syspm__functions__iofreeze_1ga5df20917d995755606672fac961e8e9b" inline="no" kind="function" prot="public" static="no" virt="non-virtual">
        <type>void</type>
        <definition>void Cy_SysPm_IoUnfreeze</definition>
        <argsstring>(void)</argsstring>
        <name>Cy_SysPm_IoUnfreeze</name>
        <param>
          <type>void</type>
        </param>
        <briefdescription>
<para>This function unfreezes the I/O cells that are automatically frozen when Hibernate is entered with the call to <ref kindref="member" refid="group__group__syspm__functions__power_1gae97647a28c370674ba57d451d21d1c51">Cy_SysPm_SystemEnterHibernate()</ref>. </para>        </briefdescription>
        <detaileddescription>
<para>I/O cells remain frozen after a wakeup from Hibernate mode until the firmware unfreezes them by calling this function.</para><para>If the firmware must retain the data value on the pin, then the value must be read and re-written to the pin's port data register before calling this function. Furthermore, the drive mode must be re-programmed before the pins are unfrozen. If this is not done, the pin will change to the default state the moment the freeze is removed.</para><para>Note that I/O cell configuration can be changed while frozen. The new configuration becomes effective only after the pins are unfrozen.</para><para><simplesect kind="par"><title>Function Usage</title><para><programlisting><codeline><highlight class="normal"><sp /><sp /><sp /><sp /></highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /></highlight><highlight class="comment">/*<sp />Scenario:<sp />There<sp />is<sp />a<sp />need<sp />to<sp />use<sp />Hibernate<sp />power<sp />mode<sp />and<sp />retain<sp />the<sp /></highlight></codeline>
<codeline><highlight class="comment"><sp /><sp /><sp /><sp />*<sp /><sp />the<sp />I/O<sp />state<sp />after<sp />the<sp />a<sp />wake-up<sp />from<sp />Hibernate</highlight></codeline>
<codeline><highlight class="comment"><sp /><sp /><sp /><sp />*/</highlight><highlight class="normal" /></codeline>
<codeline><highlight class="normal" /></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /></highlight><highlight class="comment">/*<sp />Check<sp />the<sp />I/O<sp />freeze<sp />status<sp />after<sp />the<sp />boot.</highlight></codeline>
<codeline><highlight class="comment"><sp /><sp /><sp /><sp />*<sp /><sp />If<sp />the<sp />I/O<sp />is<sp />frozen<sp />it<sp />means<sp />we<sp />have<sp />returned<sp />from<sp />Hibernate.</highlight></codeline>
<codeline><highlight class="comment"><sp /><sp /><sp /><sp />*/</highlight><highlight class="normal" /></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /></highlight><highlight class="keywordflow">if</highlight><highlight class="normal">(<ref kindref="member" refid="group__group__syspm__functions__iofreeze_1ga42fc45913557d7aecd0bfb868c74e645">Cy_SysPm_IoIsFrozen</ref>())</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp />{</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /></highlight><highlight class="comment">/*<sp />Restore<sp />the<sp />I/O<sp />configuration<sp />before<sp />unfreezing.</highlight></codeline>
<codeline><highlight class="comment"><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp />*<sp /><sp />Use<sp />configuration<sp />data<sp />programmed<sp />into<sp />firmware<sp />stored<sp />in<sp />S</highlight></codeline>
<codeline><highlight class="comment"><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp />*<sp /><sp />RSS-&gt;PWR_HIB_DATA[]<sp />or<sp />BACKUP-&gt;BREG[]<sp /></highlight></codeline>
<codeline><highlight class="comment"><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp />*<sp /><sp />before<sp />the<sp />Hibernate<sp />was<sp />entered.<sp />Th<sp />emost<sp />common<sp />use<sp />case<sp />is<sp />to</highlight></codeline>
<codeline><highlight class="comment"><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp />*<sp /><sp />restore<sp />from<sp />GPIO<sp />driver<sp />the<sp />firmware<sp />initialization<sp />structures.</highlight></codeline>
<codeline><highlight class="comment"><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp />*/</highlight><highlight class="normal" /></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><ref kindref="member" refid="group__group__syspm__functions__iofreeze_1ga5df20917d995755606672fac961e8e9b">Cy_SysPm_IoUnfreeze</ref>();</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp />}</highlight></codeline>
<codeline><highlight class="normal" /></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /></highlight><highlight class="comment">/*<sp />Save<sp />the<sp />I/O<sp />before<sp />entering<sp />Hibernate.<sp />You<sp />can<sp />store<sp />the</highlight></codeline>
<codeline><highlight class="comment"><sp /><sp /><sp /><sp />*<sp /><sp />the<sp />I/O<sp />configuration<sp />into<sp />SRSS-&gt;PWR_HIB_DATA[]<sp />or<sp /></highlight></codeline>
<codeline><highlight class="comment"><sp /><sp /><sp /><sp />*<sp /><sp />BACKUP-&gt;BREG[]<sp />registers.</highlight></codeline>
<codeline><highlight class="comment"><sp /><sp /><sp /><sp />*/</highlight><highlight class="normal" /></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /></highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /></highlight><highlight class="comment">/*<sp />Enter<sp />Hibernate<sp />mode<sp />*/</highlight><highlight class="normal" /></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /><ref kindref="member" refid="group__group__syspm__functions__power_1gae97647a28c370674ba57d451d21d1c51">Cy_SysPm_SystemEnterHibernate</ref>();</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /></highlight></codeline>
</programlisting></para></simplesect>
</para>        </detaileddescription>
        <inbodydescription>
        </inbodydescription>
        <location bodyend="2895" bodyfile="/var/tmp/gitlab-runner1/builds/7e5c9300/0/repo/psoc6pdl/drivers/source/cy_syspm.c" bodystart="2872" column="1" file="/var/tmp/gitlab-runner1/builds/7e5c9300/0/repo/psoc6pdl/drivers/include/cy_syspm.h" line="1874" />
      </memberdef>
      <memberdef const="no" explicit="no" id="group__group__syspm__functions__iofreeze_1ga42fc45913557d7aecd0bfb868c74e645" inline="no" kind="function" prot="public" static="no" virt="non-virtual">
        <type>__STATIC_INLINE bool</type>
        <definition>__STATIC_INLINE bool Cy_SysPm_IoIsFrozen</definition>
        <argsstring>(void)</argsstring>
        <name>Cy_SysPm_IoIsFrozen</name>
        <param>
          <type>void</type>
        </param>
        <briefdescription>
<para>Checks whether IOs are frozen. </para>        </briefdescription>
        <detaileddescription>
<para><simplesect kind="return"><para><itemizedlist>
<listitem><para>True if IOs are frozen.</para></listitem><listitem><para>False if IOs are unfrozen.</para></listitem></itemizedlist>
</para></simplesect>
<simplesect kind="par"><title>Function Usage</title><para><programlisting><codeline><highlight class="normal"><sp /><sp /><sp /><sp /></highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /></highlight><highlight class="comment">/*<sp />Scenario:<sp />There<sp />is<sp />a<sp />need<sp />to<sp />use<sp />Hibernate<sp />power<sp />mode<sp />and<sp />retain<sp />the<sp /></highlight></codeline>
<codeline><highlight class="comment"><sp /><sp /><sp /><sp />*<sp /><sp />the<sp />I/O<sp />state<sp />after<sp />the<sp />a<sp />wake-up<sp />from<sp />Hibernate</highlight></codeline>
<codeline><highlight class="comment"><sp /><sp /><sp /><sp />*/</highlight><highlight class="normal" /></codeline>
<codeline><highlight class="normal" /></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /></highlight><highlight class="comment">/*<sp />Check<sp />the<sp />I/O<sp />freeze<sp />status<sp />after<sp />the<sp />boot.</highlight></codeline>
<codeline><highlight class="comment"><sp /><sp /><sp /><sp />*<sp /><sp />If<sp />the<sp />I/O<sp />is<sp />frozen<sp />it<sp />means<sp />we<sp />have<sp />returned<sp />from<sp />Hibernate.</highlight></codeline>
<codeline><highlight class="comment"><sp /><sp /><sp /><sp />*/</highlight><highlight class="normal" /></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /></highlight><highlight class="keywordflow">if</highlight><highlight class="normal">(<ref kindref="member" refid="group__group__syspm__functions__iofreeze_1ga42fc45913557d7aecd0bfb868c74e645">Cy_SysPm_IoIsFrozen</ref>())</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp />{</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /></highlight><highlight class="comment">/*<sp />Restore<sp />the<sp />I/O<sp />configuration<sp />before<sp />unfreezing.</highlight></codeline>
<codeline><highlight class="comment"><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp />*<sp /><sp />Use<sp />configuration<sp />data<sp />programmed<sp />into<sp />firmware<sp />stored<sp />in<sp />S</highlight></codeline>
<codeline><highlight class="comment"><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp />*<sp /><sp />RSS-&gt;PWR_HIB_DATA[]<sp />or<sp />BACKUP-&gt;BREG[]<sp /></highlight></codeline>
<codeline><highlight class="comment"><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp />*<sp /><sp />before<sp />the<sp />Hibernate<sp />was<sp />entered.<sp />Th<sp />emost<sp />common<sp />use<sp />case<sp />is<sp />to</highlight></codeline>
<codeline><highlight class="comment"><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp />*<sp /><sp />restore<sp />from<sp />GPIO<sp />driver<sp />the<sp />firmware<sp />initialization<sp />structures.</highlight></codeline>
<codeline><highlight class="comment"><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp />*/</highlight><highlight class="normal" /></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><ref kindref="member" refid="group__group__syspm__functions__iofreeze_1ga5df20917d995755606672fac961e8e9b">Cy_SysPm_IoUnfreeze</ref>();</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp />}</highlight></codeline>
<codeline><highlight class="normal" /></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /></highlight><highlight class="comment">/*<sp />Save<sp />the<sp />I/O<sp />before<sp />entering<sp />Hibernate.<sp />You<sp />can<sp />store<sp />the</highlight></codeline>
<codeline><highlight class="comment"><sp /><sp /><sp /><sp />*<sp /><sp />the<sp />I/O<sp />configuration<sp />into<sp />SRSS-&gt;PWR_HIB_DATA[]<sp />or<sp /></highlight></codeline>
<codeline><highlight class="comment"><sp /><sp /><sp /><sp />*<sp /><sp />BACKUP-&gt;BREG[]<sp />registers.</highlight></codeline>
<codeline><highlight class="comment"><sp /><sp /><sp /><sp />*/</highlight><highlight class="normal" /></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /></highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /></highlight><highlight class="comment">/*<sp />Enter<sp />Hibernate<sp />mode<sp />*/</highlight><highlight class="normal" /></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /><ref kindref="member" refid="group__group__syspm__functions__power_1gae97647a28c370674ba57d451d21d1c51">Cy_SysPm_SystemEnterHibernate</ref>();</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /></highlight></codeline>
</programlisting></para></simplesect>
</para>        </detaileddescription>
        <inbodydescription>
        </inbodydescription>
        <location bodyend="2436" bodyfile="/var/tmp/gitlab-runner1/builds/7e5c9300/0/repo/psoc6pdl/drivers/include/cy_syspm.h" bodystart="2433" column="1" file="/var/tmp/gitlab-runner1/builds/7e5c9300/0/repo/psoc6pdl/drivers/include/cy_syspm.h" line="1875" />
      </memberdef>
      </sectiondef>
    <briefdescription>
    </briefdescription>
    <detaileddescription>
    </detaileddescription>
  </compounddef>
</doxygen>