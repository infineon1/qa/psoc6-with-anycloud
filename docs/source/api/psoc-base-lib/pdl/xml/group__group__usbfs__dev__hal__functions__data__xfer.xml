<doxygen xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" version="1.8.13" xsi:noNamespaceSchemaLocation="compound.xsd">
  <compounddef id="group__group__usbfs__dev__hal__functions__data__xfer" kind="group">
    <compoundname>group_usbfs_dev_hal_functions_data_xfer</compoundname>
    <title>Data Endpoint Transfer Functions</title>
      <sectiondef kind="func">
      <memberdef const="no" explicit="no" id="group__group__usbfs__dev__hal__functions__data__xfer_1gaf78e5b3f27c156336655d5e92e90dbc9" inline="no" kind="function" prot="public" static="no" virt="non-virtual">
        <type>__STATIC_INLINE <ref kindref="member" refid="group__group__usbfs__dev__drv__enums_1ga9a078f172cd82350179929c0133d9bbf">cy_en_usb_dev_ep_state_t</ref></type>
        <definition>__STATIC_INLINE cy_en_usb_dev_ep_state_t Cy_USBFS_Dev_Drv_GetEndpointState</definition>
        <argsstring>(USBFS_Type const *base, uint32_t endpoint, cy_stc_usbfs_dev_drv_context_t const *context)</argsstring>
        <name>Cy_USBFS_Dev_Drv_GetEndpointState</name>
        <param>
          <type>USBFS_Type const *</type>
          <declname>base</declname>
        </param>
        <param>
          <type>uint32_t</type>
          <declname>endpoint</declname>
        </param>
        <param>
          <type><ref kindref="compound" refid="structcy__stc__usbfs__dev__drv__context__t">cy_stc_usbfs_dev_drv_context_t</ref> const *</type>
          <declname>context</declname>
        </param>
        <briefdescription>
<para>Returns the state of the endpoint. </para>        </briefdescription>
        <detaileddescription>
<para><parameterlist kind="param"><parameteritem>
<parameternamelist>
<parametername>base</parametername>
</parameternamelist>
<parameterdescription>
<para>The pointer to the USBFS instance.</para></parameterdescription>
</parameteritem>
<parameteritem>
<parameternamelist>
<parametername>endpoint</parametername>
</parameternamelist>
<parameterdescription>
<para>The data endpoint number.</para></parameterdescription>
</parameteritem>
<parameteritem>
<parameternamelist>
<parametername>context</parametername>
</parameternamelist>
<parameterdescription>
<para>The pointer to the context structure <ref kindref="compound" refid="structcy__stc__usbfs__dev__drv__context__t">cy_stc_usbfs_dev_drv_context_t</ref> allocated by the user. The structure is used during the USBFS Device operation for internal configuration and data retention. The user must not modify anything in this structure.</para></parameterdescription>
</parameteritem>
</parameterlist>
<simplesect kind="return"><para>Data endpoint state <ref kindref="member" refid="group__group__usbfs__dev__drv__enums_1ga9a078f172cd82350179929c0133d9bbf">cy_en_usb_dev_ep_state_t</ref>. </para></simplesect>
</para>        </detaileddescription>
        <inbodydescription>
        </inbodydescription>
        <location bodyend="2212" bodyfile="/var/tmp/gitlab-runner1/builds/7e5c9300/0/repo/psoc6pdl/drivers/include/cy_usbfs_dev_drv.h" bodystart="2196" column="1" file="/var/tmp/gitlab-runner1/builds/7e5c9300/0/repo/psoc6pdl/drivers/include/cy_usbfs_dev_drv.h" line="1270" />
      </memberdef>
      <memberdef const="no" explicit="no" id="group__group__usbfs__dev__hal__functions__data__xfer_1ga65db7019eafec3e38e8fbca316f92673" inline="no" kind="function" prot="public" static="no" virt="non-virtual">
        <type>__STATIC_INLINE <ref kindref="member" refid="group__group__usbfs__dev__drv__enums_1gace2573b482baf5a2a0f796b72cc244ac">cy_en_usbfs_dev_drv_status_t</ref></type>
        <definition>__STATIC_INLINE cy_en_usbfs_dev_drv_status_t Cy_USBFS_Dev_Drv_LoadInEndpoint</definition>
        <argsstring>(USBFS_Type *base, uint32_t endpoint, uint8_t const *buffer, uint32_t size, cy_stc_usbfs_dev_drv_context_t *context)</argsstring>
        <name>Cy_USBFS_Dev_Drv_LoadInEndpoint</name>
        <param>
          <type>USBFS_Type *</type>
          <declname>base</declname>
        </param>
        <param>
          <type>uint32_t</type>
          <declname>endpoint</declname>
        </param>
        <param>
          <type>uint8_t const *</type>
          <declname>buffer</declname>
        </param>
        <param>
          <type>uint32_t</type>
          <declname>size</declname>
        </param>
        <param>
          <type><ref kindref="compound" refid="structcy__stc__usbfs__dev__drv__context__t">cy_stc_usbfs_dev_drv_context_t</ref> *</type>
          <declname>context</declname>
        </param>
        <briefdescription>
<para>Loads data into the IN endpoint buffer. </para>        </briefdescription>
        <detaileddescription>
<para>After data loads, the endpoint is ready to be read by the host.</para><para><parameterlist kind="param"><parameteritem>
<parameternamelist>
<parametername>base</parametername>
</parameternamelist>
<parameterdescription>
<para>The pointer to the USBFS instance.</para></parameterdescription>
</parameteritem>
<parameteritem>
<parameternamelist>
<parametername>endpoint</parametername>
</parameternamelist>
<parameterdescription>
<para>The IN data endpoint number.</para></parameterdescription>
</parameteritem>
<parameteritem>
<parameternamelist>
<parametername>buffer</parametername>
</parameternamelist>
<parameterdescription>
<para>The pointer to the buffer containing data bytes to load.</para></parameterdescription>
</parameteritem>
<parameteritem>
<parameternamelist>
<parametername>size</parametername>
</parameternamelist>
<parameterdescription>
<para>The number of bytes to load into the endpoint. This value must be less than or equal to endpoint maximum packet size.</para></parameterdescription>
</parameteritem>
<parameteritem>
<parameternamelist>
<parametername>context</parametername>
</parameternamelist>
<parameterdescription>
<para>The pointer to the context structure <ref kindref="compound" refid="structcy__stc__usbfs__dev__drv__context__t">cy_stc_usbfs_dev_drv_context_t</ref> allocated by the user. The structure is used during the USBFS Device operation for internal configuration and data retention. The user must not modify anything in this structure.</para></parameterdescription>
</parameteritem>
</parameterlist>
<simplesect kind="return"><para>The status code of the function execution <ref kindref="member" refid="group__group__usbfs__dev__drv__enums_1gace2573b482baf5a2a0f796b72cc244ac">cy_en_usbfs_dev_drv_status_t</ref>. </para></simplesect>
</para>        </detaileddescription>
        <inbodydescription>
        </inbodydescription>
        <location bodyend="2256" bodyfile="/var/tmp/gitlab-runner1/builds/7e5c9300/0/repo/psoc6pdl/drivers/include/cy_usbfs_dev_drv.h" bodystart="2245" column="1" file="/var/tmp/gitlab-runner1/builds/7e5c9300/0/repo/psoc6pdl/drivers/include/cy_usbfs_dev_drv.h" line="1274" />
      </memberdef>
      <memberdef const="no" explicit="no" id="group__group__usbfs__dev__hal__functions__data__xfer_1ga216a8de51d7d27d9fd74a716ebb5e394" inline="no" kind="function" prot="public" static="no" virt="non-virtual">
        <type>void</type>
        <definition>void Cy_USBFS_Dev_Drv_EnableOutEndpoint</definition>
        <argsstring>(USBFS_Type *base, uint32_t endpoint, cy_stc_usbfs_dev_drv_context_t *context)</argsstring>
        <name>Cy_USBFS_Dev_Drv_EnableOutEndpoint</name>
        <param>
          <type>USBFS_Type *</type>
          <declname>base</declname>
        </param>
        <param>
          <type>uint32_t</type>
          <declname>endpoint</declname>
        </param>
        <param>
          <type><ref kindref="compound" refid="structcy__stc__usbfs__dev__drv__context__t">cy_stc_usbfs_dev_drv_context_t</ref> *</type>
          <declname>context</declname>
        </param>
        <briefdescription>
<para>Enables the OUT data endpoint to be read by the Host. </para>        </briefdescription>
        <detaileddescription>
<para><parameterlist kind="param"><parameteritem>
<parameternamelist>
<parametername>base</parametername>
</parameternamelist>
<parameterdescription>
<para>The pointer to the USBFS instance.</para></parameterdescription>
</parameteritem>
<parameteritem>
<parameternamelist>
<parametername>endpoint</parametername>
</parameternamelist>
<parameterdescription>
<para>The OUT data endpoint number.</para></parameterdescription>
</parameteritem>
<parameteritem>
<parameternamelist>
<parametername>context</parametername>
</parameternamelist>
<parameterdescription>
<para>The pointer to the context structure <ref kindref="compound" refid="structcy__stc__usbfs__dev__drv__context__t">cy_stc_usbfs_dev_drv_context_t</ref> allocated by the user. The structure is used during the USBFS Device operation for internal configuration and data retention. The user must not modify anything in this structure.</para></parameterdescription>
</parameteritem>
</parameterlist>
<simplesect kind="return"><para>Status code of the function execution <ref kindref="member" refid="group__group__usbfs__dev__drv__enums_1gace2573b482baf5a2a0f796b72cc244ac">cy_en_usbfs_dev_drv_status_t</ref>.</para></simplesect>
<verbatim>embed:rst 
.. raw:: html

  &lt;div class="admonition note"&gt;&lt;p class="admonition-title"&gt;note&lt;/p&gt;&lt;p&gt;&lt;p&gt;The OUT endpoints are not enabled by default. The endpoints must be enabled before calling &lt;a href="group__group__usbfs__dev__hal__functions__data__xfer.html#group__group__usbfs__dev__hal__functions__data__xfer_1ga3699408b8336670b36c2e7a3fa6949d7"&gt;Cy_USBFS_Dev_Drv_ReadOutEndpoint&lt;/a&gt; to read data from an endpoint. &lt;/p&gt;&lt;/p&gt;&lt;/div&gt;</verbatim></para>        </detaileddescription>
        <inbodydescription>
        </inbodydescription>
        <location bodyend="522" bodyfile="/var/tmp/gitlab-runner1/builds/7e5c9300/0/repo/psoc6pdl/drivers/source/cy_usbfs_dev_drv_io.c" bodystart="502" column="1" file="/var/tmp/gitlab-runner1/builds/7e5c9300/0/repo/psoc6pdl/drivers/include/cy_usbfs_dev_drv.h" line="1280" />
      </memberdef>
      <memberdef const="no" explicit="no" id="group__group__usbfs__dev__hal__functions__data__xfer_1ga3699408b8336670b36c2e7a3fa6949d7" inline="no" kind="function" prot="public" static="no" virt="non-virtual">
        <type>__STATIC_INLINE <ref kindref="member" refid="group__group__usbfs__dev__drv__enums_1gace2573b482baf5a2a0f796b72cc244ac">cy_en_usbfs_dev_drv_status_t</ref></type>
        <definition>__STATIC_INLINE cy_en_usbfs_dev_drv_status_t Cy_USBFS_Dev_Drv_ReadOutEndpoint</definition>
        <argsstring>(USBFS_Type *base, uint32_t endpoint, uint8_t *buffer, uint32_t size, uint32_t *actSize, cy_stc_usbfs_dev_drv_context_t *context)</argsstring>
        <name>Cy_USBFS_Dev_Drv_ReadOutEndpoint</name>
        <param>
          <type>USBFS_Type *</type>
          <declname>base</declname>
        </param>
        <param>
          <type>uint32_t</type>
          <declname>endpoint</declname>
        </param>
        <param>
          <type>uint8_t *</type>
          <declname>buffer</declname>
        </param>
        <param>
          <type>uint32_t</type>
          <declname>size</declname>
        </param>
        <param>
          <type>uint32_t *</type>
          <declname>actSize</declname>
        </param>
        <param>
          <type><ref kindref="compound" refid="structcy__stc__usbfs__dev__drv__context__t">cy_stc_usbfs_dev_drv_context_t</ref> *</type>
          <declname>context</declname>
        </param>
        <briefdescription>
<para>Reads data from the OUT endpoint buffer. </para>        </briefdescription>
        <detaileddescription>
<para>Before executing a next read, the <ref kindref="member" refid="group__group__usbfs__dev__hal__functions__data__xfer_1ga216a8de51d7d27d9fd74a716ebb5e394">Cy_USBFS_Dev_Drv_EnableOutEndpoint</ref> must be called to allow the Host to write data into the endpoint.</para><para><parameterlist kind="param"><parameteritem>
<parameternamelist>
<parametername>base</parametername>
</parameternamelist>
<parameterdescription>
<para>The pointer to the USBFS instance.</para></parameterdescription>
</parameteritem>
<parameteritem>
<parameternamelist>
<parametername>endpoint</parametername>
</parameternamelist>
<parameterdescription>
<para>The OUT data endpoint number.</para></parameterdescription>
</parameteritem>
<parameteritem>
<parameternamelist>
<parametername>buffer</parametername>
</parameternamelist>
<parameterdescription>
<para>The pointer to the buffer that stores read data.</para></parameterdescription>
</parameteritem>
<parameteritem>
<parameternamelist>
<parametername>size</parametername>
</parameternamelist>
<parameterdescription>
<para>The number of bytes to read from the endpoint. This value must be less than or equal to the endpoint maximum packet size.</para></parameterdescription>
</parameteritem>
<parameteritem>
<parameternamelist>
<parametername>actSize</parametername>
</parameternamelist>
<parameterdescription>
<para>The number of actually read bytes.</para></parameterdescription>
</parameteritem>
<parameteritem>
<parameternamelist>
<parametername>context</parametername>
</parameternamelist>
<parameterdescription>
<para>The pointer to the context structure <ref kindref="compound" refid="structcy__stc__usbfs__dev__drv__context__t">cy_stc_usbfs_dev_drv_context_t</ref> allocated by the user. The structure is used during the USBFS Device operation for internal configuration and data retention. The user must not modify anything in this structure.</para></parameterdescription>
</parameteritem>
</parameterlist>
<simplesect kind="return"><para>The status code of the function execution <ref kindref="member" refid="group__group__usbfs__dev__drv__enums_1gace2573b482baf5a2a0f796b72cc244ac">cy_en_usbfs_dev_drv_status_t</ref>. </para></simplesect>
</para>        </detaileddescription>
        <inbodydescription>
        </inbodydescription>
        <location bodyend="2305" bodyfile="/var/tmp/gitlab-runner1/builds/7e5c9300/0/repo/psoc6pdl/drivers/include/cy_usbfs_dev_drv.h" bodystart="2293" column="1" file="/var/tmp/gitlab-runner1/builds/7e5c9300/0/repo/psoc6pdl/drivers/include/cy_usbfs_dev_drv.h" line="1284" />
      </memberdef>
      <memberdef const="no" explicit="no" id="group__group__usbfs__dev__hal__functions__data__xfer_1gae4dba7b4e7a47b69f1732d5a216382eb" inline="no" kind="function" prot="public" static="no" virt="non-virtual">
        <type><ref kindref="member" refid="group__group__usbfs__dev__drv__enums_1gace2573b482baf5a2a0f796b72cc244ac">cy_en_usbfs_dev_drv_status_t</ref></type>
        <definition>cy_en_usbfs_dev_drv_status_t Cy_USBFS_Dev_Drv_Abort</definition>
        <argsstring>(USBFS_Type *base, uint32_t endpoint, cy_stc_usbfs_dev_drv_context_t *context)</argsstring>
        <name>Cy_USBFS_Dev_Drv_Abort</name>
        <param>
          <type>USBFS_Type *</type>
          <declname>base</declname>
        </param>
        <param>
          <type>uint32_t</type>
          <declname>endpoint</declname>
        </param>
        <param>
          <type><ref kindref="compound" refid="structcy__stc__usbfs__dev__drv__context__t">cy_stc_usbfs_dev_drv_context_t</ref> *</type>
          <declname>context</declname>
        </param>
        <briefdescription>
<para>Abort operation for data endpoint. </para>        </briefdescription>
        <detaileddescription>
<para>If there is any bus activity after the abort operation requested, the function waits for its completion or a timeout. A timeout is the time to transfer the bulk or an interrupt packet of the maximum playload size. If this bus activity is a transfer to the aborting endpoint, the received data is lost and the endpoint transfer completion callbacks are not invoked. After the function returns a new read or write, the endpoint operation can be submitted.</para><para><parameterlist kind="param"><parameteritem>
<parameternamelist>
<parametername>base</parametername>
</parameternamelist>
<parameterdescription>
<para>The pointer to the USBFS instance.</para></parameterdescription>
</parameteritem>
<parameteritem>
<parameternamelist>
<parametername>endpoint</parametername>
</parameternamelist>
<parameterdescription>
<para>The data endpoint number.</para></parameterdescription>
</parameteritem>
<parameteritem>
<parameternamelist>
<parametername>context</parametername>
</parameternamelist>
<parameterdescription>
<para>The pointer to the context structure <ref kindref="compound" refid="structcy__stc__usbfs__dev__drv__context__t">cy_stc_usbfs_dev_drv_context_t</ref> allocated by the user. The structure is used during the USBFS Device operation for internal configuration and data retention. The user must not modify anything in this structure.</para></parameterdescription>
</parameteritem>
</parameterlist>
<simplesect kind="return"><para>Data endpoint state <ref kindref="member" refid="group__group__usbfs__dev__drv__enums_1ga9a078f172cd82350179929c0133d9bbf">cy_en_usb_dev_ep_state_t</ref> after abort was applied.</para></simplesect>
<verbatim>embed:rst 
.. raw:: html

  &lt;div class="admonition note"&gt;&lt;p class="admonition-title"&gt;note&lt;/p&gt;&lt;p&gt;&lt;p&gt;&lt;ul&gt;&lt;li&gt;&lt;p&gt;This abort operation is not supported for the ISOC endpoints because these endpoints do not have a handshake and are always accessible to the USB Host. Therefore, an abort can cause unexpected behavior.&lt;/p&gt;&lt;/li&gt;&lt;li&gt;&lt;p&gt;The function uses the critical section to protect from the endpoint transfer complete interrupt. &lt;/p&gt;&lt;/li&gt;&lt;/ul&gt;&lt;/p&gt;&lt;/p&gt;&lt;/div&gt;</verbatim></para>        </detaileddescription>
        <inbodydescription>
        </inbodydescription>
        <location bodyend="882" bodyfile="/var/tmp/gitlab-runner1/builds/7e5c9300/0/repo/psoc6pdl/drivers/source/cy_usbfs_dev_drv_io.c" bodystart="749" column="1" file="/var/tmp/gitlab-runner1/builds/7e5c9300/0/repo/psoc6pdl/drivers/include/cy_usbfs_dev_drv.h" line="1291" />
      </memberdef>
      <memberdef const="no" explicit="no" id="group__group__usbfs__dev__hal__functions__data__xfer_1ga8d912602fc11725b601da1d31db64fb1" inline="no" kind="function" prot="public" static="no" virt="non-virtual">
        <type>__STATIC_INLINE bool</type>
        <definition>__STATIC_INLINE bool Cy_USBFS_Dev_Drv_GetEndpointAckState</definition>
        <argsstring>(USBFS_Type const *base, uint32_t endpoint)</argsstring>
        <name>Cy_USBFS_Dev_Drv_GetEndpointAckState</name>
        <param>
          <type>USBFS_Type const *</type>
          <declname>base</declname>
        </param>
        <param>
          <type>uint32_t</type>
          <declname>endpoint</declname>
        </param>
        <briefdescription>
<para>Returns whether the transaction completed with ACK for a certain endpoint. </para>        </briefdescription>
        <detaileddescription>
<para><parameterlist kind="param"><parameteritem>
<parameternamelist>
<parametername>base</parametername>
</parameternamelist>
<parameterdescription>
<para>The pointer to the USBFS instance.</para></parameterdescription>
</parameteritem>
<parameteritem>
<parameternamelist>
<parametername>endpoint</parametername>
</parameternamelist>
<parameterdescription>
<para>The data endpoint number.</para></parameterdescription>
</parameteritem>
</parameterlist>
<simplesect kind="return"><para>ACK state: true - transaction completed with ACK, false - otherwise. </para></simplesect>
</para>        </detaileddescription>
        <inbodydescription>
        </inbodydescription>
        <location bodyend="2330" bodyfile="/var/tmp/gitlab-runner1/builds/7e5c9300/0/repo/psoc6pdl/drivers/include/cy_usbfs_dev_drv.h" bodystart="2324" column="1" file="/var/tmp/gitlab-runner1/builds/7e5c9300/0/repo/psoc6pdl/drivers/include/cy_usbfs_dev_drv.h" line="1295" />
      </memberdef>
      <memberdef const="no" explicit="no" id="group__group__usbfs__dev__hal__functions__data__xfer_1ga894066c0e057ad7fe16d60a1cd2dd5fa" inline="no" kind="function" prot="public" static="no" virt="non-virtual">
        <type>__STATIC_INLINE uint32_t</type>
        <definition>__STATIC_INLINE uint32_t Cy_USBFS_Dev_Drv_GetEndpointCount</definition>
        <argsstring>(USBFS_Type const *base, uint32_t endpoint)</argsstring>
        <name>Cy_USBFS_Dev_Drv_GetEndpointCount</name>
        <param>
          <type>USBFS_Type const *</type>
          <declname>base</declname>
        </param>
        <param>
          <type>uint32_t</type>
          <declname>endpoint</declname>
        </param>
        <briefdescription>
<para>Returns the number of data bytes in the transaction for a certain endpoint. </para>        </briefdescription>
        <detaileddescription>
<para>Before calling this function, ensure the Host has written data into the endpoint. The returned value is updated after the Host access to the endpoint but remains unchanged after data has been read from the endpoint buffer. A typical use case is to read the number of bytes that the Host wrote into the OUT endpoint.</para><para><parameterlist kind="param"><parameteritem>
<parameternamelist>
<parametername>base</parametername>
</parameternamelist>
<parameterdescription>
<para>The pointer to the USBFS instance.</para></parameterdescription>
</parameteritem>
<parameteritem>
<parameternamelist>
<parametername>endpoint</parametername>
</parameternamelist>
<parameterdescription>
<para>The data endpoint number.</para></parameterdescription>
</parameteritem>
</parameterlist>
<simplesect kind="return"><para>The number of data bytes in the transaction. </para></simplesect>
</para>        </detaileddescription>
        <inbodydescription>
        </inbodydescription>
        <location bodyend="2360" bodyfile="/var/tmp/gitlab-runner1/builds/7e5c9300/0/repo/psoc6pdl/drivers/include/cy_usbfs_dev_drv.h" bodystart="2355" column="1" file="/var/tmp/gitlab-runner1/builds/7e5c9300/0/repo/psoc6pdl/drivers/include/cy_usbfs_dev_drv.h" line="1296" />
      </memberdef>
      <memberdef const="no" explicit="no" id="group__group__usbfs__dev__hal__functions__data__xfer_1ga91ccc0565bc5f64a01bc439f6ad6063f" inline="no" kind="function" prot="public" static="no" virt="non-virtual">
        <type><ref kindref="member" refid="group__group__usbfs__dev__drv__enums_1gace2573b482baf5a2a0f796b72cc244ac">cy_en_usbfs_dev_drv_status_t</ref></type>
        <definition>cy_en_usbfs_dev_drv_status_t Cy_USBFS_Dev_Drv_StallEndpoint</definition>
        <argsstring>(USBFS_Type *base, uint32_t endpoint, cy_stc_usbfs_dev_drv_context_t *context)</argsstring>
        <name>Cy_USBFS_Dev_Drv_StallEndpoint</name>
        <param>
          <type>USBFS_Type *</type>
          <declname>base</declname>
        </param>
        <param>
          <type>uint32_t</type>
          <declname>endpoint</declname>
        </param>
        <param>
          <type><ref kindref="compound" refid="structcy__stc__usbfs__dev__drv__context__t">cy_stc_usbfs_dev_drv_context_t</ref> *</type>
          <declname>context</declname>
        </param>
        <briefdescription>
<para>Configures data endpoint to STALL any request intended for it. </para>        </briefdescription>
        <detaileddescription>
<para><parameterlist kind="param"><parameteritem>
<parameternamelist>
<parametername>base</parametername>
</parameternamelist>
<parameterdescription>
<para>The pointer to the USBFS instance.</para></parameterdescription>
</parameteritem>
<parameteritem>
<parameternamelist>
<parametername>endpoint</parametername>
</parameternamelist>
<parameterdescription>
<para>The data endpoint number.</para></parameterdescription>
</parameteritem>
<parameteritem>
<parameternamelist>
<parametername>context</parametername>
</parameternamelist>
<parameterdescription>
<para>The pointer to the context structure <ref kindref="compound" refid="structcy__stc__usbfs__dev__drv__context__t">cy_stc_usbfs_dev_drv_context_t</ref> allocated by the user. The structure is used during the USBFS Device operation for internal configuration and data retention. The user must not modify anything in this structure.</para></parameterdescription>
</parameteritem>
</parameterlist>
<simplesect kind="return"><para>Status code of the function execution <ref kindref="member" refid="group__group__usbfs__dev__drv__enums_1gace2573b482baf5a2a0f796b72cc244ac">cy_en_usbfs_dev_drv_status_t</ref>. </para></simplesect>
</para>        </detaileddescription>
        <inbodydescription>
        </inbodydescription>
        <location bodyend="936" bodyfile="/var/tmp/gitlab-runner1/builds/7e5c9300/0/repo/psoc6pdl/drivers/source/cy_usbfs_dev_drv_io.c" bodystart="907" column="1" file="/var/tmp/gitlab-runner1/builds/7e5c9300/0/repo/psoc6pdl/drivers/include/cy_usbfs_dev_drv.h" line="1298" />
      </memberdef>
      <memberdef const="no" explicit="no" id="group__group__usbfs__dev__hal__functions__data__xfer_1ga1f7b36adca0ee0371e6ae717c41c9ed3" inline="no" kind="function" prot="public" static="no" virt="non-virtual">
        <type><ref kindref="member" refid="group__group__usbfs__dev__drv__enums_1gace2573b482baf5a2a0f796b72cc244ac">cy_en_usbfs_dev_drv_status_t</ref></type>
        <definition>cy_en_usbfs_dev_drv_status_t Cy_USBFS_Dev_Drv_UnStallEndpoint</definition>
        <argsstring>(USBFS_Type *base, uint32_t endpoint, cy_stc_usbfs_dev_drv_context_t *context)</argsstring>
        <name>Cy_USBFS_Dev_Drv_UnStallEndpoint</name>
        <param>
          <type>USBFS_Type *</type>
          <declname>base</declname>
        </param>
        <param>
          <type>uint32_t</type>
          <declname>endpoint</declname>
        </param>
        <param>
          <type><ref kindref="compound" refid="structcy__stc__usbfs__dev__drv__context__t">cy_stc_usbfs_dev_drv_context_t</ref> *</type>
          <declname>context</declname>
        </param>
        <briefdescription>
<para>Release data endpoint STALL condition and clears data toggle bit. </para>        </briefdescription>
        <detaileddescription>
<para>The endpoint is returned to the same state as it was before STALL request.</para><para><parameterlist kind="param"><parameteritem>
<parameternamelist>
<parametername>base</parametername>
</parameternamelist>
<parameterdescription>
<para>The pointer to the USBFS instance.</para></parameterdescription>
</parameteritem>
<parameteritem>
<parameternamelist>
<parametername>endpoint</parametername>
</parameternamelist>
<parameterdescription>
<para>The data endpoint number.</para></parameterdescription>
</parameteritem>
<parameteritem>
<parameternamelist>
<parametername>context</parametername>
</parameternamelist>
<parameterdescription>
<para>The pointer to the context structure <ref kindref="compound" refid="structcy__stc__usbfs__dev__drv__context__t">cy_stc_usbfs_dev_drv_context_t</ref> allocated by the user. The structure is used during the USBFS Device operation for internal configuration and data retention. The user must not modify anything in this structure.</para></parameterdescription>
</parameteritem>
</parameterlist>
<simplesect kind="return"><para>Status code of the function execution <ref kindref="member" refid="group__group__usbfs__dev__drv__enums_1gace2573b482baf5a2a0f796b72cc244ac">cy_en_usbfs_dev_drv_status_t</ref>. </para></simplesect>
</para>        </detaileddescription>
        <inbodydescription>
        </inbodydescription>
        <location bodyend="1006" bodyfile="/var/tmp/gitlab-runner1/builds/7e5c9300/0/repo/psoc6pdl/drivers/source/cy_usbfs_dev_drv_io.c" bodystart="962" column="1" file="/var/tmp/gitlab-runner1/builds/7e5c9300/0/repo/psoc6pdl/drivers/include/cy_usbfs_dev_drv.h" line="1302" />
      </memberdef>
      </sectiondef>
    <briefdescription>
<para>The Data Endpoint Transfer functions provide an API to establish communication with the USB Host using data endpoint. </para>    </briefdescription>
    <detaileddescription>
    </detaileddescription>
  </compounddef>
</doxygen>