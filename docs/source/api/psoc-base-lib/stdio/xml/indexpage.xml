<doxygen xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" version="1.8.14" xsi:noNamespaceSchemaLocation="compound.xsd">
  <compounddef id="indexpage" kind="group">
    <compoundname>index</compoundname>
    <title>Retarget IO</title>
    <briefdescription>
    </briefdescription>
    <detaileddescription>
<para><bold>Overview</bold>
</para><para>A utility library to retarget the standard input/output (STDIO) messages to a UART port. With this library, you can directly print messages on a UART terminal using printf(). You can specify the TX pin, RX pin, and the baud rate through the <ref kindref="member" refid="group__group__board__libs_1ga21265301bf6e9239845227c2aead9293">cy_retarget_io_init()</ref> function. The UART HAL object is externally accessible so that you can use it with other UART HAL functions.</para><para><bold>NOTE:</bold> The standard library is not standard in how it treats an I/O stream. Some implement a data buffer by default. The buffer is not flushed until it is full. In that case it may appear that your I/O is not working. You should be aware of how the library buffers data, and you should identify a buffering strategy and buffer size for a specified stream. If you supply a buffer, it must exist until the stream is closed. The following line of code disables the buffer for the standard library that accompanies the GCC compiler: <verbatim>setvbuf( stdin, NULL, _IONBF, 0 );
</verbatim></para><para><bold>NOTE:</bold> If the application is built using newlib-nano, by default, floating point format strings (f) are not supported. To enable this support, you must add -u _printf_float to the linker command line.</para><para><bold>Quick Start</bold>
</para><para><orderedlist>
<listitem><para>Add #include "cy_retarget_io.h"</para></listitem><listitem><para>Call cy_retarget_io_init(CYBSP_DEBUG_UART_TX, CYBSP_DEBUG_UART_RX, CY_RETARGET_IO_BAUDRATE);</para><para>CYBSP_DEBUG_UART_TX and CYBSP_DEBUG_UART_RX pins are defined in the BSP and CY_RETARGET_IO_BAUDRATE is set to 115200. You can use a different baud rate if you prefer.</para></listitem><listitem><para>Start printing using printf()</para></listitem></orderedlist>
</para><para><bold>Enabling Conversion of '\n' into "\r\n"</bold>
</para><para>If you want to use only '\n' instead of "\r\n" for printing a new line using printf(), define the macro CY_RETARGET_IO_CONVERT_LF_TO_CRLF using the <emphasis>DEFINES</emphasis> variable in the application Makefile. The library will then append '\r' before '\n' character on the output direction (STDOUT). No conversion occurs if "\r\n" is already present.</para><para><bold>More information</bold>
</para><para><itemizedlist>
<listitem><para><ulink url="https://cypresssemiconductorco.github.io/retarget-io/html/index.html">API Reference Guide</ulink></para></listitem><listitem><para><ulink url="http://www.cypress.com">Cypress Semiconductor, an Infineon Technologies Company</ulink></para></listitem><listitem><para><ulink url="https://github.com/cypresssemiconductorco">Cypress Semiconductor GitHub</ulink></para></listitem><listitem><para><ulink url="https://www.cypress.com/products/modustoolbox-software-environment">ModusToolbox</ulink></para></listitem><listitem><para><ulink url="https://github.com/cypresssemiconductorco/Code-Examples-for-ModusToolbox-Software">PSoC 6 Code Examples using ModusToolbox IDE</ulink></para></listitem><listitem><para><ulink url="https://github.com/cypresssemiconductorco/psoc6-middleware">PSoC 6 Middleware</ulink></para></listitem><listitem><para><ulink url="https://community.cypress.com/docs/DOC-14644">PSoC 6 Resources - KBA223067</ulink> <hruler />
 © Cypress Semiconductor Corporation, 2019-2020. </para></listitem></itemizedlist>
</para>    </detaileddescription>
  </compounddef>
</doxygen>