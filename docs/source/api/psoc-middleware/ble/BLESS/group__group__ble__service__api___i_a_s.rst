==============================
Immediate Alert Service (IAS)
==============================

.. doxygengroup:: group_ble_service_api_IAS
   :project: bless
   
   
.. toctree::
   :hidden:

   group__group__ble__service__api___i_a_s__server__client.rst
   group__group__ble__service__api___i_a_s__server.rst
   group__group__ble__service__api___i_a_s__client.rst
   group__group__ble__service__api___i_a_s__definitions.rst