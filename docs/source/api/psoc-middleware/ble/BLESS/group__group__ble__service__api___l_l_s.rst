========================
Link Loss Service (LLS)
========================

.. doxygengroup:: group_ble_service_api_LLS
   :project: bless
   
.. toctree::
   :hidden:

   group__group__ble__service__api___l_l_s__server__client.rst
   group__group__ble__service__api___l_l_s__server.rst
   group__group__ble__service__api___l_l_s__client.rst
   group__group__ble__service__api___l_l_s__definitions.rst