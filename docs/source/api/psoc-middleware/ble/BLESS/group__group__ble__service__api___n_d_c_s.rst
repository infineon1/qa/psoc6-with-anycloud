===============================
Next DST Change Service (NDCS)
===============================

.. doxygengroup:: group_ble_service_api_NDCS
   :project: bless
   
.. toctree::
   :hidden:

   group__group__ble__service__api___n_d_c_s__server__client.rst
   group__group__ble__service__api___n_d_c_s__server.rst
   group__group__ble__service__api___n_d_c_s__client.rst
   group__group__ble__service__api___n_d_c_s__definitions.rst