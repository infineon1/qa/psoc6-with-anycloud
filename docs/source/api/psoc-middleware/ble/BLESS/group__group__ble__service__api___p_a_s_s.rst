==================================
Phone Alert Status Service (PASS)
==================================

.. doxygengroup:: group_ble_service_api_PASS
   :project: bless
   
.. toctree::
   :hidden:

   group__group__ble__service__api___p_a_s_s__server__client.rst
   group__group__ble__service__api___p_a_s_s__server.rst
   group__group__ble__service__api___p_a_s_s__client.rst
   group__group__ble__service__api___p_a_s_s__definitions.rst