============================
TX Power Service (TPS)
============================

.. doxygengroup:: group_ble_service_api_TPS
   :project: bless
   
.. toctree::
   :hidden:

   group__group__ble__service__api___t_p_s__server__client.rst
   group__group__ble__service__api___t_p_s__server.rst
   group__group__ble__service__api___t_p_s__client.rst
   group__group__ble__service__api___t_p_s__definitions.rst