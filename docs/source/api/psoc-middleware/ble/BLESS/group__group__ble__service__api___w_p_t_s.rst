========================================
Wireless Power Transfer Service (WPTS)
========================================

.. doxygengroup:: group_ble_service_api_WPTS
   :project: bless

.. toctree::
   :hidden:

   group__group__ble__service__api___w_p_t_s__server__client.rst
   group__group__ble__service__api___w_p_t_s__server.rst
   group__group__ble__service__api___w_p_t_s__client.rst
   group__group__ble__service__api___w_p_t_s__definitions.rst