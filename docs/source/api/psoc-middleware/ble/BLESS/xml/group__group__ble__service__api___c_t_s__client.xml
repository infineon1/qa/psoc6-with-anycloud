<doxygen xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" version="1.8.13" xsi:noNamespaceSchemaLocation="compound.xsd">
  <compounddef id="group__group__ble__service__api___c_t_s__client" kind="group">
    <compoundname>group_ble_service_api_CTS_client</compoundname>
    <title>CTS Client Functions</title>
      <sectiondef kind="func">
      <memberdef const="no" explicit="no" id="group__group__ble__service__api___c_t_s__client_1gaac2bdf801943ab8ebbc5c7edba63f6b7" inline="no" kind="function" prot="public" static="no" virt="non-virtual">
        <type><ref kindref="member" refid="group__group__ble__common__api__definitions_1ga63f6be7f53f5ad7d82c394e4cdfa619e">cy_en_ble_api_result_t</ref></type>
        <definition>cy_en_ble_api_result_t Cy_BLE_CTSC_SetCharacteristicValue</definition>
        <argsstring>(cy_stc_ble_conn_handle_t connHandle, cy_en_ble_cts_char_index_t charIndex, uint8_t attrSize, uint8_t *attrValue)</argsstring>
        <name>Cy_BLE_CTSC_SetCharacteristicValue</name>
        <param>
          <type><ref kindref="compound" refid="structcy__stc__ble__conn__handle__t">cy_stc_ble_conn_handle_t</ref></type>
          <declname>connHandle</declname>
        </param>
        <param>
          <type><ref kindref="member" refid="group__group__ble__service__api___c_t_s__definitions_1gab28471deec0e479464134b4c4c8309f5">cy_en_ble_cts_char_index_t</ref></type>
          <declname>charIndex</declname>
        </param>
        <param>
          <type>uint8_t</type>
          <declname>attrSize</declname>
        </param>
        <param>
          <type>uint8_t *</type>
          <declname>attrValue</declname>
        </param>
        <briefdescription>
<para>This function is used to write the characteristic (which is identified by charIndex) value attribute in the server. </para>        </briefdescription>
        <detaileddescription>
<para>As a result a Write Request is sent to the GATT Server and on successful execution of the request on the server side, the <ref kindref="member" refid="group__group__ble__service__api__events_1ggada4b6b6d080ad4380e3a2e3cab4f2d51a2b8cb928b29a04997f9e0985eac571f4">CY_BLE_EVT_CTSS_WRITE_CHAR</ref> event is generated. On successful request execution on the server side, the Write Response is sent to the client.</para><para><parameterlist kind="param"><parameteritem>
<parameternamelist>
<parametername>connHandle</parametername>
</parameternamelist>
<parameterdescription>
<para>The connection handle. </para></parameterdescription>
</parameteritem>
<parameteritem>
<parameternamelist>
<parametername>charIndex</parametername>
</parameternamelist>
<parameterdescription>
<para>The index of the service characteristic of type <ref kindref="member" refid="group__group__ble__service__api___c_t_s__definitions_1gab28471deec0e479464134b4c4c8309f5">cy_en_ble_cts_char_index_t</ref>. </para></parameterdescription>
</parameteritem>
<parameteritem>
<parameternamelist>
<parametername>attrSize</parametername>
</parameternamelist>
<parameterdescription>
<para>The size of the characteristic descriptor attribute. </para></parameterdescription>
</parameteritem>
<parameteritem>
<parameternamelist>
<parametername>attrValue</parametername>
</parameternamelist>
<parameterdescription>
<para>Pointer to the characteristic value data that should be sent to the server device.</para></parameterdescription>
</parameteritem>
</parameterlist>
<simplesect kind="return"><para>A return value of type <ref kindref="member" refid="group__group__ble__common__api__definitions_1ga63f6be7f53f5ad7d82c394e4cdfa619e">cy_en_ble_api_result_t</ref>.</para></simplesect>
<verbatim>embed:rst 
.. raw:: html

  &lt;table class="docutils align-default"&gt;&lt;tr&gt;&lt;td class="head"&gt;&lt;p&gt;Error Codes &lt;/p&gt;&lt;/td&gt;&lt;td class="head"&gt;&lt;p&gt;Description  &lt;/p&gt;&lt;/td&gt;&lt;/tr&gt;&lt;tr&gt;&lt;td&gt;&lt;p&gt;CY_BLE_SUCCESS &lt;/p&gt;&lt;/td&gt;&lt;td&gt;&lt;p&gt;The request was sent successfully. &lt;/p&gt;&lt;/td&gt;&lt;/tr&gt;&lt;tr&gt;&lt;td&gt;&lt;p&gt;CY_BLE_ERROR_INVALID_PARAMETER &lt;/p&gt;&lt;/td&gt;&lt;td&gt;&lt;p&gt;Validation of the input parameter failed. &lt;/p&gt;&lt;/td&gt;&lt;/tr&gt;&lt;tr&gt;&lt;td&gt;&lt;p&gt;CY_BLE_ERROR_INVALID_OPERATION &lt;/p&gt;&lt;/td&gt;&lt;td&gt;&lt;p&gt;Operation is invalid for this characteristic. &lt;/p&gt;&lt;/td&gt;&lt;/tr&gt;&lt;tr&gt;&lt;td&gt;&lt;p&gt;CY_BLE_ERROR_GATT_DB_INVALID_ATTR_HANDLE &lt;/p&gt;&lt;/td&gt;&lt;td&gt;&lt;p&gt;The peer device doesn't have the particular characteristic. &lt;/p&gt;&lt;/td&gt;&lt;/tr&gt;&lt;tr&gt;&lt;td&gt;&lt;p&gt;CY_BLE_ERROR_INVALID_STATE &lt;/p&gt;&lt;/td&gt;&lt;td&gt;&lt;p&gt;Connection with the server is not established. &lt;/p&gt;&lt;/td&gt;&lt;/tr&gt;&lt;tr&gt;&lt;td&gt;&lt;p&gt;CY_BLE_ERROR_MEMORY_ALLOCATION_FAILED &lt;/p&gt;&lt;/td&gt;&lt;td&gt;&lt;p&gt;Memory allocation failed. &lt;/p&gt;&lt;/td&gt;&lt;/tr&gt;&lt;/table&gt;</verbatim><simplesect kind="par"><title>Events</title><para>In case of successful execution (return value = <ref kindref="member" refid="group__group__ble__common__api__definitions_1gga63f6be7f53f5ad7d82c394e4cdfa619ea26e476ee364252903ce212deeb6ec064">CY_BLE_SUCCESS</ref>) the following events can appear: <linebreak />
 If the CTS service-specific callback is registered with <ref kindref="member" refid="group__group__ble__service__api___c_t_s__server__client_1gacd687f278ad25b555e9016f4a4aa0eb7">Cy_BLE_CTS_RegisterAttrCallback()</ref>:<itemizedlist>
<listitem><para><ref kindref="member" refid="group__group__ble__service__api__events_1ggada4b6b6d080ad4380e3a2e3cab4f2d51a4fdf5cfadabc8c119226bf61114c32c9">CY_BLE_EVT_CTSC_WRITE_CHAR_RESPONSE</ref> - In case if the requested attribute is successfully written on the peer device, the details (charIndex, etc.) are provided with event parameter structure of type <ref kindref="compound" refid="structcy__stc__ble__cts__char__value__t">cy_stc_ble_cts_char_value_t</ref>.</para></listitem></itemizedlist>
Otherwise (if the CTS service-specific callback is not registered):<itemizedlist>
<listitem><para><ref kindref="member" refid="group__group__ble__common__api__events_1gga7c1ef7e6d246e0f3eb6c883e23d82b86a9a5f078b557f3991ed1356d08139b094">CY_BLE_EVT_GATTC_WRITE_RSP</ref> - In case if the requested attribute is successfully written on the peer device.</para></listitem><listitem><para><ref kindref="member" refid="group__group__ble__common__api__events_1gga7c1ef7e6d246e0f3eb6c883e23d82b86a89dd75cdee5a4cf9131a12ad37363fd3">CY_BLE_EVT_GATTC_ERROR_RSP</ref> - In case if an error occurred with the requested attribute on the peer device, the details are provided with event parameters structure <ref kindref="compound" refid="structcy__stc__ble__gatt__err__param__t">cy_stc_ble_gatt_err_param_t</ref>. </para></listitem></itemizedlist>
</para></simplesect>
</para>        </detaileddescription>
        <inbodydescription>
        </inbodydescription>
        <location bodyend="1301" bodyfile="/var/tmp/gitlab-runner1/builds/mV2grwDe/0/repo/middleware-bless/cy_ble_cts.c" bodystart="1228" column="1" file="/var/tmp/gitlab-runner1/builds/mV2grwDe/0/repo/middleware-bless/cy_ble_cts.h" line="263" />
      </memberdef>
      <memberdef const="no" explicit="no" id="group__group__ble__service__api___c_t_s__client_1ga10ef7623a4d24288fa7f4888c7d757c3" inline="no" kind="function" prot="public" static="no" virt="non-virtual">
        <type><ref kindref="member" refid="group__group__ble__common__api__definitions_1ga63f6be7f53f5ad7d82c394e4cdfa619e">cy_en_ble_api_result_t</ref></type>
        <definition>cy_en_ble_api_result_t Cy_BLE_CTSC_GetCharacteristicValue</definition>
        <argsstring>(cy_stc_ble_conn_handle_t connHandle, cy_en_ble_cts_char_index_t charIndex)</argsstring>
        <name>Cy_BLE_CTSC_GetCharacteristicValue</name>
        <param>
          <type><ref kindref="compound" refid="structcy__stc__ble__conn__handle__t">cy_stc_ble_conn_handle_t</ref></type>
          <declname>connHandle</declname>
        </param>
        <param>
          <type><ref kindref="member" refid="group__group__ble__service__api___c_t_s__definitions_1gab28471deec0e479464134b4c4c8309f5">cy_en_ble_cts_char_index_t</ref></type>
          <declname>charIndex</declname>
        </param>
        <briefdescription>
<para>Gets a characteristic value of the Current Time service, which is identified by charIndex. </para>        </briefdescription>
        <detaileddescription>
<para><parameterlist kind="param"><parameteritem>
<parameternamelist>
<parametername>connHandle</parametername>
</parameternamelist>
<parameterdescription>
<para>The connection handle. </para></parameterdescription>
</parameteritem>
<parameteritem>
<parameternamelist>
<parametername>charIndex</parametername>
</parameternamelist>
<parameterdescription>
<para>The index of the service characteristic of type <ref kindref="member" refid="group__group__ble__service__api___c_t_s__definitions_1gab28471deec0e479464134b4c4c8309f5">cy_en_ble_cts_char_index_t</ref>.</para></parameterdescription>
</parameteritem>
</parameterlist>
<simplesect kind="return"><para>A return value of type <ref kindref="member" refid="group__group__ble__common__api__definitions_1ga63f6be7f53f5ad7d82c394e4cdfa619e">cy_en_ble_api_result_t</ref>.</para></simplesect>
<verbatim>embed:rst 
.. raw:: html

  &lt;table class="docutils align-default"&gt;&lt;tr&gt;&lt;td class="head"&gt;&lt;p&gt;Error Codes &lt;/p&gt;&lt;/td&gt;&lt;td class="head"&gt;&lt;p&gt;Description  &lt;/p&gt;&lt;/td&gt;&lt;/tr&gt;&lt;tr&gt;&lt;td&gt;&lt;p&gt;CY_BLE_SUCCESS &lt;/p&gt;&lt;/td&gt;&lt;td&gt;&lt;p&gt;The request was sent successfully. &lt;/p&gt;&lt;/td&gt;&lt;/tr&gt;&lt;tr&gt;&lt;td&gt;&lt;p&gt;CY_BLE_ERROR_INVALID_PARAMETER &lt;/p&gt;&lt;/td&gt;&lt;td&gt;&lt;p&gt;Validation of the input parameter failed. &lt;/p&gt;&lt;/td&gt;&lt;/tr&gt;&lt;tr&gt;&lt;td&gt;&lt;p&gt;CY_BLE_ERROR_INVALID_OPERATION &lt;/p&gt;&lt;/td&gt;&lt;td&gt;&lt;p&gt;Operation is invalid for this characteristic. &lt;/p&gt;&lt;/td&gt;&lt;/tr&gt;&lt;tr&gt;&lt;td&gt;&lt;p&gt;CY_BLE_ERROR_GATT_DB_INVALID_ATTR_HANDLE &lt;/p&gt;&lt;/td&gt;&lt;td&gt;&lt;p&gt;The peer device doesn't have the particular characteristic. &lt;/p&gt;&lt;/td&gt;&lt;/tr&gt;&lt;tr&gt;&lt;td&gt;&lt;p&gt;CY_BLE_ERROR_INVALID_STATE &lt;/p&gt;&lt;/td&gt;&lt;td&gt;&lt;p&gt;Connection with the server is not established. &lt;/p&gt;&lt;/td&gt;&lt;/tr&gt;&lt;tr&gt;&lt;td&gt;&lt;p&gt;CY_BLE_ERROR_MEMORY_ALLOCATION_FAILED &lt;/p&gt;&lt;/td&gt;&lt;td&gt;&lt;p&gt;Memory allocation failed. &lt;/p&gt;&lt;/td&gt;&lt;/tr&gt;&lt;/table&gt;</verbatim><simplesect kind="par"><title>Events</title><para>In case of successful execution (return value = <ref kindref="member" refid="group__group__ble__common__api__definitions_1gga63f6be7f53f5ad7d82c394e4cdfa619ea26e476ee364252903ce212deeb6ec064">CY_BLE_SUCCESS</ref>) the following events can appear: <linebreak />
 If the CTS service-specific callback is registered with <ref kindref="member" refid="group__group__ble__service__api___c_t_s__server__client_1gacd687f278ad25b555e9016f4a4aa0eb7">Cy_BLE_CTS_RegisterAttrCallback()</ref>:<itemizedlist>
<listitem><para><ref kindref="member" refid="group__group__ble__service__api__events_1ggada4b6b6d080ad4380e3a2e3cab4f2d51afb0fff6340b249d2c43bf2eeb9acab04">CY_BLE_EVT_CTSC_READ_CHAR_RESPONSE</ref> - In case if the requested attribute is successfully read on the peer device, the details (charIndex , value, etc.) are provided with event parameter structure of type <ref kindref="compound" refid="structcy__stc__ble__cts__char__value__t">cy_stc_ble_cts_char_value_t</ref>.</para></listitem></itemizedlist>
Otherwise (if an CTS service-specific callback is not registered):<itemizedlist>
<listitem><para><ref kindref="member" refid="group__group__ble__common__api__events_1gga7c1ef7e6d246e0f3eb6c883e23d82b86a9f66de88a47e0eb5cd2026421ffcea24">CY_BLE_EVT_GATTC_READ_RSP</ref> - If the requested attribute is successfully read on the peer device, the details (handle, value, etc.) are provided with an event parameter structure <ref kindref="compound" refid="structcy__stc__ble__gattc__read__rsp__param__t">cy_stc_ble_gattc_read_rsp_param_t</ref>. </para></listitem></itemizedlist>
</para></simplesect>
</para>        </detaileddescription>
        <inbodydescription>
        </inbodydescription>
        <location bodyend="1410" bodyfile="/var/tmp/gitlab-runner1/builds/mV2grwDe/0/repo/middleware-bless/cy_ble_cts.c" bodystart="1344" column="1" file="/var/tmp/gitlab-runner1/builds/mV2grwDe/0/repo/middleware-bless/cy_ble_cts.h" line="267" />
      </memberdef>
      <memberdef const="no" explicit="no" id="group__group__ble__service__api___c_t_s__client_1ga935941dd8677348f1b0c743657050fa0" inline="no" kind="function" prot="public" static="no" virt="non-virtual">
        <type><ref kindref="member" refid="group__group__ble__common__api__definitions_1ga63f6be7f53f5ad7d82c394e4cdfa619e">cy_en_ble_api_result_t</ref></type>
        <definition>cy_en_ble_api_result_t Cy_BLE_CTSC_SetCharacteristicDescriptor</definition>
        <argsstring>(cy_stc_ble_conn_handle_t connHandle, cy_en_ble_cts_char_index_t charIndex, cy_en_ble_cts_char_descriptors_t descrIndex, uint8_t attrSize, uint8_t *attrValue)</argsstring>
        <name>Cy_BLE_CTSC_SetCharacteristicDescriptor</name>
        <param>
          <type><ref kindref="compound" refid="structcy__stc__ble__conn__handle__t">cy_stc_ble_conn_handle_t</ref></type>
          <declname>connHandle</declname>
        </param>
        <param>
          <type><ref kindref="member" refid="group__group__ble__service__api___c_t_s__definitions_1gab28471deec0e479464134b4c4c8309f5">cy_en_ble_cts_char_index_t</ref></type>
          <declname>charIndex</declname>
        </param>
        <param>
          <type><ref kindref="member" refid="group__group__ble__service__api___c_t_s__definitions_1ga7b76c9e806cf77a56bff5932f438166f">cy_en_ble_cts_char_descriptors_t</ref></type>
          <declname>descrIndex</declname>
        </param>
        <param>
          <type>uint8_t</type>
          <declname>attrSize</declname>
        </param>
        <param>
          <type>uint8_t *</type>
          <declname>attrValue</declname>
        </param>
        <briefdescription>
<para>Sets a characteristic descriptor of the Current Time Characteristic of the Current Time service. </para>        </briefdescription>
        <detaileddescription>
<para>Internally, Write Request is sent to the GATT Server and on successful execution of the request on the server side, the following events can be generated:<itemizedlist>
<listitem><para><ref kindref="member" refid="group__group__ble__service__api__events_1ggada4b6b6d080ad4380e3a2e3cab4f2d51ac51646365561e6761fa7207f2e4fed1b">CY_BLE_EVT_CTSS_NOTIFICATION_ENABLED</ref></para></listitem><listitem><para><ref kindref="member" refid="group__group__ble__service__api__events_1ggada4b6b6d080ad4380e3a2e3cab4f2d51a69b4488d7b897eba8caaeff8e7db8bd8">CY_BLE_EVT_CTSS_NOTIFICATION_DISABLED</ref></para></listitem></itemizedlist>
</para><para><parameterlist kind="param"><parameteritem>
<parameternamelist>
<parametername>connHandle</parametername>
</parameternamelist>
<parameterdescription>
<para>The connection handle. </para></parameterdescription>
</parameteritem>
<parameteritem>
<parameternamelist>
<parametername>charIndex</parametername>
</parameternamelist>
<parameterdescription>
<para>The index of the service characteristic of type <ref kindref="member" refid="group__group__ble__service__api___c_t_s__definitions_1gab28471deec0e479464134b4c4c8309f5">cy_en_ble_cts_char_index_t</ref>. </para></parameterdescription>
</parameteritem>
<parameteritem>
<parameternamelist>
<parametername>descrIndex</parametername>
</parameternamelist>
<parameterdescription>
<para>The index of the service characteristic descriptor of type <ref kindref="member" refid="group__group__ble__service__api___c_t_s__definitions_1ga7b76c9e806cf77a56bff5932f438166f">cy_en_ble_cts_char_descriptors_t</ref>. </para></parameterdescription>
</parameteritem>
<parameteritem>
<parameternamelist>
<parametername>attrSize</parametername>
</parameternamelist>
<parameterdescription>
<para>The size of the characteristic descriptor attribute. </para></parameterdescription>
</parameteritem>
<parameteritem>
<parameternamelist>
<parametername>attrValue</parametername>
</parameternamelist>
<parameterdescription>
<para>Pointer to the characteristic descriptor value data that should be sent to the server device.</para></parameterdescription>
</parameteritem>
</parameterlist>
<simplesect kind="return"><para>A return value of type <ref kindref="member" refid="group__group__ble__common__api__definitions_1ga63f6be7f53f5ad7d82c394e4cdfa619e">cy_en_ble_api_result_t</ref>.</para></simplesect>
<verbatim>embed:rst 
.. raw:: html

  &lt;table class="docutils align-default"&gt;&lt;tr&gt;&lt;td class="head"&gt;&lt;p&gt;Error Codes &lt;/p&gt;&lt;/td&gt;&lt;td class="head"&gt;&lt;p&gt;Description  &lt;/p&gt;&lt;/td&gt;&lt;/tr&gt;&lt;tr&gt;&lt;td&gt;&lt;p&gt;CY_BLE_SUCCESS &lt;/p&gt;&lt;/td&gt;&lt;td&gt;&lt;p&gt;The request was sent successfully. &lt;/p&gt;&lt;/td&gt;&lt;/tr&gt;&lt;tr&gt;&lt;td&gt;&lt;p&gt;CY_BLE_ERROR_INVALID_PARAMETER &lt;/p&gt;&lt;/td&gt;&lt;td&gt;&lt;p&gt;Validation of the input parameter failed. &lt;/p&gt;&lt;/td&gt;&lt;/tr&gt;&lt;tr&gt;&lt;td&gt;&lt;p&gt;CY_BLE_ERROR_INVALID_OPERATION &lt;/p&gt;&lt;/td&gt;&lt;td&gt;&lt;p&gt;Operation is invalid for this characteristic. &lt;/p&gt;&lt;/td&gt;&lt;/tr&gt;&lt;tr&gt;&lt;td&gt;&lt;p&gt;CY_BLE_ERROR_GATT_DB_INVALID_ATTR_HANDLE &lt;/p&gt;&lt;/td&gt;&lt;td&gt;&lt;p&gt;The peer device doesn't have the particular characteristic. &lt;/p&gt;&lt;/td&gt;&lt;/tr&gt;&lt;tr&gt;&lt;td&gt;&lt;p&gt;CY_BLE_ERROR_INVALID_STATE &lt;/p&gt;&lt;/td&gt;&lt;td&gt;&lt;p&gt;The state is not valid. &lt;/p&gt;&lt;/td&gt;&lt;/tr&gt;&lt;tr&gt;&lt;td&gt;&lt;p&gt;CY_BLE_ERROR_MEMORY_ALLOCATION_FAILED &lt;/p&gt;&lt;/td&gt;&lt;td&gt;&lt;p&gt;Memory allocation failed. &lt;/p&gt;&lt;/td&gt;&lt;/tr&gt;&lt;/table&gt;</verbatim><simplesect kind="par"><title>Events</title><para>In case of successful execution (return value = <ref kindref="member" refid="group__group__ble__common__api__definitions_1gga63f6be7f53f5ad7d82c394e4cdfa619ea26e476ee364252903ce212deeb6ec064">CY_BLE_SUCCESS</ref>) the following events can appear: <linebreak />
 If the CTS service-specific callback is registered with <ref kindref="member" refid="group__group__ble__service__api___c_t_s__server__client_1gacd687f278ad25b555e9016f4a4aa0eb7">Cy_BLE_CTS_RegisterAttrCallback()</ref>:<itemizedlist>
<listitem><para><ref kindref="member" refid="group__group__ble__service__api__events_1ggada4b6b6d080ad4380e3a2e3cab4f2d51a6b06a4f5efd10fa2958716c749fd1003">CY_BLE_EVT_CTSC_WRITE_DESCR_RESPONSE</ref> - In case if the requested attribute is successfully written on the peer device, the details (charIndex, descrIndex etc.) are provided with event parameter structure of type <ref kindref="compound" refid="structcy__stc__ble__cts__descr__value__t">cy_stc_ble_cts_descr_value_t</ref>.</para></listitem></itemizedlist>
Otherwise (if an CTS service-specific callback is not registered):<itemizedlist>
<listitem><para><ref kindref="member" refid="group__group__ble__common__api__events_1gga7c1ef7e6d246e0f3eb6c883e23d82b86a9a5f078b557f3991ed1356d08139b094">CY_BLE_EVT_GATTC_WRITE_RSP</ref> - If the requested attribute is successfully written on the peer device.</para></listitem><listitem><para><ref kindref="member" refid="group__group__ble__common__api__events_1gga7c1ef7e6d246e0f3eb6c883e23d82b86a89dd75cdee5a4cf9131a12ad37363fd3">CY_BLE_EVT_GATTC_ERROR_RSP</ref> - If an error occurred with the requested attribute on the peer device, the details are provided with an event parameter structure ( <ref kindref="compound" refid="structcy__stc__ble__gatt__err__param__t">cy_stc_ble_gatt_err_param_t</ref>). </para></listitem></itemizedlist>
</para></simplesect>
</para>        </detaileddescription>
        <inbodydescription>
        </inbodydescription>
        <location bodyend="1516" bodyfile="/var/tmp/gitlab-runner1/builds/mV2grwDe/0/repo/middleware-bless/cy_ble_cts.c" bodystart="1466" column="1" file="/var/tmp/gitlab-runner1/builds/mV2grwDe/0/repo/middleware-bless/cy_ble_cts.h" line="270" />
      </memberdef>
      <memberdef const="no" explicit="no" id="group__group__ble__service__api___c_t_s__client_1gaeff9182394de09e361ccdd9a3de4f53b" inline="no" kind="function" prot="public" static="no" virt="non-virtual">
        <type><ref kindref="member" refid="group__group__ble__common__api__definitions_1ga63f6be7f53f5ad7d82c394e4cdfa619e">cy_en_ble_api_result_t</ref></type>
        <definition>cy_en_ble_api_result_t Cy_BLE_CTSC_GetCharacteristicDescriptor</definition>
        <argsstring>(cy_stc_ble_conn_handle_t connHandle, cy_en_ble_cts_char_index_t charIndex, uint8_t descrIndex)</argsstring>
        <name>Cy_BLE_CTSC_GetCharacteristicDescriptor</name>
        <param>
          <type><ref kindref="compound" refid="structcy__stc__ble__conn__handle__t">cy_stc_ble_conn_handle_t</ref></type>
          <declname>connHandle</declname>
        </param>
        <param>
          <type><ref kindref="member" refid="group__group__ble__service__api___c_t_s__definitions_1gab28471deec0e479464134b4c4c8309f5">cy_en_ble_cts_char_index_t</ref></type>
          <declname>charIndex</declname>
        </param>
        <param>
          <type>uint8_t</type>
          <declname>descrIndex</declname>
        </param>
        <briefdescription>
<para>Gets a characteristic descriptor of the Current Time Characteristic of the Current Time service. </para>        </briefdescription>
        <detaileddescription>
<para><parameterlist kind="param"><parameteritem>
<parameternamelist>
<parametername>connHandle</parametername>
</parameternamelist>
<parameterdescription>
<para>The connection handle. </para></parameterdescription>
</parameteritem>
<parameteritem>
<parameternamelist>
<parametername>charIndex</parametername>
</parameternamelist>
<parameterdescription>
<para>The index of the service characteristic of type <ref kindref="member" refid="group__group__ble__service__api___c_t_s__definitions_1gab28471deec0e479464134b4c4c8309f5">cy_en_ble_cts_char_index_t</ref>. </para></parameterdescription>
</parameteritem>
<parameteritem>
<parameternamelist>
<parametername>descrIndex</parametername>
</parameternamelist>
<parameterdescription>
<para>The index of the service characteristic descriptor of type <ref kindref="member" refid="group__group__ble__service__api___c_t_s__definitions_1ga7b76c9e806cf77a56bff5932f438166f">cy_en_ble_cts_char_descriptors_t</ref>.</para></parameterdescription>
</parameteritem>
</parameterlist>
<simplesect kind="return"><para>A return value of type <ref kindref="member" refid="group__group__ble__common__api__definitions_1ga63f6be7f53f5ad7d82c394e4cdfa619e">cy_en_ble_api_result_t</ref>.</para></simplesect>
<verbatim>embed:rst 
.. raw:: html

  &lt;table class="docutils align-default"&gt;&lt;tr&gt;&lt;td class="head"&gt;&lt;p&gt;Error Codes &lt;/p&gt;&lt;/td&gt;&lt;td class="head"&gt;&lt;p&gt;Description  &lt;/p&gt;&lt;/td&gt;&lt;/tr&gt;&lt;tr&gt;&lt;td&gt;&lt;p&gt;CY_BLE_SUCCESS &lt;/p&gt;&lt;/td&gt;&lt;td&gt;&lt;p&gt;The request was sent successfully. &lt;/p&gt;&lt;/td&gt;&lt;/tr&gt;&lt;tr&gt;&lt;td&gt;&lt;p&gt;CY_BLE_ERROR_INVALID_PARAMETER &lt;/p&gt;&lt;/td&gt;&lt;td&gt;&lt;p&gt;Validation of the input parameter failed. &lt;/p&gt;&lt;/td&gt;&lt;/tr&gt;&lt;tr&gt;&lt;td&gt;&lt;p&gt;CY_BLE_ERROR_INVALID_OPERATION &lt;/p&gt;&lt;/td&gt;&lt;td&gt;&lt;p&gt;This operation is not permitted on the specified attribute. &lt;/p&gt;&lt;/td&gt;&lt;/tr&gt;&lt;tr&gt;&lt;td&gt;&lt;p&gt;CY_BLE_ERROR_INVALID_STATE &lt;/p&gt;&lt;/td&gt;&lt;td&gt;&lt;p&gt;The state is not valid. &lt;/p&gt;&lt;/td&gt;&lt;/tr&gt;&lt;tr&gt;&lt;td&gt;&lt;p&gt;CY_BLE_ERROR_MEMORY_ALLOCATION_FAILED &lt;/p&gt;&lt;/td&gt;&lt;td&gt;&lt;p&gt;Memory allocation failed. &lt;/p&gt;&lt;/td&gt;&lt;/tr&gt;&lt;tr&gt;&lt;td&gt;&lt;p&gt;CY_BLE_ERROR_GATT_DB_INVALID_ATTR_HANDLE &lt;/p&gt;&lt;/td&gt;&lt;td&gt;&lt;p&gt;Peer device doesn't have a particular descriptor. &lt;/p&gt;&lt;/td&gt;&lt;/tr&gt;&lt;/table&gt;</verbatim><simplesect kind="par"><title>Events</title><para>In case of successful execution (return value = <ref kindref="member" refid="group__group__ble__common__api__definitions_1gga63f6be7f53f5ad7d82c394e4cdfa619ea26e476ee364252903ce212deeb6ec064">CY_BLE_SUCCESS</ref>) the following events can appear: <linebreak />
 If the CTS service-specific callback is registered with <ref kindref="member" refid="group__group__ble__service__api___c_t_s__server__client_1gacd687f278ad25b555e9016f4a4aa0eb7">Cy_BLE_CTS_RegisterAttrCallback()</ref>:<itemizedlist>
<listitem><para><ref kindref="member" refid="group__group__ble__service__api__events_1ggada4b6b6d080ad4380e3a2e3cab4f2d51a2b1fc6ef769c3cc71b456cebcb506ecd">CY_BLE_EVT_CTSC_READ_DESCR_RESPONSE</ref> - In case if the requested attribute is successfully read on the peer device, the details (charIndex, descrIndex, value, etc.) are provided with event parameter structure of type <ref kindref="compound" refid="structcy__stc__ble__cts__descr__value__t">cy_stc_ble_cts_descr_value_t</ref>.</para></listitem></itemizedlist>
Otherwise (if an CTS service-specific callback is not registered):<itemizedlist>
<listitem><para><ref kindref="member" refid="group__group__ble__common__api__events_1gga7c1ef7e6d246e0f3eb6c883e23d82b86a9f66de88a47e0eb5cd2026421ffcea24">CY_BLE_EVT_GATTC_READ_RSP</ref> - If the requested attribute is successfully read on the peer device, the details (handle, value, etc.) are provided with an event parameter structure <ref kindref="compound" refid="structcy__stc__ble__gattc__read__rsp__param__t">cy_stc_ble_gattc_read_rsp_param_t</ref>.</para></listitem><listitem><para><ref kindref="member" refid="group__group__ble__common__api__events_1gga7c1ef7e6d246e0f3eb6c883e23d82b86a89dd75cdee5a4cf9131a12ad37363fd3">CY_BLE_EVT_GATTC_ERROR_RSP</ref> - If an error occurred with the requested attribute on the peer device, the details are provided with an event parameter structure <ref kindref="compound" refid="structcy__stc__ble__gatt__err__param__t">cy_stc_ble_gatt_err_param_t</ref>. </para></listitem></itemizedlist>
</para></simplesect>
</para>        </detaileddescription>
        <inbodydescription>
        </inbodydescription>
        <location bodyend="1608" bodyfile="/var/tmp/gitlab-runner1/builds/mV2grwDe/0/repo/middleware-bless/cy_ble_cts.c" bodystart="1565" column="1" file="/var/tmp/gitlab-runner1/builds/mV2grwDe/0/repo/middleware-bless/cy_ble_cts.h" line="275" />
      </memberdef>
      </sectiondef>
    <briefdescription>
<para>API unique to CTS designs configured as a GATT Client role. </para>    </briefdescription>
    <detaileddescription>
<para>A letter 'c' is appended to the API name: Cy_BLE_CTSC_ </para>    </detaileddescription>
  </compounddef>
</doxygen>