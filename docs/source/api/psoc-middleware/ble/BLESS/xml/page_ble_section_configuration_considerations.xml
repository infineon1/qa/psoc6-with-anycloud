<doxygen xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" version="1.8.13" xsi:noNamespaceSchemaLocation="compound.xsd">
  <compounddef id="page_ble_section_configuration_considerations" kind="group">
    <compoundname>page_ble_section_configuration_considerations</compoundname>
    <title>Configuration Considerations</title>
    <detaileddescription>
<para /><para>This section explains how to configure the Bluetooth Low Energy (BLE) middleware for operation in either:<orderedlist>
<listitem><para>Complete BLE Protocol Single and Dual CPU modes</para></listitem><listitem><para>Controller-only mode (HCI over Software API)</para></listitem></orderedlist>
</para><para>The following figure shows the common flow for PSoC 6 BLE Middleware configuration: <image name="ble_config_common_flow.png" type="html" />
</para><para>Refer to the following sections for BLE configuration details:<itemizedlist>
<listitem><para><ref kindref="member" refid="page_ble_section_configuration_considerations_1group_ble_section_conf_cons_gui">Generate Configuration in the BT Configurator</ref></para></listitem><listitem><para><ref kindref="member" refid="page_ble_section_configuration_considerations_1group_ble_section_conf_cons_prebuild">BLE Stack Components and CM0+ Pre-built Images</ref><itemizedlist>
<listitem><para><ref kindref="member" refid="page_ble_section_configuration_considerations_1group_ble_subsection_conf_cons_libraries">BLE Stack Libraries</ref></para></listitem><listitem><para><ref kindref="member" refid="page_ble_section_configuration_considerations_1group_ble_subsection_conf_cons_precompiled">CM0+ BLESS Controller Pre-compiled Image</ref></para></listitem></itemizedlist>
</para></listitem><listitem><para><ref kindref="member" refid="page_ble_section_configuration_considerations_1group_ble_section_conf_cons_intr">Configure BLESS Interrupt</ref></para></listitem><listitem><para><ref kindref="member" refid="page_ble_section_configuration_considerations_1group_ble_section_conf_cons_init_enable">Initialize and Enable PSoC 6 BLE Middleware in Complete BLE Protocol Mode</ref><itemizedlist>
<listitem><para><ref kindref="member" refid="page_ble_section_configuration_considerations_1group_ble_subsection_conf_cons_single">Complete BLE Protocol Single CPU mode</ref></para></listitem><listitem><para><ref kindref="member" refid="page_ble_section_configuration_considerations_1group_ble_subsection_conf_cons_dual">Complete BLE Protocol Dual CPU Mode</ref></para></listitem></itemizedlist>
</para></listitem><listitem><para><ref kindref="member" refid="page_ble_section_configuration_considerations_1group_ble_section_conf_cons_init_enable_hci">Initialize and Enable PSoC 6 BLE Middleware in Controller-only Mode (HCI over Software API)</ref></para></listitem><listitem><para><ref kindref="member" refid="page_ble_section_configuration_considerations_1group_ble_section_conf_cons_ota_share_code">Over-The-Air Bootloading with Code Sharing</ref></para></listitem></itemizedlist>
</para>
<para><heading level="1">Generate Configuration in the BT Configurator</heading></para>
<para><orderedlist>
<listitem><para>Run the stand-alone BT Configurator to configure the BLE design.</para></listitem><listitem><para>After configuring the BLE General, GAP, GATT, and Link layer settings, save the generated source and header files.</para></listitem><listitem><para>Add these files to your application.</para></listitem></orderedlist>
</para><para>The generated files contain the BLE configuration structure, cy_ble_config (type of <ref kindref="compound" refid="structcy__stc__ble__config__t">cy_stc_ble_config_t</ref> ), and a set of macros that can be used in the application. Refer to <ulink url="https://www.cypress.com/ModusToolboxBLEConfig">ModusToolbox BT Configurator Tool Guide</ulink>.</para>

<para><heading level="1">BLE Stack Components and CM0+ Pre-built Images</heading></para>
<para>The BLE stack components and the pre-compiled image for the CM0+ BLE Sub-system (BLESS) controller are parts of the PSoC 6 BLE Middleware that include different variants of pre-built BLE stack libraries.</para><para>The user may specify the BLE stack components and CM0+ pre-built image via the COMPONENTS variable in the application level Makefile. The following table describes all the BLE stack components, pre-built images, and the relationship between them in different BLE operating modes:</para><para /><para>The following fragments of the application level Makefile show how to enable different BLE modes. </para>
<para><bold>BLE Stack Libraries</bold></para>
<para>The BLE stack libraries are compliant with the Arm Embedded Application Binary Interface (EABI). They are compiled with the Arm compiler version 5.03. The following table shows the mapping between the BLE stack libraries and the user-configured COMPONENT:</para><para />

<para><bold>CM0+ BLESS Controller Pre-compiled Image</bold></para>
<para>Pre-compiled BLESS controller image executed on the Cortex M0+ core of the PSoC 6 dual- core MCU. The image is provided as C arrays ready to be compiled as part of the Cortex M4 application. The Cortex M0+ application code is placed in internal flash by the Cortex M4 linker script. This image is used only in BLE Dual CPU mode. In this mode, the BLE functionality is split between CM0+ (controller) and CM4 (host). It uses IPC for communication between two CPU cores where both the controller and host run.</para><para>A BLESS controller pre-built image executes the following steps:</para><para><itemizedlist>
<listitem><para>configures a BLESS interrupt</para></listitem><listitem><para>registers an IPC-pipe callback for the PSoC 6 BLE Middleware to initialize and enable the BLE controller when the PSoC 6 BLE Middleware operates in BLE Dual CPU mode</para></listitem><listitem><para>starts the CM4 core at CY_CORTEX_M4_APPL_ADDR=0x10020000</para></listitem><listitem><para>goes into a while loop where it processes BLE controller events and puts the CM0+ core into CPU deep sleep.</para></listitem></itemizedlist>
</para><para>To use this image, update the ram, flash, and FLASH_CM0P_SIZE values in the linker script for CM4:</para><para>Example for the GCC compiler: <programlisting><codeline><highlight class="normal">...</highlight></codeline>
<codeline><highlight class="normal">MEMORY</highlight></codeline>
<codeline><highlight class="normal">{</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp />...</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp />ram<sp /><sp /><sp /><sp /><sp /><sp /><sp />(rwx)<sp /><sp /><sp />:<sp />ORIGIN<sp />=<sp />0x08003000,<sp />LENGTH<sp />=<sp />0x044800</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp />flash<sp /><sp /><sp /><sp /><sp />(rx)<sp /><sp /><sp /><sp />:<sp />ORIGIN<sp />=<sp />0x10000000,<sp />LENGTH<sp />=<sp />0x100000</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp />...<sp /></highlight></codeline>
<codeline><highlight class="normal">}</highlight></codeline>
<codeline><highlight class="normal">...</highlight></codeline>
<codeline><highlight class="normal">/*<sp />The<sp />size<sp />and<sp />start<sp />addresses<sp />of<sp />the<sp />Cortex-M0+<sp />application<sp />image<sp />*/</highlight></codeline>
<codeline><highlight class="normal">FLASH_CM0P_SIZE<sp /><sp />=<sp />0x20000;</highlight></codeline>
<codeline><highlight class="normal">...</highlight></codeline>
</programlisting></para><para>Example for the IAR compiler: <programlisting><codeline><highlight class="normal">...</highlight></codeline>
<codeline><highlight class="normal">/*<sp />RAM<sp />*/</highlight></codeline>
<codeline><highlight class="normal">define<sp />symbol<sp />__ICFEDIT_region_IRAM1_start__<sp />=<sp />0x08003000;</highlight></codeline>
<codeline><highlight class="normal">define<sp />symbol<sp />__ICFEDIT_region_IRAM1_end__<sp /><sp /><sp />=<sp />0x08047800;</highlight></codeline>
<codeline><highlight class="normal">/*<sp />Flash<sp />*/</highlight></codeline>
<codeline><highlight class="normal">define<sp />symbol<sp />__ICFEDIT_region_IROM1_start__<sp />=<sp />0x10000000;</highlight></codeline>
<codeline><highlight class="normal">define<sp />symbol<sp />__ICFEDIT_region_IROM1_end__<sp /><sp /><sp />=<sp />0x10100000;</highlight></codeline>
<codeline><highlight class="normal">...</highlight></codeline>
<codeline><highlight class="normal">/*<sp />The<sp />size<sp />and<sp />start<sp />addresses<sp />of<sp />the<sp />Cortex-M0+<sp />application<sp />image<sp />*/</highlight></codeline>
<codeline><highlight class="normal">define<sp />symbol<sp />FLASH_CM0P_SIZE<sp /><sp />=<sp />0x20000;</highlight></codeline>
<codeline><highlight class="normal">...</highlight></codeline>
</programlisting></para><para>Example for the IAR compiler: <programlisting><codeline><highlight class="normal">...</highlight></codeline>
<codeline><highlight class="normal">;<sp />RAM</highlight></codeline>
<codeline><highlight class="normal">#define<sp />RAM_START<sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp />0x08003000</highlight></codeline>
<codeline><highlight class="normal">#define<sp />RAM_SIZE<sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp />0x00044800</highlight></codeline>
<codeline><highlight class="normal">;<sp />Flash</highlight></codeline>
<codeline><highlight class="normal">#define<sp />FLASH_START<sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp />0x10000000</highlight></codeline>
<codeline><highlight class="normal">#define<sp />FLASH_SIZE<sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp />0x00100000</highlight></codeline>
<codeline><highlight class="normal">...</highlight></codeline>
<codeline><highlight class="normal">/*<sp />The<sp />size<sp />and<sp />start<sp />addresses<sp />of<sp />the<sp />Cortex-M0+<sp />application<sp />image<sp />*/</highlight></codeline>
<codeline><highlight class="normal">#define<sp />FLASH_CM0P_SIZE<sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp />0x20000</highlight></codeline>
<codeline><highlight class="normal">...</highlight></codeline>
</programlisting></para>


<para><heading level="1">Configure BLESS Interrupt</heading></para>
<para>The interrupt is mandatory for PSoC 6 BLE Middleware operation. The BLESS hardware block provides interrupt sources. To configure interrupts, the <ref kindref="member" refid="group__group__ble__common__api__functions_1gaf973ee7f4097d25ee569fd8bd3172265">Cy_BLE_BlessIsrHandler()</ref> function is called in the interrupt handler to process interrupt events generated by BLESS.</para><para>The BLESS interrupt is configured on the core where the BLE controller is running. The following table shows details of interrupt configuration depending on the BLE core modes.</para><para /><para>The following code shows how to implement the ISR handler for the BLESS interrupt service. The <ref kindref="member" refid="group__group__ble__common__api__functions_1gaf973ee7f4097d25ee569fd8bd3172265">Cy_BLE_BlessIsrHandler()</ref> function is called from BLESS ISR to process interrupt events generated by BLESS: <programlisting><codeline><highlight class="comment">/*******************************************************************************</highlight></codeline>
<codeline><highlight class="comment">*<sp />Function<sp />Name:<sp />BlessInterrupt</highlight></codeline>
<codeline><highlight class="comment">****************************************************************************/</highlight><highlight class="normal" /></codeline>
<codeline><highlight class="normal" /><highlight class="keyword">static</highlight><highlight class="normal"><sp /></highlight><highlight class="keywordtype">void</highlight><highlight class="normal"><sp />BlessInterrupt(</highlight><highlight class="keywordtype">void</highlight><highlight class="normal">)</highlight></codeline>
<codeline><highlight class="normal">{</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /></highlight><highlight class="comment">/*<sp />Call<sp />interrupt<sp />processing<sp />*/</highlight><highlight class="normal" /></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /><ref kindref="member" refid="group__group__ble__common__api__functions_1gaf973ee7f4097d25ee569fd8bd3172265">Cy_BLE_BlessIsrHandler</ref>();</highlight></codeline>
<codeline><highlight class="normal">}</highlight></codeline>
</programlisting> Finally, the BLESS interrupt is configured and interrupt handler routines are hooked up to NVIC. The pointer to the BLESS interrupt configuration structure (blessIsrCfg) is stored in the BLE configuration structure ( cy_ble_config): <programlisting><codeline><highlight class="comment">/*<sp />Assign<sp />BLESS<sp />interrupt<sp />number<sp />and<sp />priority<sp />*/</highlight><highlight class="normal" /></codeline>
<codeline><highlight class="normal" /><highlight class="keyword">const</highlight><highlight class="normal"><sp />cy_stc_sysint_t<sp />blessIsrCfg<sp />=</highlight></codeline>
<codeline><highlight class="normal">{</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /><sp /></highlight><highlight class="comment">/*<sp />The<sp />BLESS<sp />interrupt<sp />*/</highlight><highlight class="normal" /></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /><sp />.intrSrc<sp /><sp /><sp /><sp /><sp /><sp />=<sp />bless_interrupt_IRQn,</highlight></codeline>
<codeline><highlight class="normal" /></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /><sp /></highlight><highlight class="comment">/*<sp />The<sp />interrupt<sp />priority<sp />number<sp />*/</highlight><highlight class="normal" /></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /><sp />.intrPriority<sp />=<sp />1u</highlight></codeline>
<codeline><highlight class="normal">};</highlight></codeline>
<codeline><highlight class="normal" /></codeline>
<codeline><highlight class="normal" /><highlight class="comment">/*<sp />Hook<sp />interrupt<sp />service<sp />routines<sp />for<sp />BLESS<sp />*/</highlight><highlight class="normal" /></codeline>
<codeline><highlight class="normal">(void)<sp />Cy_SysInt_Init(&amp;blessIsrCfg,<sp />&amp;BlessInterrupt);</highlight></codeline>
<codeline><highlight class="normal" /></codeline>
<codeline><highlight class="normal" /><highlight class="comment">/*<sp />Store<sp />pointer<sp />to<sp />blessIsrCfg<sp />in<sp />BLE<sp />configuration<sp />structure<sp />*/</highlight><highlight class="normal" /></codeline>
<codeline><highlight class="normal">cy_ble_config.hw-&gt;blessIsrConfig<sp />=<sp />&amp;blessIsrCfg;</highlight></codeline>
</programlisting></para>

<para><heading level="1">Initialize and Enable PSoC 6 BLE Middleware in Complete BLE Protocol Mode</heading></para>
<para />
<para><bold>Complete BLE Protocol Single CPU mode</bold></para>
<para>In this mode, the BLE functionality is entirely on the CM4 CPU core. It uses a software interface for communication between the controller and host. The following figure shows the configuration flow for the BLE middleware in Single CPU mode.</para><para><image name="ble_config_single_mode.png" type="html" />
</para><para><orderedlist>
<listitem><para>Generate configuration in the BT Configurator, refer to <ulink url="https://www.cypress.com/ModusToolboxBLEConfig">ModusToolbox BT Configurator Tool Guide</ulink></para></listitem><listitem><para>Specify the following BLE Stack Component and CM0+ Pre-built:<itemizedlist>
<listitem><para>BLESS_HOST,</para></listitem><listitem><para>BLESS_CONTROLLER,</para></listitem><listitem><para>CM0P_SLEEP or other CM0+ image (e.g. CM0P_CRYPTO, etc).<linebreak />
 Refer to section <ref kindref="member" refid="page_ble_section_configuration_considerations_1group_ble_section_conf_cons_prebuild">BLE Stack Components and CM0+ Pre-built Images</ref> for detailed information.</para></listitem></itemizedlist>
</para></listitem><listitem><para>Initialize the BLESS interrupt as shown in section <ref kindref="member" refid="page_ble_section_configuration_considerations_1group_ble_section_conf_cons_intr">Configure BLESS Interrupt</ref>.</para></listitem><listitem><para>Initialize and enable the PSoC 6 BLE Middleware in the application code: <orderedlist>
<listitem>
<para>Call the <ref kindref="member" refid="group__group__ble__common__api__functions_1ga3d3e2bf00cc87f8dfc17fa0c68fd7506">Cy_BLE_RegisterEventCallback()</ref> function to register the generic callback function. The callback function is of type cy_ble_callback_t, as defined by: void (*cy_ble_callback_t)(uint32_t eventCode, void *eventParam). </para></listitem>
<listitem>
<para>Call Cy_BLE_Init(&amp;cy_ble_config), where cy_ble_config is the configuration structure exposed in cycfg_ble.c (generated by the BT Configurator). </para></listitem>
<listitem>
<para>Call <ref kindref="member" refid="group__group__ble__common__api__functions_1ga71930f6a2639719f896326a9bb73f543">Cy_BLE_Enable()</ref> to enable the BLE host and controller. </para></listitem>
<listitem>
<para>Optionally, to enable BLE low-power mode, call the <ref kindref="member" refid="group__group__ble__common__api__functions_1ga11dd55190fcf99efcc2dd0f625014f4b">Cy_BLE_EnableLowPowerMode()</ref> function. </para></listitem>
<listitem>
<para>Register the service-specific callback functions to be used. For example, use Cy_BLE_IAS_RegisterAttrCallback( IasEventHandler) to register service-specific callback function IasEventHandler for the Immediate Alert service. This function is also of type cy_ble_callback_t and is passed as a parameter to the service-specific callback registration function. The callback function is used to evaluate service-specific events and to take the appropriate action as defined by your application. Then, build a service-specific state machine using these events. </para></listitem>
</orderedlist>
</para></listitem></orderedlist>
</para>

<para><bold>Complete BLE Protocol Dual CPU Mode</bold></para>
<para /><para>In this mode, the BLE functionality is split between the CM0+ (controller) and CM4 (host) CPU cores. The controller part is implemented in BLESS controller CM0+ pre-built image. It uses IPC for communication between the two CPU cores that run the controller and host.The following figure shows the configuration flow for the PSoC 6 BLE Middleware in Dual CPU mode:</para><para><image name="ble_config_dual_mode.png" type="html" />
</para><para><orderedlist>
<listitem><para>Generate configuration in the BT Configurator, refer to <ulink url="https://www.cypress.com/ModusToolboxBLEConfig">ModusToolbox BT Configurator Tool Guide</ulink></para></listitem><listitem><para>Specify the following BLE Stack Component and CM0+ Pre-built:<itemizedlist>
<listitem><para>BLESS_HOST_IPC,</para></listitem><listitem><para>CM0P_BLESS.<linebreak />
 Refer to section <ref kindref="member" refid="page_ble_section_configuration_considerations_1group_ble_section_conf_cons_prebuild">BLE Stack Components and CM0+ Pre-built Images</ref> for detailed information.</para></listitem></itemizedlist>
</para></listitem><listitem><para>The BLESS interrupt is initialized in the CM0+ BLESS controller pre-built image, so you don't need to do this.</para></listitem><listitem><para>Initialize and enable the PSoC 6 BLE Middleware in the application code: <orderedlist>
<listitem>
<para>Call the <ref kindref="member" refid="group__group__ble__common__api__functions_1ga3d3e2bf00cc87f8dfc17fa0c68fd7506">Cy_BLE_RegisterEventCallback()</ref> function to register the generic callback function. The callback function is of type cy_ble_callback_t, as defined by: void (*cy_ble_callback_t)(uint32_t eventCode, void *eventParam). </para></listitem>
<listitem>
<para>Call Cy_BLE_Init(&amp;cy_ble_config), where cy_ble_config is the configuration structure exposed in cycfg_ble.c (generated by the BT Configurator). </para></listitem>
<listitem>
<para>Call <ref kindref="member" refid="group__group__ble__common__api__functions_1ga71930f6a2639719f896326a9bb73f543">Cy_BLE_Enable()</ref> to enable the BLE host and controller. </para></listitem>
<listitem>
<para>Optionally, to enable BLE low-power mode, call the <ref kindref="member" refid="group__group__ble__common__api__functions_1ga11dd55190fcf99efcc2dd0f625014f4b">Cy_BLE_EnableLowPowerMode()</ref> function. </para></listitem>
<listitem>
<para>Register the service-specific callback functions to be used. For example, use Cy_BLE_IAS_RegisterAttrCallback( IasEventHandler) to register service-specific callback function IasEventHandler for the Immediate Alert service. This function is also of type cy_ble_callback_t and is passed as a parameter to the service-specific callback registration function. The callback function is used to evaluate service-specific events and to take the appropriate action as defined by your application. Then, build a service-specific state machine using these events. </para></listitem>
</orderedlist>
</para></listitem></orderedlist>
</para><para>The following code snippet shows the BLE initialization: <programlisting><codeline><highlight class="normal">void<sp />main(void)</highlight></codeline>
<codeline><highlight class="normal">{</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp />/*<sp />...<sp />Initializes<sp />the<sp />BLE<sp />interrupt<sp />(skipped<sp />in<sp />this<sp />code<sp />snippet)<sp />...*/</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /></highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp />/*<sp />Registers<sp />the<sp />generic<sp />callback<sp />functions<sp /><sp />*/</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp />Cy_BLE_RegisterEventCallback(AppCallBack);</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /></highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp />/*<sp />Initializes<sp />the<sp />BLE<sp />host<sp />*/</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp />Cy_BLE_Init(&amp;cy_ble_config);</highlight></codeline>
<codeline />
<codeline><highlight class="normal"><sp /><sp /><sp /><sp />/*<sp />Enables<sp />BLE<sp />Low-power<sp />mode<sp />(LPM)*/</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp />Cy_BLE_EnableLowPowerMode();</highlight></codeline>
<codeline />
<codeline><highlight class="normal"><sp /><sp /><sp /><sp />/*<sp />Enables<sp />BLE<sp />*/</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp />Cy_BLE_Enable();</highlight></codeline>
<codeline />
<codeline><highlight class="normal"><sp /><sp /><sp /><sp />/*<sp />Registers<sp />the<sp />service-specific<sp />callback<sp />functions<sp />(e.g.<sp />IAS)<sp />*/</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp />Cy_BLE_IAS_RegisterAttrCallback(IasEventHandler);</highlight></codeline>
<codeline><highlight class="normal"><sp /></highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp />for<sp />(;;)</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp />{</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp />/*<sp />Cy_BLE_ProcessEvents()<sp />allows<sp />the<sp />BLE<sp />stack<sp />to<sp />process<sp />pending<sp />events<sp />*/</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp />Cy_BLE_ProcessEvents();</highlight></codeline>
<codeline><highlight class="normal"><sp /></highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp />/*<sp />The<sp />main<sp />BLE<sp />application<sp />loop<sp />...<sp />*/<sp /><sp /><sp /><sp /><sp /><sp /></highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp />}</highlight></codeline>
<codeline><highlight class="normal">}</highlight></codeline>
</programlisting></para>


<para><heading level="1">Initialize and Enable PSoC 6 BLE Middleware in Controller-only Mode (HCI over Software API)</heading></para>
<para> <image name="ble_config_controller_only_mode.png" type="html" />
</para><para><orderedlist>
<listitem><para>In the BT Configurator, select the option BLE Controller-only (HCI) in the General Tab and generate configuration., refer to <ulink url="https://www.cypress.com/ModusToolboxBLEConfig">ModusToolbox BT Configurator Tool Guide</ulink></para></listitem><listitem><para>Specify the following BLE Stack Component and CM0+ Pre-built:<itemizedlist>
<listitem><para>BLESS_CONTROLLER,</para></listitem><listitem><para>CM0P_SLEEP or other CM0+ image (e.g. CM0P_CRYPTO, etc).<linebreak />
 Refer to section <ref kindref="member" refid="page_ble_section_configuration_considerations_1group_ble_section_conf_cons_prebuild">BLE Stack Components and CM0+ Pre-built Images</ref> for detailed information.</para></listitem></itemizedlist>
</para></listitem><listitem><para>Initialize the BLESS interrupt as shown in section <ref kindref="member" refid="page_ble_section_configuration_considerations_1group_ble_section_conf_cons_intr">Configure BLESS Interrupt</ref>.</para></listitem><listitem><para>Initialize and enable the PSoC 6 BLE Middleware in the application code: <orderedlist>
<listitem>
<para>Call Cy_BLE_RegisterEventCallback(StackEventHandler) to register an event callback function to receive events from the BLE controller. </para></listitem>
<listitem>
<para>Call Cy_BLE_Init(&amp;cy_ble_config) where cy_ble_config is the configuration structure exposed in cycfg_ble.c (generated by the BLE Configurator). </para></listitem>
<listitem>
<para>Call <ref kindref="member" refid="group__group__ble__common__api__functions_1ga71930f6a2639719f896326a9bb73f543">Cy_BLE_Enable()</ref> to enable the BLE controller. </para></listitem>
</orderedlist>
</para></listitem></orderedlist>
</para><para><emphasis>Send an HCI packet to the BLE stack controller</emphasis><linebreak />
 Use the <ref kindref="member" refid="group__group__ble__common___h_c_i__api__functions_1ga2963d8a11c1aac9442e171e6e33e34fb">Cy_BLE_SoftHciSendAppPkt()</ref> function to send an HCI packet to the BLE stack controller. The application allocates memory for the buffer to hold the HCI packet passed as an input parameter. This function copies the HCI packet into the controller's HCI buffer. Hence, the application may deallocate the memory buffer created to hold the HCI packet, once the API returns.</para><para><emphasis>Receive an HCI event (or ACL packet) from BLE stack controller</emphasis><linebreak />
 Use the <ref kindref="member" refid="group__group__ble__common___h_c_i__api__functions_1ga2963d8a11c1aac9442e171e6e33e34fb">Cy_BLE_SoftHciSendAppPkt()</ref> function to send an HCI packet to the BLE stack controller. The application allocates memory for the buffer to hold the HCI packet passed as an input parameter. This function copies the HCI packet into the controller's HCI buffer. Hence, the application may deallocate the memory buffer created to hold the HCI packet, once the API returns.</para>

<para><heading level="1">Over-The-Air Bootloading with Code Sharing</heading></para>
<para>This feature is used in the over-the-air (OTA) implementation. It allows sharing BLE component code between two component instances: one instance with profile-specific code and one with a stack. To configure OTA with code sharing, define CY_BLE_SHARING_MODE in the project. This parameter allows choosing between the following options:</para><para> <programlisting><codeline><highlight class="normal">/*<sp />Allocate<sp />memory<sp />for<sp />BLE<sp />stack<sp />*/</highlight></codeline>
<codeline><highlight class="normal">cy_ble_config.stackParam-&gt;memoryHeapPtr<sp />=<sp />(uint8<sp />*)malloc(CY_BLE_STACK_RAM_SIZE);</highlight></codeline>
<codeline />
<codeline><highlight class="normal">if(cy_ble_config.stackParam-&gt;memoryHeapPtr<sp />==<sp />NULL)</highlight></codeline>
<codeline><highlight class="normal">{</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp />Cy_SysLib_Halt(0x00u);</highlight></codeline>
<codeline><highlight class="normal">}</highlight></codeline>
</programlisting>  </para>
    </detaileddescription>
  </compounddef>
</doxygen>