===========================
WICED Bluetooth Host Stack
===========================

.. raw:: html

   <script type="text/javascript">
   window.location.href = "ble/ble.html"
   </script>


.. toctree::
   :hidden:

   ble/ble.rst
   dual_mode/dual_mode.rst