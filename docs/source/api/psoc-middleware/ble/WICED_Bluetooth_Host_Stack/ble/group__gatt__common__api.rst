=================
Connection API
=================

.. raw:: html

   <h3 class='simpleheader'>API Reference</h3>
   <hr style="margin-top:0px;border-top: 1px solid #404040 !important;">


.. toctree::
   
   group__gatt__eatt__functions.rst
   

.. doxygengroup:: gatt_common_api
   :project: WICED_Bluetooth_Host_Stack-ble
   :members:
   :protected-members:
   :private-members:
   :undoc-members:
