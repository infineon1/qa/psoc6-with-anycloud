====================================================
Logical Link Control and Adaptation Protocol (L2CAP)
====================================================

.. doxygengroup:: l2cap
   :project: WICED_Bluetooth_Host_Stack-ble
   :members:
   :protected-members:
   :private-members:
   :undoc-members:

   
.. toctree::
   :hidden:
   
   structwiced__bt__l2cap__fcr__options__t.rst
   structwiced__bt__l2cap__cfg__information__t.rst
   structwiced__bt__l2cap__appl__information__t.rst
   structwiced__bt__l2cap__le__appl__information__t.rst
   structwiced__bt__l2cap__fixed__chnl__reg__t.rst