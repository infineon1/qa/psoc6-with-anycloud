==========================
Timer Management Services
==========================

.. doxygengroup:: timer
   :project: WICED_Bluetooth_Host_Stack-ble
   :members:
   :protected-members:
   :private-members:
   :undoc-members: