===========================================
Bluetooth Stack Initialize & Configuration
===========================================

.. doxygengroup:: wiced_bt_cfg
   :project: WICED_Bluetooth_Host_Stack-ble
   :members:
   :protected-members:
   :private-members:
   :undoc-members:
   