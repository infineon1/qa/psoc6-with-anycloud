===================================
Bluetooth Stack Platform Interface
===================================

.. doxygengroup:: wiced_bt_platform_group
   :project: WICED_Bluetooth_Host_Stack-ble
   :members:
   :protected-members:
   :private-members:
   :undoc-members: