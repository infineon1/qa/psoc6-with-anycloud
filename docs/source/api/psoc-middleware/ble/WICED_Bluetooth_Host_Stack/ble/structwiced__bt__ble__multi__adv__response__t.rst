=============================================
wiced_bt_ble_multi_adv_response_t struct
=============================================

.. doxygenstruct:: wiced_bt_ble_multi_adv_response_t
   :project: WICED_Bluetooth_Host_Stack-ble
   :members:
   :protected-members:
   :private-members:
   :undoc-members:
