=============================================
wiced_bt_dev_encryption_status_t struct
=============================================

.. doxygenstruct:: wiced_bt_dev_encryption_status_t
   :project: WICED_Bluetooth_Host_Stack-ble
   :members:
   :protected-members:
   :private-members:
   :undoc-members:
