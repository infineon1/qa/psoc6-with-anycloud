=============================================
wiced_bt_dev_name_and_class_t struct
=============================================

.. doxygenstruct:: wiced_bt_dev_name_and_class_t
   :project: WICED_Bluetooth_Host_Stack-ble
   :members:
   :protected-members:
   :private-members:
   :undoc-members:
