<doxygen xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" version="1.8.15" xsi:noNamespaceSchemaLocation="compound.xsd">
  <compounddef id="wiced__bt__stack_8h" kind="file" language="C++">
    <compoundname>wiced_bt_stack.h</compoundname>
    <includes local="yes" refid="wiced__bt__cfg_8h">wiced_bt_cfg.h</includes>
      <sectiondef kind="user-defined">
      <memberdef const="no" explicit="no" id="group__wiced__bt__cfg_1gabac5a1c3126b145f079c6797790844af" inline="no" kind="function" prot="public" static="no" virt="non-virtual">
        <type><ref kindref="member" refid="group___result_1gacfc32ead8a827d53416f01b19eb58310">wiced_result_t</ref></type>
        <definition>wiced_result_t wiced_bt_stack_init</definition>
        <argsstring>(wiced_bt_management_cback_t *p_bt_management_cback, const wiced_bt_cfg_settings_t *p_bt_cfg_settings)</argsstring>
        <name>wiced_bt_stack_init</name>
        <param>
          <type><ref kindref="member" refid="group__wicedbt___device_management_1ga86af8a8bb467047c9af6007f15b0d344">wiced_bt_management_cback_t</ref> *</type>
          <declname>p_bt_management_cback</declname>
        </param>
        <param>
          <type>const <ref kindref="compound" refid="structwiced__bt__cfg__settings__t">wiced_bt_cfg_settings_t</ref> *</type>
          <declname>p_bt_cfg_settings</declname>
        </param>
        <briefdescription>
<para>Initialize the Bluetooth controller and stack; register callback for Bluetooth event notification. </para>
        </briefdescription>
        <detaileddescription>
<para><parameterlist kind="param"><parameteritem>
<parameternamelist>
<parametername direction="in">p_bt_management_cback</parametername>
</parameternamelist>
<parameterdescription>
<para>: Callback for receiving Bluetooth management events </para>
</parameterdescription>
</parameteritem>
<parameteritem>
<parameternamelist>
<parametername direction="in">p_bt_cfg_settings</parametername>
</parameternamelist>
<parameterdescription>
<para>: Bluetooth stack configuration <ref kindref="compound" refid="structwiced__bt__cfg__settings__t">wiced_bt_cfg_settings_t</ref></para>
</parameterdescription>
</parameteritem>
</parameterlist>
<simplesect kind="return"><para><bold> WICED_BT_SUCCESS </bold> : on success; <linebreak />
 <bold> WICED_BT_FAILED </bold> : if an error occurred </para>
</simplesect>
<verbatim>embed:rst 
.. raw:: html

  &lt;div class="admonition note"&gt;&lt;p class="admonition-title"&gt;note&lt;/p&gt;&lt;p&gt;&lt;p&gt;This API must be called before using any BT functionality. &lt;br /&gt;If p_bt_cfg_settings is null, stack uses default parameters defined in wiced_bt_cfg.h &lt;br /&gt; However, it is strongly recommended that applications define the configuration to appropriate values based on the application use case. &lt;/p&gt;&lt;/p&gt;&lt;/div&gt;</verbatim></para>
        </detaileddescription>
        <inbodydescription>
        </inbodydescription>
        <location column="1" file="C:/r1/builds/45XoCijv/0/repo/btstack/wiced_include/wiced_bt_stack.h" line="53" />
      </memberdef>
      <memberdef const="no" explicit="no" id="group__wiced__bt__cfg_1gaa7ee2ece60f1e4d174e85167c85377c7" inline="no" kind="function" prot="public" static="no" virt="non-virtual">
        <type><ref kindref="member" refid="group___result_1gacfc32ead8a827d53416f01b19eb58310">wiced_result_t</ref></type>
        <definition>wiced_result_t wiced_bt_stack_deinit</definition>
        <argsstring>(void)</argsstring>
        <name>wiced_bt_stack_deinit</name>
        <param>
          <type>void</type>
        </param>
        <briefdescription>
<para>This is a blocking call (returns after all de-initialisation procedures are complete) It is recommended that the application disconnect any outstanding connections prior to invoking this function. </para>
        </briefdescription>
        <detaileddescription>
<para><simplesect kind="return"><para><bold> WICED_BT_SUCCESS </bold> : on success; <linebreak />
 <bold> WICED_BT_ERROR </bold> : if an error occurred </para>
</simplesect>
</para>
        </detaileddescription>
        <inbodydescription>
        </inbodydescription>
        <location column="1" file="C:/r1/builds/45XoCijv/0/repo/btstack/wiced_include/wiced_bt_stack.h" line="63" />
      </memberdef>
      </sectiondef>
    <briefdescription>
<para>Bluetooth Management (BTM) Application Programming Interface. </para>
    </briefdescription>
    <detaileddescription>
<para>The BTM consists of several management entities:<orderedlist>
<listitem><para>Device Control - controls the local device</para>
</listitem><listitem><para>Device Discovery - manages inquiries, discover database</para>
</listitem><listitem><para>ACL Channels - manages ACL connections (BR/EDR and LE)</para>
</listitem><listitem><para>SCO Channels - manages SCO connections</para>
</listitem><listitem><para>Security - manages all security functionality</para>
</listitem><listitem><para>Power Management - manages park, sniff, hold, etc.</para>
</listitem></orderedlist>
</para>
<para>WICED Bluetooth Framework Functions </para>
    </detaileddescription>
    <programlisting>
<codeline lineno="1"><highlight class="comment">/*</highlight></codeline>
<codeline lineno="2"><highlight class="comment"><sp />*<sp />$<sp />Copyright<sp />Cypress<sp />Semiconductor<sp />$</highlight></codeline>
<codeline lineno="3"><highlight class="comment"><sp />*/</highlight><highlight class="normal" /></codeline>
<codeline lineno="4"><highlight class="normal" /></codeline>
<codeline lineno="20"><highlight class="preprocessor">#pragma<sp />once</highlight><highlight class="normal" /></codeline>
<codeline lineno="21"><highlight class="normal" /><highlight class="preprocessor">#include<sp />"wiced_bt_cfg.h"</highlight><highlight class="normal" /></codeline>
<codeline lineno="22"><highlight class="normal" /></codeline>
<codeline lineno="23"><highlight class="normal" /><highlight class="comment">/******************************************************</highlight></codeline>
<codeline lineno="24"><highlight class="comment"><sp />*<sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp />Function<sp />Declarations</highlight></codeline>
<codeline lineno="25"><highlight class="comment"><sp />******************************************************/</highlight><highlight class="normal" /></codeline>
<codeline lineno="26"><highlight class="normal" /><highlight class="preprocessor">#ifdef<sp />__cplusplus</highlight><highlight class="normal" /></codeline>
<codeline lineno="27"><highlight class="normal" /><highlight class="keyword">extern</highlight><highlight class="normal"><sp /></highlight><highlight class="stringliteral">"C"</highlight><highlight class="normal"><sp />{</highlight></codeline>
<codeline lineno="28"><highlight class="normal" /><highlight class="preprocessor">#endif</highlight><highlight class="normal" /></codeline>
<codeline lineno="29"><highlight class="normal" /></codeline>
<codeline lineno="30"><highlight class="normal" /><highlight class="comment">/****************************************************************************/</highlight></codeline>
<codeline lineno="37"><highlight class="comment">/****************************************************************************/</highlight><highlight class="normal" /></codeline>
<codeline lineno="38"><highlight class="normal" /></codeline>
<codeline lineno="53"><highlight class="normal"><ref kindref="member" refid="group___result_1gacfc32ead8a827d53416f01b19eb58310">wiced_result_t</ref><sp /><ref kindref="member" refid="group__wiced__bt__cfg_1gabac5a1c3126b145f079c6797790844af">wiced_bt_stack_init</ref>(<ref kindref="member" refid="group__wicedbt___device_management_1ga86af8a8bb467047c9af6007f15b0d344">wiced_bt_management_cback_t</ref><sp />*p_bt_management_cback,</highlight></codeline>
<codeline lineno="54"><highlight class="normal"><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /></highlight><highlight class="keyword">const</highlight><highlight class="normal"><sp /><ref kindref="compound" refid="structwiced__bt__cfg__settings__t">wiced_bt_cfg_settings_t</ref><sp />*p_bt_cfg_settings);</highlight></codeline>
<codeline lineno="55"><highlight class="normal" /></codeline>
<codeline lineno="63"><highlight class="normal"><ref kindref="member" refid="group___result_1gacfc32ead8a827d53416f01b19eb58310">wiced_result_t</ref><sp /><ref kindref="member" refid="group__wiced__bt__cfg_1gaa7ee2ece60f1e4d174e85167c85377c7">wiced_bt_stack_deinit</ref>(<sp /></highlight><highlight class="keywordtype">void</highlight><highlight class="normal"><sp />);</highlight></codeline>
<codeline lineno="64"><highlight class="normal" /></codeline>
<codeline lineno="65"><highlight class="normal" /></codeline>
<codeline lineno="69"><highlight class="preprocessor">#ifdef<sp />__cplusplus</highlight><highlight class="normal" /></codeline>
<codeline lineno="70"><highlight class="normal">}</highlight></codeline>
<codeline lineno="71"><highlight class="normal" /><highlight class="preprocessor">#endif</highlight><highlight class="normal" /></codeline>
<codeline lineno="72"><highlight class="normal" /></codeline>
<codeline lineno="73"><highlight class="normal" /></codeline>
    </programlisting>
    <location file="C:/r1/builds/45XoCijv/0/repo/btstack/wiced_include/wiced_bt_stack.h" />
  </compounddef>
</doxygen>