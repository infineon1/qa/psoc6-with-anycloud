========================
Server API
========================


.. raw:: html

   <h3 class='simpleheader'>API Reference</h3>
   <hr style="margin-top:0px;border-top: 1px solid #404040 !important;">


.. toctree::
   :titlesonly:
   
   group__gattsr__api__functions.rst
   group__gattdb__api__functions.rst
   group__gatt__robust__caching__api__functions.rst
   
   
.. doxygengroup:: gatt_server_api_functions
   :project: WICED_Bluetooth_Host_Stack-dual_mode
   :members:
   :protected-members:
   :private-members:
   :undoc-members:
   
