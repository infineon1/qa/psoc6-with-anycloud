========
Common
========

.. doxygengroup:: l2cap_common_api_functions
   :project: WICED_Bluetooth_Host_Stack-dual_mode
   :members:
   :protected-members:
   :private-members:
   :undoc-members: