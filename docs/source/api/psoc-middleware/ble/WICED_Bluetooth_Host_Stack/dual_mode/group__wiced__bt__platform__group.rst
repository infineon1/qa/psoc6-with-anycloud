===================================
Bluetooth Stack Platform Interface
===================================

.. doxygengroup:: wiced_bt_platform_group
   :project: WICED_Bluetooth_Host_Stack-dual_mode
   :members:
   :protected-members:
   :private-members:
   :undoc-members: