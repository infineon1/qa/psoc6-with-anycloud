=====================
MPEG-1,2 Support
=====================

.. doxygengroup:: wicedbt_a2dp_mpeg_1_2
   :project: WICED_Bluetooth_Host_Stack-dual_mode
   :members:
   :protected-members:
   :private-members:
   :undoc-members: