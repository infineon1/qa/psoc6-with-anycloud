====================================================
Audio/Video Distribution Transport (AVDT)
====================================================


.. raw:: html

   <h3 class='simpleheader'>API Reference</h3>
   <hr style="margin-top:0px;border-top: 1px solid #404040 !important;">


.. toctree::
   :titlesonly:
   
   group__wicedbt__av__a2d__helper.rst
   
   
.. doxygengroup:: wicedbt_avdt
   :project: WICED_Bluetooth_Host_Stack-dual_mode
   :members:
   :protected-members:
   :private-members:
   :undoc-members:
   
