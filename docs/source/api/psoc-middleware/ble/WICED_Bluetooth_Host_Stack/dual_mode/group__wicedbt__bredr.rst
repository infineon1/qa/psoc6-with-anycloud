===================================================
BR/EDR (Bluetooth Basic Rate / Enhanced Data Rate)
===================================================

.. doxygengroup:: wicedbt_bredr
   :project: WICED_Bluetooth_Host_Stack-dual_mode
   :members:
   :protected-members:
   :private-members:
   :undoc-members:
   

.. raw:: html

   <h3 class='simpleheader'>API Reference</h3>
   <hr style="margin-top:0px;border-top: 1px solid #404040 !important;">


.. toctree::
   :titlesonly:
   
   group__wiced__bredr__api.rst
   group__br__edr__sec__api__functions.rst

   
   

   
