<doxygen xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" version="1.8.14" xsi:noNamespaceSchemaLocation="compound.xsd">
  <compounddef id="_r_e_a_d_m_e_8doxygen_8md" kind="file" language="Markdown">
    <compoundname>README.doxygen.md</compoundname>
    <briefdescription>
    </briefdescription>
    <detaileddescription>
    </detaileddescription>
    <programlisting>
<codeline><highlight class="normal">#<sp />CY8CKIT-028-EPD<sp />shield<sp />support<sp />library</highlight></codeline>
<codeline />
<codeline><highlight class="normal">The<sp />E-ink<sp />Display<sp />Shield<sp />Board<sp />(CY8CKIT-028-EPD)<sp />has<sp />been<sp />designed<sp />such<sp />that<sp />an<sp />ultra-low-power<sp />E-ink<sp />display,<sp />sensors<sp />and<sp />a<sp />microphone<sp />can<sp />interface<sp />with<sp />PSoC<sp />MCUs.</highlight></codeline>
<codeline />
<codeline><highlight class="normal">It<sp />comes<sp />with<sp />the<sp />features<sp />below<sp />to<sp />enable<sp />everyday<sp />objects<sp />to<sp />connect<sp />to<sp />the<sp />Internet<sp />of<sp />Things<sp />(IoT).</highlight></codeline>
<codeline />
<codeline><highlight class="normal">*<sp />Ultra-low-power<sp />2.7"<sp />E-ink<sp />Display<sp />(E2271CS021)</highlight></codeline>
<codeline><highlight class="normal">*<sp />Motion<sp />Sensor<sp />(BMI-160)</highlight></codeline>
<codeline><highlight class="normal">*<sp />Temperature<sp />Sensor<sp />(NCP18XH103F03RB)</highlight></codeline>
<codeline><highlight class="normal">*<sp />PDM<sp />Microphone<sp />example<sp />code<sp />(SPK0838HT4HB)</highlight></codeline>
<codeline />
<codeline><highlight class="normal">The<sp />shield<sp />library<sp />provides<sp />support<sp />for:</highlight></codeline>
<codeline><highlight class="normal">*<sp />Initializing/freeing<sp />all<sp />of<sp />the<sp />hardware<sp />peripheral<sp />resources<sp />on<sp />the<sp />board</highlight></codeline>
<codeline><highlight class="normal">*<sp />Defining<sp />all<sp />pin<sp />mappings<sp />from<sp />the<sp />Arduino<sp />interface<sp />to<sp />the<sp />different<sp />peripherals</highlight></codeline>
<codeline><highlight class="normal">*<sp />Providing<sp />access<sp />to<sp />each<sp />of<sp />the<sp />underlying<sp />peripherals<sp />on<sp />the<sp />board</highlight></codeline>
<codeline />
<codeline><highlight class="normal">This<sp />library<sp />makes<sp />use<sp />of<sp />support<sp />libraries:<sp />[display-eink-e2271cs021](https://github.com/cypresssemiconductorco/display-eink-e2271cs021),<sp />[sensor-motion-bmi160](https://github.com/cypresssemiconductorco/sensor-motion-bmi160),<sp />and<sp />[thermistor](https://github.com/cypresssemiconductorco/thermistor).<sp />These<sp />can<sp />be<sp />seen<sp />in<sp />the<sp />libs<sp />directory<sp />and<sp />can<sp />be<sp />used<sp />directly<sp />instead<sp />of<sp />through<sp />the<sp />shield<sp />if<sp />desired.</highlight></codeline>
<codeline />
<codeline><highlight class="normal">The<sp />E-ink<sp />Display<sp />Shield<sp />Board<sp />uses<sp />the<sp />Arduino<sp />Uno<sp />pin<sp />layout,<sp />enabling<sp />this<sp />shield<sp />board<sp />to<sp />be<sp />used<sp />with<sp />the<sp />PSoC<sp />4<sp />and<sp />PSoC<sp />6<sp />MCU<sp />based<sp />Pioneer<sp />Kits.</highlight></codeline>
<codeline />
<codeline><highlight class="normal">![](board.png)</highlight></codeline>
<codeline />
<codeline><highlight class="normal">#<sp />Quick<sp />Start<sp />Guide</highlight></codeline>
<codeline />
<codeline><highlight class="normal">*<sp />[Basic<sp />shield<sp />usage](#basic-shield-usage)</highlight></codeline>
<codeline><highlight class="normal">*<sp />[Display<sp />usage](https://github.com/cypresssemiconductorco/display-eink-e2271cs021#quick-start)</highlight></codeline>
<codeline><highlight class="normal">*<sp />[Motion<sp />Sensor<sp />usage](https://github.com/cypresssemiconductorco/sensor-motion-bmi160#quick-start)</highlight></codeline>
<codeline><highlight class="normal">*<sp />[Thermistor<sp />usage](https://github.com/cypresssemiconductorco/thermistor#quick-start)</highlight></codeline>
<codeline />
<codeline />
<codeline><highlight class="normal">##<sp />Basic<sp />shield<sp />usage</highlight></codeline>
<codeline><highlight class="normal">The<sp />E-ink<sp />library<sp />can<sp />be<sp />also<sp />used<sp />standalone.</highlight></codeline>
<codeline><highlight class="normal">Follow<sp />the<sp />steps<sp />below<sp />to<sp />create<sp />a<sp />simple<sp />application<sp />which<sp />shows<sp />an<sp />interesting<sp />pattern<sp />on<sp />the<sp />display.</highlight></codeline>
<codeline><highlight class="normal">1.<sp />Create<sp />an<sp />empty<sp />application</highlight></codeline>
<codeline><highlight class="normal">2.<sp />Add<sp />this<sp />library<sp />to<sp />the<sp />application</highlight></codeline>
<codeline><highlight class="normal">3.<sp />Place<sp />the<sp />following<sp />code<sp />in<sp />the<sp />main.c<sp />file:</highlight></codeline>
<codeline><highlight class="normal">```cpp</highlight></codeline>
<codeline><highlight class="normal">#include<sp />"cyhal.h"</highlight></codeline>
<codeline><highlight class="normal">#include<sp />"cybsp.h"</highlight></codeline>
<codeline><highlight class="normal">#include<sp />"cy8ckit_028_epd_pins.h"</highlight></codeline>
<codeline><highlight class="normal">#include<sp />"mtb_e2271cs021.h"</highlight></codeline>
<codeline />
<codeline><highlight class="normal">cyhal_spi_t<sp />spi;</highlight></codeline>
<codeline />
<codeline><highlight class="normal">const<sp />mtb_e2271cs021_pins_t<sp />pins<sp />=</highlight></codeline>
<codeline><highlight class="normal">{</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp />.spi_mosi<sp />=<sp />CY8CKIT_028_EPD_PIN_DISPLAY_SPI_MOSI,</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp />.spi_miso<sp />=<sp />CY8CKIT_028_EPD_PIN_DISPLAY_SPI_MISO,</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp />.spi_sclk<sp />=<sp />CY8CKIT_028_EPD_PIN_DISPLAY_SPI_SCLK,</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp />.spi_cs<sp />=<sp />CY8CKIT_028_EPD_PIN_DISPLAY_CS,</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp />.reset<sp />=<sp />CY8CKIT_028_EPD_PIN_DISPLAY_RST,</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp />.busy<sp />=<sp />CY8CKIT_028_EPD_PIN_DISPLAY_BUSY,</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp />.discharge<sp />=<sp />CY8CKIT_028_EPD_PIN_DISPLAY_DISCHARGE,</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp />.enable<sp />=<sp />CY8CKIT_028_EPD_PIN_DISPLAY_EN,</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp />.border<sp />=<sp />CY8CKIT_028_EPD_PIN_DISPLAY_BORDER,</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp />.io_enable<sp />=<sp />CY8CKIT_028_EPD_PIN_DISPLAY_IOEN,</highlight></codeline>
<codeline><highlight class="normal">};</highlight></codeline>
<codeline />
<codeline><highlight class="normal">uint8_t<sp />previous_frame[PV_EINK_IMAGE_SIZE]<sp />=<sp />{0};</highlight></codeline>
<codeline><highlight class="normal">uint8_t<sp />current_frame[PV_EINK_IMAGE_SIZE]<sp />=<sp />{0};</highlight></codeline>
<codeline />
<codeline><highlight class="normal">int<sp />main(void)</highlight></codeline>
<codeline><highlight class="normal">{</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp />cy_rslt_t<sp />result;</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp />uint32_t<sp />i;</highlight></codeline>
<codeline />
<codeline><highlight class="normal"><sp /><sp /><sp /><sp />/*<sp />Initialize<sp />the<sp />device<sp />and<sp />board<sp />peripherals<sp />*/</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp />result<sp />=<sp />cybsp_init();</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp />CY_ASSERT(result<sp />==<sp />CY_RSLT_SUCCESS);</highlight></codeline>
<codeline />
<codeline><highlight class="normal"><sp /><sp /><sp /><sp />__enable_irq();</highlight></codeline>
<codeline />
<codeline><highlight class="normal"><sp /><sp /><sp /><sp />/*<sp />Initialize<sp />SPI<sp />and<sp />EINK<sp />display<sp />*/</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp />result<sp />=<sp />cyhal_spi_init(&amp;spi,<sp />CY8CKIT_028_EPD_PIN_DISPLAY_SPI_MOSI,</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp />CY8CKIT_028_EPD_PIN_DISPLAY_SPI_MISO,</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp />CY8CKIT_028_EPD_PIN_DISPLAY_SPI_SCLK,<sp />NC,<sp />NULL,<sp />8,</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp />CYHAL_SPI_MODE_00_MSB,<sp />false);</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp />if<sp />(CY_RSLT_SUCCESS<sp />==<sp />result)</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp />{</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp />result<sp />=<sp />cyhal_spi_set_frequency(&amp;spi,<sp />20000000);</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp />}</highlight></codeline>
<codeline />
<codeline><highlight class="normal"><sp /><sp /><sp /><sp />result<sp />=<sp />mtb_e2271cs021_init(&amp;pins,<sp />&amp;spi);</highlight></codeline>
<codeline />
<codeline><highlight class="normal"><sp /><sp /><sp /><sp />/*<sp />Fill<sp />the<sp />EINK<sp />buffer<sp />with<sp />an<sp />interesting<sp />pattern<sp />*/</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp />for<sp />(i<sp />=<sp />0;<sp />i<sp />&lt;<sp />PV_EINK_IMAGE_SIZE;<sp />i++)</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp />{</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp />current_frame[i]<sp />=<sp />i<sp />%<sp />0xFF;</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp />}</highlight></codeline>
<codeline />
<codeline><highlight class="normal"><sp /><sp /><sp /><sp />/*<sp />Update<sp />the<sp />display<sp />*/</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp />mtb_e2271cs021_show_frame(previous_frame,<sp />current_frame,<sp />MTB_E2271CS021_FULL_4STAGE,<sp />true);</highlight></codeline>
<codeline />
<codeline><highlight class="normal"><sp /><sp /><sp /><sp />for<sp />(;;)</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp />{</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp />}</highlight></codeline>
<codeline><highlight class="normal">}</highlight></codeline>
<codeline><highlight class="normal">```</highlight></codeline>
<codeline><highlight class="normal">4.<sp />Build<sp />the<sp />application<sp />and<sp />program<sp />the<sp />kit.</highlight></codeline>
<codeline />
<codeline><highlight class="normal">###<sp />More<sp />information</highlight></codeline>
<codeline />
<codeline><highlight class="normal">*<sp />[API<sp />Reference<sp />Guide](https://cypresssemiconductorco.github.io/CY8CKIT-028-EPD/html/index.html)</highlight></codeline>
<codeline><highlight class="normal">*<sp />[CY8CKIT-028-EPD<sp />Documentation](https://www.cypress.com/documentation/development-kitsboards/e-ink-display-shield-board-cy8ckit-028-epd)</highlight></codeline>
<codeline><highlight class="normal">*<sp />[SEGGER<sp />emWin<sp />Middleware<sp />Library](https://github.com/cypresssemiconductorco/emwin)</highlight></codeline>
<codeline><highlight class="normal">*<sp />[Cypress<sp />Semiconductor,<sp />an<sp />Infineon<sp />Technologies<sp />Company](http://www.cypress.com)</highlight></codeline>
<codeline><highlight class="normal">*<sp />[Cypress<sp />Semiconductor<sp />GitHub](https://github.com/cypresssemiconductorco)</highlight></codeline>
<codeline><highlight class="normal">*<sp />[ModusToolbox](https://www.cypress.com/products/modustoolbox-software-environment)</highlight></codeline>
<codeline />
<codeline><highlight class="normal">---</highlight></codeline>
<codeline><highlight class="normal">©<sp />Cypress<sp />Semiconductor<sp />Corporation,<sp />2019-2020.</highlight></codeline>
    </programlisting>
    <location file="bsp/shields/CY8CKIT-028-EPD/README.doxygen.md" />
  </compounddef>
</doxygen>