========================
API Reference
========================

Provides the list of API Reference


+-----------------------------------+-----------------------------------+
|  \ `Macros <group__group__dfu__m  |                                   |
| acro.html>`__                     |                                   |
+-----------------------------------+-----------------------------------+
|  \ `User Config                   |                                   |
| Macros <group__group__dfu__macro_ |                                   |
| _config.html>`__                  |                                   |
+-----------------------------------+-----------------------------------+
|  \ `DFU                           | The state of updating             |
| State <group__group__dfu__macro__ |                                   |
| state.html>`__                    |                                   |
+-----------------------------------+-----------------------------------+
|  \ `DFU                           |                                   |
| Commands <group__group__dfu__macr |                                   |
| o__commands.html>`__              |                                   |
+-----------------------------------+-----------------------------------+
|  \ `Read/Write Data IO Control    | The values of the ctl parameter   |
| Values <group__group__dfu__macro_ | to the                            |
| _ioctl.html>`__                   | `Cy_DFU_ReadData() <group__group_ |
|                                   | _dfu__functions.html#ga9709777e59 |
|                                   | f6cbdc9ca70c1ca041e414>`__        |
|                                   | and                               |
|                                   | `Cy_DFU_WriteData() <group__group |
|                                   | __dfu__functions.html#ga9f589c0df |
|                                   | 780bd29477ab4d73cf4063b>`__       |
|                                   | functions                         |
+-----------------------------------+-----------------------------------+
|  \ `Response                      |                                   |
| Size <group__group__dfu__macro__r |                                   |
| esponse__size.html>`__            |                                   |
+-----------------------------------+-----------------------------------+
|  \ `Functions <group__group__dfu_ |                                   |
| _functions.html>`__               |                                   |
+-----------------------------------+-----------------------------------+
|  \ `Global                        |                                   |
| Variables <group__group__dfu__glo |                                   |
| bals.html>`__                     |                                   |
+-----------------------------------+-----------------------------------+
|  \ `External ELF file             | CyMCUElfTools adds these symbols  |
| symbols <group__group__dfu__globa | to a generated ELF file           |
| ls__external__elf__symbols.ht     |                                   |
| ml>`__                            |                                   |
+-----------------------------------+-----------------------------------+
|  \ `Data                          |                                   |
| Structures <group__group__dfu__da |                                   |
| ta__structs.html>`__              |                                   |
+-----------------------------------+-----------------------------------+
|  \ `Enumerated                    |                                   |
| Types <group__group__dfu__enums.h |                                   |
| tml>`__                           |                                   |
+-----------------------------------+-----------------------------------+

.. toctree::
   :hidden:

   group__group__dfu__macro.rst
   group__group__dfu__functions.rst
   group__group__dfu__globals.rst
   group__group__dfu__data__structs.rst
   group__group__dfu__enums.rst









