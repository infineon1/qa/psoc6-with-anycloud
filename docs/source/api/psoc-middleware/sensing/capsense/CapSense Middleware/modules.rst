==============
API Reference
==============

.. toctree::
   :hidden:

   group__group__capsense__high__level.rst
   group__group__capsense__low__level.rst
   group__group__capsense__data__structure.rst
   group__group__capsense__enums.rst
   group__group__capsense__macros.rst
   group__group__capsense__callbacks.rst
   group__group__capsense__return__status.rst
   
   
Provides the list of API Reference

+-----------------------------------+-----------------------------------+
|  \ `High-level                    | High-level functions represent    |
| Functions <group__group__capsense | the highest abstraction layer of  |
| __high__level.html>`__            | the CapSense middleware           |
+-----------------------------------+-----------------------------------+
|  \ `Low-level                     | The low-level functions represent |
| Functions <group__group__capsense | the lower layer of abstraction in |
| __low__level.html>`__             | support of `High-level            |
|                                   | Functions <group__group__capsense |
|                                   | __high__level.html>`__            |
+-----------------------------------+-----------------------------------+
|  \ `CapSense Data                 | The CapSense Data Structure       |
| Structure <group__group__capsense | organizes configuration           |
| __data__structure.html>`__        | parameters, input, and output     |
|                                   | data shared among different FW    |
|                                   | modules within the CapSense       |
+-----------------------------------+-----------------------------------+
|  \ `CapSense                      | The CapSense structures           |
| Structures <group__group__capsens |                                   |
| e__structures.html>`__            |                                   |
+-----------------------------------+-----------------------------------+
|  \ `Gesture                       | The Gesture-related structures    |
| Structures <group__group__capsens |                                   |
| e__gesture__structures.html>`__   |                                   |
+-----------------------------------+-----------------------------------+
|  \ `Enumerated                    | Documents CapSense related        |
| Types <group__group__capsense__en | enumerated types                  |
| ums.html>`__                      |                                   |
+-----------------------------------+-----------------------------------+
|  \ `Macros <group__group__capsen  | Specifies constants used in       |
| se__macros.html>`__               | CapSense middleware               |
+-----------------------------------+-----------------------------------+
|  \ `General                       | General macros                    |
| Macros <group__group__capsense__m |                                   |
| acros__general.html>`__           |                                   |
+-----------------------------------+-----------------------------------+
|  \ `Settings                      | Settings macros                   |
| Macros <group__group__capsense__m |                                   |
| acros__settings.html>`__          |                                   |
+-----------------------------------+-----------------------------------+
|  \ `Pin-related                   | Pin-related macros                |
| Macros <group__group__capsense__m |                                   |
| acros__pin.html>`__               |                                   |
+-----------------------------------+-----------------------------------+
|  \ `Processing                    | Processing macros                 |
| Macros <group__group__capsense__m |                                   |
| acros__process.html>`__           |                                   |
+-----------------------------------+-----------------------------------+
|  \ `Touch-related                 | Touch-related macros              |
| Macros <group__group__capsense__m |                                   |
| acros__touch.html>`__             |                                   |
+-----------------------------------+-----------------------------------+
|  \ `Gesture                       | Gesture macros                    |
| Macros <group__group__capsense__m |                                   |
| acros__gesture.html>`__           |                                   |
+-----------------------------------+-----------------------------------+
|  \ `Miscellaneous                 | Miscellaneous macros              |
| Macros <group__group__capsense__m |                                   |
| acros__miscellaneous.html>`__     |                                   |
+-----------------------------------+-----------------------------------+
|  \ `Built-in Self-test            | Built-in Self-test macros         |
| Macros <group__group__capsense__m |                                   |
| acros__bist.html>`__              |                                   |
+-----------------------------------+-----------------------------------+
|  \ `Callbacks <group__group__caps | Callbacks allow the user to       |
| ense__callbacks.html>`__          | execute Custom code called from   |
|                                   | the CapSense middleware when an   |
|                                   | event occurs                      |
+-----------------------------------+-----------------------------------+
|  \ `Group_capsense_return_status  |                                   |
| <group__group__capsense__return__ |                                   |
| status.html>`__                   |                                   |
+-----------------------------------+-----------------------------------+


