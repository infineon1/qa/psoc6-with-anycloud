==============
API Reference
==============

.. toctree::
   :hidden:

   group__group__csdadc__macros.rst
   group__group__csdadc__enums.rst
   group__group__csdadc__data__structures.rst
   group__group__csdadc__functions.rst
   group__group__csdadc__callback.rst
   

Provides the list of API Reference

+----------------------------------+----------------------------------+
|  \ `Macros <group                | This section describes the       |
| __group__csdadc__macros.html>`__ | CSDADC Macros                    |
+----------------------------------+----------------------------------+
|  \ `Enumerated                   | Describes the enumeration types  |
| types <grou                      | defined by the CSDADC            |
| p__group__csdadc__enums.html>`__ |                                  |
+----------------------------------+----------------------------------+
|  \ `Data                         | Describes the data structures    |
| Structures <group__group__c      | defined by the CSDADC            |
| sdadc__data__structures.html>`__ |                                  |
+----------------------------------+----------------------------------+
|  \ `Functions <group__g          | This section describes the       |
| roup__csdadc__functions.html>`__ | CSDADC Function Prototypes       |
+----------------------------------+----------------------------------+
|  \ `Callback <group__            | This section describes the       |
| group__csdadc__callback.html>`__ | CSDADC Callback Function         |
+----------------------------------+----------------------------------+

