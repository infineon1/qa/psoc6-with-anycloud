==============
API Reference
==============

.. toctree::
   :hidden:

   group__group__csdidac__macros.rst
   group__group__csdidac__enums.rst
   group__group__csdidac__data__structures.rst
   group__group__csdidac__functions.rst
   

Provides the list of API Reference

+----------------------------------+----------------------------------+
|  \ `Macros <group_               | This section describes the       |
| _group__csdidac__macros.html>`__ | CSDIDAC macros                   |
+----------------------------------+----------------------------------+
|  \ `Enumerated                   | Describes the enumeration types  |
| types <group                     | defined by the CSDIDAC           |
| __group__csdidac__enums.html>`__ |                                  |
+----------------------------------+----------------------------------+
|  \ `Data                         | Describes the data structures    |
| Structures <group__group__cs     | defined by the CSDIDAC           |
| didac__data__structures.html>`__ |                                  |
+----------------------------------+----------------------------------+
|  \ `Functions <group__gr         | This section describes the       |
| oup__csdidac__functions.html>`__ | CSDIDAC Function Prototypes      |
+----------------------------------+----------------------------------+


