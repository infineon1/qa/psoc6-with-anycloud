<doxygen xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" version="1.8.14" xsi:noNamespaceSchemaLocation="compound.xsd">
  <compounddef id="indexpage" kind="group">
    <compoundname>index</compoundname>
    <title>BMI-160 Inertial Measurement Unit (Motion Sensor)</title>
    <briefdescription>
    </briefdescription>
    <detaileddescription>
<para><bold>Overview</bold>
</para><para>This library provides functions for interfacing with the BMI-160 I2C/SPI 16-bit Inertial Measurement Unit with three axis accelerometer and three axis gyroscope as used on the CY8CKIT-028-TFT and CY8CKIT-028-TFT shields.</para><para>Data Sheet: <ulink url="https://www.bosch-sensortec.com/products/motion-sensors/imus/bmi160.html">https://www.bosch-sensortec.com/products/motion-sensors/imus/bmi160.html</ulink> GitHub: <ulink url="https://github.com/BoschSensortec/BMI160_driver">https://github.com/BoschSensortec/BMI160_driver</ulink></para><para><bold>Quick Start</bold>
</para><para>Follow the steps below to create a simple application which outputs the accelerometer and gyroscope data from the motion sensor to the uart<orderedlist>
<listitem><para>Create an empty PSoC6 application</para></listitem><listitem><para>Add this library to the application</para></listitem><listitem><para>Add retarget-io library using the Library Manager</para></listitem><listitem><para>Place following code in the main.c file.</para></listitem><listitem><para>Define I2C SDA and SCL as appropriate for your hardware/shield kit <programlisting filename=".cpp"><codeline><highlight class="preprocessor">#include<sp />"cyhal.h"</highlight><highlight class="normal" /></codeline>
<codeline><highlight class="normal" /><highlight class="preprocessor">#include<sp />"cybsp.h"</highlight><highlight class="normal" /></codeline>
<codeline><highlight class="normal" /><highlight class="preprocessor">#include<sp />"cy_retarget_io.h"</highlight><highlight class="normal" /></codeline>
<codeline><highlight class="normal" /><highlight class="preprocessor">#include<sp />"mtb_bmi160.h"</highlight><highlight class="normal" /></codeline>
<codeline><highlight class="normal" /></codeline>
<codeline><highlight class="normal"><ref kindref="compound" refid="structmtb__bmi160__t">mtb_bmi160_t</ref><sp />motion_sensor;</highlight></codeline>
<codeline><highlight class="normal">cyhal_i2c_t<sp />i2c;</highlight></codeline>
<codeline><highlight class="normal">cyhal_i2c_cfg_t<sp />i2c_cfg<sp />=<sp />{</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp />.is_slave<sp />=<sp /></highlight><highlight class="keyword">false</highlight><highlight class="normal">,</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp />.address<sp />=<sp />0,</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp />.frequencyhal_hz<sp />=<sp />400000</highlight></codeline>
<codeline><highlight class="normal">};</highlight></codeline>
<codeline><highlight class="normal" /></codeline>
<codeline><highlight class="normal" /><highlight class="preprocessor">#define<sp />IMU_I2C_SDA<sp />(?)<sp />//<sp />Define<sp />me</highlight><highlight class="normal" /></codeline>
<codeline><highlight class="normal" /><highlight class="preprocessor">#define<sp />IMU_I2C_SCL<sp />(?)<sp />//<sp />Define<sp />me</highlight><highlight class="normal" /></codeline>
<codeline><highlight class="normal" /></codeline>
<codeline><highlight class="normal" /><highlight class="keywordtype">int</highlight><highlight class="normal"><sp />main(</highlight><highlight class="keywordtype">void</highlight><highlight class="normal">)</highlight></codeline>
<codeline><highlight class="normal">{</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp />cy_rslt_t<sp />result;</highlight></codeline>
<codeline><highlight class="normal" /></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /></highlight><highlight class="comment">/*<sp />Initialize<sp />the<sp />device<sp />and<sp />board<sp />peripherals<sp />*/</highlight><highlight class="normal" /></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp />result<sp />=<sp />cybsp_init();</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp />CY_ASSERT(result<sp />==<sp />CY_RSLT_SUCCESS);</highlight></codeline>
<codeline><highlight class="normal" /></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp />__enable_irq();</highlight></codeline>
<codeline><highlight class="normal" /></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /></highlight><highlight class="comment">/*<sp />Initialize<sp />retarget-io<sp />to<sp />use<sp />the<sp />debug<sp />UART<sp />port<sp />*/</highlight><highlight class="normal" /></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp />result<sp />=<sp />cy_retarget_io_init(CYBSP_DEBUG_UART_TX,<sp />CYBSP_DEBUG_UART_RX,<sp />CY_RETARGET_IO_BAUDRATE);</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp />CY_ASSERT(result<sp />==<sp />CY_RSLT_SUCCESS);</highlight></codeline>
<codeline><highlight class="normal" /></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /></highlight><highlight class="comment">/*<sp />Initialize<sp />i2c<sp />for<sp />motion<sp />sensor<sp />*/</highlight><highlight class="normal" /></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp />result<sp />=<sp />cyhal_i2c_init(&amp;i2c,<sp />IMU_I2C_SDA,<sp />IMU_I2C_SCL,<sp />NULL);</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp />CY_ASSERT(result<sp />==<sp />CY_RSLT_SUCCESS);</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp />result<sp />=<sp />cyhal_i2c_configure(&amp;i2c,<sp />&amp;i2c_cfg);</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp />CY_ASSERT(result<sp />==<sp />CY_RSLT_SUCCESS);</highlight></codeline>
<codeline><highlight class="normal" /></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /></highlight><highlight class="comment">/*<sp />Initialize<sp />motion<sp />sensor<sp />*/</highlight><highlight class="normal" /></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp />result<sp />=<sp /><ref kindref="member" refid="group__group__board__libs_1gac8f26d8dfa22eaef5a39303409133dfc">mtb_bmi160_init_i2c</ref>(&amp;motion_sensor,<sp />&amp;i2c,<sp />MTB_BMI160_DEFAULT_ADRESS);</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp />CY_ASSERT(result<sp />==<sp />CY_RSLT_SUCCESS);</highlight></codeline>
<codeline><highlight class="normal" /></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /></highlight><highlight class="keywordflow">for</highlight><highlight class="normal"><sp />(;;)</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp />{</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /></highlight><highlight class="comment">/*<sp />Get<sp />the<sp />accel<sp />and<sp />gyro<sp />data<sp />and<sp />print<sp />the<sp />results<sp />to<sp />the<sp />UART<sp />*/</highlight><highlight class="normal" /></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><ref kindref="compound" refid="structmtb__bmi160__data__t">mtb_bmi160_data_t</ref><sp />data;</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><ref kindref="member" refid="group__group__board__libs_1ga6e259c8f5f575e06fc3d35c525b4038f">mtb_bmi160_read</ref>(&amp;motion_sensor,<sp />&amp;data);</highlight></codeline>
<codeline><highlight class="normal" /></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp />printf(</highlight><highlight class="stringliteral">"Accel:<sp />X:%6d<sp />Y:%6d<sp />Z:%6d\r\n"</highlight><highlight class="normal">,<sp />data.<ref kindref="member" refid="structmtb__bmi160__data__t_1ad5ac1a5381c64919a50c422822b7b283">accel</ref>.x,<sp />data.<ref kindref="member" refid="structmtb__bmi160__data__t_1ad5ac1a5381c64919a50c422822b7b283">accel</ref>.y,<sp />data.<ref kindref="member" refid="structmtb__bmi160__data__t_1ad5ac1a5381c64919a50c422822b7b283">accel</ref>.z);</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp />printf(</highlight><highlight class="stringliteral">"Gyro<sp />:<sp />X:%6d<sp />Y:%6d<sp />Z:%6d\r\n\r\n"</highlight><highlight class="normal">,<sp />data.<ref kindref="member" refid="structmtb__bmi160__data__t_1a48802d2731e2f1f3722181451f4fafc7">gyro</ref>.x,<sp />data.<ref kindref="member" refid="structmtb__bmi160__data__t_1a48802d2731e2f1f3722181451f4fafc7">gyro</ref>.y,<sp />data.<ref kindref="member" refid="structmtb__bmi160__data__t_1a48802d2731e2f1f3722181451f4fafc7">gyro</ref>.z);</highlight></codeline>
<codeline><highlight class="normal" /></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp />cyhal_system_delay_ms(1000);</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp />}</highlight></codeline>
<codeline><highlight class="normal">}</highlight></codeline>
</programlisting></para></listitem><listitem><para>Build the application and program the kit.</para></listitem></orderedlist>
</para><para><bold>More information</bold>
</para><para><itemizedlist>
<listitem><para><ulink url="https://cypresssemiconductorco.github.io/sensor-motion-bmi160/html/index.html">API Reference Guide</ulink></para></listitem><listitem><para><ulink url="http://www.cypress.com">Cypress Semiconductor, an Infineon Technologies Company</ulink></para></listitem><listitem><para><ulink url="https://github.com/cypresssemiconductorco">Cypress Semiconductor GitHub</ulink></para></listitem><listitem><para><ulink url="https://www.cypress.com/products/modustoolbox-software-environment">ModusToolbox</ulink></para></listitem><listitem><para><ulink url="https://github.com/cypresssemiconductorco/Code-Examples-for-ModusToolbox-Software">PSoC 6 Code Examples using ModusToolbox IDE</ulink></para></listitem><listitem><para><ulink url="https://github.com/cypresssemiconductorco/psoc6-middleware">PSoC 6 Middleware</ulink></para></listitem><listitem><para><ulink url="https://community.cypress.com/docs/DOC-14644">PSoC 6 Resources - KBA223067</ulink> <hruler />
 © Cypress Semiconductor Corporation, 2019-2020. </para></listitem></itemizedlist>
</para>    </detaileddescription>
  </compounddef>
</doxygen>