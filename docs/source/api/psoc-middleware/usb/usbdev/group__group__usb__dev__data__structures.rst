================
Data Structures
================

.. toctree::

   group__group__usb__dev__structures__device.rst
   group__group__usb__dev__structures__device__descr.rst
   group__group__usb__dev__structures__control.rst
   group__group__usb__dev__structures__class.rst
   group__group__usb__dev__structures__func__ptr.rst

.. doxygengroup:: group_usb_dev_data_structures
   :project: usbdev


