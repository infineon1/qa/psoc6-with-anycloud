==========
HID Class
==========

.. doxygengroup:: group_usb_dev_hid
   :project: usbdev


.. toctree::

   group__group__usb__dev__hid__macros.rst
   group__group__usb__dev__hid__functions.rst
   group__group__usb__dev__hid__data__structures.rst
   group__group__usb__dev__hid__enums.rst