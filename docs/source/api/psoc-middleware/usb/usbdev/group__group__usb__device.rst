========
Device
========

.. doxygengroup:: group_usb_device
   :project: usbdev


.. toctree::

   group__group__usb__dev__macros.rst
   group__group__usb__dev__functions.rst
   group__group__usb__dev__data__structures.rst
   group__group__usb__dev__enums.rst