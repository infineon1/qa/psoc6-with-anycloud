<doxygen xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" version="1.8.13" xsi:noNamespaceSchemaLocation="compound.xsd">
  <compounddef id="group__group__usb__dev__functions__common" kind="group">
    <compoundname>group_usb_dev_functions_common</compoundname>
    <title>Initialization Functions</title>
      <sectiondef kind="func">
      <memberdef const="no" explicit="no" id="group__group__usb__dev__functions__common_1ga12b36659b72aa85166e85323eb96e36a" inline="no" kind="function" prot="public" static="no" virt="non-virtual">
        <type><ref kindref="member" refid="group__group__usb__dev__enums_1ga143c13515bfded27b9dc5ebc18d4cdaf">cy_en_usb_dev_status_t</ref></type>
        <definition>cy_en_usb_dev_status_t Cy_USB_Dev_Init</definition>
        <argsstring>(USBFS_Type *base, struct cy_stc_usbfs_dev_drv_config const *drvConfig, struct cy_stc_usbfs_dev_drv_context *drvContext, cy_stc_usb_dev_device_t const *device, cy_stc_usb_dev_config_t const *config, cy_stc_usb_dev_context_t *context)</argsstring>
        <name>Cy_USB_Dev_Init</name>
        <param>
          <type>USBFS_Type *</type>
          <declname>base</declname>
        </param>
        <param>
          <type>struct cy_stc_usbfs_dev_drv_config const *</type>
          <declname>drvConfig</declname>
        </param>
        <param>
          <type>struct cy_stc_usbfs_dev_drv_context *</type>
          <declname>drvContext</declname>
        </param>
        <param>
          <type><ref kindref="compound" refid="structcy__stc__usb__dev__device__t">cy_stc_usb_dev_device_t</ref> const *</type>
          <declname>device</declname>
        </param>
        <param>
          <type><ref kindref="compound" refid="structcy__stc__usb__dev__config__t">cy_stc_usb_dev_config_t</ref> const *</type>
          <declname>config</declname>
        </param>
        <param>
          <type><ref kindref="compound" refid="structcy__stc__usb__dev__context__t">cy_stc_usb_dev_context_t</ref> *</type>
          <declname>context</declname>
        </param>
        <briefdescription>
<para>Initialize the USB Device stack and underneath USBFS hardware driver. </para>        </briefdescription>
        <detaileddescription>
<para><parameterlist kind="param"><parameteritem>
<parameternamelist>
<parametername>base</parametername>
</parameternamelist>
<parameterdescription>
<para>The pointer to the USBFS instance.</para></parameterdescription>
</parameteritem>
<parameteritem>
<parameternamelist>
<parametername>drvConfig</parametername>
</parameternamelist>
<parameterdescription>
<para>The pointer to the USBFS driver configuration structure.</para></parameterdescription>
</parameteritem>
<parameteritem>
<parameternamelist>
<parametername>drvContext</parametername>
</parameternamelist>
<parameterdescription>
<para>The pointer to the USBFS driver context structure allocated by the user. The structure is used during the USBFS driver operation for internal configuration and data retention. The user must not modify anything in this structure.</para></parameterdescription>
</parameteritem>
<parameteritem>
<parameternamelist>
<parametername>device</parametername>
</parameternamelist>
<parameterdescription>
<para>The pointer to the device structure <ref kindref="compound" refid="structcy__stc__usb__dev__device__t">cy_stc_usb_dev_device_t</ref>.</para></parameterdescription>
</parameteritem>
<parameteritem>
<parameternamelist>
<parametername>config</parametername>
</parameternamelist>
<parameterdescription>
<para>The pointer to the driver configuration structure <ref kindref="compound" refid="structcy__stc__usb__dev__config__t">cy_stc_usb_dev_config_t</ref>.</para></parameterdescription>
</parameteritem>
<parameteritem>
<parameternamelist>
<parametername>context</parametername>
</parameternamelist>
<parameterdescription>
<para>The pointer to the context structure <ref kindref="compound" refid="structcy__stc__usb__dev__context__t">cy_stc_usb_dev_context_t</ref> allocated by the user. The structure is used during the USB Device operation for internal configuration and data retention. The user must not modify anything in this structure.</para></parameterdescription>
</parameteritem>
</parameterlist>
<simplesect kind="return"><para>Status code of the function execution <ref kindref="member" refid="group__group__usb__dev__enums_1ga143c13515bfded27b9dc5ebc18d4cdaf">cy_en_usb_dev_status_t</ref>.</para></simplesect>
<verbatim>embed:rst 
.. raw:: html

  &lt;div class="admonition note"&gt;&lt;p class="admonition-title"&gt;note&lt;/p&gt;&lt;p&gt;&lt;p&gt;The configuration of USB clocks, pins, and interrupts is not handled by this function and must be done on the application level. &lt;/p&gt;&lt;/p&gt;&lt;/div&gt;</verbatim></para>        </detaileddescription>
        <inbodydescription>
        </inbodydescription>
        <location bodyend="239" bodyfile="/var/tmp/gitlab-runner1/builds/7e5c9300/0/repo/middleware-usbdev/cy_usb_dev.c" bodystart="177" column="1" file="/var/tmp/gitlab-runner1/builds/7e5c9300/0/repo/middleware-usbdev/cy_usb_dev.h" line="1259" />
      </memberdef>
      <memberdef const="no" explicit="no" id="group__group__usb__dev__functions__common_1gad3b25c869fcca5706d0ebfe135df0783" inline="no" kind="function" prot="public" static="no" virt="non-virtual">
        <type>void</type>
        <definition>void Cy_USB_Dev_DeInit</definition>
        <argsstring>(cy_stc_usb_dev_context_t *context)</argsstring>
        <name>Cy_USB_Dev_DeInit</name>
        <param>
          <type><ref kindref="compound" refid="structcy__stc__usb__dev__context__t">cy_stc_usb_dev_context_t</ref> *</type>
          <declname>context</declname>
        </param>
        <briefdescription>
<para>De-initialize the USB Device stack and underneath hardware driver. </para>        </briefdescription>
        <detaileddescription>
<para><parameterlist kind="param"><parameteritem>
<parameternamelist>
<parametername>context</parametername>
</parameternamelist>
<parameterdescription>
<para>The pointer to the context structure <ref kindref="compound" refid="structcy__stc__usb__dev__context__t">cy_stc_usb_dev_context_t</ref> allocated by the user. The structure is used during the USB Device operation for internal configuration and data retention. The user must not modify anything in this structure. </para></parameterdescription>
</parameteritem>
</parameterlist>
</para>        </detaileddescription>
        <inbodydescription>
        </inbodydescription>
        <location bodyend="265" bodyfile="/var/tmp/gitlab-runner1/builds/7e5c9300/0/repo/middleware-usbdev/cy_usb_dev.c" bodystart="255" column="1" file="/var/tmp/gitlab-runner1/builds/7e5c9300/0/repo/middleware-usbdev/cy_usb_dev.h" line="1266" />
      </memberdef>
      <memberdef const="no" explicit="no" id="group__group__usb__dev__functions__common_1ga368f2e081a58b7eef81a17c4781fca8e" inline="no" kind="function" prot="public" static="no" virt="non-virtual">
        <type><ref kindref="member" refid="group__group__usb__dev__enums_1ga143c13515bfded27b9dc5ebc18d4cdaf">cy_en_usb_dev_status_t</ref></type>
        <definition>cy_en_usb_dev_status_t Cy_USB_Dev_Connect</definition>
        <argsstring>(bool blocking, int32_t timeout, cy_stc_usb_dev_context_t *context)</argsstring>
        <name>Cy_USB_Dev_Connect</name>
        <param>
          <type>bool</type>
          <declname>blocking</declname>
        </param>
        <param>
          <type>int32_t</type>
          <declname>timeout</declname>
        </param>
        <param>
          <type><ref kindref="compound" refid="structcy__stc__usb__dev__context__t">cy_stc_usb_dev_context_t</ref> *</type>
          <declname>context</declname>
        </param>
        <briefdescription>
<para>Enables pull-up on D+ (hardware supports only full-speed device) line to signal USB Device connection on USB Bus. </para>        </briefdescription>
        <detaileddescription>
<para><parameterlist kind="param"><parameteritem>
<parameternamelist>
<parametername>blocking</parametername>
</parameternamelist>
<parameterdescription>
<para>Wait until device is configured.</para></parameterdescription>
</parameteritem>
<parameteritem>
<parameternamelist>
<parametername>timeout</parametername>
</parameternamelist>
<parameterdescription>
<para>Defines in milliseconds the time for which this function can block. If that time expires, the USB Device is disconnected and the function returns. To wait forever, pass <ref kindref="member" refid="group__group__usb__dev__macros_1gad8a978b86bb53dd8e46d9f9db9399ee8">CY_USB_DEV_WAIT_FOREVER</ref>.</para></parameterdescription>
</parameteritem>
<parameteritem>
<parameternamelist>
<parametername>context</parametername>
</parameternamelist>
<parameterdescription>
<para>The pointer to the context structure <ref kindref="compound" refid="structcy__stc__usb__dev__context__t">cy_stc_usb_dev_context_t</ref> allocated by the user. The structure is used during the USB Device operation for internal configuration and data retention. The user must not modify anything in this structure. </para></parameterdescription>
</parameteritem>
</parameterlist>
</para>        </detaileddescription>
        <inbodydescription>
        </inbodydescription>
        <location bodyend="348" bodyfile="/var/tmp/gitlab-runner1/builds/7e5c9300/0/repo/middleware-usbdev/cy_usb_dev.c" bodystart="312" column="1" file="/var/tmp/gitlab-runner1/builds/7e5c9300/0/repo/middleware-usbdev/cy_usb_dev.h" line="1267" />
      </memberdef>
      <memberdef const="no" explicit="no" id="group__group__usb__dev__functions__common_1ga15cde7caa00a43d253198ea70935e47c" inline="no" kind="function" prot="public" static="no" virt="non-virtual">
        <type>void</type>
        <definition>void Cy_USB_Dev_Disconnect</definition>
        <argsstring>(cy_stc_usb_dev_context_t *context)</argsstring>
        <name>Cy_USB_Dev_Disconnect</name>
        <param>
          <type><ref kindref="compound" refid="structcy__stc__usb__dev__context__t">cy_stc_usb_dev_context_t</ref> *</type>
          <declname>context</declname>
        </param>
        <briefdescription>
<para>Disables pull-up on D+ (hardware supports only full-speed device) line to signal USB Device disconnection on USB Bus. </para>        </briefdescription>
        <detaileddescription>
<para><parameterlist kind="param"><parameteritem>
<parameternamelist>
<parametername>context</parametername>
</parameternamelist>
<parameterdescription>
<para>The pointer to the context structure <ref kindref="compound" refid="structcy__stc__usb__dev__context__t">cy_stc_usb_dev_context_t</ref> allocated by the user. The structure is used during the USB Device operation for internal configuration and data retention. The user must not modify anything in this structure. </para></parameterdescription>
</parameteritem>
</parameterlist>
</para>        </detaileddescription>
        <inbodydescription>
        </inbodydescription>
        <location bodyend="372" bodyfile="/var/tmp/gitlab-runner1/builds/7e5c9300/0/repo/middleware-usbdev/cy_usb_dev.c" bodystart="365" column="1" file="/var/tmp/gitlab-runner1/builds/7e5c9300/0/repo/middleware-usbdev/cy_usb_dev.h" line="1269" />
      </memberdef>
      </sectiondef>
    <briefdescription>
    </briefdescription>
    <detaileddescription>
    </detaileddescription>
  </compounddef>
</doxygen>