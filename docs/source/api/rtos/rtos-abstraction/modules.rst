==============
API Reference
==============

.. toctree::
   :hidden:

   group__group__abstraction__rtos__common.rst
   group__group__abstraction__rtos__mutex.rst
   group__group__abstraction__rtos__queue.rst
   group__group__abstraction__rtos__semaphore.rst
   group__group__abstraction__rtos__threads.rst
   group__group__abstraction__rtos__time.rst
   group__group__abstraction__rtos__timer.rst
   group__group__worker__thread__util.rst


The following provides a list of API documentation

+-----------------------------------+-------------------------------------+
| `Common <group__group__abstraction| General types and defines for       |
| __rtos__common.html>`__           | working with RTOS abstraction layer |
+-----------------------------------+-------------------------------------+
| `RTOS Specific Types and          | The following defines and types have|
| Defines <group__group__abstraction| values that are specific to each    |
| __rtos__port.html>`__             | RTOS port                           |
+-----------------------------------+-------------------------------------+
| `Mutex <group__group__abstraction_| APIs for acquiring and working with |
| _rtos__mutex.html>`__             | Mutexes                             |
+-----------------------------------+-------------------------------------+
| `Queue <group__group__abstraction_| APIs for creating and working with  |
| _rtos__queue.html>`__             | Queues                              |
+-----------------------------------+-------------------------------------+
| `Semaphore <group__group__abstract| APIs for acquiring and working with |
| ion__rtos__semaphore.html>`__     | Semaphores                          |
+-----------------------------------+-------------------------------------+
| `Threads <group__group__abstractio| APIs for creating and working with  |
| n__rtos__threads.html>`__         | Threads                             |
+-----------------------------------+-------------------------------------+
| `Time <group__group__abstraction__| APIs for getting the current time   | 
| rtos__time.html>`__               | and waiting                         |
+-----------------------------------+-------------------------------------+
| `Timer <group__group__abstraction_| APIs for creating and working with  |
| _rtos__timer.html>`__             | Timers                              |
+-----------------------------------+-------------------------------------+
| `Worker Thread Utility <group__gro| Worker thread utility that allows   |
| up__worker__thread__util.html>`__ | functions to be run a different     |
|                                   | thread context                      |
+-----------------------------------+-------------------------------------+



