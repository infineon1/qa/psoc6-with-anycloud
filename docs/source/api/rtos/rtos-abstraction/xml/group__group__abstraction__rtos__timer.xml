<doxygen xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" version="1.8.14" xsi:noNamespaceSchemaLocation="compound.xsd">
  <compounddef id="group__group__abstraction__rtos__timer" kind="group">
    <compoundname>group_abstraction_rtos_timer</compoundname>
    <title>Timer</title>
      <sectiondef kind="user-defined">
      <memberdef const="no" explicit="no" id="group__group__abstraction__rtos__timer_1ga0fa10fe9a8589d185f27e238ee375bc8" inline="no" kind="function" prot="public" static="no" virt="non-virtual">
        <type>cy_rslt_t</type>
        <definition>cy_rslt_t cy_rtos_init_timer</definition>
        <argsstring>(cy_timer_t *timer, cy_timer_trigger_type_t type, cy_timer_callback_t fun, cy_timer_callback_arg_t arg)</argsstring>
        <name>cy_rtos_init_timer</name>
        <param>
          <type><ref kindref="member" refid="group__group__abstraction__rtos__port_1gacf9d99e8262de333b482417053756624">cy_timer_t</ref> *</type>
          <declname>timer</declname>
        </param>
        <param>
          <type><ref kindref="member" refid="group__group__abstraction__rtos__timer_1ga0606aecc2c90e9315f7da5e3daa3ffc1">cy_timer_trigger_type_t</ref></type>
          <declname>type</declname>
        </param>
        <param>
          <type><ref kindref="member" refid="group__group__abstraction__rtos__timer_1ga92b29af1c848137e18dec1ae1cb5ffec">cy_timer_callback_t</ref></type>
          <declname>fun</declname>
        </param>
        <param>
          <type><ref kindref="member" refid="group__group__abstraction__rtos__port_1gaae8f42a65f808106d77113794ab7dd30">cy_timer_callback_arg_t</ref></type>
          <declname>arg</declname>
        </param>
        <briefdescription>
<para>Create a new timer. </para>        </briefdescription>
        <detaileddescription>
<para>This function initializes a timer object. <verbatim>embed:rst 
.. raw:: html

  &lt;div class="admonition note"&gt;&lt;p class="admonition-title"&gt;note&lt;/p&gt;&lt;p&gt;&lt;p&gt;The timer is not active until start is called. &lt;/p&gt;&lt;/p&gt;&lt;/div&gt;</verbatim><verbatim>embed:rst 
.. raw:: html

  &lt;div class="admonition note"&gt;&lt;p class="admonition-title"&gt;note&lt;/p&gt;&lt;p&gt;&lt;p&gt;The callback may be (likely will be) called from a different thread.&lt;/p&gt;&lt;/p&gt;&lt;/div&gt;</verbatim><parameterlist kind="param"><parameteritem>
<parameternamelist>
<parametername direction="out">timer</parametername>
</parameternamelist>
<parameterdescription>
<para>Pointer to the timer handle to initialize </para></parameterdescription>
</parameteritem>
<parameteritem>
<parameternamelist>
<parametername direction="in">type</parametername>
</parameternamelist>
<parameterdescription>
<para>Type of timer (periodic or once) </para></parameterdescription>
</parameteritem>
<parameteritem>
<parameternamelist>
<parametername direction="in">fun</parametername>
</parameternamelist>
<parameterdescription>
<para>The function </para></parameterdescription>
</parameteritem>
<parameteritem>
<parameternamelist>
<parametername direction="in">arg</parametername>
</parameternamelist>
<parameterdescription>
<para>Argument to pass along to the callback function</para></parameterdescription>
</parameteritem>
</parameterlist>
<simplesect kind="return"><para>The status of the creation request. [<ref kindref="member" refid="group__group__abstraction__rtos__common_1gaf58fac450d9fff4472f03ad68f6e546e">CY_RSLT_SUCCESS</ref>, <ref kindref="member" refid="group__group__abstraction__rtos__common_1ga89a0a071bd7a41b5d1f3edb969ee1647">CY_RTOS_GENERAL_ERROR</ref>] </para></simplesect>
</para>        </detaileddescription>
        <inbodydescription>
        </inbodydescription>
        <location column="1" file="output/libs/COMPONENT_ABS_RTOS/abstraction-rtos/include/cyabs_rtos.h" line="635" />
      </memberdef>
      <memberdef const="no" explicit="no" id="group__group__abstraction__rtos__timer_1ga8ec7a094161e4af7b0765e4e89dd4f21" inline="no" kind="function" prot="public" static="no" virt="non-virtual">
        <type>cy_rslt_t</type>
        <definition>cy_rslt_t cy_rtos_start_timer</definition>
        <argsstring>(cy_timer_t *timer, cy_time_t num_ms)</argsstring>
        <name>cy_rtos_start_timer</name>
        <param>
          <type><ref kindref="member" refid="group__group__abstraction__rtos__port_1gacf9d99e8262de333b482417053756624">cy_timer_t</ref> *</type>
          <declname>timer</declname>
        </param>
        <param>
          <type><ref kindref="member" refid="group__group__abstraction__rtos__port_1ga141349b315f2266d5e0e8b8566eb608b">cy_time_t</ref></type>
          <declname>num_ms</declname>
        </param>
        <briefdescription>
<para>Start a timer. </para>        </briefdescription>
        <detaileddescription>
<para><parameterlist kind="param"><parameteritem>
<parameternamelist>
<parametername direction="in">timer</parametername>
</parameternamelist>
<parameterdescription>
<para>Pointer to the timer handle </para></parameterdescription>
</parameteritem>
<parameteritem>
<parameternamelist>
<parametername direction="in">num_ms</parametername>
</parameternamelist>
<parameterdescription>
<para>The number of milliseconds to wait before the timer fires</para></parameterdescription>
</parameteritem>
</parameterlist>
<simplesect kind="return"><para>The status of the creation request. [<ref kindref="member" refid="group__group__abstraction__rtos__common_1gaf58fac450d9fff4472f03ad68f6e546e">CY_RSLT_SUCCESS</ref>, <ref kindref="member" refid="group__group__abstraction__rtos__common_1ga89a0a071bd7a41b5d1f3edb969ee1647">CY_RTOS_GENERAL_ERROR</ref>] </para></simplesect>
</para>        </detaileddescription>
        <inbodydescription>
        </inbodydescription>
        <location column="1" file="output/libs/COMPONENT_ABS_RTOS/abstraction-rtos/include/cyabs_rtos.h" line="645" />
      </memberdef>
      <memberdef const="no" explicit="no" id="group__group__abstraction__rtos__timer_1ga45f1cfa24d69049bd66ee8c41e089d01" inline="no" kind="function" prot="public" static="no" virt="non-virtual">
        <type>cy_rslt_t</type>
        <definition>cy_rslt_t cy_rtos_stop_timer</definition>
        <argsstring>(cy_timer_t *timer)</argsstring>
        <name>cy_rtos_stop_timer</name>
        <param>
          <type><ref kindref="member" refid="group__group__abstraction__rtos__port_1gacf9d99e8262de333b482417053756624">cy_timer_t</ref> *</type>
          <declname>timer</declname>
        </param>
        <briefdescription>
<para>Stop a timer. </para>        </briefdescription>
        <detaileddescription>
<para><parameterlist kind="param"><parameteritem>
<parameternamelist>
<parametername direction="in">timer</parametername>
</parameternamelist>
<parameterdescription>
<para>Pointer to the timer handle</para></parameterdescription>
</parameteritem>
</parameterlist>
<simplesect kind="return"><para>The status of the creation request. [<ref kindref="member" refid="group__group__abstraction__rtos__common_1gaf58fac450d9fff4472f03ad68f6e546e">CY_RSLT_SUCCESS</ref>, <ref kindref="member" refid="group__group__abstraction__rtos__common_1ga89a0a071bd7a41b5d1f3edb969ee1647">CY_RTOS_GENERAL_ERROR</ref>] </para></simplesect>
</para>        </detaileddescription>
        <inbodydescription>
        </inbodydescription>
        <location column="1" file="output/libs/COMPONENT_ABS_RTOS/abstraction-rtos/include/cyabs_rtos.h" line="653" />
      </memberdef>
      <memberdef const="no" explicit="no" id="group__group__abstraction__rtos__timer_1ga54237d816523d346c34ed6c1ff3882fe" inline="no" kind="function" prot="public" static="no" virt="non-virtual">
        <type>cy_rslt_t</type>
        <definition>cy_rslt_t cy_rtos_is_running_timer</definition>
        <argsstring>(cy_timer_t *timer, bool *state)</argsstring>
        <name>cy_rtos_is_running_timer</name>
        <param>
          <type><ref kindref="member" refid="group__group__abstraction__rtos__port_1gacf9d99e8262de333b482417053756624">cy_timer_t</ref> *</type>
          <declname>timer</declname>
        </param>
        <param>
          <type>bool *</type>
          <declname>state</declname>
        </param>
        <briefdescription>
<para>Returns state of a timer. </para>        </briefdescription>
        <detaileddescription>
<para><parameterlist kind="param"><parameteritem>
<parameternamelist>
<parametername direction="in">timer</parametername>
</parameternamelist>
<parameterdescription>
<para>Pointer to the timer handle </para></parameterdescription>
</parameteritem>
<parameteritem>
<parameternamelist>
<parametername direction="out">state</parametername>
</parameternamelist>
<parameterdescription>
<para>Return value for state, true if running, false otherwise</para></parameterdescription>
</parameteritem>
</parameterlist>
<simplesect kind="return"><para>The status of the creation request. [<ref kindref="member" refid="group__group__abstraction__rtos__common_1gaf58fac450d9fff4472f03ad68f6e546e">CY_RSLT_SUCCESS</ref>, <ref kindref="member" refid="group__group__abstraction__rtos__common_1ga89a0a071bd7a41b5d1f3edb969ee1647">CY_RTOS_GENERAL_ERROR</ref>] </para></simplesect>
</para>        </detaileddescription>
        <inbodydescription>
        </inbodydescription>
        <location column="1" file="output/libs/COMPONENT_ABS_RTOS/abstraction-rtos/include/cyabs_rtos.h" line="662" />
      </memberdef>
      <memberdef const="no" explicit="no" id="group__group__abstraction__rtos__timer_1ga050a503bed20e53055e20aec0c7a4d3e" inline="no" kind="function" prot="public" static="no" virt="non-virtual">
        <type>cy_rslt_t</type>
        <definition>cy_rslt_t cy_rtos_deinit_timer</definition>
        <argsstring>(cy_timer_t *timer)</argsstring>
        <name>cy_rtos_deinit_timer</name>
        <param>
          <type><ref kindref="member" refid="group__group__abstraction__rtos__port_1gacf9d99e8262de333b482417053756624">cy_timer_t</ref> *</type>
          <declname>timer</declname>
        </param>
        <briefdescription>
<para>Deinit the timer. </para>        </briefdescription>
        <detaileddescription>
<para>This function deinitializes the timer and frees all consumed resources.</para><para><parameterlist kind="param"><parameteritem>
<parameternamelist>
<parametername direction="in">timer</parametername>
</parameternamelist>
<parameterdescription>
<para>Pointer to the timer handle</para></parameterdescription>
</parameteritem>
</parameterlist>
<simplesect kind="return"><para>The status of the creation request. [<ref kindref="member" refid="group__group__abstraction__rtos__common_1gaf58fac450d9fff4472f03ad68f6e546e">CY_RSLT_SUCCESS</ref>, <ref kindref="member" refid="group__group__abstraction__rtos__common_1ga89a0a071bd7a41b5d1f3edb969ee1647">CY_RTOS_GENERAL_ERROR</ref>] </para></simplesect>
</para>        </detaileddescription>
        <inbodydescription>
        </inbodydescription>
        <location column="1" file="output/libs/COMPONENT_ABS_RTOS/abstraction-rtos/include/cyabs_rtos.h" line="672" />
      </memberdef>
      </sectiondef>
      <sectiondef kind="enum">
      <memberdef id="group__group__abstraction__rtos__timer_1ga0606aecc2c90e9315f7da5e3daa3ffc1" kind="enum" prot="public" static="no" strong="no">
        <type />
        <name>cy_timer_trigger_type_t</name>
        <enumvalue id="group__group__abstraction__rtos__timer_1gga0606aecc2c90e9315f7da5e3daa3ffc1af7a191ed0c1879be4d0840a166299839" prot="public">
          <name>CY_TIMER_TYPE_PERIODIC</name>
          <briefdescription>
<para>called periodically until stopped </para>          </briefdescription>
          <detaileddescription>
          </detaileddescription>
        </enumvalue>
        <enumvalue id="group__group__abstraction__rtos__timer_1gga0606aecc2c90e9315f7da5e3daa3ffc1af554aa26a56ebee9da223be7c1f9d0e1" prot="public">
          <name>CY_TIMER_TYPE_ONCE</name>
          <briefdescription>
<para>called once only </para>          </briefdescription>
          <detaileddescription>
          </detaileddescription>
        </enumvalue>
        <enumvalue id="group__group__abstraction__rtos__timer_1gga0606aecc2c90e9315f7da5e3daa3ffc1ace817780efe3765b840bc941c1c7fd64" prot="public">
          <name>cy_timer_type_periodic</name>
          <initializer>= CY_TIMER_TYPE_PERIODIC</initializer>
          <briefdescription>
          </briefdescription>
          <detaileddescription>
<para><xrefsect id="deprecated_1_deprecated000001"><xreftitle>Deprecated</xreftitle><xrefdescription><para>replaced by <ref kindref="member" refid="group__group__abstraction__rtos__timer_1gga0606aecc2c90e9315f7da5e3daa3ffc1af7a191ed0c1879be4d0840a166299839">CY_TIMER_TYPE_PERIODIC</ref> </para></xrefdescription></xrefsect></para>          </detaileddescription>
        </enumvalue>
        <enumvalue id="group__group__abstraction__rtos__timer_1gga0606aecc2c90e9315f7da5e3daa3ffc1a184291ed50b432a85713085e3230fb69" prot="public">
          <name>cy_timer_type_once</name>
          <initializer>= CY_TIMER_TYPE_ONCE</initializer>
          <briefdescription>
          </briefdescription>
          <detaileddescription>
<para><xrefsect id="deprecated_1_deprecated000002"><xreftitle>Deprecated</xreftitle><xrefdescription><para>replaced by <ref kindref="member" refid="group__group__abstraction__rtos__timer_1gga0606aecc2c90e9315f7da5e3daa3ffc1af554aa26a56ebee9da223be7c1f9d0e1">CY_TIMER_TYPE_ONCE</ref> </para></xrefdescription></xrefsect></para>          </detaileddescription>
        </enumvalue>
        <briefdescription>
<para><bold>cy_timer_trigger_type_t: </bold><linebreak />The type of timer.</para></briefdescription>
        <detaileddescription>
        </detaileddescription>
        <inbodydescription>
        </inbodydescription>
        <location bodyend="135" bodyfile="output/libs/COMPONENT_ABS_RTOS/abstraction-rtos/include/cyabs_rtos.h" bodystart="129" column="1" file="output/libs/COMPONENT_ABS_RTOS/abstraction-rtos/include/cyabs_rtos.h" line="130" />
      </memberdef>
      </sectiondef>
      <sectiondef kind="typedef">
      <memberdef id="group__group__abstraction__rtos__timer_1ga92b29af1c848137e18dec1ae1cb5ffec" kind="typedef" prot="public" static="no">
        <type>void(*</type>
        <definition>typedef void(* cy_timer_callback_t) (cy_timer_callback_arg_t arg)</definition>
        <argsstring>)(cy_timer_callback_arg_t arg)</argsstring>
        <name>cy_timer_callback_t</name>
        <briefdescription>
<para>The callback function to be called by a timer. </para>        </briefdescription>
        <detaileddescription>
        </detaileddescription>
        <inbodydescription>
        </inbodydescription>
        <location bodyend="-1" bodyfile="output/libs/COMPONENT_ABS_RTOS/abstraction-rtos/include/cyabs_rtos.h" bodystart="151" column="1" file="output/libs/COMPONENT_ABS_RTOS/abstraction-rtos/include/cyabs_rtos.h" line="151" />
      </memberdef>
      </sectiondef>
    <briefdescription>
<para>APIs for creating and working with Timers. </para>    </briefdescription>
    <detaileddescription>
    </detaileddescription>
  </compounddef>
</doxygen>