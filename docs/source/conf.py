# Configuration file for the Sphinx documentation builder.
#
# This file only contains a selection of the most common options. For a full
# list see the documentation:
# https://www.sphinx-doc.org/en/master/usage/configuration.html

# -- Path setup --------------------------------------------------------------

# If extensions (or modules to document with autodoc) are in another directory,
# add these directories to sys.path here. If the directory is relative to the
# documentation root, use os.path.abspath to make it absolute, like shown here.
#
import os
# import sys
# sys.path.insert(0, os.path.abspath('.'))
import xml.etree.ElementTree as ET
from os import path
import json



# -- Project information -----------------------------------------------------

project = ' PSoC 6 with AnyCloud'
copyright = '2020 Infineon Technologies AG'
author = 'Infineon'


# -- General configuration ---------------------------------------------------

# Add any Sphinx extension module names here, as strings. They can be
# extensions coming with Sphinx (named 'sphinx.ext.*') or your custom
# ones.
extensions = ['breathe']

# Add any paths that contain templates here, relative to this directory.
templates_path = ['_templates']

# List of patterns, relative to source directory, that match files and
# directories to ignore when looking for source files.
# This pattern also affects html_static_path and html_extra_path.
exclude_patterns = []


# -- Options for HTML output -------------------------------------------------

# The theme to use for HTML and HTML Help pages.  See the documentation for
# a list of builtin themes.
#
html_theme = 'sphinx_rtd_theme'

breathe_projects = {'lpa':'feature-guide/lpd/lpa/xml/','ota':'feature-guide/iot/firmware/ota/xml/','clib-support':'api/rtos/freertos/clib-support/xml/','corelib':'api/psoc-base-lib/corelib/xml/',
'hal':'api/psoc-base-lib/hal/xml/','pdl':'api/psoc-base-lib/pdl/xml/','stdio':'api/psoc-base-lib/stdio/xml/','bless':'api/psoc-middleware/ble/BLESS/xml/',
'WICED_Bluetooth_Host_Stack-dual_mode':'api/psoc-middleware/ble/WICED_Bluetooth_Host_Stack/dual_mode/xml/','WICED_Bluetooth_Host_Stack-ble':'api/psoc-middleware/ble/WICED_Bluetooth_Host_Stack/ble/xml/',
'emwin':'api/psoc-middleware/display/emwin/xml/','rgb-led':'api/psoc-middleware/display/rgb-led/xml/','usbdev':'api/psoc-middleware/usb/usbdev/xml/','dfu':'api/psoc-middleware/memory/dfu/xml/',
'eeprom':'api/psoc-middleware/memory/eeprom/xml/','serial-flash':'api/psoc-middleware/memory/serial-flash/xml/','capsense':'api/psoc-middleware/sensing/capsense/CapSense Middleware/xml/',
'csdadc':'api/psoc-middleware/sensing/csdadc/xml/','csdidac':'api/psoc-middleware/sensing/csdidac/xml/','light-sensor':'api/psoc-middleware/sensing/light-sensor/xml/','thermistor':'api/psoc-middleware/sensing/thermistor/xml/',
'CY8CKIT-028-EPD':'api/psoc-middleware/kits-support/CY8CKIT-028-EPD/xml/','display-eink-e2271cs021':'api/psoc-middleware/kits-support/CY8CKIT-028-EPD/display-eink-e2271cs021/xml/',
'CY8CKIT-028-TFT':'api/psoc-middleware/kits-support/CY8CKIT-028-TFT/xml/','audio-codec-ak4954a':'api/psoc-middleware/kits-support/CY8CKIT-028-TFT/audio-codec-ak4954a/xml/',
'display-tft-st7789v':'api/psoc-middleware/kits-support/CY8CKIT-028-TFT/display-tft-st7789v/xml/','sensor-light':'api/psoc-middleware/kits-support/CY8CKIT-028-TFT/sensor-light/xml/',
'lpa-api':'api/iot-middleware/lpa/xml/', 'ota-api':'api/iot-middleware/ota/xml/','sensor-atmo-bme680':'api/psoc-middleware/sensing/sensor-atmo-bme680/xml/','sensor-motion-bmi160':'api/psoc-middleware/sensing/sensor-motion-bmi160/xml/',
'CY8CKIT-032':'api/psoc-middleware/kits-support/CY8CKIT-032/xml/','http-server':'api/iot-middleware/http-server/xml/','MQTT':'api/iot-middleware/MQTT/xml/','secure-sockets':'api/iot-middleware/secure-sockets/xml/',
'udb-sdio-whd':'api/iot-middleware/udb-sdio-whd/xml/','wifi-base':'api/iot-middleware/wifi-base/xml/','wifi-connection-manager':'api/iot-middleware/wifi-connection-manager/xml/',
'TARGET_PSOC6-GENERIC':'Hardware_Reference/TARGET_PSOC6-GENERIC/xml','TARGET_CY8C4548AZI-S485':'Hardware_Reference/Supported_Boards/TARGET_CY8C4548AZI-S485/xml/',
'TARGET_CY8CKIT-041-40XX':'Hardware_Reference/Supported_Boards/TARGET_CY8CKIT-041-40XX/xml/','TARGET_CY8CKIT-041-41XX':'Hardware_Reference/Supported_Boards/TARGET_CY8CKIT-041-41XX/xml/',
'TARGET_CY8CKIT-062-BLE':'Hardware_Reference/Supported_Boards/TARGET_CY8CKIT-062-BLE/xml/','TARGET_CY8CKIT-062-WIFI-BT':'Hardware_Reference/Supported_Boards/TARGET_CY8CKIT-062-WIFI-BT/xml',
'TARGET_CY8CKIT-062S2-43012':'Hardware_Reference/Supported_Boards/TARGET_CY8CKIT-062S2-43012/xml/','TARGET_CY8CKIT-062S4':'Hardware_Reference/Supported_Boards/TARGET_CY8CKIT-062S4/xml/',
'TARGET_CY8CKIT-064B0S2-4343W':'Hardware_Reference/Supported_Boards/TARGET_CY8CKIT-064B0S2-4343W/xml/','TARGET_CY8CKIT-064S0S2-4343W':'Hardware_Reference/Supported_Boards/TARGET_CY8CKIT-064S0S2-4343W/xml/',
'TARGET_CY8CKIT-145-40XX':'Hardware_Reference/Supported_Boards/TARGET_CY8CKIT-145-40XX/xml/','TARGET_CY8CKIT-149':'Hardware_Reference/Supported_Boards/TARGET_CY8CKIT-149/xml/',
'TARGET_CY8CPROTO-062-4343W':'Hardware_Reference/Supported_Boards/TARGET_CY8CPROTO-062-4343W/xml/','TARGET_CY8CPROTO-062S3-4343W':'Hardware_Reference/Supported_Boards/TARGET_CY8CPROTO-062S3-4343W/xml/',
'TARGET_CY8CPROTO-063-BLE':'Hardware_Reference/Supported_Boards/TARGET_CY8CPROTO-063-BLE/xml/','TARGET_CY8CPROTO-064B0S1-BLE':'Hardware_Reference/Supported_Boards/TARGET_CY8CPROTO-064B0S1-BLE/xml/',
'TARGET_CY8CPROTO-064B0S3':'Hardware_Reference/Supported_Boards/TARGET_CY8CPROTO-064B0S3/xml/','TARGET_CY8CPROTO-064S1-SB':'Hardware_Reference/Supported_Boards/TARGET_CY8CPROTO-064S1-SB/xml/',
'TARGET_CYBLE-416045-EVAL':'Hardware_Reference/Supported_Boards/TARGET_CYBLE-416045-EVAL/xml/','TARGET_CYW9P62S1-43012EVB-01':'Hardware_Reference/Supported_Boards/TARGET_CYW9P62S1-43012EVB-01/xml/',
'TARGET_CYW9P62S1-43438EVB-01':'Hardware_Reference/Supported_Boards/TARGET_CYW9P62S1-43438EVB-01/xml/','whd-bsp-integration':'Hardware_Reference/whd-bsp-integration/xml/',
'connectivity-utilities':'api/iot-middleware/connectivity-utilities/xml/','whd':'api/iot-middleware/whd/xml/','rtos':'api/rtos/rtos-abstraction/xml/'}

html_theme_options = {
    'display_version': False,
    'prev_next_buttons_location': 'bottom',
    # Toc options
    'collapse_navigation': False,
    'sticky_navigation': True,
    'navigation_depth': 6,
    'includehidden': True,
    'titles_only': False
}

breathe_default_project = 'capsense'

# Add any paths that contain custom static files (such as style sheets) here,
# relative to this directory. They are copied after the builtin static files,
# so a file named "default.css" will overwrite the builtin "default.css".
html_static_path = ['_static']
html_theme_path = ['_themes']

html_logo = '_static/image/IFX_LOGO_RGB.svg'

# The name of an image file (within the static path) to use as favicon of the
# docs.  This file should be a Windows icon file (.ico) being 16x16 or 32x32
# pixels large.
html_favicon = '_static/image/IFX_ICON.ico'

def setup(app):
#    app.add_css_file('css/custom.css')
    app.add_css_file('css/theme_overrides.css')
    app.add_js_file('js/custom.js')
    app.add_js_file('js/searchtools.js')
    app.add_js_file('js/hotjar.js')
    app.add_js_file('js/sajari.js')



htmlNamesForXmlString='{"row":"tr","entry":"td", "para":"p", "listitem":"li", "orderedlist":"ol", "itemizedlist":"ul", "ulink":"a", "ref":"a", "linebreak":"br", "bold":"b","emphasis":"i","image":"img"}'
htmlNamesForXml = json.loads(htmlNamesForXmlString)
totalDirectiveList=['seealso', 'attention', 'caution', 'danger', 'error', 'hint', 'important', 'note', 'tip', 'warning', 'admonition', 'pre']
for (k, filePath) in breathe_projects.items():
  files = os.listdir(filePath)
  for nameOfFile in files:
    if nameOfFile.endswith(".xml"):
#      print(filePath)
      fileName=filePath+nameOfFile
      tempfilename=filePath+'temp'+nameOfFile
#      print(fileName)
#      print(path.exists(fileName))
      ulinkArray=[]
      titleArray=[]
      if path.exists(fileName):
        with open(fileName, encoding='UTF-8') as f:
          tree = ET.parse(f)
          root = tree.getroot()
          
#          print(ET.tostring(root, encoding='utf8').decode('utf8'))
          for elem in root.getiterator():
            if ((elem.tag == 'compounddef' or elem.tag == 'compound') and elem.attrib['kind']!=None and elem.attrib['kind'] == 'page'):
              elem.attrib['kind'] = 'group'
            try:
                                
              if elem.tag == 'image':
                if elem.attrib['type']!= None and elem.attrib['type'] == 'latex':
                  elem.attrib.pop('type')
                  elem.attrib.pop('name')
                  elem.tag = elem.tag.replace(elem.tag,'removeimage')
                  
              if 'memberdef' == elem.tag and elem.attrib['kind'] == 'enum':
                nameElem = elem.find('name')
                briefDescElem = elem.find('briefdescription')
                if(briefDescElem!=None):
                  nameText = str(nameElem.text).strip()
                  paraElem = briefDescElem.find("para")
                  paraElemString = ET.tostring(paraElem, encoding='utf8').decode('utf8')
                  paraElemString = paraElemString.replace("<?xml version='1.0' encoding='utf8'?>","")
                  paraElemString = paraElemString.replace("<para>", "").replace("</para>", "").strip()
                  
                  if not paraElemString.startswith("<bold>"+nameText):
                    paraElemString = "<para><bold>"+nameText+": </bold><linebreak/>"+paraElemString+"</para>"
                  else:
                    paraElemString = "<para>"+paraElemString+"</para>"
                
                  briefDescElem.remove(paraElem)
                  briefDescElem.append(ET.fromstring(paraElemString))
              
              
#              if 'memberdef' == elem.tag and elem.attrib['kind'] == 'define':
#                nameElem = elem.find('name')
#                initElem = elem.find('initializer')
#                briefDescElem = elem.find('briefdescription')
#                if(briefDescElem!=None):
#                  nameText = str(nameElem.text).strip()
#                  paraElem = briefDescElem.find("para")
#                  paraElemString = ET.tostring(paraElem, encoding='utf8').decode('utf8')
#                  paraElemString = paraElemString.replace("<?xml version='1.0' encoding='utf8'?>","")
#                  paraElemString = paraElemString.replace("<para>", "").replace("</para>", "").strip()
                  
#                  if not paraElemString.startswith("<bold>"+nameText):
#                    initElemString =  ET.tostring(initElem, encoding='utf8').decode('utf8')
#                    initElemString = initElemString.replace("<?xml version='1.0' encoding='utf8'?>","")
#                    initElemString = initElemString.replace("<initializer>", "").replace("</initializer>", "").strip()
#                    paraElemString = "<para><bold>"+nameText+" "+initElemString+"</bold><linebreak/>"+paraElemString+"</para>"
#                  else:
#                    paraElemString = "<para>"+paraElemString+"</para>"
                
#                  print(paraElemString);
#                  briefDescElem.remove(paraElem)
#                  briefDescElem.append(ET.fromstring(paraElemString))

              if 'ulink' == elem.tag: 
                if elem.attrib['url']!= None:
#                  print(elem.attrib['url'])  
                  linkurl = elem.attrib["url"];
                  linkurlval = ''
                  if not (linkurl.startswith("http") or linkurl.startswith("www")):
                    if linkurl.endswith(".pdf") or linkurl.endswith(".txt") or linkurl.endswith(".py") or linkurl.endswith(".h"): 
                      linkurlvalarray = linkurl.split('/')
                      linkurlval = linkurlvalarray[len(linkurlvalarray)-1]
                      linkPath = filePath.replace("xml/", "")
                      linkPathArray = linkPath.split('/')
                      linkPath = "_static/file/"+linkPath+linkurlval
                      for cnt in linkPathArray:
                        if cnt!=None and len(cnt.strip())>0:
                          linkPath = "../"+linkPath
                      
                      
                      elem.attrib["url"] = linkPath
                  
                  ulinkArray.append('<ulink url="'+elem.attrib["url"]+'">')

              if ((elem.tag).startswith("sect") and not (elem.tag).startswith('section')): 
                if elem.attrib['id']!= None:
                  elem.attrib.pop('id')
                
                elemchild = elem.find("title")
                if (elem.tag=='sect1'):
                  elemchild = elem.find("title")
                  text=elemchild.text
                  elemchild.text=""
                  heading = ET.SubElement(elemchild,'heading')
                  heading.text = text
                  heading.set('level','1')
                  elemchild.tag = elemchild.tag.replace('title', 'para')
                  
                else:
                  text=elemchild.text
                  elemchild.text=""
                  heading = ET.SubElement(elemchild,'bold')
                  heading.text = text
                  elemchild.tag = elemchild.tag.replace('title', 'para')

                elem.tag = elem.tag.replace(elem.tag,'removesec')

            except AttributeError:
              pass
              
#          print("before write")
          tree.write(tempfilename, encoding="utf-8")

        if path.exists(tempfilename):
          roottext=''    
          with open(tempfilename, encoding='UTF-8') as file:
            tree = ET.parse(file)
            root = tree.getroot()
            
            xmlstr = ET.tostring(root, encoding='utf8').decode('utf8')
            roottext = xmlstr.replace('<removesec>','').replace('</removesec>','')
            roottext = roottext.replace('<removeimage>','').replace('</removeimage>','').replace('<removeimage />','').replace('<removeimage/>','')
            for ulink in ulinkArray: 
              roottext = roottext.replace(ulink+"<bold>",ulink)

            roottext = roottext.replace('</bold></ulink>','</ulink>').replace('</bold> </ulink>','</ulink>').replace('</bold>\n</ulink>','</ulink>')
            roottext = roottext.replace('<computeroutput>','').replace('</computeroutput>','')
            roottext = roottext.replace('<ndash/>','-')
#            roottext = roottext.replace('<para><heading level="1">','<title>', 1).replace('</heading></para>','</title>', 1)
#            roottext = roottext.replace('<parahruler>','<para>').replace('</parahruler>','</para><hruler/>')

#          print(roottext)
        
          tree = ET.ElementTree(ET.fromstring(roottext))
          root = tree.getroot()
          
          
          for headingElem in root.iter('heading'):
            headingVal = int(headingElem.attrib['level'])
            if headingVal>2:
              headingElem.tag = headingElem.tag.replace(headingElem.tag,'bold') 
              headingElem.attrib.pop('level')
          
          for simSctElem in root.iter('simplesect'):
            if len(simSctElem.items()) > 0 and simSctElem.attrib['kind']!=None and simSctElem.attrib['kind'] in totalDirectiveList:
              admDirVal = simSctElem.attrib['kind']
              simSctElem.attrib.pop('kind')
              for elemchild in simSctElem.iter():
                if elemchild.tag  in htmlNamesForXml:
                  elemchild.tag = elemchild.tag.replace(elemchild.tag,htmlNamesForXml[elemchild.tag]) 
#                  print(elemchild.items());    
                  for (attrname1, attval1) in elemchild.items():
                    if attrname1=='url':
                      elemchild.set('href',attval1)
                         
                    if attrname1=='refid': 
                      if path.exists(filePath+attval1+".xml"):
                        elemchild.set('href',attval1+'.html#')
                      else:
                        attval1.split('_')
                        x = attval1.split('_')
                        arr=[] 
                        count=0 
                        for val in x: 
                          if count<(len(x)-1):
                            arr.append(val) 
                          count=count+1 
                           
                        str1='_'.join(arr)
                        if path.exists(filePath+str1+".xml"):
                          elemchild.set('href',str1+'.html#'+attval1)
                        else:
                          elemchild.set('href','#'+attval1)
                        
                    elemchild.attrib.pop(attrname1)
                
                elif elemchild.tag=='programlisting':
                  elemchild.set('class','highlight-default notranslate')
                  elemchild.tag = elemchild.tag.replace(elemchild.tag,'div') 
                  
                elif elemchild.tag=='codeline':
                  elemchild.set('class','highlight')
                  elemchild.tag = elemchild.tag.replace(elemchild.tag,'div')

                elif elemchild.tag=='highlight':
                  elemchild.attrib.pop('class')
                  elemchild.tag = elemchild.tag.replace(elemchild.tag,'pre')
                  
                elif elemchild.tag=='sp':
                  elemchild.tag = elemchild.tag.replace(elemchild.tag,'span')
                    
              simplesectString =  ET.tostring(simSctElem, encoding='utf8').decode('utf8')
              simplesectString = simplesectString.replace("<?xml version='1.0' encoding='utf8'?>","")
              
              if admDirVal=='pre':
                simplesectString = simplesectString.replace('<simplesect>','<div class="admonition-precondition admonition"><p class="admonition-title"><span>Precondition</span></p><p>').replace('</simplesect>','</p></div>')
              else:
                simplesectString = simplesectString.replace('<simplesect>','<div class="admonition '+admDirVal+'"><p class="admonition-title">'+admDirVal+'</p><p>').replace('</simplesect>','</p></div>')
                
              simplesectString = simplesectString.replace('\n','')
              simplesectString = 'embed:rst \n.. raw:: html\n\n  '+simplesectString
           
              simSctElem.clear()
              simSctElem.text=simplesectString
              simSctElem.tag = simSctElem.tag.replace('simplesect','verbatim')
              
            elif len(simSctElem.items()) > 0 and simSctElem.attrib['kind']!=None:
              simTitVal = simSctElem.attrib['kind']
              if simTitVal=='par':
                for simParaElem in simSctElem.iter('para'):
                  simParaElemStr =  ET.tostring(simParaElem, encoding='utf8').decode('utf8')
                  simParaElemStr = simParaElemStr.replace("<?xml version='1.0' encoding='utf8'?>","") 
                  isParachanged=False
                  if '<heading level="1">' in simParaElemStr :
                    simParaElemStr = simParaElemStr.replace('<para><heading level="1">','').replace('</heading></para>','')
                    isParachanged=True
                  if '<para><bold>' in simParaElemStr :
                    simParaElemStr = simParaElemStr.replace('<para><bold>','').replace('</bold></para>','')
                    isParachanged=True
                    
                  if isParachanged :
                    simParaElem.tag = simParaElem.tag.replace('para','title') 
                    simParaElem.clear()
                    simParaElem.text=simParaElemStr
           
          for tableElem in root.iter('table'):
#            print(tableElem.items())
            for (attrname, attval) in tableElem.items():
#              print(attrname)
              tableElem.attrib.pop(attrname)
            for rowElem in tableElem.iter():
              if rowElem.tag  in htmlNamesForXml:
                rowElem.tag = rowElem.tag.replace(rowElem.tag,htmlNamesForXml[rowElem.tag]) 
                
                for (attrname1, attval1) in rowElem.items():
                  if attrname1=='thead' and attval1.lower()=='yes':
                    rowElem.set('class','head')
                        
                  if attrname1=='url':
                    rowElem.set('href',attval1)

                  if attrname1=='name' and rowElem.tag=='img':
                    imagePath = filePath.replace("xml/", "")
                    imagePathArray = imagePath.split('/')
                    imagePath = "_static/image/"+imagePath+attval1
                    for cnt in imagePathArray:
                      if cnt!= None and len(cnt.strip())>0:
                        imagePath = "../"+imagePath


                    rowElem.set('src',imagePath)
                    rowElem.set('alt',attval1)
                    rowElem.set('height','100%')
                    rowElem.text='helloimage'
                  
                  if attrname1=='refid': 
                    if path.exists(filePath+attval1+".xml"):
                        rowElem.set('href',attval1+'.html#')
                    else:
                      attval1.split('_')
                      x = attval1.split('_')
                      arr=[] 
                      count=0 
                      for val in x: 
                        if count<(len(x)-1):
                          arr.append(val) 
                        count=count+1 
                           
                      str1='_'.join(arr)
                      if path.exists(filePath+str1+".xml"):
                        rowElem.set('href',str1+'.html#'+attval1)
                      else:
                        rowElem.set('href','#'+attval1)
                      
                  if attrname1!='rowspan' and attrname1!='colspan': 
                    rowElem.attrib.pop(attrname1)
              
              elif rowElem.tag=='programlisting':
                rowElem.tag = rowElem.tag.replace(rowElem.tag,'div')
                rowElem.set('class','highlight-default notranslate')
                  
              elif rowElem.tag=='codeline':
                rowElem.tag = rowElem.tag.replace(rowElem.tag,'div')
                rowElem.set('class','highlight')

              elif rowElem.tag=='highlight':
                rowElem.attrib.pop('class')
                rowElem.tag = rowElem.tag.replace(rowElem.tag,'pre')
                  
              elif rowElem.tag=='sp':
                rowElem.tag = rowElem.tag.replace(rowElem.tag,'span')
              
            tablestr =  ET.tostring(tableElem, encoding='utf8').decode('utf8')
            tablestr = tablestr.replace("<?xml version='1.0' encoding='utf8'?>","") 
            tablestr = tablestr.replace('<table>','<table class="docutils align-default">')
            tablestr = tablestr.replace('\n','')
            tablestr = tablestr.replace('<td><p><img','<td width="25%"><img').replace('helloimage</img></p></td>', '</td>').replace('helloimage</img>   </p></td>', '</td>')
#            tablestr = tablestr.replace('<','\&lt;').replace('>','\&gt;')
            tablestr="embed:rst \n.. raw:: html\n\n  "+tablestr
        
            tableElem.tag = tableElem.tag.replace('table','verbatim') 
            tableElem.clear()
            tableElem.text=tablestr

          tree.write(fileName, encoding="utf-8")
          os.remove(tempfilename)
        
#      print("END Loop")