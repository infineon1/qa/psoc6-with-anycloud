========================
IoT Protocols
========================

.. raw:: html

   <script type="text/javascript">
   window.location.href = "MQTT_Client_Example.html"
   </script>


.. toctree::
   :hidden:
      
   MQTT_Client_Example.rst
   TCP_Client_Example.rst
   TCP_Server_Examaple.rst
   Secure_TCP_Client_Example.rst
   Secure_TCP_Server_Example.rst
   UDP_Client_Example.rst
   UDP_Server_Example.rst
   HTTPS_Server_Example.rst
