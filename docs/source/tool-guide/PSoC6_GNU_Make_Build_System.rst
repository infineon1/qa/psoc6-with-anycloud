=============================
PSoC6 GNU Make Build System
=============================

This package provides the build recipe make files and scripts for building and programming PSoC 6 applications. Builds can be run either through a command-line interface (CLI) or through the ModusToolbox Integrated Devlopment Environment (IDE).

.. raw:: html

   <a href="https://github.com/cypresssemiconductorco/psoc6make" target="_blank">Click here to be taken to the GitHub repository.</a><br><br>


